+++
title = "Álgebra Lineal"
weight = 10
+++

El álgebra lineal es, a grosso modo, la disciplina que estudia los vectores 
$$
\vec{v} =
\begin{pmatrix}
   -1 \cr 0 \cr \pi \cr e
\end{pmatrix}.
$$
No de manera individual, sino en conjunto, tales como el conjunto de los vectores del plano que parten del origen

![Plano vectorial](../images/plane.jpg)

Los estudia a través de su aritmética, es decir, de operaciones tales como la suma y el producto por escalares, y los relaciona mediante las denominadas aplicaciones lineales que, como veremos, se corresponde con las matrices
$$
A = 
\begin{pmatrix}
   0 & 1 & 3 & 2 \cr 
   5 & -6 & 9 & 0
\end{pmatrix}.
$$

Estos nos permitirá resolver problemas tales como sistemas de ecuaciones lineales (demostrando la validez del método que seguramente ya conoces) y demostrar resultados tan relevantes como el **Teorema Fundamental del Álgebra**, que establece que todo polinomio no constante posee al menos una raíz compleja.