+++
title = "Determinantes"
weight = 40
+++



Al comienzo del curso hacı́amos referencia al antiguo tablero chino para
contar, en el que cañas de bambú coloreadas se manipulaban de acuerdo a
ciertas reglas para resolver un sistema de ecuaciones lineales. El
tablero chino parece que se usaba por el 200 a.C., y mantuvo su
mecanismo durante un milenio. El tablero y las reglas para usarlo
llegaron a Japón, donde [Seki
Kowa](http://www-history.mcs.st-and.ac.uk/Biographies/Seki.html)
(1642-1708), un gran matemático japonés, sintetizó las antiguas ideas
chinas de manipulación de rectángulos. Kowa formuló el concepto de lo
que hoy llamamos determinante para facilitar la resolución de sistemas
lineales. Se piensa que su definición data de poco antes de 1683.

Alrededor de los mismos años, entre 1678 y 1693, [Gottfried W.
Leibniz](http://www-history.mcs.st-and.ac.uk/Biographies/Leibniz.html)
(1646-1716), un matemático alemán, desarrollaba su propio concepto de
determinante de forma independiente, junto con aplicaciones de
manipulación de rectángulos de números para resolver sistemas de
ecuaciones lineales. El trabajo de Leibniz solamente trata sistemas de
tres ecuaciones con tres incógnitas, mientras que Seki Kowa dio un
tratamiento general para sistemas de $n$ ecuaciones con $n$ incógnitas.
Parece que tanto Kowa como Leibniz desarrollaron lo que se llamó
posteriormente regla de Cramer, pero no en la misma forma ni notación.
Estos dos hombres tuvieron algo en común: sus ideas sobre la resolución
de sistemas lineales nunca fueron adoptadas por la comunidad matemática
de su tiempo, y sus descubrimientos se desvanecieron rápidamente en el
olvido. Al final, el concepto de determinante se redescubrió, y la
materia ha sido intensamente tratada en el periodo de 1750 a 1900.
Durante el mismo, los determinantes se convirtieron en la mayor
herramienta usada para analizar y resolver sistemas lineales, mientras
que la teorı́a de matrices permanecı́a relativamente poco desarrollada.
Pero las matemáticas, como un rı́o, están siempre cambiando su curso, y
grandes afluentes se pueden secar y convertirse en riachuelos, mientras
que pequeños arroyuelos se convierten en poderosos torrentes. Esto es
precisamente lo que ocurrió con las matrices y determinantes. El estudio
y uso de los determinantes llevó al álgebra matricial de Cayley, y hoy
las matrices y el álgebra lineal están en la corriente principal de la
matemática aplicada, mientras que el papel de los determinantes ha sido
relegado a una zona de remanso, por seguir con la analogı́a fluvial. Sin
embargo, todavı́a es importante comprender qué es un determinante y
aprender sus propiedades fundamentales. Nuestro objetivo no es aprender
determinantes por su propio interés, sino que exploraremos aquellas
propiedades que son útiles en el posterior desarrollo de la teorı́a de
matrices y sus aplicaciones.

Existen varias aproximaciones para la definición del determinante de una
matriz, de las que destacamos dos vı́as. La primera es de tipo inductivo,
en donde el determinante de una matriz de orden $n$ se calcula a partir
de los determinantes de matrices de orden $n-1$. La segunda se basa en
una definición directa, que precisa el concepto de permutación y de su
clasificación en tipo par o impar. Desarrollaremos ambas, con un teorema
de unicidad que garantiza la igualdad de ambas definiciones. La elección
de una forma u otra depende del aspecto teórico en el que se quiera
insistir.

## Definición inductiva

Para definir el determinante de una matriz cuadrada cualquiera,
comenzaremos definiendo el de las matrices de orden 1 y, a partir de
ahí, definiremos el determinante de una matriz de orden $n>1$ suponiendo
que sabemos definir el determinante de cualquier matriz cuadrada de
orden $n-1$.

Sea $A=(a\sb{ij})$ una matriz cuadrada de orden $n>1$ con coeficientes en
un cuerpo $\K$. Si suponemos que sabemos definir el determinante de las
matrices de orden $n-1$, llamaremos **cofactor** del elemento $a\sb{ij}$
al escalar $$\widehat{A}\sb{ij} = (-1)^{i+j} \det(M\sb{ij}(A)),$$ donde
$M\sb{ij}(A)$ es la matriz de orden $n-1$ que se obtiene al eliminar la
fila $i$ y la columna $j$ de la matriz $A$ (estamos suponiendo que
sabemos definir el cofactor, porque $M\sb{ij}(A)$ tiene orden $n-1$).
Podemos definir entonces la función determinante como sigue:

{{% definition %}}
Determinante: forma inductiva

Sea $A = (a\sb{ij})$ una matriz cuadrada de orden $n$.

-   Si $n=1$, entonces $\det(A) = a\sb{11}$.

-   Si $n > 1$, entonces
    $$\det(A) = a\sb{11} \widehat{A}\sb{11} + a\sb{21} \widehat{A}\sb{21} + \cdots + a\sb{n1} \widehat{A}\sb{n1},$$
{{% /definition %}}

Esta forma de definir el determinante se llama "desarrollo del
determinante por la primera columna\", puesto que definimos $\det(A)$
como la suma de los productos de cada elemento de la primera columna por
su cofactor correspondiente.

Es muy común la notación $|A|$ para referirse al determinante de $A$,
aunque en estas notas generalmente se utilizará $\det(A)$.

{{% example name="Ejemplo" %}}
1.  Para una matriz $2 \times 2$ se tiene $$\begin{aligned}
        \det \left( \begin{array}{cc} a\sb{11} & a\sb{12} \cr a\sb{21} & a\sb{22} \end{array} \right) & = a\sb{11} \widehat{A}\sb{11} + a\sb{21} \widehat{A}\sb{21} \cr & = a\sb{11} a\sb{22} - a\sb{21} a\sb{12}.
\end{aligned}$$

2.  Para la matriz identidad, $$\det(I\sb{n}) = 1 \cdot \det(I\sb{n-1}),$$ y
    por inducción $\det(I\sb{n}) = 1$.

3.  Para una matriz $3 \times 3$ se tiene $$\begin{aligned}
          \det \left( \begin{array}{ccc} a\sb{11} & a\sb{12} & a\sb{13} \cr a\sb{21} & a\sb{22} & a\sb{23} \cr a\sb{31} & a\sb{32} & a\sb{33} \end{array} \right) & = a\sb{11} \widehat{A}\sb{11} + a\sb{21} \widehat{A}\sb{21} + a\sb{31} \widehat{A}\sb{31} \cr & = a\sb{11} \det \left( \begin{array}{cc} a\sb{22} & a\sb{23} \cr a\sb{32} & a\sb{33} \end{array} \right) - a\sb{21} \det \left( \begin{array}{cc} a\sb{12} & a\sb{13} \cr a\sb{32} & a\sb{33} \end{array} \right) \cr & + a\sb{31} \det \left( \begin{array}{cc} a\sb{12} & a\sb{13} \cr a\sb{22} & a\sb{23} \end{array} \right) \cr & = a\sb{11} (a\sb{22} a\sb{33} - a\sb{32} a\sb{23}) - a\sb{21} (a\sb{12} a\sb{33} - a\sb{32} a\sb{13}) \cr & + a\sb{31} (a\sb{12} a\sb{23} - a\sb{22} a\sb{13}) \cr & = a\sb{11} a\sb{22} a\sb{33} + a\sb{13} a\sb{21} a\sb{32} + a\sb{12} a\sb{23} a\sb{31} \cr & - a\sb{11} a\sb{23} a\sb{32} - a\sb{12} a\sb{21} a\sb{33} - a\sb{13} a\sb{22} a\sb{31}.
\end{aligned}$$ Esta es la que se conoce como **regla de Sarrus**,
    que admite la representación gráfica de la
    Figura [\[Regla de Sarrus\]](#Regla de Sarrus){reference-type="ref"
    reference="Regla de Sarrus"} para los sumandos positivos y
    negativos.

    4.  El **determinante de una matriz triangular superior** es el producto
    de los elementos de su diagonal principal. En efecto, si $n=1$ el
    resultado es evidente. Por inducción, suponemos que $n>1$ y que el
    resultado es cierto para matrices triangulares superiores de orden
    $n-1$. Entonces, dada una matriz triangular superior $A$ de orden
    $n$, como sabemos que $a\sb{ij}=0$ cuando $i>j$, tenemos:
    $$\det(A)=a\sb{11}\widehat{A}\sb{11}+a\sb{21}\widehat{A}\sb{21}+\cdots+a\sb{n1}\widehat{A}\sb{n1}= a\sb{11}\widehat{A}\sb{11}.$$
    Pero $\widehat{A}\sb{11}=(-1)^{1+1}\det (M\sb{11}(A))=\det (M\sb{11}(A))$,
    donde $M\sb{11}(A)$ es triangular superior, de orden $n-1$, y los
    elementos de su diagonal principal son $a\sb{22},\ldots,a\sb{nn}$. Por
    hipótesis de inducción, $\widehat{A}\sb{11}=a\sb{22}\cdots a\sb{nn}$. Por
    tanto, $\det(A)=a\sb{11}a\sb{22}\cdots a\sb{nn}$.

    {{% /example %}} 


{{% example name="Ejemplo" %}}
Calculemos el determinante de una matriz $4\times 4$ (cuidado: no hay
una regla de Sarrus para matrices de orden mayor que 3).
$$\begin{aligned}
    \det \left( \begin{array}{rrrr} 1 & 2 & 3 & 4 \cr 0 & 1 & 2 & 4 \cr 2 & 3 & 4 & 5 \cr 2 & 0 & 1 & 2 \end{array} \right) & = 1 \cdot \det \left( \begin{array}{rrr} 1 & 2 & 4 \cr 3 & 4 & 5 \cr 0 & 1 & 2 \end{array} \right) - 0 \cdot \det \left( \begin{array}{rrr} 2 & 3 & 4 \cr 3 & 4 & 5 \cr 0 & 1 & 2 \end{array} \right)\cr & + 2 \cdot \det \left( \begin{array}{rrr} 2 & 3 & 4 \cr 1 & 2 & 4 \cr 0 & 1 & 2 \end{array} \right) - 2 \cdot \det \left( \begin{array}{rrr} 2 & 3 & 4 \cr 1 & 2 & 4 \cr 3 & 4 & 5 \end{array} \right) \cr & = 3 -4 -2 = -3.
\end{aligned}$$
{{% /example %}}

De los ejemplos anteriores, observamos que esta forma de calcular el
valor de un determinante es muy costosa en cuanto al número de
operaciones, pues una matriz de orden $n$ contiene $n!$ sumandos, cada
uno de ellos con un producto de $n$ elementos. Por ellos, vamos a
estudiar qué propiedades posee esta función que permita su evaluación de
una forma más simple.

## Determinante y filas de una matriz

{{% theorem name="Lineal en las filas" %}}
La aplicación determinante es lineal en las filas. Es decir, se cumplen
las siguientes propiedades:

-   Dadas matrices $A$, $B$, $C$ de orden $n$, si hay un índice $i$ tal
    que
    $$A\sb{i\ast}=B\sb{i\ast}+C\sb{i\ast} \qquad \textrm{y} \qquad A\sb{t\ast}=B\sb{t\ast}=C\sb{t\ast}\ (\forall t\neq i),$$
    entonces: $$\det(A)=\det(B)+\det(C).$$

-   Dadas matrices $A$, $B$ de orden $n$ y un escalar $\alpha\in \K$, si
    hay un índice $i$ tal que
    $$A\sb{i\ast}=\alpha B\sb{i\ast} \qquad \textrm{y} \qquad A\sb{t\ast}=B\sb{t\ast}\ (\forall t\neq i),$$
    entonces: $$\det(A)=\alpha\det(B).$$
{{% /theorem %}}

{{% proof %}}
-   Sean $A$, $B$ y $C$ como en la primera parte del enunciado. Debemos
    probar que $\det(A)=\det(B)+\det(C)$, es decir,
    $$\det \left( \begin{array}{c} A\sb{1\ast} \cr \vdots \cr B\sb{i\ast} + C\sb{i\ast} \cr \vdots \cr A\sb{n\ast} \end{array} \right) = \det \left( \begin{array}{c} A\sb{1\ast} \cr \vdots \cr B\sb{i\ast} \cr \vdots \cr A\sb{n\ast} \end{array} \right) + \det \left( \begin{array}{c} A\sb{1\ast} \cr \vdots \cr C\sb{i\ast} \cr \vdots \cr A\sb{n\ast} \end{array} \right).$$
    La prueba es por inducción sobre $n$. Si $n=1$, tenemos
    $\det(A)=a\sb{11}=b\sb{11}+c\sb{11}=\det(B)+\det(C)$, luego el resultado
    es cierto. Supongamos que $n > 1$ y que el resultado es cierto para
    matrices de orden $n-1$.

    Observemos cómo son los elementos de la primera columna de $A$, y
    sus cofactores. Por un lado tenemos
    $$a\sb{i1}=b\sb{i1}+c\sb{i1} \qquad \textrm{y} \qquad a\sb{t1}=b\sb{t1}=c\sb{t1}\ (\forall t\neq i).$$
    Por otro lado, como todas las filas de $A$, $B$ y $C$ son iguales
    salvo la fila $i$, tenemos $M\sb{i1}(A)=M\sb{i1}(B)=M\sb{i1}(C)$. Por
    tanto, $$\widehat{A}\sb{i1}=\widehat{B}\sb{i1}=\widehat{C}\sb{i1}.$$ Por
    último, si $t\neq i$ y consideramos las submatrices $M\sb{t1}(A)$,
    $M\sb{t1}(B)$ y $M\sb{t1}(C)$, observamos que todas sus filas coinciden
    salvo una (la que corresponde a la fila $i$ de $A$). Esta fila, en
    $M\sb{t1}(A)$, es la suma de las filas correspondientes en $M\sb{t1}(B)$
    y $M\sb{t1}(C)$. Es decir, estas tres submatrices satisfacen las
    condiciones del enunciado, y tienen orden $n-1$. Por hipótesis de
    inducción, tenemos
    $\det(M\sb{t1}(A))=\det(M\sb{t1}(B))+\det(M\sb{t1}(B))$. Por tanto,
    $$\widehat{A}\sb{i1}=\widehat{B}\sb{i1}+\widehat{C}\sb{i1}\ (\forall t\neq i).$$
    De todas estas observaciones obtenemos, por una parte
    $$a\sb{i1}\widehat{A}\sb{i1}=(b\sb{i1}+c\sb{i1})\widehat{A}\sb{i1} = b\sb{i1}\widehat{A}\sb{i1}+c\sb{i1}\widehat{A}\sb{i1}
         = b\sb{i1}\widehat{B}\sb{i1}+c\sb{i1}\widehat{C}\sb{i1},$$ y por otra
    parte, si $t\neq i$,
    $$a\sb{t1}\widehat{A}\sb{t1}=a\sb{t1}(\widehat{B}\sb{t1}+\widehat{C}\sb{t1}) = a\sb{t1}\widehat{B}\sb{t1}+a\sb{t1}\widehat{C}\sb{t1}
         = b\sb{t1}\widehat{B}\sb{t1}+c\sb{t1}\widehat{C}\sb{t1}.$$ Ası́ que
    $a\sb{r1}\widehat{A}\sb{r1}= b\sb{r1}\widehat{B}\sb{r1}+c\sb{r1}\widehat{C}\sb{r1}$
    para todo $r=1,\ldots,n$. Por tanto: $$\begin{aligned}
      \begin{split}
        \det(A) & = a\sb{11} \widehat{A}\sb{11} + \cdots + a\sb{n1} \widehat{A}\sb{n1}
    \cr & = (b\sb{11} \widehat{B}\sb{11}+c\sb{11} \widehat{C}\sb{11}) + \cdots + (b\sb{n1} \widehat{B}\sb{n1}  + c\sb{n1} \widehat{C}\sb{n1})
    \cr & = (b\sb{11} \widehat{B}\sb{11} + \cdots + b\sb{n1} \widehat{B}\sb{n1})  + (c\sb{11} \widehat{C}\sb{11} + \cdots + c\sb{n1} \widehat{C}\sb{n1})
    \cr & = \det(B) + \det(C).
      \end{split}
\end{aligned}$$

-   Sean ahora $A$ y $B$ como en la segunda parte del enunciado. Hay que
    probar que $\det(A)=\alpha\det(B)$, es decir,
    $$\det \left( \begin{array}{c} A\sb{1\ast} \cr \vdots \cr \alpha B\sb{i\ast} \cr \vdots \cr A\sb{n\ast} \end{array} \right) = \alpha \det \left( \begin{array}{c} A\sb{1\ast} \cr \vdots \cr B\sb{i\ast} \cr \vdots \cr A\sb{n\ast} \end{array} \right).$$
    Aplicamos inducción sobre $n$. Si $n=1$,
    $\det(A)=a\sb{11}=\alpha b\sb{11}=\alpha\det(B)$, luego el resultado es
    cierto. Supongamos que $n>1$ y que el resultado es cierto para
    matrices de orden $n-1$. En esta ocasión tenemos, por un lado
    $$a\sb{i1}=\alpha b\sb{i1} \qquad \textrm{y} \qquad a\sb{t1}=b\sb{t1}\ (\forall t\neq i),$$
    y por otro lado $M\sb{i1}(A)=M\sb{i1}(B)$, por lo que
    $$\widehat{A}\sb{i1}=\widehat{B}\sb{i1}$$ y, si $t\neq i$, las matrices
    $M\sb{t1}(A)$ y $M\sb{t1}(B)$ cumplen las hipótesis del enunciado y son
    de orden $n-1$, luego por hipótesis de inducción tenemos
    $\det(M\sb{t1}(A))=\alpha \det(M\sb{t1}(B))$. Por tanto,
    $$\widehat{A}\sb{t1}=\alpha\widehat{B}\sb{t1}\ (\forall t\neq i).$$ De
    aquí obtenemos, por una parte
    $$a\sb{i1}\widehat{A}\sb{i1}=(\alpha b\sb{i1})\widehat{B}\sb{i1}= \alpha (b\sb{i1}\widehat{B}\sb{i1}),$$
    y por otra parte
    $$a\sb{t1}\widehat{A}\sb{t1}=b\sb{t1}(\alpha \widehat{B}\sb{t1})= \alpha (b\sb{t1}\widehat{B}\sb{t1}) \ (\forall t\neq i).$$
    Por tanto, tenemos
    $a\sb{r1}\widehat{A}\sb{r1}=\alpha (b\sb{r1}\widehat{B}\sb{r1})$ para todo
    $r=1,\ldots,n$. De donde finalmente obtenemos: $$\begin{aligned}
        \begin{split}
          \det(A) & = a\sb{11} \widehat{A}\sb{11} + \cdots + a\sb{n1} \widehat{A}\sb{n1}
          \cr & = \alpha (b\sb{11}\widehat{B}\sb{11}) + \cdots + \alpha (b\sb{n1}\widehat{B}\sb{n1})
          \cr & = \alpha (b\sb{11}\widehat{B}\sb{11} + \cdots + b\sb{n1}\widehat{B}\sb{n1})
          \cr & = \alpha \det(B).
        \end{split}
\end{aligned}$$
{{% /proof %}}

Una consecuencia inmediata de lo anterior es que si $A$ tiene una fila
de ceros, entonces $\det(A) = 0$; supongamos que la fila $A\sb{i\ast}$
contiene únicamente valores nulos. En tal caso, podemos sacar el factor
cero y tenemos el resultado.

{{% theorem name="Alternada" %}}
 Si $A$ es una matriz con dos filas iguales, entonces
$\det(A) =  0$. Si $B$ es la matriz que resulta de intercambiar dos
filas de la matriz $A$, entonces $\det(B) = -\det(A)$.
{{% /theorem %}}

{{% proof %}}
-   **Caso 1**. Las filas iguales de la matriz $A$ son consecutivas.
    Hacemos la prueba por inducción. Para $n=2$ es una sencilla
    comprobación. Supongamos entonces $n>2$. Si las filas iguales son
    $A\sb{i\ast}$ y $A\sb{i+1\ast}$, entonces $a\sb{i1} = a\sb{i+1,1}$ y
    $$\det(A) = a\sb{11} \widehat{A}\sb{11} + \cdots + a\sb{i1} \widehat{A}\sb{i1} + a\sb{i1} \widehat{A}\sb{i+1,1} + \cdots + a\sb{n1} \widehat{A}\sb{n1}.$$
    Para $t\ne i, i+1$, el cofactor $\widehat{A}\sb{t1}$ corresponde a una
    matriz de orden $n-1$ con dos filas iguales, por lo que
    $\widehat{A}\sb{t1} = 0$ para $t \ne i, i+1$. Por otro lado,
    $$\widehat{A}\sb{i1} = (-1)^{i+1} \det(M\sb{i1}),\qquad  \widehat{A}\sb{i+1,1} = (-1)^{i+2} \det(M\sb{i+1,1}),$$
    y las matrices $M\sb{i1}$ y $M\sb{i+1,1}$ son iguales. En consecuencia,
    $\widehat{A}\sb{i1} = - \widehat{A}\sb{i+1,1}$ y $\det(A) = 0$.

-   **Caso 2**. Intercambio de dos filas consecutivas. Por el caso 1,
    sabemos que $$\begin{aligned}
        0 & = \det \left( \begin{array}{c} A\sb{1\ast} \cr \vdots \cr A\sb{i\ast} + A\sb{i+1,\ast} \cr A\sb{i\ast} + A\sb{i+1,\ast} \cr \vdots \cr A\sb{n\ast} \end{array} \right) \cr & = \det \left( \begin{array}{c} A\sb{1\ast} \cr \vdots \cr A\sb{i\ast} \cr A\sb{i\ast} \cr \vdots \cr A\sb{n\ast} \end{array} \right) + \det \left( \begin{array}{c} A\sb{1\ast} \cr \vdots \cr A\sb{i\ast} \cr A\sb{i+1,\ast} \cr \vdots \cr A\sb{n\ast} \end{array} \right) + \det \left( \begin{array}{c} A\sb{1\ast} \cr \vdots \cr A\sb{i+1,\ast} \cr A\sb{i+1,\ast} \cr \vdots \cr A\sb{n\ast} \end{array} \right) + \det \left( \begin{array}{c} A\sb{1\ast} \cr \vdots \cr A\sb{i+1,\ast} \cr A\sb{i\ast} \cr \vdots \cr A\sb{n\ast} \end{array} \right) \cr & = 0 + \det(A) + 0 + \det(B),
\end{aligned}$$ donde $B$ es la matriz que se obtiene a partir de
    $A$ por el intercambio de la fila $i$ con la fila $i+1$. Por tanto,
    $\det(B) = - \det(A)$.

-   **Caso 3**. Las filas iguales de la matriz $A$ no son necesariamente
    consecutivas. Supongamos que la matriz $A$ tiene iguales las filas
    $A\sb{i\ast}$ y $A\sb{t\ast}$. Por el caso 2, podemos realizar intercambios de
    filas hasta que sean consecutivas. Cada intercambio supone una
    alteración del signo del determinante, pero el resultado final es
    una matriz con determinante nulo, por el caso 1.

-   **Caso 4**. Intercambio de dos filas. Se procede de igual manera que
    en el caso 2.
{{% /proof %}}

Una consecuencia de lo anterior es que el determinante de una matriz $A$
no cambia si una fila $A\sb{t\ast}$ es sustituida por
$A\sb{t\ast} + \alpha A\sb{i\ast}$, donde $\alpha$ es un escalar y $t \ne i$. En
efecto, $$\begin{aligned}
\det \left( \begin{array}{c} A\sb{1\ast} \cr \vdots \cr A\sb{t\ast}+ \alpha A\sb{i\ast} \cr \vdots \cr A\sb{i\ast} \cr \vdots \cr A\sb{n\ast} \end{array} \right) & = \det \left( \begin{array}{c} A\sb{1\ast} \cr \vdots \cr A\sb{t\ast} \cr  \vdots \cr A\sb{i\ast} \cr \vdots \cr A\sb{n\ast} \end{array} \right) + \det \left( \begin{array}{c} A\sb{1\ast} \cr \vdots \cr \alpha A\sb{i\ast} \cr \vdots \cr A\sb{i\ast} \cr \vdots \cr  A\sb{n\ast} \end{array} \right) \cr & = \det(A) + \alpha \det \left( \begin{array}{c} A\sb{1\ast} \cr \vdots \cr A\sb{i\ast} \cr \vdots \cr A\sb{i\ast} \cr \vdots \cr A\sb{n\ast} \end{array} \right) = \det(A),
\end{aligned}$$ pues la última matriz tiene dos filas iguales.

Se puede demostrar que el determinante es la única aplicación
$$f:\MatK{n}{n}{\K}\to \K$$ que es lineal en las filas, alternada y tal
que $f(I\sb{n})=1$. Por tanto, estas propiedades caracterizan la aplicación
"determinante\".

Observermos que, gracias a las propiedades anteriores, conocemos cómo
cambia el determinante de una matriz al aplicarle una transformación
elemental de filas. Por otra parte, sabemos que el determinante de una
matriz triangular superior es muy fácil de calcular (es el producto de
los elementos de su diagonal principal). Por tanto, podemos calcular el
determinante de una matriz aplicándole transformaciones elementales de
filas hasta obtener una matriz escalonada (que es triangular superior),
calculando el determinante de esta última, y recordando cómo ha cambiado
el determinante al aplicar las transformaciones.

Del mismo modo, podemos aplicar transformaciones elementales de filas a
una matriz (sabiendo cómo varía el determinante), para simplificarla de
manera que sea más sencillo calcular el determinante de la matriz
obtenida.

{{% example name="Ejemplo" %}}
$$\det\begin{pmatrix} 1 & 2 & 3 \cr 4 & 5 & 6 \cr 7 & 8 & 1 \end{pmatrix} =
\det\begin{pmatrix} 1 & 2 & 3 \cr 0 & -3 & -6 \cr 0 & -6 & -20 \end{pmatrix} =$$
$$(-3)(-2)\det\begin{pmatrix} 1 & 2 & 3 \cr 0 & 1 & 2 \cr 0 & 3 & 10 \end{pmatrix} =
6 \det\begin{pmatrix} 1 & 2 \cr 3 & 10 \end{pmatrix} = 6\cdot 4 = 24$$ En
la primera igualdad hemos usado dos transformaciones de tipo III (que no
afectan al determinante), en la segunda hemos aplicado dos
transformaciones de tipo II (a la segunda y a la tercera fila), luego
hemos desarrollado el determinante por la primera columna y por último
hemos calculado el determinante de una matriz $2\times 2$.
{{% /example %}}

## Determinante, producto y trasposición

Las propiedades demostradas en la sección anterior se pueden expresar en
términos de las acciones de las matrices elementales.

{{% theorem name="Efecto de las transformaciones elementales" %}}
Sea $A$ una matriz de orden $n$. Entonces

-   $\det(E\sb{ij} A) = - \det(A)$ si $E\sb{ij}$ es una matriz elemental de
    tipo I.

-   $\det(E\sb{i}(\alpha) A) = \alpha \det(A)$ si $\alpha \in \K-\{0\}$.

-   $\det(E\sb{ij}(\alpha) A) = \det(A)$ si $\alpha \in \K$.
{{% /theorem %}}

{{% proof %}}
Las tres propiedades son otra forma de expresar el carácter lineal y
alternado del determinante que hemos visto antes.
{{% /proof %}}

{{% theorem name="Determinante de las matrices elementales" %}}
Para las matrices elementales se tiene que

-   $\det(E\sb{ij}) = -1$.

-   $\det(E\sb{i}(\alpha)) = \alpha$.

-   $\det(E\sb{ij}(\alpha)) = 1$.

En general, si $E\sb{1}, \ldots, E\sb{s}$ son matrices elementales, entonces
$$\det(E\sb{1} \cdots E\sb{s} A) = \det(E\sb{1}) \cdots \det(E\sb{s}) \det(A)$$ para
cualquier matriz $A$ cuadrada.
{{% /theorem %}}

{{% proof %}}
Si tomamos $A = I\sb{n}$ en el resultado anterior, entonces
$$\det(E\sb{ij}) = -1, \qquad \det(E\sb{i}(\alpha)) = \alpha, \qquad \det(E\sb{ij}(\alpha)) = 1.$$
La segunda parte se demuestra por inducción sobre $s$. Para $s=1$ es el
resultado anterior. Supuesto probado el resultado para $s-1$ matrices,
se tiene que $$\begin{aligned}
  \det(E\sb{1} \cdot E\sb{2} \cdots E\sb{s} \cdot A) & = \det(E\sb{1}) \det(E\sb{2} \cdots E\sb{s} \cdot A) & \text{ por el primer caso } \cr
  & = \det(E\sb{1}) (\underbrace{\det(E\sb{2}) \cdots \det(E\sb{s})}\sb{s-1 \text{ factores }} \cdot \det(A)) & \text{ inducción } \cr
  & = \det(E\sb{1}) \cdots \det(E\sb{s}) \det(A).
\end{aligned}$$
{{% /proof %}}

Observemos que hemos demostrado lo siguiente: si $E$ es una matriz
elemental, $\det(EA)=\det(E)\det(A)$. Más adelante demostraremos que el
mismo resultado es cierto cuando $E$ es una matriz cualquiera.

Cuando hablamos del cálculo de la inversa de una matriz, decı́amos que
las matrices que no tienen inversa se denominan singulares. Esta
nomenclatura es clásica y está asociada al valor del determinante de la
matriz, puesto que una matriz singular es aquella cuyo determinante es
cero:

{{% theorem name="Existencia de inversa y determinante" %}}
 Sea $\K$ un cuerpo y
$A \in \MatK{n}{n}{\K}$. Entonces $A$ tiene inversa si y solamente si
$\det(A) \ne 0$.
{{% /theorem %}}

{{% proof %}}
Supongamos que $A$ tiene inversa. Entonces $A$ es producto de matrices
elementales $A = E\sb{1} \cdots E\sb{k}$, de donde
$$\det(A) = \det(E\sb{1}) \cdots \det(E\sb{k})$$ y todos los factores son no
nulos. Recı́procamente, supongamos que $A$ no tiene inversa. En tal caso,
su forma escalonada reducida por filas $E\sb{A}$ tiene una fila de ceros, y
$E\sb{A} = E\sb{1} \cdots E\sb{k} A$, con $E\sb{i}$ matrices elementales. Entonces
$$0 = \det(E\sb{A}) = \det(E\sb{1}) \cdots \det(E\sb{k}) \det(A).$$ Como las
matrices $E\sb{1}, \ldots, E\sb{k}$ son elementales, tienen determinante no
nulo, y por tanto $\det(A) = 0$ (de otro modo el producto anterior no
sería nulo).
{{% /proof %}}

Las matrices **singulares** son por tanto aquellas con determinante nulo
o, equivalentemente, las matrices no invertibles. Supongamos que $A$ es
una matriz cuadrada de orden $n$ singular. Entonces sabemos que
$\rg(A) < n$. Esto implica que una columna de $A$ se expresa como
combinación lineal de las restantes.

Ya podemos demostrar una importante propiedad: el determinante de
matrices es multiplicativo.

{{% theorem name="Producto de matrices y determinante" %}}
 Si $A$ y $B$ son matrices cuadradas,
entonces $\det(AB) = \det(A) \det(B)$.
{{% /theorem %}}

{{% proof %}}
Supongamos que $A$ es singular. Entonces $\det(A)=0$, y además sabemos
que $A=E\sb{1}\cdots E\sb{k} E\sb{A}$, donde $E\sb{1},\ldots,E\sb{k}$ son matrices
elementales y $E\sb{A}$ es la forma escalonada reducida por filas de $A$,
que tiene una fila de ceros (al menos la última). En este caso la última
fila de $E\sb{A} B$ también es de ceros, luego $\det(E\sb{A} B)=0$. Por tanto
$$\begin{aligned}
 \det(AB) & = \det(E\sb{1}\cdots E\sb{k} E\sb{A} B) = \det(E\sb{1}) \cdots \det(E\sb{k}) \det(E\sb{A} B) = 0 \cr & = \det(A) \det(B).
\end{aligned}$$

Si $A$ es invertible, entonces $A = E\sb{1} E\sb{2} \cdots E\sb{k}$ es producto de
matrices elementales. Sabemos que la regla del producto es válida para
estas matrices, por lo que $$\begin{aligned}
  \det(AB) & =  \det(E\sb{1} E\sb{2} \cdots E\sb{k} B) = \det(E\sb{1}) \det(E\sb{2}) \cdots \det(E\sb{k}) \det(B) \cr & =  \det(E\sb{1} E\sb{2} \cdots E\sb{k}) \det(B) = \det(A) \det(B).
\end{aligned}$$
{{% /proof %}}

{{% theorem name="Determinante de matrices diagonales por bloques" %}}
 Si $A$ y $D$ son
matrices cuadradas,
$\det \left( \begin{array}{cc} A & B \cr  O  & D \end{array} \right) = \det(A) \det(D)$.
{{% /theorem %}}

{{% proof %}}
Escribamos
$$C = \left( \begin{array}{cc} A\sb{n \times n} & B \cr  O  & D\sb{p \times p} \end{array} \right),$$
y vamos a probar el resultado por inducción en $n$, el orden de $A$.
Para $n=1$ se tiene que
$$\det(C) = a\sb{11} \widehat{C}\sb{11} = a\sb{11} \det(D)= \det(A)\det(D).$$
Supongamos el resultado probado para matrices cuadradas de orden $n-1$.
Por la definición de determinante,
$$\det(C) = a\sb{11} \widehat{C}\sb{11} + a\sb{21} \widehat{C}\sb{21} + \cdots + a\sb{n1} \widehat{C}\sb{n1},\qquad  \widehat{C}\sb{i1} = (-1)^{i+1} \det (M\sb{i1}(C)),$$
donde $M\sb{i1}(C)$ es la matriz que resulta de $C$ al eliminar la fila
$i$ y la columna $1$. Entonces estas matrices tienen la forma
$$M\sb{i1}(C) = \left( \begin{array}{cc} M\sb{i1}(A) & B'_i \cr  O  & D \end{array} \right),$$
donde $M\sb{i1}(A)$ es la matriz que se obtiene de $A$ al eliminar la fila
$i$ y la columna $1$. Por la hipótesis de inducción aplicada a
$M\sb{i1}(C)$, se tiene que $$\det(M\sb{i1}(C)) = \det(M\sb{i1}(A)) \det(D),$$
y, finalmente, $$\begin{aligned}
   \det(C) & = a\sb{11} (-1)^{i+1} \det(M\sb{i1}(A)) \det(D) + a\sb{21} (-1)^{i+2} \det(M\sb{i2}(A)) \det(D) + \cdots + \cr & a\sb{n1} (-1)^{n+1} \det(M\sb{n1}(A)) \det(D) \cr & = \det(A) \det(D).
\end{aligned}$$
{{% /proof %}}

Por último, el tratamiento que hemos hecho por filas se puede hacer por
columnas.

{{% theorem name="Matriz traspuesta y determinante" %}}
 Sea $A$ una matriz cuadrada. Entonces
$\det(A) = \det(A^t)$.
{{% /theorem %}}

{{% proof %}}
Si $A$ es singular, entonces $A^t$ es singular, de donde
$$\det(A) = 0 = \det(A^t).$$ Si $A$ no es singular, entonces podemos
expresar $A$ como producto de matrices elementales $A = E\sb{1} \cdots E\sb{k}$,
de donde $A^t = E\sb{k}^t \cdots E\sb{1}^t$ es producto de matrices elementales.
Para cada matriz elemental se tiene que $\det(E\sb{i}) = \det(E\sb{i}^t)$,
puesto que $E\sb{ij}^t=E\sb{ij}$, $E\sb{i}(\alpha)^t=E\sb{i}(\alpha)$ y
$E\sb{ij}(\alpha)=E\sb{ji}(\alpha)$. Por tanto, $\det(A) = \det(A^t)$.
{{% /proof %}}

El resultado anterior permite extender las propiedades anteriores a las
columnas de una matriz.

{{% theorem name="Extensión de propiedades a las columnas" %}}
-   La aplicación determinante es lineal en las columnas.

-   Si $A$ es una matriz con dos columnas iguales, entonces
    $\det(A) =  0$. Si $B$ es la matriz que resulta de intercambiar dos
    columnas de la matriz $A$, entonces $\det(B) = -\det(A)$.

-   $\det(A E\sb{ij}) = - \det(A)$.

-   $\det(A E\sb{i}(\alpha)) = \alpha \det(A)$ si $\alpha$ no nulo.

-   $\det(A E\sb{ij}(\alpha)) = \det(A)$ si $\alpha \in \K$.
{{% /theorem %}}

{{% proof %}}
Todas las propiedades se obtienen de las propiedades análogas para
filas, trasponiendo las matrices implicadas.
{{% /proof %}}

{{% remark %}}
Habı́amos visto que si $\det(A) = 0$, entonces una columna de $A$ es
combinación lineal de las restantes. De forma análoga, si $\det(A) = 0$,
también una fila de $A$ es combinación lineal de las restantes.
{{% /remark %}}

Terminamos esta sección observando que la definición inicial de
determinante (desarrollo por la primera columna) podría haberse hecho
utilizando cualquier otra columna, o cualquier fila. Esto hace que si
una matriz tiene una fila o una columna con muchos ceros, sea
conveniente desarrollar el determinante por esa fila o esa columna, para
ahorrar cálculos.

{{% theorem name="Desarrollo del determinante por cualquier fila o columna" %}}
 El determinante
de una matriz se puede obtener mediante el desarrollo por cualquiera de
sus filas o columnas. Es decir, dados $i,j\in \{1,\ldots,n\}$
cualesquiera, se tiene: $$\begin{aligned}
    \det(A) & = a\sb{i1} \widehat{A}\sb{i1} + a\sb{i2} \widehat{A}\sb{i2} + \cdots + a\sb{in} \widehat{A}\sb{in} \cr & = a\sb{1j} \widehat{A}\sb{1j}+ a\sb{2j} \widehat{A}\sb{2j} + \cdots + a\sb{nj} \widehat{A}\sb{nj}.
\end{aligned}$$
{{% /theorem %}}

{{% proof %}}
Si $A = (a\sb{ij})$ y $B$ es la matriz que se obtiene al intercambiar la
primera columna con la columna $j$-ésima, entonces
$$B = \left( \begin{array}{cccccc} a\sb{1j} & a\sb{12} & \cdots & a\sb{11} & \cdots & a\sb{1n} \cr a\sb{2j} & a\sb{22} & \cdots & a\sb{21} & \cdots & a\sb{2n} \cr \vdots & \vdots & \ddots & \vdots & & \vdots \cr a\sb{nj} & a\sb{n2} & \cdots & a\sb{n1} & \cdots & a\sb{nn} \end{array} \right)
  \qquad \text{y}  \qquad
  \det(B) = -\det(A).$$ Por la definición,
$$\det(B) = a\sb{1j} \widehat{B}\sb{11} + a\sb{2j} \widehat{B}\sb{21} + \cdots + a\sb{nj} \widehat{B}\sb{n1},$$
y los cofactores de $B$ verifican que
$\widehat{B}\sb{k1} = - \widehat{A}\sb{kj}$ para todo $j=1,\ldots,n$ (se
pasa de $M\sb{k1}(B)$ a $M\sb{kj}(A)$ mediante $j-2$ trasposiciones de
columnas, y teniendo en cuenta los términos $(-1)^{k+1}$ y $(-1)^{k+j}$
de los cofactores, que serán iguales o distintos si $j$ es impar o par,
siempre tendremos los cofactores con signos opuestos). Por tanto,
$$\begin{aligned}
  -\det(A)=\det(B) & = -a\sb{1j} \widehat{A}\sb{1j} - a\sb{2j} \widehat{A}\sb{2j} - \cdots - a\sb{nj} \widehat{A}\sb{nj},
\end{aligned}$$ de donde
$\det(A)=a\sb{1j} \widehat{A}\sb{1j} + a\sb{2j} \widehat{A}\sb{2j} + \cdots + a\sb{nj} \widehat{A}\sb{nj}$,
como queríamos demostrar.

Para las filas, usamos la igualdad del determinante con el de la matriz
traspuesta.
{{% /proof %}}

## Rango y determinantes

En esta sección veremos cómo el rango de una matriz cualquiera puede
calcularse usando determinantes. Para ello necesitamos las siguientes
definiciones

{{% definition %}}
Submatrices Sea $A$ una matriz de orden $m\times n$ y elegimos un
subconjunto $I \subset \{1,\ldots,m\}$, y un subconjunto
$J\in \{1,\ldots,n\}$. Se define la **submatriz** de $A$ determinada por
$I$ y $J$ como la matriz $[A]\sb{I,J}$ cuyas entradas son las entradas
$[A]\sb{ij}$ de $A$ tales que $i\in I$ y $j\in J$. En otras palabras, es
la matriz que se obtiene de $A$ al eliminar las filas que no están en
$I$ y las columnas que no están en $J$.
{{% /definition %}}

{{% example name="Ejemplo" %}}
1.  En la definición de cofactor asociado al elemento $a\sb{ij}$ de una
    matriz cuadrada $A$, tomábamos la submatriz $M\sb{ij}(A)$ que se
    obtenı́a al eliminar la fila $i$ y la columna $j$ de la matriz $A$.
    Entonces $M\sb{ij}(A)=[A]\sb{I,J}$, donde
    $I = \{ 1, \ldots, n\} \setminus \{i\}$ y
    $J = \{ 1, \ldots, n\} \setminus \{j\}$.

2.  En la matriz
    $$A = \left( \begin{array}{rrrr} 1 & -1 & 0 & 2 \cr 2 & -1 & 1 & -1 \cr 0 & 0 & 1 & 0 \end{array} \right),$$
    la submatriz de $A$ que se obtiene para
    $I = \{1, 3 \}, J = \{3, 4\}$ es
    $$[A]\sb{I,J} = \left( \begin{array}{rrrr} 0 & 2 \cr 1 & 0 \end{array} \right).$$
{{% /example %}}

{{% definition %}}
Menores

Dada una matriz $A$, $m\times n$, los ***menores*** de $A$ son los
determinantes de las submatrices cuadradas de $A$. Es decir, los
escalares $$m\sb{I,J} = \det([A]\sb{I,J}),$$ donde $I\subset \{1,\ldots,m\}$
y $J\subset \{1,\ldots,n\}$ son dos conjuntos con el mismo número de
elementos.

Diremos que $m\sb{I,J}$ es un menor de **orden** $s$ si la submatriz
$[A]\sb{I,J}$ tiene orden $s\times s$, es decir, si $I$ y $J$ tienen ambos
$s$ elementos.
{{% /definition %}}

{{% example name="Ejemplo" %}}
En la matriz
$$A = \left( \begin{array}{rrrr} 1 & -1 & 0 & 2 \cr 2 & -1 & 1 & -1 \cr 0 & 0 & 1 & 0 \end{array} \right),$$
para $I = \{1, 3 \}, J = \{3, 4\}$ se obtiene el menor de orden $2$
$$m\sb{I,J} = \det \left( \begin{array}{rrrr} 0 & 2 \cr 1 & 0 \end{array} \right) = -2.$$
{{% /example %}}

Recordemos que el rango de una matriz $A$ es igual al número de columnas
básicas, o al número de filas distintas de cero de cualquier forma
escalonada, en particular de su forma escalonada reducida por filas. Con
respecto a las submatrices, el rango se comporta de la siguiente manera:

{{% theorem name="Rango y submatrices" %}}
 Si $[A]\sb{I,J}$ es una submatriz de $A$, entonces
$\rg([A]\sb{I,J}) \leq \rg (A)$.
{{% /theorem %}}

{{% proof %}}
Supongamos que $A$ es una matriz de orden $m\times n$ y rango $r$.
Sabemos que una serie de operaciones elementales por filas transforman
$A$ en $E\sb{A}$, su forma escalonada reducida por filas. Si llamamos
$M=\{1,\ldots,m\}$, podemos considerar la submatriz $[A]\sb{M,J}$, que
estará formada por algunas columnas de $A$. Si aplicamos a $[A]\sb{M,J}$
las mismas operaciones elementales, obtendremos las columnas
correspondientes de la matriz $E$, es decir, obtendremos una matriz que
tendrá a lo más $r$ filas no nulas. Esto implica que la forma escalonada
reducida por filas de $[A]\sb{M,J}$ tiene a lo más $r$ filas no nulas,
luego $\rg (A\sb{M,J})\leq r=\rg (A)$.

Hemos probado entonces que, dada una matriz $A$ de rango $r$, toda
submatriz formada por columnas completas de $A$ tiene a lo sumo rango
$r$. Pero observemos que $([A]\sb{I,J})^t$ es una submatriz de
$([A]\sb{M,J})^t$ formada por columnas completas, luego
$$\rg ([A]\sb{I,J})=\rg (([A]\sb{I,J})^t) \leq \rg (([A]\sb{M,J})^t) = \rg ([A]\sb{M,J}) \leq \rg (A),$$
como querı́amos demostrar.
{{% /proof %}}

Veamos que existe otra definición de rango, a partir de los menores de
una matriz $A$.

{{% theorem name="Rango y menores" %}}
El rango de $A$ es igual al mayor orden alcanzado por los menores no
nulos de $A$. Es decir, $\rg (A)=r$ si y sólo si:

-   Existe un menor $m\sb{I,J}\neq 0$ de orden $r$.

-   Todos los menores de $A$ de orden mayor que $r$ son nulos.
{{% /theorem %}}

{{% proof %}}
Supongamos que $A$ es una matriz de orden $m\times n$, que tiene rango
$r$. Sabemos que hay unas operaciones elementales por filas que
transforman $A$ en su forma escalonada reducida por filas $E$.

Sean $p\sb{1},\ldots,p\sb{r}$ las columnas básicas de $A$, y definamos
$M=\{1,\ldots,m\}$ y $J=\{p\sb{1},\ldots,p\sb{r}\}$. La submatriz $[A]\sb{M,J}$
está formada por las columnas básicas de $A$. Si aplicamos a $[A]\sb{M,J}$
las mismas operaciones elementales que a $A$, obtendremos las columnas
básicas de $E$, es decir, una matriz $m\times r$ cuyas primeras $r$
filas forman la matriz identidad, y las restantes $m-r$ filas son de
ceros. Como esta matriz es una reducida por filas con $r$ pivotes,
$[A]\sb{M,J}$ tiene rango $r$.

Consideremos ahora $([A]\sb{M,J})^t$. Sabemos que también tiene rango $r$,
luego el mismo argumento anterior nos dice que tomando sus columnas
básicas obtenemos una matriz de rango $r$. Sean $q\sb{1},\ldots,q\sb{r}$ sus
columnas básicas. Observemos que tomar las columnas $q\sb{1},\ldots,q\sb{r}$ de
$([A]\sb{M,J})^t$ es lo mismo que definir $I=\{q\sb{1},\ldots,q\sb{r}\}$ y
considerar la matriz $([A]\sb{I,J})^t$. Esta matriz tendrá entonces rango
$r$, luego la matriz $[A]\sb{I,J}$ también lo tendrá. Pero entonces
$A\sb{I,J}$ es una submatriz de $A$, de orden $r\times r$ y rango $r$,
luego su determinante es distinto de cero. Es decir, el menor $m\sb{I,J}$,
de orden $r$, es no nulo.

Consideremos ahora un menor $m\sb{I',J'}$ de orden $k>r$. Como una
submatriz no puede tener mayor rango que la matriz original,
$[A]\sb{I',J'}$ es una matriz de orden $k\times k$ y rango menor o igual a
$r<k$, luego su determinante debe ser cero. Es decir, $m\sb{I',J'}=0$.

Hemos demostrado entonces que para todo $r$, una matriz de rango $r$
admite un menor no nulo de orden $r$, y todos sus menores de orden mayor
que $r$ son nulos. Por tanto, el mayor orden alcanzado por los menores
no nulos de $A$ es igual al rango de $A$, como querı́amos demostrar.
{{% /proof %}}

Más adelante veremos que para calcular el rango de una matriz usando
menores, no es necesario ver que se anulan *todos* los menores de un
cierto orden.

## Método del orlado

Recordemos que el rango de una matriz $A$ es $r$ si y sólo si existe un
menor no nulo de orden $r$, y todos los menores de orden mayor que $r$
son nulos. En principio, paras demostrar que una matriz tiene rango $r$
habrı́a que encontrar un menor no nulo de orden $r$, y calcular todos los
menores de orden mayor que $r$ para ver que se anulan. En esta sección
veremos que esto no es necesario. Si encontramos una submatriz cuadrada
$M$ de $A$ tal que $\det(M)\neq 0$, y vemos que se anulan todos los
menores de orden $r+1$ que corresponden a submatrices de $A$ *que
contienen a $M$*, entonces $\rg (A)=r$.

{{% theorem name="Teorema principal del método del orlado" %}}
 Sea $A$ una matriz $m\times n$
con un menor no nulo de orden $r$, es decir, con una submatriz cuadrada
$M$ de orden $r$ tal que $\det(M)\neq 0$. Supongamos que todas las
submatrices cuadradas de orden $r+1$ que contienen a $M$ tienen
determinante 0. Entonces $\rg (A)=r$.
{{% /theorem %}}

{{% proof %}}
Observemos que intercambiar filas o columnas de una matriz no cambia su
rango, ni el hecho de que sus menores de un cierto orden sean o no sean
todos nulos. Por tanto, podemos suponer que la submatriz $M$ está
formada por las primeras $r$ filas y columnas de $A$. Es decir, podremos
escribir $A$ de la forma: $$A=\begin{pmatrix}
      M & B \cr
      C & D
   \end{pmatrix}.$$ Estamos suponiendo que $\det(M)\neq 0$, y que
cualquier submatriz de orden $r+1$ que contenga a $M$ tiene determinante
cero. Como $M$ tiene orden $r$ y $\det(M)\neq 0$, sabemos que
$\rango(M)=r$. Como $M$ es una submatriz de $A$, esto implica que
$\rango(A)\geq r$.

Por reducción al absurdo, supongamos que $\rango(A)>r$. Esto implica que
$A$ tiene más de $r$ columnas básicas. Las primeras $r$ columnas son
básicas (porque la reducida por filas de
$\begin{pmatrix}M \cr C \end{pmatrix}$ es
$\begin{pmatrix}I \cr O \end{pmatrix}$, al ser $M$ de rango $r$). Sea $j$
la primera columna básica de $A$ mayor que $r$, y consideremos
$T=\{1,\ldots,m\}$ y $J=\{1,\ldots,r,j\}$. Entonces
$$[A]\sb{T,J}=\begin{pmatrix}
      M & \mathbf{u} \cr
      C & \mathbf{v}
   \end{pmatrix}$$ es una matriz cuyas $r+1$ columnas son todas básicas.
Por tanto $\rango([A]\sb{T,J})=r+1$.

Consideremos ahora $[A]\sb{T,J}^t=\begin{pmatrix}
      M^t & C^t \cr
       \mathbf{u}^t & \mathbf{v}^t \end{pmatrix}$. Sus primeras $r$
columnas son básicas, puesto que contienen a $M^t$, que tiene rango $r$
como submatriz. Sea $i$ la primera columna básica de $[A]\sb{T,J}^t$ mayor
que $r$. Si llamamos $I=\{1,\ldots,r,i\}$, tenemos que
$$[A]\sb{I,J}=\begin{pmatrix}
      M & \mathbf{u} \cr
      \mathbf{w}^t & a\sb{ij}
     \end{pmatrix}$$ es una submatriz cuadrada de $A$, de orden $r+1$,
que tiene rango $r+1$ puesto que $[A]\sb{I,J}^t$ es una matriz formada por
$r+1$ columnas básicas de $[A]\sb{M,J}^t$. Por tanto,
$\det([A]\sb{I,J})\neq 0$, lo que contradice las hipótesis, porque esta
submatriz contiene a $M$ y tiene orden $r+1$. Por tanto, $\rango(A)=r$.
{{% /proof %}}

A partir de este resultado, el **método del orlado** para calcular el
rango de una matriz $A$ es el siguiente.

{{% example name="Método del orlado" %}}
Sea $A$ una matriz $m\times n$. El método del orlado para calcular el
rango de $A$ es como sigue. Si $A =  O $, entonces $\rg (A)=0$. En
caso contrario, tomamos una submatriz $M$ de orden $r$ tal que
$\det(M)\neq 0$ y realizamos los siguientes pasos:

1.  Hallamos, si existe, una submatriz de orden $r+1$ que contenga a $M$
    con determinante no nulo.

2.  Si no la hay, $\rg (A)=r$.

3.  Si la hay, repetimos el paso 1 aumentando $r$ una unidad, y
    reemplazando $M$ por la submatriz encontrada.
{{% /example %}}

{{% example name="Ejemplo" %}}
Consideremos la matriz
$$A = \left( \begin{array}{rrrr} 3 & 6 & 5 & 9 \cr 1 & 1 & 2 & 4 \cr 1 & -2 & 3 & 7 \end{array} \right).$$
La submatriz $M$ de $A$ asociada a las dos primeras filas y las dos
primeras columnas cumple
$$\det(M) = \det \left( \begin{array}{rr} 3 & 6 \cr 1 & 1 \end{array} \right) = -3 \ne 0.$$
Realicemos el orlado de esta submatriz con la tercera fila:
$$\begin{aligned}
  \det(M\sb{1}) & = \det \left( \begin{array}{rrr} 3 & 6 & 5 \cr 1 & 1 & 2 \cr 1 & -2 & 3 \end{array} \right) = 0, \cr
  \det(M\sb{2}) & = \det \left( \begin{array}{rrr} 3 & 6 & 9 \cr 1 & 1 & 4 \cr 1 & -2 & 7 \end{array} \right) = 0.
\end{aligned}$$ Por tanto, $\rg(A) = 2$.
{{% /example %}}

## Regla de Cramer

![image](fotos/cramer.eps)

{{% theorem name="Regla de" %}}
 Cramer Sea $A\sb{n \times n} \vec{x} = \vec{b}$ un sistema con $A$
matriz invertible y $\vec{u}$ su solución. Entonces la $i$-ésima
componente $u\sb{i}$ del vector $\vec{u}$ verifica que
$$u\sb{i} = \frac{\det(A\sb{i})}{\det(A)},$$ donde
$A\sb{i} = \left( \begin{array}{c|c|c|c|c|c|c} A\sb{\ast 1} & \cdots & A\sb{\ast i-1} & \vec{b} & A\sb{\ast i+1} & \cdots & A\sb{\ast n} \end{array} \right)$.
Esto es, $A\sb{i}$ es la matriz que se obtiene de $A$ cambiando la columna
$A\sb{\ast i}$ por $\vec{b}$.
{{% /theorem %}}

{{% proof %}}
Consideremos las matrices $I\sb{i}(\vec{u}), i=1,\ldots,n,$ que se obtienen
al sustituir la columna $i$-ésima de la matriz identidad $I$ por el
vector $\vec{u}$. Si $A \vec{u} = \vec{b}$, la definición de multiplicación
de matrices implica que $$\begin{aligned}
    A \cdot I\sb{i}(\vec{u}) & = A \left( \begin{array}{ccccc} \vec{e}\sb{1} & \ldots & \vec{u} & \ldots & \vec{e}\sb{n} \end{array} \right) \cr & = \left( \begin{array}{ccccc} A \vec{e}\sb{1} & \ldots & A \vec{u} & \ldots & A \vec{e}\sb{n} \end{array} \right) \cr & = \left( \begin{array}{ccccc} A\sb{\ast 1} & \ldots & \vec{b} & \ldots & A\sb{\ast n} \end{array} \right) = A\sb{i}.
\end{aligned}$$ Por la propiedad multiplicativa de los determinantes,
$$\det(A) \det(I\sb{i}(\vec{u})) = \det(A\sb{i}).$$ Observemos que
$\det(I\sb{i}(\vec{u})) = u\sb{i}$, pues basta realizar el desarrollo del
determinante por la $i$-ésima fila. De aquı́,
$\det(A) \cdot u\sb{i} = \det(A\sb{i})$ y tenemos el resultado por el carácter
invertible de $A$.
{{% /proof %}}

La regla de Cramer tiene únicamente un interés teórico, pues no se
aplica en cálculo numérico.

{{% example name="Ejemplo" %}}
Consideremos el sistema $A \vec{x} = \vec{b}$, con
$$A = \left( \begin{array}{rrr} 1 & 4 & 5 \cr 4 & 18 & 26 \cr 3 & 16 & 30 \end{array} \right), \vec{b} = \left( \begin{array}{r} 6 \cr 0 \cr -6 \end{array} \right).$$
Según la regla de Cramer, $$\begin{aligned}
x\sb{1} & =  \frac{\det(A\sb{1})}{\det(A)} = \frac{\left| \begin{array}{ccc} 6&4&5\cr{}0&18&26\cr{}-6&16&30\end{array} \right| }{6} = \frac{660}{6} = 110, \cr
x\sb{2} & =  \frac{\det(A\sb{2})}{\det(A)} = \frac{ \left| \begin{array}{ccc} 1&6&5\cr{}4&0&26
\cr{}3&-6&30\end{array} \right|}{6} = \frac{-216}{6} = -36, \cr
x\sb{3} & =  \frac{\det(A\sb{3})}{\det(A)} = \frac{\left| \begin{array}{ccc} 1&4&6\cr{}4&18&0\cr{}3&16&-6\end{array} \right|}{6} = \frac{48}{6} = 8.
\end{aligned}$$
{{% /example %}}

## Cofactores y matriz inversa

Recordemos que el cofactor de $A\sb{n \times n}$ asociado con la posición
$(i,j)$ se define como
$$\widehat{A}\sb{ij} = (-1)^{i+j} \det(M\sb{ij}(A)),$$ donde $M\sb{ij}(A)$ es
la submatriz de orden $n-1$ que se obtiene borrando la fila $i$-ésima y
la columna $j$-ésima de la matriz $A$. La matriz de cofactores se notará
por $\widehat{A} = (\widehat{A}\sb{ij})$.

{{% example name="Ejemplo" label="ej:cofactor" %}}
Los cofactores de la matriz
$$A = \left( \begin{array}{rrr} 1 & 4 & 5 \cr 4 & 18 & 26 \cr 3 & 16 & 30 \end{array} \right)$$
son $$\begin{aligned}
\widehat{A}\sb{11} & =  (-1)^2 \det \left( \begin{array}{cc} 18&26\cr{}16&30\end{array} \right) = 124, \cr
\widehat{A}\sb{12} & =  (-1)^3 \det \left( \begin{array}{cc} 4&26\cr{}3&30\end{array} \right) = -42, \cr
\widehat{A}\sb{13} & =  (-1)^4 \det \left( \begin{array}{cc} 4&18\cr{}3&16\end{array} \right) = 10, \cr
& \vdots & \cr
\widehat{A}\sb{33} & =  (-1)^6 \det \left( \begin{array}{cc} 1&4\cr{}4&18\end{array} \right) = 2.
\end{aligned}$$ La matriz de cofactores es igual entonces a
$$\widehat{A} = \left( \begin{array}{ccc} 124&-42&10\cr{}-40&15&-4\cr{}14&-6&2\end{array} \right).$$
{{% /example %}}

{{% theorem name="Inversa y matriz adjunta" %}}
 Se define la matriz adjunta de $A\sb{n \times n}$
como $\Adj(A) = \widehat{A}^t$, la traspuesta de la matriz de
cofactores. Si $A$ es invertible, entonces
$$A^{-1} = \frac{\Adj(A)}{\det(A)}.$$
{{% /theorem %}}

{{% proof %}}
El elemento $[A^{-1}]\sb{ij}$ es la $i$-ésima componente de la solución
del sistema $A \vec{x} = \vec{e}\sb{j}$, donde $\vec{e}\sb{j}$ es el $j$-ésimo
vector de la base estándar. Por la regla de Cramer,
$$[A^{-1}]\sb{ij} = x\sb{i} = \frac{\det(A\sb{i})}{\det(A)},$$ donde $A\sb{i}$ es la
matriz que se obtiene al cambiar la columna $i$-ésima de $A$ por
$\vec{e}\sb{j}$, y el desarrollo de $\det(A\sb{i})$ por la columna $i$-ésima
implica que $$\begin{array}{rccccl}
\det(A\sb{i}) & = & \det & \left( \begin{array}{ccccc} a\sb{11} & \cdots & 0 & \cdots & a\sb{1n} \cr \vdots & & \vdots & & \vdots \cr a\sb{j1} & \cdots & 1 & \cdots & a\sb{jn} \cr \vdots & & \vdots & & \vdots \cr a\sb{n1} & \cdots & 0 & \cdots & a\sb{nn} \end{array} \right) & = & \widehat{A}\sb{ji}. \cr
 & & & \uparrow & \cr
 & & & \begin{array}{l} \text{ columna } i \cr \text{ vector } \vec{e}\sb{j} \end{array}
\end{array}$$
{{% /proof %}}

{{% example name="Ejemplo" %}}
El cálculo de la inversa de la matriz del ejemplo
[\[ej:cofactor\]](#ej:cofactor){reference-type="ref"
reference="ej:cofactor"}
$$A = \left( \begin{array}{ccc} 1&4&5\cr{}4&18&26\cr{}3&16&30\end{array} \right)$$
mediante la matriz adjunta
$$\Adj(A) = \widehat{A}^t = \left( \begin{array}{ccc} 124&-40&14\cr{}-42&15&-6\cr{}10&-4&2\end{array} \right)$$
nos da
$$A^{-1} = \frac{\Adj(A)}{\det(A)} = \frac{1}{6} \left( \begin{array}{ccc} 124&-40&14\cr{}-42&15&-6\cr{}10&-4&2\end{array} \right) = \left( \begin{array}{ccc} {\frac {62}{3}}&-{\frac {20}{3}}&7/3\cr{}-7&5/2&-1\cr{}5/3&-2/3&1/3
\end{array} \right).$$
{{% /example %}}

## \* Determinantes y permutaciones

Denotaremos $S\sb{n}$ al conjunto de las permutaciones de un conjunto de $n$
elementos, es decir, al conjunto de las aplicaciones biyectivas
$$\sigma:\: \{1,\ldots,n\} \longrightarrow \{1,\ldots,n\}.$$ Dada una
permutación $\sigma\in S\sb{n}$, una de las caracterı́sticas de $\sigma$ que
va a tener más importancia en este tema será su ***número de
inversiones***, $ni(\sigma)$, que es el número de pares $(i,j)$ tales
que $i<j$ y $\sigma(i)>\sigma(j)$. Hay una forma gráfica muy útil para
describir el número de inversiones de $\sigma$: Si disponemos dos copias
del conjunto $\{1,\ldots,n\}$, donde los números están alineados
verticalmente como en el ejemplo de la
figura [\[fig:permutacion\]](#fig:permutacion){reference-type="ref"
reference="fig:permutacion"}, la permutación $\sigma$ viene representada
por $n$ segmentos, o lı́neas, que unen el elemento $i$ de la primera
copia al elemento $\sigma(i)$ de la segunda. Es claro que $ni(\sigma)$
es simplemente el número de cruces (intersecciones) que aparecen en esta
representación. (Nota: se debe evitar que más de dos rectas se corten en
el mismo punto, para poder contar los cruces de forma más clara.)

![image](fotos/permutacion.eps)

A partir de $ni(\sigma)$, se obtiene el ***signo*** (o la ***paridad***)
de la permutación $\sigma$, dado simplemente por la fórmula:
$$\sg(\sigma) = (-1)^{ni(\sigma)}.$$ Observemos que $\sg(\sigma)=1$ si
$ni(\sigma)$ es un número par, y $\sg(\sigma)=-1$ si $ni(\sigma)$ es un
número impar. En el primer caso diremos que $\sigma$ es una
***permutación par***, mientras que en el segundo diremos que $\sigma$
es una ***permutación impar***.

{{% theorem name="Composición de permutaciones y signo" %}}
 La composición de dos permutaciones
pares, o de dos permutaciones impares, es una permutación par. La
composición de una permutación par y una impar, es una permutación
impar.
{{% /theorem %}}

{{% proof %}}
Sean $\sigma,\tau\in S\sb{n}$ dos permutaciones, que representaremos
gráficamente de forma consecutiva, como en la
figura [\[fig:permutacion\]](#fig:permutacion){reference-type="ref"
reference="fig:permutacion"}. Observemos que la composición
$\tau\circ \sigma$ se obtiene siguiendo las lı́neas desde la primera
copia de $\{1,\ldots,n\}$ a la tercera. Un par $(i,j)$ será una
inversión de $\tau\circ \sigma$ si las lı́neas correspondientes se cruzan
una sóla vez (o bien en $\sigma$ o bien en $\tau$). Por el contrario,
$(i,j)$ no será una inversión de $\tau\circ \sigma$ si las lı́neas
correspondientes no se cruzan, o si se cruzan dos veces (los dos cruces
marcados en la
figura [\[fig:permutacion\]](#fig:permutacion){reference-type="ref"
reference="fig:permutacion"}). Si llamamos $m$ al número de pares
$(i,j)$ tales que sus lı́neas correspondientes se cruzan dos veces, este
argumento nos dice que $ni(\tau\circ \sigma)= ni(\sigma)+ni(\tau)-2m$.
De aquı́ se deduce inmediatamente el resultado sobre la paridad de
$\tau\circ \sigma$.
{{% /proof %}}

Una consecuencia inmediata del resultado anterior es la siguiente
propiedad:

{{% theorem name="Permutación inversa y signo" %}}
 Dada $\sigma\in S\sb{n}$, se tiene
$\sg(\sigma) = \sg(\sigma^{-1})$.
{{% /theorem %}}

{{% proof %}}
Al ser $\sigma^{-1}\circ\sigma=\mbox{Id}$ una permutación par (la
identidad es la única permutación sin inversiones), el resultado
anterior implica que $\sigma$ y $\sigma^{-1}$ tienen la misma paridad.
{{% /proof %}}

{{% remark %}}
Si $\sigma$ está representada por la correspondencia
$$\left( \begin{array}{cccc} 1 & 2 & \ldots & n \cr i\sb{1} & i\sb{2} & \ldots & i\sb{n} \end{array} \right),$$
la permutación inversa se puede obtener efectuando cambios en la parte
inferior para llegar a la secuencia $1, 2, \ldots, n$:
$$\left( \begin{array}{cccc} j\sb{1} & j\sb{2} & \ldots & j\sb{n} \cr 1 & 2 & \ldots & n \end{array} \right).$$
{{% /remark %}}

Más adelante necesitaremos algún que otro resultado sobre permutaciones,
pero ahora ya podemos dar la definición principal de este tema.

{{% definition %}}
Determinante por permutaciones Sea $A$ una matriz cuadrada de orden
$n\times n$. Se define el **determinante** por permutaciones de $A$,
denotado $|A|$, como:
$$|A|=\sum\sb{\sigma\in S\sb{n}}{\sg(\sigma)\cdot [A]\sb{1\sigma(1)}[A]\sb{2\sigma(2)}\cdots [A]\sb{n\sigma(n)}}.$$
{{% /definition %}}

Observemos que $|A|$ es una suma de $n!$ términos, puesto que hay un
término por cada permutación de $S\sb{n}$. Cada sumando contiene un producto
de la forma $a\sb{1\sigma(1)}a\sb{2\sigma(2)}\cdots a\sb{n\sigma(n)}$. Estos
$n$ escalares son $n$ entradas de la matriz $A$ que están en $n$ filas
distintas, y también en $n$ columnas distintas. Por otro lado, si
elegimos $n$ entradas de $A$ que estén en filas distintas y en columnas
distintas, el producto de esos $n$ escalares aparecerá en uno de los
sumandos que definen $|A|$: la permutación correspondiente será la que
asocia, a cada fila, la columna donde está la entrada elegida. Por
tanto, $|A|$ es la suma de todos estos posibles productos, cada uno de
ellos con un signo determinado por la permutación correspondiente.

{{% example name="Ejemplo" %}}
Si $A$ es una matriz de orden $2\times 2$, sólo hay dos permutaciones
posibles en $S\sb{2}$, la identidad y la que permuta los elementos $1$ y
$2$. La primera es par, y da lugar al sumando $a\sb{11}a\sb{22}$. La segunda
es impar, y da lugar al sumando $-a\sb{12}a\sb{21}$. Por tanto, en el caso
$n=2$, tenemos la conocida fórmula:
$$\left|\begin{matrix} a\sb{11} & a\sb{12} \cr a\sb{21} & a\sb{22}\end{matrix}\right|=a\sb{11}a\sb{22} - a\sb{12}a\sb{21}.$$
{{% /example %}}

{{% example name="Ejemplo" %}}
Sea $A$ una matriz de orden $3\times 3$. Como hay $6$ permutaciones en
$S\sb{3}$, la fórmula que define $|A|$ consta de $6$ sumandos, tres de ellos
correspondientes a permutaciones pares y tres correspondientes a
permutaciones impares. Concretamente: $$\begin{aligned}
   \left|\begin{matrix} a\sb{11} & a\sb{12} & a\sb{13} \cr  a\sb{21} & a\sb{22} & a\sb{23} \cr a\sb{31} & a\sb{32} & a\sb{33} \end{matrix}\right| & = & a\sb{11}a\sb{22}a\sb{33}+a\sb{12}a\sb{23}a\sb{31}+a\sb{13}a\sb{21}a\sb{32} \cr & & -a\sb{11}a\sb{23}a\sb{32}-a\sb{12}a\sb{21}a\sb{33}-a\sb{13}a\sb{22}a\sb{31}.
\end{aligned}$$
{{% /example %}}

Dos resultados inmediatos son los siguientes. No necesitamos dar la
demostración, al ser evidentes a partir de la definición del
determinante.

{{% theorem name="Fila o columna de ceros" %}}
 Si una matriz cuadrada $A$ tiene una fila o una
columna de ceros, entonces $|A|=0$.
{{% /theorem %}}

{{% theorem name="Determinante de la matriz identidad" %}}
 Si $I$ es la matriz identidad de
orden $n\times n$, entonces $|I|=1$.
{{% /theorem %}}

### \* Efecto por trasposición y operaciones elementales

{{% theorem name="Invariancia por trasposición" %}}
 Para toda matriz cuadrada $A$, se tiene:
$|A|=|A^t|.$
{{% /theorem %}}

{{% proof %}}
Por definición, tenemos $$\begin{aligned}
|A^t| & = & \sum\sb{\sigma\in S\sb{n}}{\sg(\sigma)\cdot [A]\sb{1\sigma(1)}^t [A]\sb{2\sigma(2)}^t\cdots [A]\sb{n\sigma(n)}^t} \cr
      & = & \sum\sb{\sigma\in S\sb{n}}{\sg(\sigma)\cdot [A]\sb{\sigma(1)1}[A]\sb{\sigma(2)2}\cdots [A]\sb{\sigma(n)n}} \cr
      & = & \sum\sb{\sigma\in S\sb{n}}{\sg(\sigma)\cdot [A]\sb{\sigma(1)\sigma^{-1}(\sigma(1))}[A]\sb{\sigma(2)\sigma^{-1}(\sigma(2))}\cdots [A]\sb{\sigma(n)\sigma^{-1}(\sigma(n))}}.
\end{aligned}$$ Reordenando los términos en cada producto, para que los
primeros ı́ndices estén en orden ascendente, se tiene: $$\begin{aligned}
|A^t| & = & \sum\sb{\sigma\in S\sb{n}}{\sg(\sigma)\cdot [A]\sb{1\sigma^{-1}(1)}[A]\sb{2\sigma^{-1}(2)}\cdots [A]\sb{n\sigma^{-1}(n)}}.
\end{aligned}$$ Finalmente, como $\sg(\sigma) = \sg(\sigma^{-1})$, nos
queda: $$\begin{aligned}
|A^t| & = & \sum\sb{\sigma\in S\sb{n}}{\sg(\sigma^{-1})\cdot [A]\sb{1\sigma^{-1}(1)}[A]\sb{2\sigma^{-1}(2)}\cdots [A]\sb{n\sigma^{-1}(n)}}.
\end{aligned}$$ Pero toda permutación de $S\sb{n}$ tiene una única inversa,
es decir, el conjunto de las inversas de todas las permutaciones es, de
nuevo, $S\sb{n}$. Luego en el sumatorio anterior $\sigma^{-1}$ toma como
valor todas las posibles permutaciones, por tanto dicho sumatorio es
igual a $|A|$, como querı́amos demostrar.
{{% /proof %}}

Veamos ahora el efecto que producen las operaciones elementales por
filas en el determinante de una matriz.

{{% theorem name="Determinante y operaciones elementales de tipo" %}}
 I Si la matriz cuadrada
$B$ se obtiene de $A$ al intercambiar dos filas (o dos columnas),
entonces: $$|B|=-|A|.$$
{{% /theorem %}}

{{% proof %}}
Supongamos que $B$ se obtiene de $A$ al intercambiar las filas $i$ y
$j$, con $i<j$. Tendremos: $$\begin{aligned}
 |B| & = & \sum\sb{\sigma\in S\sb{n}}{\sg(\sigma)\cdot [B]\sb{1\sigma(1)}\cdots [B]\sb{i\sigma(i)}\cdots [B]\sb{j\sigma(j)}\cdots [B]\sb{n\sigma(n)}} \cr
       & = & \sum\sb{\sigma\in S\sb{n}}{\sg(\sigma)\cdot [A]\sb{1\sigma(1)}\cdots [A]\sb{j\sigma(i)}\cdots [A]\sb{i\sigma(j)}\cdots [A]\sb{n\sigma(n)}} \cr
       & = & \sum\sb{\sigma\in S\sb{n}}{\sg(\sigma)\cdot [A]\sb{1\sigma(1)}\cdots [A]\sb{i\sigma(j)}\cdots [A]\sb{j\sigma(i)}\cdots [A]\sb{n\sigma(n)}}.
\end{aligned}$$ Si llamamos $\tau$ a la permutación (llamada
trasposición) que intercambia los elementos $i$ y $j$, dejando
invariantes los demás, se tiene: $$\begin{aligned}
 |B| & = & \sum\sb{\sigma\in S\sb{n}}{\sg(\sigma)\cdot [A]\sb{1\sigma (\tau(1))}\cdots [A]\sb{i\sigma (\tau(i))}\cdots [A]\sb{j\sigma (\tau (j))}\cdots [A]\sb{n\sigma (\tau(n))}}.
\end{aligned}$$ Ahora observemos que una trasposición siempre es impar,
como se puede ver en su representación gráfica: hay $j-i-1$ lı́neas que
empiezan entre $i$ y $j$, y todas ellas se cruzan con las lı́neas $i$ y
$j$; además tenemos el cruce de la lı́nea $i$ con la lı́nea $j$, y no hay
ningún otro cruce. Por tanto, $ni(\tau)=2(j-i-1)+1$, luego $\tau$ es
impar. Esto implica que $\sg(\sigma) = -\sg(\sigma\circ \tau)$, y ası́
tenemos: $$\begin{aligned}
 |B| & = & \sum\sb{\sigma\in S\sb{n}}{-\sg(\sigma\circ \tau)\cdot [A]\sb{1\sigma (\tau(1))}\cdots [A]\sb{i\sigma (\tau(i))}\cdots [A]\sb{j\sigma (\tau (j))}\cdots [A]\sb{n\sigma (\tau(n))}} \cr
  & = & -\sum\sb{\sigma\in S\sb{n}}{\sg(\sigma\circ \tau)\cdot [A]\sb{1\sigma (\tau(1))}\cdots [A]\sb{i\sigma (\tau(i))}\cdots [A]\sb{j\sigma (\tau (j))}\cdots [A]\sb{n\sigma (\tau(n))}}.
\end{aligned}$$ El conjunto de las permutaciones $\sigma\circ \tau$
cuando $\sigma\in S\sb{n}$ es, de nuevo, el conjunto $S\sb{n}$ de todas las
permutaciones. Luego en este sumatorio $\sigma\circ \tau$ toma como
valores todas las posibles permutaciones y la suma da precisamente
$|A|$. Por tanto se obtiene $|B|=-|A|$, como querı́amos demostrar.

Supongamos ahora que $B$ se obtiene de $A$ al permutar dos columnas. En
este caso $B^t$ se obtiene de $A^t$ al permutar dos filas, luego
$|B|=|B^t|=-|A^t|=-|A|$.
{{% /proof %}}

Un corolario inmediato de este resultado es el siguiente:

{{% theorem name="Determinante de matrices con filas o columnas iguales" %}}
 Si $A$ es una
matriz cuadrada con dos filas o dos columnas iguales, entonces
$$|A|=0.$$
{{% /theorem %}}

{{% proof %}}
En este caso, $A$ se obtiene de sı́ misma al intercambiar dos filas o dos
columnas (aquellas que son iguales). Por tanto, $|A|=-|A|$, luego
$|A|=0$.
{{% /proof %}}

{{% remark %}}
En la demostración anterior, hemos supuesto que $2\neq 0$ en el cuerpo
$k$. En efecto, $|A|=-|A|$ es equivalente $2|A|=0$, que implica $|A|=0$
siempre que $2\neq 0$. Hay cuerpos en los que esto no ocurre (por
ejemplo $\mathbb Z/\mathbb Z 2$), aunque incluso en este caso el
resultado sigue siendo cierto. Un buen ejercicio es demostrar este
resultado directamente con la definición de determinante. En concreto,
supongamos que $A\sb{i\ast} = A\sb{j\ast}$, con $i \ne j$. Entonces para
$1 \le k \le n$ tenemos que $a\sb{ik} = a\sb{jk}$. Si $\sigma \in S\sb{n}$, sea
$\sigma' = \sigma \circ (i j)$. Vamos a probar que
$$a\sb{1 \sigma(1)} \cdots a\sb{n\sigma(n)} = a\sb{1 \sigma'(1)} \cdots a\sb{n\sigma'(n)}.$$
Esto se verifica porque $\sigma(k) = \sigma'(k)$ si $k \ne i, j$, de
donde $a\sb{k\sigma(k)} = a\sb{k\sigma'(k)}$, mientras que
$\sigma'(i) = \sigma(j)$ y $\sigma'(j) = \sigma(i)$, por lo que
$a\sb{i\sigma'(i)} = a\sb{i\sigma(j)} = a\sb{j\sigma(j)}$ y
$a\sb{j\sigma'(j)} = a\sb{j\sigma(i)} = a\sb{i\sigma(i)}$.

Pero $\sg(\sigma') = -\sg(\sigma)$, luego
$$\sg(\sigma) a\sb{1 \sigma(1)} \cdots a\sb{n\sigma(n)} + \sg(\sigma') a\sb{1 \sigma'(1)} \cdots a\sb{n\sigma'(n)} = 0.$$
La aplicación $\sigma \mapsto \sigma'$ establece una correspondencia
biyectiva entre las permutaciones pares e impares de $S\sb{n}$, por lo que
$$\begin{aligned}
  |A| & = \sum\sb{\sigma \in S\sb{n}} \sg(\sigma) a\sb{1 \sigma(1)} \cdots a\sb{n\sigma(n)} \cr & = \sum\sb{\sg(\sigma) = 1} (\sg(\sigma) a\sb{1 \sigma(1)} \cdots a\sb{n\sigma(n)} + \sg(\sigma') a\sb{1 \sigma'(1)} \cdots a\sb{n\sigma'(n)}) \cr & = 0.
\end{aligned}$$ Observemos que la prueba es válida incluso cuando
$2 = 0$ en el cuerpo base.
{{% /remark %}}

Para estudiar el efecto que una operación elemental de tipo II o III
induce en el determinante de una matriz, veremos un resultado más
general, que incluye a ambos.

{{% theorem name="Determinante y combinaciones lineales de filas" %}}
Sean $B,A\sb{1},\ldots,A\sb{t}$ matrices $n\times n$ tales que:

-   La fila $i$ de $B$ es una combinación lineal de las filas $i$ de
    $A\sb{1},\ldots,A\sb{t}$, es decir, existen unos escalares
    $\alpha\sb{1},\ldots,\alpha\sb{t}$ tales que
    $$[B]\sb{i\ast}=\alpha\sb{1} [A\sb{1}]\sb{i\ast}+\cdots \alpha\sb{t} [A\sb{t}]\sb{i\ast}.$$

-   Para todo $k\neq i$, las filas $k$ de $B,A\sb{1},\ldots,A\sb{t}$ son todas
    iguales.

Entonces $$\qquad |B| = \alpha\sb{1}|A\sb{1}|+\cdots+ \alpha\sb{t}|A\sb{t}|.$$
{{% /theorem %}}

{{% proof %}}
Para todo $k\neq i$, sabemos que la entrada $(k,j)$ de cualquiera de las
matrices estudiadas es $[B]\sb{k,j}$. Para la fila $i$, denotaremos
$[A\sb{m}]\sb{i,j}$ a la entrada $(i,j)$ de la matriz $A\sb{m}$, para
$m=1,\ldots,t$. Entonces tenemos
$[B]\sb{i,j}=\alpha\sb{1} [A\sb{1}]\sb{i,j}+\cdots+\alpha\sb{t} [A\sb{t}]\sb{i,j}$, para todo
$j=1,\ldots,n$.

La fórmula del determinante de $A$ queda: $$\begin{aligned}
    |B| & = & \sum\sb{\sigma\in S\sb{n}}{\sg(\sigma)\cdot [B]\sb{1\sigma(1)}\cdots [B]\sb{i\sigma(i)}} \cdots [B]\sb{n\sigma(n)} \cr
 & = & \sum\sb{\sigma\in S\sb{n}}{\sg(\sigma)\cdot [B]\sb{1\sigma(1)}\cdots \left(\alpha\sb{1} [A\sb{1}]\sb{i\sigma(i)}+ \cdots + \alpha\sb{t} [A\sb{t}]\sb{i\sigma(i)}\right) \cdots [B]\sb{n\sigma(n)}} \cr
& = & \left(\sum\sb{\sigma\in S\sb{n}}{\sg(\sigma)\cdot [B]\sb{1\sigma(1)}\cdots (\alpha\sb{1} [A\sb{1}]\sb{i\sigma(i)}) \cdots [B]\sb{n\sigma(n)}}\right) + \cdots
\cr & & \hspace{5cm}
\cdots + \left(\sum\sb{\sigma\in S\sb{n}}{\sg(\sigma)\cdot [B]\sb{1\sigma(1)}\cdots (\alpha\sb{t} [A\sb{t}]\sb{i\sigma(i)}) \cdots [B]\sb{n\sigma(n)}}\right)\cr
& = & \alpha\sb{1} \left(\sum\sb{\sigma\in S\sb{n}}{\sg(\sigma)\cdot [B]\sb{1\sigma(1)}\cdots [A\sb{1}]\sb{i\sigma(i)} \cdots [B]\sb{n\sigma(n)}}\right) + \cdots
\cr & & \hspace{5.2cm}
\cdots + \alpha\sb{t} \left(\sum\sb{\sigma\in S\sb{n}}{\sg(\sigma)\cdot [B]\sb{1\sigma(1)}\cdots  [A\sb{t}]\sb{i\sigma(i)} \cdots [B]\sb{n\sigma(n)}}\right)\cr
& = &  \alpha\sb{1} |A\sb{1}|+ \cdots + \alpha\sb{t}|A\sb{t}|,
\end{aligned}$$ como querı́amos demostrar.
{{% /proof %}}

{{% example name="Ejemplo" %}}
A partir de la combinación lineal
$(2, \ 3, \ -1) = 2(1, \ 5, \ 1) + (-1)(0,\ 7,\ 3)$, se deduce la
igualdad: $$\left|\begin{matrix}
      13 & 57 & 36 \cr
       2 &  3 & -1 \cr
       248 & 504 & 311
   \end{matrix}\right| =
   2\    \left|\begin{matrix}
      13 & 57 & 36 \cr
       1 &  5 & 1 \cr
       248 & 504 & 311
   \end{matrix}\right| +
   (-1) \
      \left|\begin{matrix}
      13 & 57 & 36 \cr
       0 &  7 & 3 \cr
       248 & 504 & 311
   \end{matrix}\right|.$$
{{% /example %}}

{{% theorem name="Determinante y operaciones elementales de tipo" %}}
 II Si la matriz cuadrada
$B$ se obtiene de $A$ al multiplicar una fila (o columna) por un escalar
$\alpha$, entonces: $$|B|=\alpha |A|.$$
{{% /theorem %}}

{{% proof %}}
Inmediato a partir de los resultados anteriores, tomando $t=1$,
$\alpha\sb{1}=\alpha$ y $A\sb{1}=A$.
{{% /proof %}}

{{% theorem name="Determinante y operaciones elementales de tipo" %}}
 III Si la matriz cuadrada
$B$ se obtiene de $A$ al sumar a una fila (o columna) un múltiplo de
otra, entonces: $$|B|= |A|.$$
{{% /theorem %}}

{{% proof %}}
Supongamos que $B$ se obtiene de $A$ al sumar, a la fila $i$, la fila
$j$ multiplicada por $\alpha\in k$. Denotemos $A\sb{1}=A$, y sea $A\sb{2}$ la
matriz que se obtiene de $A$ al sustituir la fila $i$ for la $j$,
dejando la fila $j$ como está: es decir, las filas $i$ y $j$ de $A\sb{2}$
son ambas iguales a la fila $j$ de $A$. En este caso, $B$, $A\sb{1}$ y $A\sb{2}$
satisfacen las hipótesis de los resultados anteriores: las filas $k$ con
$k\neq i$ son todas iguales, y la fila $i$ de $B$ es combinación lineal
de las filas $i$ de $A\sb{1}$ y $A\sb{2}$. Concretamente:
$$[B]\sb{i\ast}=[A]\sb{i\ast}+\alpha[A]\sb{j\ast}= [A\sb{1}]\sb{i\ast}+\alpha[A\sb{2}]\sb{i\ast},$$ por
lo que se tiene $|B|=|A\sb{1}|+\alpha |A\sb{2}|$. Como $A\sb{2}$ es una matriz con
dos filas iguales, $|A\sb{2}|=0$, luego $|B|=|A\sb{1}|=|A|$.

El caso de las operaciones elementales por columnas se demuestra igual.
{{% /proof %}}

## \* Igualdad de ambas definiciones

El objetivo de esta sección es probar que el determinante de una matriz
definido por recurrencia y el definido por permutaciones coinciden. Una
forma es comprobar que el determinante por permutaciones es igual al
desarrollo por cofactores de una columna de la matriz. Vamos a seguir
otro método, basado en la unicidad de las propiedades del determinante
que hemos probado para las dos definiciones. Por ello, necesitamos una
definición para estas funciones.

{{% definition %}}
Función determinante

Sea $\K$ un cuerpo y $n$ un entero positivo. Una función
$\delta\sb{n}:\MatK{n}{n}{\K} \to \K$ es una **función determinante** si
satisface las siguientes propiedades:

1.  $\delta\sb{n}(I) = 1$, donde $I$ es la matriz identidad.

2.  $\delta\sb{n}(A) = 0$ si $A$ tiene una fila de ceros.

3.  $\delta\sb{n}(E\sb{ij} A) = -\delta\sb{n}(A)$, donde $E\sb{ij}$ es una matriz
    elemental de tipo I.

4.  $\delta\sb{n}(E\sb{i}(\alpha) A) = \alpha \delta\sb{n}(A)$, donde $E\sb{i}(\alpha)$
    es una matriz elemental de tipo $II$.

5.  $\delta\sb{n}(E\sb{ij}(\alpha) A) = \delta\sb{n}(A)$, donde $E\sb{ij}(\alpha)$
    es una matriz elemental de tipo $III$.
{{% /definition %}}

Ya sabemos que el determinante por recurrencia y el determinante por
permutaciones son funciones determinante. Para probar que coinciden,
basta ver que una función determinante está unı́vocamente determinada.

{{% theorem name="Unicidad de la función determinante" %}}
 Sea $\K$ un cuerpo. Para cada entero
positivo $n$ existe a lo más una única función determinante
$\delta\sb{n}:\MatK{n}{n}{\K} \to \K$. En consecuencia, $\det(A) = |A|$.
{{% /theorem %}}

{{% proof %}}
Sean $\delta\sb{n}, \gamma\sb{n}$ dos funciones determinante y llamemos
$\beta = \delta\sb{n} - \gamma\sb{n}$. La función $\beta$ satisface las
siguientes condiciones:

-   $\beta(I) = 0$.

-   $\beta(A) = 0$ si $A$ tiene una fila de ceros.

-   $\beta(E\sb{ij} A) = -\beta(A)$, donde $E\sb{ij}$ es una matriz
    elemental de tipo $I$.

-   $\beta(E\sb{i}(\alpha) A) = \alpha \beta(A)$, donde $E\sb{i}(\alpha)$ es una
    matriz elemental de tipo $II$.

-   $\beta(E\sb{ij}(\alpha) A) = \beta(A)$, donde $E\sb{ij}(\alpha)$ es una
    matriz elemental de tipo $III$.

Lo anterior prueba que si $E$ es una matriz elemental, entonces
$\beta(A)$ y $\beta(EA)$ son ambos nulos o ambos distintos de cero. Dada
una matriz $A$ de orden $n$, existen matrices elementales
$E\sb{1}, \ldots, E\sb{r}$ tales que $E\sb{1} \cdots E\sb{r} A = E\sb{A}$, donde $E\sb{A}$ es la
forma escalonada reducida por filas de $A$. Si $A$ tiene rango $n$,
entonces $E\sb{A} = I$ y $\beta(E\sb{1} \cdots E\sb{r} A) = 0$, de donde
$\beta(A) = 0$. Si $A$ es de rango inferior, entonces $E\sb{A}$ contiene una
fila de ceros, por lo que $\beta(E\sb{A}) = 0$ y $\beta(A) = 0$
{{% /proof %}}

{{% remark %}}
La definición de la función determinante se puede hacer sobre anillos,
como $\Z$ o $K[X\sb{1}, \ldots, X\sb{n}]$, donde sigue siendo válida la regla
del producto. Si el anillo es un dominio, podemos trabajar en el cuerpo
de fracciones para obtener algunos resultados. Sin embargo, hay algunas
propiedades de los vectores que no se verifican en los anillos. Por
ejemplo, el determinante de una matriz puede ser cero, pero eso no
implica que una columna o fila sea combinación lineal de las restantes.
Por ejemplo, la matriz con coeficientes enteros
$$A = \left( \begin{array}{rr} 12 & 18 \cr -6 & -9 \end{array} \right)$$
es singular, pero ninguna columna es múltiplo entero de la otra, aunque
sı́ son $\Z$-linealmente independientes.

Cuando se consideran espacios de funciones, hay que tener cuidado con
algunas propiedades relativas a la independencia lineal. Por ejemplo,
sea $n > 1$ y $U$ un intervalo abierto de $\R$. Sea $R$ el conjunto de
funciones $f:U \to \R$ que son diferenciables $n-1$ veces al menos. Dada
$f \in R$, notaremos por $Df$ su derivada y $D^h f$ su derivada
$h$-ésima. Dadas $f\sb{1}, \ldots, f\sb{n} \in R$, la función
$$W(f\sb{1},\ldots, f\sb{n})(t) = \det \left( \begin{array}{cccc} f\sb{1}(t) & f\sb{2}(t) & \ldots & f\sb{n}(t) \cr (D f\sb{1})(t) & (D f\sb{2})(t) & \ldots & (D f\sb{n})(t) \cr \vdots & \vdots & & \vdots \cr (D^{n-1} f\sb{1})(t) & (D{n-1} f\sb{2})(t) & \ldots & (D^{n-1} f\sb{n})(t) \end{array} \right)$$
se denomina Wronskiano de $f\sb{1}, \ldots, f\sb{n}$. Se puede probar que si
$W(f\sb{1},\ldots,f\sb{n})(t) \ne 0$ para algún $t \in U$, entonces el conjunto
$\{ f\sb{1}, \ldots, f\sb{n} \}$ es $\R$-linealmente independiente. El recı́proco
es falso. Sea $U$ un intervalo que contiene al origen consideremos las
funciones $f\sb{1}(t) = t^3, f\sb{2}(t) = |t|^3$. Entonces $\{ f\sb{1}, f\sb{2} \}$ es
un conjunto $\R$-linealmente independiente, pero $W(f\sb{1}, f\sb{2})(t) = 0$
para todo $t \in U$.
{{% /remark %}}

## \* Coste de cálculo del determinante

Con la definición por permutaciones, el determinante de una matriz de
orden $n$ se calcula mediante $n!$ sumandos, cada uno de ellos con $n-1$
productos. Por tanto, el número de operaciones necesarias son $n! -1$
sumas y $(n-1) \cdot n!$ productos, lo que hace un total de $n(n!) -1$
operaciones. Para una matriz de orden $25$, son necesarias del orden de
$3.9 \times 10^{26}$ operaciones. Un ordenador que realice $10^{15}$
operaciones por segundo (un petaflop) necesitará 3170 años para
completar el cálculo.

Veamos qué ocurre si usamos el desarrollo por cofactores de una fila o
columna. Usemos, Por ejemplo, el desarrollo por la primera columna:
tenemos $$\det(A) = \sum\sb{i=1}^n a\sb{i1} \widehat{A}\sb{i1}.$$ Llamemos
$p\sb{n}$ al número de operaciones necesarias mediante este método. Para
$n=2$ tenemos dos productos y una suma, luego $p\sb{2} = 3$. Supongamos
calculado $p\sb{n-1}$, el coste de un determinante de orden $n-1$.
Entonces el desarrollo por cofactores necesita el cálculo de $n$
determinantes de orden $n-1$, $n$ productos y $n-1$ sumas. Tenemos ası́
la ecuación
$$p\sb{n} = np\sb{n-1} + n + (n-1) = np\sb{n-1} + 2n -1, \text{ si } n > 2.$$
Los valores de $p\sb{n}$ crecen de manera proporcional a $n!$. Por ejemplo,
$p\sb{15} \approx 3.55 \times 10^{12}, p\sb{25} \approx 4.21 \times 10^{25}$.
Es ligeramente menos costoso que el desarrollo por permutaciones.

En el caso de un cuerpo, las transformaciones elementales nos permiten
realizar el cálculo del determinante con un coste del orden de
$2/3 n^3$, tal como se tiene para la eliminación gaussiana. Esto
funciona correctamente cuando se trabaja en un cuerpo, pero en anillos
más generales no se tiene la posibilidad de dividir por un escalar no
nulo, como es el caso de $\Z$ o de los anillos de polinomios. Existen
variantes de la eliminación gaussiana que evitan divisiones y permiten,
con un pequeño coste añadido, tratar estos casos.

{{% example name="Ejemplo" %}}
![image](fotos/Chio.eps)

El desarrollo de un determinante de orden $n$ por los elementos de una
fila y sus adjuntos es muy costoso, pues el número de términos crece
rápidamente y los cálculos son muy complicados. El método óptimo para
resolverlo es a través de la eliminación gaussiana, que alcanza una
complejidad de $2/3 n^3$ operaciones. Para ciertos determinantes no muy
grandes, y con entradas números enteros se puede usar el método de
condensación pivotal de Chiò.

Sea $A\sb{n \times n} = (a\sb{ij})$ una matriz, donde $a\sb{11} \ne 0$ (se
puede hacer análogamente para cualquier elemento no nulo de la matriz).
Multiplicamos cada fila, excepto la primera, por $a\sb{11}$. Entonces
$$a\sb{11}^{n-1} \det(A) = \det \left( \begin{array}{ccccc} a\sb{11} & a\sb{12} & \ldots & a\sb{1n} \cr a\sb{21} a\sb{11} & a\sb{22} a\sb{11} & \ldots & a\sb{2n} a\sb{11} \cr \vdots & \vdots & & \vdots \cr a\sb{n1} a\sb{11} & a\sb{n2} a\sb{11} & \ldots & a\sb{nn} a\sb{11} \end{array} \right).$$
Ahora restamos a la segunda fila la primera multiplicada por $a\sb{21}$, a
la tercera la primera multiplicada por $a\sb{31}$, hasta la $n$-ésima, que
le restamos la primera multiplicada por $a\sb{n1}$. Nos queda
$$\begin{aligned}
a\sb{11}^{n-1} \det(A) & = & \det \left( \begin{array}{ccccc} a\sb{11} & a\sb{12} & \ldots & a\sb{1n} \cr 0 & a\sb{22} a\sb{11} - a\sb{21} a\sb{12} & \ldots & a\sb{2n} a\sb{11} - a\sb{21} a\sb{1n} \cr \vdots & \vdots & & \vdots \cr 0 & a\sb{n2} a\sb{11} - a\sb{12} a\sb{n1} & \ldots & a\sb{nn} a\sb{11} - a\sb{1n} a\sb{n1} \end{array} \right) \cr & = & \det \left( \begin{array}{cccccc} a\sb{11} & a\sb{12} & a\sb{13} & \ldots & a\sb{1n} \cr 0 & \left| \begin{array}{cc} a\sb{11} & a\sb{12} \cr a\sb{21} & a\sb{22} \end{array} \right| & \left| \begin{array}{cc} a\sb{11} & a\sb{13} \cr a\sb{21} & a\sb{23} \end{array} \right| & \ldots & \left| \begin{array}{cc} a\sb{11} & a\sb{1n} \cr a\sb{21} & a\sb{2n} \end{array} \right| \cr \vdots & \vdots & \vdots & & \vdots \cr 0 & \left| \begin{array}{cc} a\sb{11} & a\sb{12} \cr a\sb{n1} & a\sb{n2} \end{array} \right| & \left| \begin{array}{cc} a\sb{11} & a\sb{13} \cr a\sb{n1} & a\sb{n3} \end{array} \right| & \ldots & \left| \begin{array}{cc} a\sb{11} & a\sb{1n} \cr a\sb{n1} & a\sb{nn} \end{array} \right| \end{array} \right) \cr & = & a\sb{11} \det(B).
\end{aligned}$$ Entonces $\det(A) = \frac{1}{a\sb{11}^{n-2}} \det(B)$. Si
el elemento fuera el $(i\sb{0},j\sb{0})$, hay que incluir un factor
$(-1)^{i\sb{0}+j\sb{0}}$ por el último desarrollo. Se suele buscar un elemento
que sea igual a $1$, para simplificar las operaciones. Por ejemplo,
$$\begin{aligned}
  \det \left( \begin{array}{rrrr} 1 & 2 & 3 & 4 \cr 8 & 7 & 6 & 5 \cr 1 & 8 & 2 & 7 \cr 3 & 6 & 4 & 5 \end{array} \right) & = & 1 \cdot \det \left( \begin{array}{ccc} \left| \begin{array}{rr} 1 & 2 \cr 8 & 7 \end{array} \right| & \left| \begin{array}{rr} 1 & 3 \cr 8 & 6 \end{array} \right| & \left| \begin{array}{rr} 1 & 4 \cr 8 & 5 \end{array} \right| \cr \left| \begin{array}{rr} 1 & 2 \cr 1 & 8 \end{array} \right| & \left| \begin{array}{rr} 1 & 3 \cr 1 & 2 \end{array} \right| & \left| \begin{array}{rr} 1 & 4 \cr 1 & 7 \end{array} \right| \cr \left| \begin{array}{rr} 1 & 2 \cr 3 & 6 \end{array} \right| & \left| \begin{array}{rr} 1 & 3 \cr 3 & 4 \end{array} \right| & \left| \begin{array}{rr} 1 & 4 \cr 3 & 5 \end{array} \right| \end{array} \right) \cr & = & \det \left( \begin{array}{rrr} -9 & -18 & -27 \cr 6 & -1 & 3 \cr 0 & -5 & -7 \end{array} \right) = (-9) \cdot \det \left( \begin{array}{rrr} 1 & 2 & 3 \cr 6 & -1 & 3 \cr 0 & -5 & -7 \end{array} \right) \cr & = & (-9) \cdot \det \left( \begin{array}{cc} \left| \begin{array}{rr} 1 & 2 \cr 6 & -1 \end{array} \right| & \left| \begin{array}{rr} 1 & 3 \cr 6 & 3 \end{array} \right| \cr \left| \begin{array}{rr} 1 & 2 \cr 0 & -5 \end{array} \right| & \left| \begin{array}{rr} 1 & 3 \cr 0 & -7 \end{array} \right| \end{array} \right) \cr & = & (-9) \cdot \det \left( \begin{array}{rr} -13 & -15 \cr -5 & -7 \end{array} \right) = (-9)(91 -75) = -144.
\end{aligned}$$ Este método consume más operaciones que la eliminación
gaussiana. Sin embargo, es uno de los que se utilizan dentro del
contexto de eliminación gaussiana libre de fracciones, usada en cálculo
simbólico o para matrices con entradas en dominios no cuerpos.
{{% /example %}}

La resolución de un sistema de ecuaciones mediante la regla de Cramer
presenta también desventajas con respecto a la eliminación gaussiana.
Mediante la eliminación, sabemos que un sistema de ecuaciones tiene un
coste de resolución del orden de $\frac{2}{3} n^3$; como hemos comentado
antes, este es el orden del cálculo de un determinante. La regla de
Cramer supone el cálculo de $n+1$ determinantes: uno para el
determinante de la matriz de coeficientes del sistema, y $n$ con los
determinantes en los que se ha cambiado una columna. Por ello, la regla
de Cramer tiene un coste del orden de $\frac{2}{3} n^4$, que es
sensiblemente superior. Además, la eliminación gaussiana permite la
resolución simultánea de varios sistemas con la misma matriz de
coeficientes, pero diferentes términos independientes. La regla de
Cramer obliga a recalcular $n$ determinantes.
