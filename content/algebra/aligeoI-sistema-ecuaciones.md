+++
title = "Sistemas de ecuaciones lineales"
weight = 30
+++



## Introducción

Un problema fundamental que aparece en matemáticas y en otras ciencias
es el análisis y resolución de $m$ ecuaciones algebraicas con $n$
incógnitas. El estudio de un sistema de ecuaciones lineales simultáneas
está ı́ntimamente ligado al estudio de una matriz rectangular de números
definida por los coeficientes de las ecuaciones. Esta relación parece
que se ha notado desde el momento en que aparecieron estos problemas.

El primer análisis registrado de ecuaciones simultáneas lo encontramos
en el libro chino *Jiu zhang Suan-shu* ( *Nueve Capı́tulos sobre las
artes matemáticas*), escrito alrededor del 200 a.C. Al comienzo del
capı́tulo VIII, aparece un problema de la siguiente forma:

*Tres gavillas de buen cereal, dos gavillas de cereal mediocre y una
gavilla de cereal malo se venden por 39 dou. Dos gavillas de bueno, tres
mediocres y una mala se venden por 34 dou. Y una buena, dos mediocres y
tres malas se venden por 26 dou. ¿Cuál es el precio recibido por cada
gavilla de buen cereal, cada gavilla de cereal mediocre, y cada gavilla
de cereal malo?*

[]{#chinos label="chinos"} Hoy en dı́a, este problema lo formuları́amos
como un sistema de tres ecuaciones con tres incógnitas:
$$\begin{array}{rcrcrcr} 3x\sb{1} & + & 2x\sb{2} & + & x\sb{3} & = & 39,\cr 2x\sb{1} & + & 3x\sb{2} & + & x\sb{3} & = & 34, \cr
x\sb{1} & + & 2x\sb{2} & + & 3x\sb{3} & = & 26,
\end{array}$$ donde $x\sb{1}, x\sb{2}$ y $x\sb{3}$ representan el precio de una
gavilla de buen, mediocre y mal cereal, respectivamente. Los chinos
vieron el problema esencial. Colocaron los coeficientes de este sistema,
representados por cañas de bambú de color, como un cuadrado sobre un
tablero de contar (similar a un ábaco), y manipulaban las filas del
cuadrado según ciertas reglas establecidas. Su tablero de contar y sus
reglas encontraron su camino hacia Japón y finalmente aparecieron en
Europa, con las cañas de color sustituidas por números y el tablero
reemplazado por tinta y papel.

![image](fotos/Countingrod)

En Europa, esta técnica llegó a ser conocida como *eliminación
gaussiana*, en honor del matemático alemán [Carl F.
Gauss](http://www-groups.dcs.st-and.ac.uk/~history/Mathematicians/Gauss.html).

![image](fotos/Gauss\sb{1}828)

El objetivo de este capı́tulo es conocer esta técnica de eliminación.
Comenzamos con una descripción de los sistemas de ecuaciones y la
notación que emplearemos para tratarlos.

## Equivalencia de sistemas

{{% remark %}}
En lo que sigue consideraremos fijado un cuerpo $\K$ de coeficientes. En
el texto nos referiremos a los elementos del cuerpo como **números** o
**escalares**. El lector bien puede pensar que $\K$ es el cuerpo $\Q$ de
los números racionales, $\R$ de los reales o incluso $\C$ de los
complejos. Aunque debe tener en cuenta que todo lo dicho sobre sistemas
de ecuaciones lineales y matrices es cierto en general para cualquier
cuerpo $\K$.
{{% /remark %}}

{{% definition %}}
Ecuación lineal Sea $n\geq 1$ un número natural. Una **ecuación lineal**
es una expresión de la forma
$$a\sb{1} x\sb{1} + a\sb{2} x\sb{2} + \cdots + a\sb{n} x\sb{n} = b,$$ donde
$a\sb{1},\ a\sb{2},\ldots,\ a\sb{n}$ y $b$ son números conocidos y
$x\sb{1}\ x\sb{2},\ldots,\ x\sb{n}$ son incógnitas. Los números $a\sb{i}$ se denominan
**coeficientes** de la ecuación, mientras que $b$ es el **término
independiente**.
{{% /definition %}}

Una **solución** de la ecuación lineal anterior es una lista de números
$\alpha\sb{1},\alpha\sb{2},\ldots ,\alpha\sb{n} \in \K$
que la satisfacen, es decir, que verifican
$$a\sb{1}\alpha\sb{1}+a\sb{2}\alpha\sb{2}+\cdots +a\sb{n}\alpha\sb{n} = b.$$ Escribiremos esta
solución de dos posibles maneras:
$$\left\\{\begin{array}{l} x\sb{1}=\alpha\sb{1} \cr x\sb{2}=\alpha\sb{2} \cr \vdots \cr x\sb{n}=\alpha\sb{n}\end{array}\right. \qquad \text{o bien} \qquad
\left( \begin{array}{c} x\sb{1} \cr x\sb{2} \cr \vdots \cr x\sb{n} \end{array} \right)=
\left( \begin{array}{c} \alpha\sb{1} \cr \alpha\sb{2} \cr \vdots \cr \alpha\sb{n} \end{array} \right).$$

{{% example name="Ejemplo" %}}
La expresión $3x\sb{1} + 2x\sb{2} = -1$ es una ecuación lineal. Una solución es
$\left\\{\begin{array}{l} x\sb{1} = 1 \cr x\sb{2} = -2\end{array}\right.$.

La expresión $0 \cdot x\sb{1} = 2$ es una ecuación lineal, pero no tiene
solución.
{{% /example %}}

{{% definition %}}
Sistema de ecuaciones lineales Sean $m\geq 1$ y $n\geq 1$ números
naturales. Un **sistema de ecuaciones lineales** o **sistema lineal de
ecuaciones** es un conjunto de $m$ ecuaciones lineales y $n$ incógnitas
de la forma $${\mathcal S} \equiv \left\\{\begin{array}{ccccccccc}
 a\sb{11} x\sb{1} & + & a\sb{12} x\sb{2} & + & \ldots & + & a\sb{1n} x\sb{n} & = & b\sb{1}, \cr
 a\sb{21} x\sb{1} & + & a\sb{22} x\sb{2} & + & \ldots & + & a\sb{2n} x\sb{n} & = & b\sb{2} \cr & & & & \vdots \cr
 a\sb{m1} x\sb{1} & + & a\sb{m2} x\sb{2} & + & \ldots & + & a\sb{mn} x\sb{n} & = & b\sb{m},
 \end{array}\right.$$ donde las $x\sb{i}$ son las incógnitas y los
$a\sb{ij}, b\sb{i}$ son números. Los números $a\sb{ij}$ se denominan
**coeficientes** del sistema, y el conjunto de los $b\sb{i}$ **términos
independientes** del sistema.
{{% /definition %}}

Una **solución** del sistema de ecuaciones lineales anterior es una
lista de números $\alpha\sb{1},\alpha\sb{2},\ldots ,\alpha\sb{n} \in \K$ que
satisface cada ecuación del sistema, es decir, que verifica
$$a\sb{i1}\alpha\sb{1} + a\sb{i2} \alpha\sb{2} + \cdots + a\sb{in} \alpha\sb{n} = b\sb{i}, \text{ para cada } i=1,\ldots,m.$$
Para abreviar, hablaremos con frecuencia de un sistema de ecuaciones,
eliminando la palabra "lineales", pues serán el único tipo de ecuaciones
que trataremos en este curso.

El problema es determinar si un sistema de ecuaciones tiene solución o
no y, en el primer caso, determinar cuántas soluciones tiene y
calcularlas todas. Ya con una ecuación $a\sb{1} x\sb{1} = b\sb{1}$ nos encontramos
con diferentes posibilidades.

-   Si $a\sb{1} \ne 0$, entonces la ecuación tiene una única solución
    $x\sb{1} = b\sb{1}/a\sb{1}$.

-   Si $a\sb{1} = 0, b\sb{1} \ne 0$, entonces no hay solución.

-   Si $a\sb{1} = 0 = b\sb{1}$, entonces cualquier valor de $x\sb{1}$ es solución.

En sistemas con más ecuaciones encontramos el mismo fenómeno. Veamos
distintos ejemplos.

{{% example name="Ejemplo" %}}
Consideremos el sistema lineal planteado por el problema chino de las
gavillas de cereal visto en la página :
$${\cal S}\equiv\left\\{\begin{array}{l}
        3x\sb{1}+2x\sb{2}+x\sb{3}=39,\cr
        2x\sb{1}+3x\sb{2}+x\sb{3}=34,\cr
        x\sb{1}+2x\sb{2}+3x\sb{3}=26.
       \end{array}\right.$$ Vamos a resolverlo utilizando el *método de
sustitución*. Cualquier solución de la tercera ecuación debe cumplir:
$x\sb{1} = 26-2x\sb{2}-3x\sb{3}$. Sustituyendo esta expresión de $x\sb{1}$ en las otras
ecuaciones tenemos $$\left\\{\begin{array}{l}
        -4x\sb{2}-8x\sb{3}=-39,\cr
        -x\sb{2}-5x\sb{3}=-18.
       \end{array}\right.$$ Estas ecuaciones también deben cumplirse
para cualquier solución del sistema original. De la segunda ecuación
obtenemos $x\sb{2}=-5x\sb{3}+18$. Sustituyendo esta expresión de $x\sb{2}$ en la
primera de las dos ecaciones, se obtiene $$12 x\sb{3}=33.$$ Por tanto,
debemos tener $x\sb{3}=11/4$. Como también debe cumplirse la ecuación
$x\sb{2}=5x\sb{3}-18$, esto implica que $x\sb{2}=17/4$. Por último, como debe
cumplirse $x\sb{1}=26-2x\sb{2}-3x\sb{3}$, también debemos tener $x\sb{1}=37/4$. Por
tanto, estos tres valores son los únicos que podrían darnos una solución
del sistema original.

Podemos comprobar que estos tres valores de las incógnitas cumplen las
tres ecuaciones originales, luego el sistema lineal ${\cal S}$ tiene
como única solución
$$\left(\begin{array}{c} x\sb{1}\cr x\sb{2}\cr x\sb{3}\end{array}\right)=\left(\begin{array}{c} 37/4\cr 17/4\cr 11/4\end{array}\right).$$
{{% /example %}}

{{% example name="Ejemplo" %}}
[]{#incompatible label="incompatible"} Consideremos ahora el sistema de
ecuaciones lineales $${\cal S}\equiv\left\\{\begin{array}{l}
        x\sb{1}+x\sb{2}=0,\cr
        x\sb{1}+x\sb{2}=1.
       \end{array}\right.$$ En este caso, si unos valores de $x\sb{1}$ y
$x\sb{2}$ cumplen la primera ecuación, no pueden cumplir la segunda. Por
tanto, este sistema de ecuaciones lineales no tiene solución.
{{% /example %}}

{{% example name="Ejemplo" %}}
Consideremos por último el siguiente sistema:
$${\cal S}\equiv\left\\{\begin{array}{r}
        x\sb{1}-x\sb{2}=0,\cr
        x\sb{3}=4.
       \end{array}\right.$$ En este caso vemos que $x\sb{3}$ debe tomar el
valor 4. Pero la primera ecuación sólo nos indica que $x\sb{1}$ debe tomar
el mismo valor que $x\sb{2}$. Por tanto, las soluciones del sistema son
todas las listas de números de la forma:
$$\left(\begin{array}{c} x\sb{1}\cr x\sb{2}\cr x\sb{3}\end{array}\right)=\left(\begin{array}{c} \alpha\cr \alpha\cr 4\end{array}\right),$$
donde $\alpha$ es un número cualquiera de $\K$. En este caso el sistema
lineal tiene tantas soluciones como elementos hay en el cuerpo de
escalares $\K$.
{{% /example %}}

Las tres posiblidades que hemos visto en los ejemplos anteriores son las
que nos encontraremos al estudiar sistemas de ecuaciones lineales en
general.

{{% definition %}}
Compatibilidad de sistemas lineales

Decimos que un sistema lineal ${\cal S}$ es

-   **compatible determinado** si tiene una única solución.

-   **compatible indeterminado** si tiene más de una solución.

-   **incompatible** si no tiene soluciones.
{{% /definition %}}

Una noción importante a la hora de resolver sistemas de ecuaciones
lineales es la siguiente:

{{% definition %}}
Sistema de ecuaciones equivalentes Dos sistemas lineales con $n$
incógnitas se dicen **equivalentes** si tienen los mismos conjuntos de
soluciones. Es decir, si toda solución del primero es solución del
segundo, y toda solución del segundo es solución del primero.
{{% /definition %}}

{{% example name="Ejemplo" %}}
Veamos los siguientes casos con coeficientes en $\R$.

1.  Consideremos los sistemas de ecuaciones
    $$\mathcal{S}\sb{1} :  \left\\{ \begin{array}{l} x\sb{1} = -1, \cr x\sb{2} = 2. \end{array} \right.
        \qquad
        \mathcal{S}\sb{2} : \left\\{ \begin{array}{l} 3x\sb{1} + x\sb{2} = -1, \cr 2x\sb{1} + x\sb{2} = 0, \cr 5x\sb{1} + 2x\sb{2} = -1. \end{array} \right.$$
    El conjunto de soluciones de ambos sistemas está formado por una
    única solución:
    $\left\\{\begin{array}{l} x\sb{1} = -1 \cr x\sb{2} = 2 \end{array}\right.$.
    Luego $\mathcal S\sb{1}$ y $\mathcal S\sb{2}$ son sistemas equivalentes,
    aunque tengan distinto número de ecuaciones.

2.  Consideremos los sistemas en dos incógnitas dados por
    $$\mathcal{S}\sb{1}: \left\\{ 1 \cdot x\sb{1} + 0 \cdot x\sb{2} = 1, \right.
        \qquad
        \mathcal{S}\sb{2} : \left\\{ \begin{array}{l} x\sb{1} = 1, \cr x\sb{2} = 0. \end{array} \right.$$
    El conjunto de soluciones del primer sistema es
    $\left\\{\begin{pmatrix} 1 \cr \beta \end{pmatrix};\ \beta\in \R\right\\}$.
    Este conjunto contiene estrictamente al conjunto de soluciones de
    $\mathcal{S}\sb{2}$. Por tanto $\mathcal S\sb{1}$ y $\mathcal S\sb{2}$ no son
    sistemas equivalentes.
{{% /example %}}

## Eliminación gaussiana

La eliminación gaussiana es una herramienta que nos permitirá determinar
si un sistema tiene solución y, en tal caso, calcularlas todas. Es un
*algoritmo* que sistemáticamente transforma un sistema en otro más
simple, pero *equivalente*. La idea es llegar a un sistema lo más
sencillo posible, eliminando variables, y obtener al final un sistema
que sea fácilmente resoluble, como sucede en el siguiente ejemplo.

{{% example name="Ejemplo" %}}
El sistema
$${\mathcal S} \equiv \left\\{ \begin{array}{rr} 2x\sb{1} + 5x\sb{2} = -1, \cr 2x\sb{2} = 3, \end{array} \right.$$
se puede resolver fácilmente, gracias a que $x\sb{1}$ no aparece en la
segunda ecuación. Esto nos permite despejar $x\sb{2} = 3/2$ y sustituir este
valor en la primera ecuación para obtener
$$2x\sb{1} + 5 \frac{3}{2} = -1, \qquad \text{de donde} \qquad x\sb{1} = \frac{1}{2} \left(-1-\frac{15}{2}\right) = -\frac{17}{4}.$$
{{% /example %}}

El proceso de eliminación se basa en tres operaciones simples que
transforman un sistema en otro equivalente. Para describir estas
operaciones, llamemos $E\sb{k}$ a la $k$-ésima ecuación
$$E\sb{k}: \quad a\sb{k1} x\sb{1} + a\sb{k2} x\sb{2} + \ldots + a\sb{kn} x\sb{n} = b\sb{k}$$ y
escribamos el sistema como
$${\mathcal S} \equiv \left\\{ \begin{array}{c} E\sb{1} \cr E\sb{2} \cr \vdots \cr E\sb{m}
 \end{array} \right\\}.$$ Dado un sistema de ecuaciones ${\mathcal S}$
como este, cada una de las siguientes **transformaciones elementales**
produce un sistema equivalente ${\mathcal S}'$.

1.  Intercambiar la ecuación $i$ con la ecuación $j$, donde
    $1\leq i<j\leq m$. Esto es, si
    $${\mathcal S} \equiv \left\\{ \begin{array}{c} E\sb{1} \cr \vdots \cr E\sb{i} \cr \vdots \cr
      E\sb{j} \cr \vdots \cr E\sb{m}
     \end{array} \right\\}, \quad \text{entonces} \quad {\mathcal S}' \equiv \left\\{ \begin{array}{c} E\sb{1} \cr \vdots \cr E\sb{j} \cr \vdots \cr
      E\sb{i} \cr \vdots \cr E\sb{m}
     \end{array} \right\\}. \qquad \text{Escribiremos}\quad \mathcal{S} \mapright{E\sb{ij}} \mathcal{S}'.$$

2.  Multiplicar la ecuación $i$ por número no nulo. Esto es,
    $${\mathcal S}' \equiv \left\\{ \begin{array}{c} E\sb{1} \cr \vdots \cr \alpha E\sb{i} \cr \vdots \cr E\sb{m}
     \end{array} \right\\}, \quad \text{donde}\quad \alpha \ne 0. \qquad \text{Escribiremos} \quad\mathcal{S} \mapright{\alpha E\sb{i}} \mathcal{S}'.$$

3.  Sumar a la ecuación $j$ un múltiplo de la ecuación $i$. Esto es,
    $${\mathcal S}' \equiv \left\\{ \begin{array}{c} E\sb{1} \cr \vdots \cr E\sb{i} \cr \vdots \cr
      E\sb{j} + \alpha E\sb{i} \cr \vdots \cr E\sb{m}
     \end{array} \right\\}, \quad \text{donde}\quad \alpha \in \K. \qquad \text{Escribiremos}\quad \mathcal{S} \mapright{E\sb{j} + \alpha E\sb{i}} \mathcal{S}'.$$

{{% example name="Ejemplo" %}}
Sea el sistema $${\cal S}\equiv\left\\{\begin{array}{ccccccccccc}
        2x\sb{1}&-&x\sb{2}&-&2x\sb{3}&+&3x\sb{4}&-&x\sb{5}&=&1\cr
        x\sb{1}&+&2x\sb{2}&-&x\sb{3}&+&x\sb{4}&+&2x\sb{5}&=&0\cr
        3x\sb{1}&+&x\sb{2}&+&x\sb{3}&+&x\sb{4}&+&x\sb{5}&=&0\cr
        -x\sb{1}&+&x\sb{2}&-&x\sb{3}&+&x\sb{4}&-&x\sb{5}&=&2
       \end{array}\right. .$$ El intercambio de las ecuaciones (2) y (4)
produce el sistema $${\cal S}'\equiv\left\\{\begin{array}{ccccccccccc}
        2x\sb{1}&-&x\sb{2}&-&2x\sb{3}&+&3x\sb{4}&-&x\sb{5}&=&1\cr
        -x\sb{1}&+&x\sb{2}&-&x\sb{3}&+&x\sb{4}&-&x\sb{5}&=&2\cr
        3x\sb{1}&+&x\sb{2}&+&x\sb{3}&+&x\sb{4}&+&x\sb{5}&=&0\cr
        x\sb{1}&+&2x\sb{2}&-&x\sb{3}&+&x\sb{4}&+&2x\sb{5}&=&0
       \end{array}\right.$$ Hemos aplicado la transformación elemental
$\mathcal{S} \mapright{E\sb{24}} \mathcal{S}'$.
{{% /example %}}

{{% example name="Ejemplo" %}}
Con el mismo sistema $\mathcal S$ del ejemplo anterior, multiplicando la
cuarta ecuación por 2 obtenemos el sistema
$${\cal S}'\equiv\left\\{\begin{array}{ccccccccccc}
        2x\sb{1}&-&x\sb{2}&-&2x\sb{3}&+&3x\sb{4}&-&x\sb{5}&=&1\cr
        x\sb{1}&+&2x\sb{2}&-&x\sb{3}&+&x\sb{4}&+&2x\sb{5}&=&0\cr
        3x\sb{1}&+&x\sb{2}&+&x\sb{3}&+&x\sb{4}&+&x\sb{5}&=&0\cr
        -2x\sb{1}&+&2x\sb{2}&-&2x\sb{3}&+&2x\sb{4}&-&2x\sb{5}&=&4
       \end{array}\right.$$ En este caso hemos aplicado
$\mathcal{S} \mapright{2 E\sb{4}} \mathcal{S}'$.
{{% /example %}}

{{% example name="Ejemplo" %}}
Con el mismo sistema $\mathcal S$ de los dos ejemplos anteriores, si a
la tercera ecuación le sumamos el triple de la cuarta ecuación,
obtenemos $${\cal S}'\equiv\left\\{\begin{array}{ccccccccccc}
        2x\sb{1}&-&x\sb{2}&-&2x\sb{3}&+&3x\sb{4}&-&x\sb{5}&=&1\cr
        x\sb{1}&+&2x\sb{2}&-&x\sb{3}&+&x\sb{4}&+&2x\sb{5}&=&0\cr
        & &4x\sb{2}&-&2x\sb{3}&+&4x\sb{4}&-&2x\sb{5}&=&6\cr
        -x\sb{1}&+&x\sb{2}&-&x\sb{3}&+&x\sb{4}&-&x\sb{5}&=&2
       \end{array}\right.$$ La transformación elemental aplicada se
expresa como $\mathcal{S} \mapright{E\sb{3} + 3 E\sb{4}} \mathcal{S}'$.
{{% /example %}}

{{% theorem name="Sistemas equivalentes por transformaciones elementales" %}}
 Si el sistema
${\mathcal S}'$ se obtiene a partir del sistema ${\mathcal S}$ por una
concatenación de transformaciones elementales, entonces ${\mathcal S}'$
es un sistema equivalente a ${\mathcal S}$.
{{% /theorem %}}

{{% proof %}}
Supongamos primero que $\mathcal S'$ se obtiene a partir de $\mathcal S$
aplicando una sola transformación elemental.

Si $\mathcal S \mapright{E\sb{ij}} \mathcal S'$ (**transformación de tipo
1**), es evidente que cualquier solución de ${\cal S}$ satisface las
ecuaciones de ${\cal S}'$ y viceversa, porque las ecuaciones son las
mismas. Luego ambos sistemas son equivalentes.

Si $\mathcal S \mapright{\alpha E\sb{i}} \mathcal S'$ para un cierto
$\alpha\neq 0$, (**transformación de tipo 2**), cualquier solución de
$\mathcal S$ satisface $E\sb{i}$, y por tanto satisface $\alpha E\sb{i}$, luego
cumplirá todas las ecuaciones de $\mathcal S'$. Recı́procamente, como
$\alpha\neq 0$, tenemos
$\mathcal S' \mapright{\alpha^{-1} E\sb{i}} \mathcal S$, por lo que
podemos aplicar el mismo razonamiento y deducir que toda solución de
$\mathcal S'$ es solución de $\mathcal S$. Por tanto, ambos sistemas son
equivalentes.

Por último, si $\mathcal S \mapright{E\sb{j}+\alpha E\sb{i}} \mathcal S'$ para
un cierto $\alpha\in \K$, (**transformación de tipo 3**), observamos que
toda solución de $\mathcal S$ cumple $E\sb{j}$ y también $E\sb{i}$, por tanto
cumple $E\sb{j}$ y también $\alpha E\sb{i}$, luego cumple $E\sb{j}+\alpha E\sb{i}$. Esto
implica que toda solución de $\mathcal S$ es también solución de
$\mathcal S'$. El recíproco se deduce al observar que podemos pasar de
$\mathcal S'$ a $\mathcal S$ mediante la transformación elemental
$\mathcal S' \mapright{E\sb{j}-\alpha E\sb{i}} \mathcal S$, y por tanto podemos
aplicar el mismo razonamiento para deducir que toda solución de
$\mathcal S'$ es solución de $\mathcal S$.

Por tanto, hemos demostrado que si $\mathcal S'$ se obtiene a partir de
$\mathcal S$ al aplicar una sola transformación elemental, entonces
$\mathcal S$ y $\mathcal S'$ son sistemas equivalentes. Si ahora
consideramos un sistema $\mathcal S'$ que se obtiene a partir de
$\mathcal S$ mediante una sucesión de transformaciones elementales,
tendremos una sucesión de sistemas que comienza por $\mathcal S$ y
termina por $\mathcal S'$, donde cada uno es equivalente al siguiente.
Por tanto, $\mathcal S$ es equivalente a $\mathcal S'$.
{{% /proof %}}

El problema más común en la práctica es la resolución de un sistema con
$n$ ecuaciones y $n$ incógnitas, lo que se conoce como un **sistema
cuadrado**, que tenga solución única. En este caso, la eliminación
gaussiana es directa, y más tarde estudiaremos las diferentes
posibilidades. Lo que sigue es un ejemplo tı́pico.

{{% example name="Ejemplo" %}}
[]{#ap1:ej1 label="ap1:ej1"} Consideremos el sistema $$\label{eq:ejemp}
\begin{array}{rrrrrcr} 2x\sb{1} & + & x\sb{2} & + & x\sb{3} & = & 1, \cr 6x\sb{1} & + & 2x\sb{2}
& + & x\sb{3} & = & -1, \cr -2x\sb{1} & + & 2x\sb{2} & + & x\sb{3} & = & 7.
\end{array}$$ En cada paso, la estrategia es centrarse en una posición,
llamada *posición pivote*, y eliminar todos los términos con esa
incógnita en las siguientes ecuaciones usando las tres operaciones
elementales. Desde un punto de vista visual, hablaremos de los términos
*por debajo* de la posición pivote. El coeficiente en la posición pivote
se denomina *pivote*, y debe ser un número distinto de cero. Si un
coeficiente en una posición pivote es cero, entonces la ecuación
considerada se intercambia con una ecuación *posterior* para producir un
pivote no nulo. Esto siempre es posible para sistemas cuadrados con
solución única, como veremos más adelante.

**Paso 1.** Selecciona una posición pivote en la fila 1, consigue un
pivote no nulo en esa posición y anula los términos que estén debajo.

La primera posición pivote corresponde a la primera incógnita de la
primera ecuación. En nuestro ejemplo, el elemento $\fbox{2}$ del sistema
es el pivote del primer paso:
$$\begin{array}{rrrrrcr} \fbox{2}x\sb{1} & + & x\sb{2} & + & x\sb{3} & = & 1, \cr 6x\sb{1} & + & 2x\sb{2}
& + & x\sb{3} & = & -1, \cr -2x\sb{1} & + & 2x\sb{2} & + & x\sb{3} & = & 7.
\end{array}$$

A continuación eliminamos todos los términos por debajo del pivote.

-   Resta tres veces la primera ecuación de la segunda para generar el
    sistema equivalente
    $$\begin{array}{rrrrrcrc} \fbox{2}x\sb{1} & + & x\sb{2} & + & x\sb{3} & = & 1, \cr  & - & x\sb{2}
      & - & 2x\sb{3} & = & -4, & (E\sb{2} - 3E\sb{1}) \cr -2x\sb{1} & + & 2x\sb{2} & + & x\sb{3} & = & 7.
      \end{array}$$

-   Suma la primera ecuación a la tercera para formar el sistema
    equivalente
    $$\begin{array}{rrrrrcrc} \fbox{2}x\sb{1} & + & x\sb{2} & + & x\sb{3} & = & 1, \cr  & - & x\sb{2}
      & - & 2x\sb{3} & = & -4, \cr  & & 3x\sb{2} & + & 2x\sb{3} & = & 8 & (E\sb{3} + E\sb{1}).
      \end{array}$$

**Paso 2.** Selecciona una posición pivote en la fila 2, consigue un
pivote no nulo en esa posición y anula los términos que estén debajo.

La segunda posición pivote, en este tipo de sistemas, corresponde a la
segunda incógnita de la segunda ecuación. Si el coeficiente no es cero,
entonces es nuestro pivote. En otro caso, intercambiamos con una
ecuación que esté por *debajo* de esta posición para colocar el elemento
no nulo en la posición pivote.

-   En nuestro ejemplo, $-1$ es el segundo pivote:
    $$\begin{array}{rrrrrcr} 2x\sb{1} & + & x\sb{2} & + & x\sb{3} & = & 1, \cr  & &\fbox{(-1)} x\sb{2}
      & - & 2x\sb{3} & = & -4, \cr  & & 3x\sb{2} & + & 2x\sb{3} & = & 8.
      \end{array}$$

Ahora eliminamos todos los términos por debajo del pivote.

-   Suma tres veces la segunda ecuación a la tercera para llegar al
    sistema equivalente:
    $$\begin{array}{rrrrrcrc} 2x\sb{1} & + & x\sb{2} & + & x\sb{3} & = & 1, \cr  & & \fbox{(-1)} x\sb{2}
      & - & 2x\sb{3} & = & -4, \cr  & & & - & 4x\sb{3} & = & -4 & (E\sb{3} +3E\sb{2}).
      \end{array}$$

En general, si tenemos más de tres ecuaciones (en un sistema cuadrado
con solución única), las posiciones pivote son las que corresponden a la
incógnita $i$ de la ecuación $i$. Si el coeficiente correspondiente es
nulo, siempre se podrá intercambiar esta ecuación con otra posterior,
para obtener un pivote no nulo. A continuación podremos eliminar todos
los coeficientes por debajo de él.

Continuamos ası́ el proceso hasta llegar a la última ecuación. En este
ejemplo, el tercer pivote es $-4$, pero como ya no hay nada por debajo
que eliminar, paramos el proceso.

En este punto, decimos que hemos **triangulado** el sistema. Un sistema
de esta clase se resuelve muy fácilmente mediante el método de
**sustitución hacia atrás**, en el que la última ecuación se resuelve
para la última incógnita y se sustituye hacia atrás en la penúltima
ecuación, la cual se vuelve a resolver para la penúltima incógnita, y
continuamos ası́ hasta llegar a la primera ecuación. En nuestro ejemplo,
de la última ecuación obtenemos $$x\sb{3} = 1.$$ Sustituimos $x\sb{3}=1$ en la
segunda ecuación, y tenemos $$x\sb{2} = 4 - 2x\sb{3} = 4 - 2 (1) = 2.$$ Por
último, sustituimos $x\sb{3}=1$ y $x\sb{2}=2$ en la primera ecuación para
obtener $$x\sb{1} = \frac{1}{2}(1-x\sb{2}-x\sb{3}) = \frac{1}{2}(1-2-1) = -1,$$ que
completa la solución.
{{% /example %}}

Hemos visto cómo funciona la eliminación gaussiana, aplicando
transformaciones elementales a un sistema hasta convertirlo en otro
equivalente que sea sencillo de resolver. Pero lo hemos visto sólo en un
caso especial: los sistemas cuadrados con solución única. Además, no
hemos demostrado que este método funcione siempre, ni cuál es el
procedimiento si el sistema no es cuadrado, o si no tiene solución
única.

Para estudiar el caso general, vamos a simplificar la notación.
Observemos que, para definir un sistema de ecuaciones lineales, sólo
necesitamos saber el número de ecuaciones, el número de incógnitas, los
coeficientes que forman cada ecuación y los términos independientes.
Esta información se puede representar de forma concisa mediante una
**matriz**, es decir, una disposición rectangular de números
distribuidos en filas y columnas.

Consideremos un sistema de ecuaciones
$${\mathcal S} \equiv \left\\{\begin{array}{ccccccccc} a\sb{11} x\sb{1} & + &  a\sb{12} x\sb{2} & + & \ldots & + & a\sb{1n} x\sb{n} & = & b\sb{1} \cr
 a\sb{21} x\sb{1} & + & a\sb{22} x\sb{2} & + &  \ldots & + & a\sb{2n} x\sb{n} & = & b\sb{2} \cr & & & & \vdots \cr
 a\sb{m1} x\sb{1} & + & a\sb{m2} x\sb{2} & + & \ldots & + & a\sb{mn} x\sb{n} & = & b\sb{m}
 \end{array}\right.$$

{{% definition %}}
Matrices de un sistema de ecuaciones Llamamos **matriz de coeficientes**
del sistema lineal ${\mathcal S}$ a la matriz
$$A = \left( \begin{array}{cccc} a\sb{11} & a\sb{12} & \ldots & a\sb{1n} \cr a\sb{21} & a\sb{22} & \ldots & a\sb{2n} \cr \vdots & \vdots & \ddots &
\vdots \cr a\sb{m1} & a\sb{m2} & \ldots & a\sb{mn} \end{array} \right).$$ Si
añadimos a la matriz de coeficientes una columna que contenga a los
términos independientes, tenemos la **matriz ampliada** del sistema:
$$(A|\vec{b}) = \left( \begin{array}{cccc|c} a\sb{11} & a\sb{12} & \ldots & a\sb{1n} & b\sb{1} \cr a\sb{21} & a\sb{22} & \ldots & a\sb{2n} & b\sb{2} \cr \vdots & \vdots & \ddots &
\vdots \cr a\sb{m1} & a\sb{m2} & \ldots & a\sb{mn} & b\sb{m} \end{array} \right).$$
{{% /definition %}}

{{% example name="Ejemplo" %}}
En el sistema ([\[eq:ejemp\]](#eq:ejemp){reference-type="ref"
reference="eq:ejemp"}), la matriz de coeficientes es
$$A = \left( \begin{array}{rrr} 2 & 1 & 1 \cr 6 & 2 & 1 \cr -2 & 2 & 1 \end{array} \right)$$
y la matriz ampliada es
$$(A|\vec{b}) = \left( \begin{array}{rrr|r} 2 & 1 & 1 & 1 \cr 6 & 2 & 1 & -1 \cr -2 & 2 & 1 & 7 \end{array} \right).$$
{{% /example %}}

Además de representar los sistemas de ecuaciones lineales, las matrices
son objetos fundamentales del álgebra lineal. Por eso las estudiaremos
ahora concierto detalle, y más adelante volveremos a la resolución de
sistemas de ecuaciones lineales.

## Matrices: suma y trasposición

Dado un cuerpo $\K$ (que por ahora podemos suponer que es $\Q$, $\R$ o
$\C$), denotaremos por $\K^n$ al conjunto de $n$-uplas de elementos de
$\K$, es decir, de listas ordenadas de $n$ elementos de $\K$. Una
$n$-upla se suele representar de dos posibles maneras, como un *vector
fila* (a veces con comas entre sus componentes), o como un *vector
columna*:
$$\mathbf v=\begin{pmatrix} \alpha\sb{1} & \alpha\sb{2} & \cdots & \alpha\sb{n} \end{pmatrix}, \qquad  \mathbf v=\begin{pmatrix} \alpha\sb{1} \cr \alpha\sb{2} \cr \vdots \cr \alpha\sb{n} \end{pmatrix}.$$

Dados números naturales $m,n\geq 1$, una *matriz* de orden $m\times n$
con entradas (o componentes) en $\K$ es una disposición rectangular de
$mn$ elementos de $\K$ formando $m$ filas y $n$ columnas:
$$A = \left( \begin{array}{cccc} a\sb{11} & a\sb{12} & \ldots & a\sb{1n}
\cr a\sb{21} & a\sb{22} & \ldots & a\sb{2n} \cr \vdots & \vdots & \ddots &
\vdots \cr a\sb{m1} & a\sb{m2} & \ldots & a\sb{mn} \end{array} \right).$$

Normalmente denotamos una matriz utilizando una letra mayúscula, y
expresamos sus elementos con la misma letra en minúscula, y con dos
subı́ndices. El primer subı́ndice de un elemento de la matriz indica la
*fila*, y el segundo subı́ndice denota la *columna* donde se encuentra.
Por ejemplo, si $$\label{ap1:eq2}
A = \left( \begin{array}{rrrr} 2 & 1 & 3 & 4 \cr 8 & 6 & 5 & -9 \cr
-3 & 8 & 3 & 7 \end{array} \right), \quad \text{entonces} \quad a\sb{11} = 2,
a\sb{12} = 1, \ldots, a\sb{34} = 7.$$ En ocasiones escribiremos $A=(a\sb{ij})$
para indicar que $A$ es una matriz cuyas componentes serán denotadas
$a\sb{ij}$, con $1\leq i\leq m$ y $1\leq j \leq n$. En ocasiones, por
conveniencia, al elemento $a\sb{ij}$ lo denotaremos $[A]\sb{ij}$ (esto suele
suceder cuando el nombre de la matriz no es simplemente una letra
mayúscula, como veremos muy pronto al definir la suma de matrices).

Al conjunto formado por todas las matrices de orden $m\times n$ con
entradas en $\K$ lo denotaremos $\MatK{m}{n}{\K}$.

Una *submatriz* de una matriz dada $A$ es una matriz formada por las
componentes de $A$ que están en un conjunto seleccionado de filas y
columnas. Por ejemplo,
$$B = \left( \begin{array}{rr} 2 & 4 \cr -3 & 7 \end{array} \right)$$ es
una submatriz de $A$ formada por las componentes que se encuentran en
las filas 1 y 3, y en las columnas 1 y 4 de la matriz $A$.

Una matriz $A$ se dice que tiene **orden** $m \times n$ si $A$ tiene
exactamente $m$ filas y $n$ columnas. La matriz $A$ de
([\[ap1:eq2\]](#ap1:eq2){reference-type="ref" reference="ap1:eq2"}) es
una matriz $3 \times 4$. Por convenio, las matrices $1 \times 1$ se
identifican con escalares.

Para enfatizar que una matriz $A$ es de orden $m \times n$, usaremos la
notación $A\sb{m \times n}$. Cuando $m = n$, es decir, cuando el número de
filas y columnas coincide, diremos que la matriz es **cuadrada**. Para
hablar de matrices en general, que no sean necesariamente cuadradas,
usaremos la expresión matriz **rectangular**.

A las matrices que tienen una sola fila o una sola columna las
llamaremos, respectivamente, **vectores fila** o **vectores columna**.
Los notaremos con las letras $\vec{u}, \vec{v}, \vec{w},\ldots$,
generalmente en negrita, y a veces sus componentes las escribiremos con
un único subíndice.

Escribiremos $A\sb{i\ast}$ para denotar la fila $i$ de la matriz $A$, y
escribiremos $A\sb{\ast j}$ para denotar la columna $j$. Por ejemplo, si $A$
es la matriz de ([\[ap1:eq2\]](#ap1:eq2){reference-type="ref"
reference="ap1:eq2"}), entonces
$$A\sb{2\ast} = \left( \begin{array}{rrrr} 8 & 6 & 5 & -9 \end{array}
\right) \quad \mbox{y}\quad A\sb{\ast 2} = \left( \begin{array}{r} 1 \cr 6 \cr 8
\end{array} \right).$$ La matriz de orden $m \times n$ con todas sus
entradas nulas (es decir, iguales a cero) se denomina **matriz nula** y
la notaremos como $ O \sb{m \times n}$. Cuando el contexto lo permita,
eliminaremos el orden para aliviar la notación.

Haremos uso de dos operaciones que generalizaremos más adelante al
hablar de espacios vectoriales. Dados dos vectores columna
$\vec{u}, \vec{v}$ del mismo orden, consideramos su suma
$\vec{w} = \vec{u} + \vec{v}$ como el vector columna del mismo orden y
cuyas componentes $w\sb{i}$ son las sumas de las componentes respectivas
$u\sb{i} + v\sb{i}$. De igual forma, definimos $\vec{w} = \alpha \vec{u}$ como el
vector columna del mismo orden que $\vec{u}$ y con componentes
$w\sb{i} = \alpha u\sb{i}$. Podemos hacer algo análogo con las filas.

Dados $\vec{u}\sb{1}, \vec{u}\sb{2}, \ldots, \vec{u}\sb{s}$ vectores del mismo orden,
llamamos **combinación lineal** de estos vectores a una expresión de la
forma
$$\alpha\sb{1} \vec{u}\sb{1} + \alpha\sb{2} \vec{u}\sb{2} + \cdots + \alpha\sb{s} \vec{u}\sb{s},$$
donde $\alpha\sb{1},\ldots,\alpha\sb{s}$ son escalares (elementos de $\K$). El
resultado de esta combinacuón lineal (obtenido al realizar las
operaciones) vuelve a ser un vector del mismo orden que
$\vec u\sb{1},\vec u\sb{2},\ldots, \vec u\sb{s}$.

Dos matrices $A = (a\sb{ij})$ y $B = (b\sb{ij})$ son **iguales** cuando $A$
y $B$ tienen el mismo orden y las entradas correspondientes son iguales.
Consideremos el siguiente caso:
$$\vec{u} = \left( \begin{array}{r} 1 \cr 2 \cr 3 \end{array} \right) \quad \mbox{y} \quad  \vec{v} = \left( \begin{array}{rrr} 1 & 2 & 3 \end{array} \right).$$
Aunque podamos pensar que $\vec{u}$ y $\vec{v}$ describen el mismo punto
en $\R^3$, no podemos decir que sean iguales como matrices, pues sus
formas son diferentes.

{{% definition %}}
Suma de matrices

Si $A$ y $B$ son matrices **del mismo orden** $m \times n$, se definen
las siguientes matrices, todas de orden $m\times n$:

La matriz $A+B$, llamada **suma** de $A$ y $B$, se define como
$$[A+B]\sb{ij} = a\sb{ij} + b\sb{ij}.$$ La matriz $-A$, llamada **opuesta** de
$A$, se define como $$[-A]\sb{ij} = (-a\sb{ij}).$$ La matriz $A-B$, llamada
**diferencia** de $A$ y $B$ se define como
$$[A-B]\sb{ij} = a\sb{ij}-b\sb{ij}.$$
{{% /definition %}}

{{% example name="Ejemplo" %}}
Sean
$$A = \left( \begin{array}{rrrr} 2&-3&4&0\cr 1&-2&1&1\cr 0&0&0&0 \end{array} \right),\qquad B =  \left( \begin{array}{rrrr} -4&-2&0&3\cr -3&1&-1&-2
\cr -1&4&2&-1 \end{array} \right).$$ Entonces $$\begin{gathered}
  A + B =  \left( \begin{array}{cccc} -2&-5&4&3 \cr -2&-1&0&-1 \cr -1&4&2&-1 \end{array} \right),\qquad
   -A =  \left( \begin{array}{cccc} -2&3&-4&0\cr{}-1&2&-1&-1
\cr{}0&0&0&0\end{array} \right),  \cr
A-B = \left( \begin{array}{cccc} 6&-1&4&-3\cr{}4&-3&2&3
\cr{}1&-4&-2&1\end{array} \right).
\end{gathered}$$ Si
$$C =  \left( \begin{array}{ccccc} -1&-3&-2&-2&1\cr{}-3&-3&0
&1&-3\cr{}1&-2&3&2&1\end{array} \right),$$ no podemos
calcular $A + C$, pues $A$ es de orden $3 \times 4$ y $C$ es de orden
$3 \times 5$.
{{% /example %}}

{{% theorem name="Propiedades de la suma de matrices" %}}
Sean $A, B$ y $C$ matrices de orden $m \times n$. Se verifican las
siguientes propiedades:

-   $A-B=A+(-B)$.

-   $(A+B)+C = A+(B+C)$.

-   $A+B = B+A$.

-   $A +  O  =   O  + A = A$.

-   $A+(-A) = (-A)+A =  O $.
{{% /theorem %}}

{{% proof %}}
Estas propiedades se obtienen gracias a que las propiedades análogas son
ciertas para los escalares.

-   $[A-B]\sb{ij}=a\sb{ij}-b\sb{ij}=a\sb{ij}+(-b\sb{ij})= [A]\sb{ij}+[-B]\sb{ij}= [A+(-B)]\sb{ij}$.
    Por tanto, $A-B$ y $A+(-B)$ son matrices del mismo orden y con las
    mismas componentes en cada posición, luego son iguales. Lo mismo
    sucederá en los restantes casos.

-   $[(A+B) + C]\sb{ij} = [A+B]\sb{ij}+C\sb{ij}=(a\sb{ij} + b\sb{ij}) + c\sb{ij} = a\sb{ij} + (b\sb{ij} + c\sb{ij}) = [A]\sb{ij}+[B+C]\sb{ij} = [A+(B+C)]\sb{ij}$.

-   $[A+B]\sb{ij}=a\sb{ij} + b\sb{ij} = b\sb{ij} + a\sb{ij}=[B+A]\sb{ij}$.

-   $[A+ O ]\sb{ij}=a\sb{ij}+0 = a\sb{ij} = [A]\sb{ij}$. Análogo con
    $ O +A$.

-   $[A + (-A)]\sb{ij}=a\sb{ij} + (-a\sb{ij}) = 0 = [ O ]\sb{ij}$. Análogo
    con $(-A)+A$.
{{% /proof %}}

{{% definition %}}
Multiplicación por un escalar Dado un escalar $\alpha\in K$ y una matriz
$A$ de orden $m \times n$, se defie el producto $\alpha A$ como la
matriz de orden $m \times n$ que verifica
$$[\alpha A]\sb{ij} = \alpha a\sb{ij}.$$ Análogamente se define
$A\alpha=(a\sb{ij}\alpha)=\alpha A$.
{{% /definition %}}

{{% example name="Ejemplo" %}}
Si
$$A =  \left( \begin{array}{cccc} 2&-3&4&0\cr{}1&-2&1&1
\cr{}0&0&0&0\end{array} \right),$$ entonces
$$(-3) \cdot A =  \left( \begin{array}{cccc} -6&9&-12&0\cr{}-3&6&-3&-3
\cr{}0&0&0&0\end{array} \right), \qquad 0 \cdot A  =\left( \begin{array}{cccc} 0&0&0&0\cr{}0&0&0&0
\cr{}0&0&0&0\end{array} \right).$$
{{% /example %}}

{{% theorem name="Propiedades de la multiplicación por un escalar" %}}
Sean $A, B$ matrices de orden $m \times n$, y $\alpha, \beta\in \K$
escalares. Se verifican las siguientes propiedades:

-   $(\alpha \beta) A = \alpha (\beta A)$.

-   $\alpha(A+B) = \alpha A + \alpha B$.

-   $(\alpha + \beta) A = \alpha A + \beta A$.

-   $1 \cdot A = A, \qquad 0 \cdot A =  O $.
{{% /theorem %}}

{{% proof %}}
La demostración de estas propiedades se basa en que las mismas se
verifican para el producto de escalares del cuerpo $\K$.

Todas las matrices que consideramos son de orden $m\times n$, por tanto
sólo tenemos que comprobar que sus entradas son iguales.

-   $[(\alpha \beta) A]\sb{ij}=(\alpha \beta) a\sb{ij} = \alpha (\beta a\sb{ij})= \alpha [\beta A]\sb{ij} = [\alpha(\beta A)]\sb{ij}$.

-   $[\alpha(A+B)]\sb{ij}=\alpha[A+B]\sb{ij}=\alpha (a\sb{ij} + b\sb{ij}) = \alpha a\sb{ij} + \alpha b\sb{ij}=[\alpha A]\sb{ij}+[\alpha B]\sb{ij}=[\alpha A+\alpha B]\sb{ij}$.

-   $[(\alpha + \beta) A]\sb{ij}=(\alpha + \beta) a\sb{ij}= \alpha a\sb{ij} + \beta a\sb{ij}=[\alpha A]\sb{ij}+[\beta A]\sb{ij}=[\alpha A+\beta A]\sb{ij}$.

-   $[1 \cdot A]\sb{ij}=1 \cdot a\sb{ij} = a\sb{ij}=[A]\sb{ij}$. Por otro lado,
    $[0 \cdot A]\sb{ij}=0 \cdot a\sb{ij} = 0 = [ O ]\sb{ij}$.
{{% /proof %}}

{{% definition %}}
Trasposición Dada una matriz $A$ de orden $m\times n$, se define $A^t$,
la **traspuesta** de $A$, como la matriz de orden $n\times m$ que
verifica $$[A^t]\sb{ij} = a\sb{ji}.$$
{{% /definition %}}

Observemos que las filas de $A$ corresponden a las columnas de $A^t$, y
viceversa.

{{% example name="Ejemplo" %}}
$$\text{Si}\quad A =  \left( \begin{array}{rrrr} 2&-3&4&0\cr{}1&-2&1&1
\cr{}0&0&0&0\end{array} \right), \qquad \text{entonces}\qquad
  A^t =  \left( \begin{array}{rrr} 2&1&0\cr{}-3&-2&0
\cr{}4&1&0\cr{}0&1&0\end{array}
 \right).$$
{{% /example %}}

En el caso en que el cuerpo de escalares sea $\mathbb C$, existe una
operación importante llamada **conjugación** de números complejos.
Sabemos que todo número complejo $z\in \mathbb C$ se escribe de forma
única como $z=a+bi$, donde $a,b\in \R$ y donde $i\in\C$ es tal que
$i^2=-1$. El **conjugado** de $z$ es el número complejo:
$$\overline{z}=a-bi.$$ Es fácil comprobar que
$\overline{z\sb{1}+z\sb{2}}=\overline{z\sb{1}}+\overline{z\sb{2}}$ y que
$\overline{z\sb{1}\cdot z\sb{2}}=\overline{z\sb{1}}\cdot \overline{z\sb{2}}$, para
cualesquiera $z\sb{1},z\sb{2}\in \C$. Observemos que si $z$ es un número real (o
en particular si es racional), entonces $\overline{z}=z$.

La siguiente definición tiene sentido si el cuerpo $\K$ de escalares es
$\C$, $\R$ o $\Q$, y tiene interés únicamente si $\K=\C$.

{{% definition %}}
Conjugada y traspuesta conjugada

Sea $A\in \MatK{m}{n}{\C}$.

La matriz $\overline{A}$, **conjugada** de $A$, es la matriz de orden
$m \times n$ que verifica
$$\left[\overline{A}\right]\sb{ij} = \overline{a\sb{ij}}.$$

La matriz $A^{\ast}$, **traspuesta conjugada** de $A$, es la matriz de orden
$n \times m$ que verifica $$\left[A^{\ast}\right]\sb{ij} = \overline{a\sb{ji}}.$$
{{% /definition %}}

Observemos que $A^{\ast}=\overline{A^t} = \left(\overline A\right)^t$, de ahı́
su nombre de *traspuesta conjugada*, o también *conjugada traspuesta*.
Observemos también que, si $\K=\R$ o $\K=\Q$, estas definiciones tienen
sentido, pero tendrı́amos $\overline{A}=A$ y $A^{\ast}=A^t$.

{{% example name="Ejemplo" %}}
$$\text{Si}\quad \vec{u} = \left( \begin{array}{c} 1 \cr 1-i \cr i \cr 2 \end{array} \right), \quad \text{entonces}\quad  \overline{\vec{u}} = \left( \begin{array}{c} 1 \cr 1+i \cr -i \cr 2 \end{array} \right),
  \quad
  \vec{u}^{\ast} = \left( \begin{array}{rrrr} 1 & 1+i & -i & 2 \end{array} \right).$$
{{% /example %}}

{{% theorem name="Propiedades de la matriz traspuesta" %}}
Sean $A$ y $B$ matrices de orden $m\times n$, y sea $\alpha\in \K$ un
escalar. Se tiene:

-   $(A^t)^t = A$.

-   $(A+B)^t = A^t + B^t$.

-   $(\alpha A)^t = \alpha A^t$.
{{% /theorem %}}

{{% proof %}}
Las matrices consideradas tienen todas orden $n\times m$, por tanto sólo
tenemos que comprobar que sus entradas son iguales.

-   $[(A^t)^t]\sb{ij}=[A^t]\sb{ji}=[A]\sb{ij}$.

-   $[(A+B)^t]\sb{ij}=[A+B]\sb{ji}=a\sb{ji}+b\sb{ji}=[A^t]\sb{ij}+[B^t]\sb{ij}=[A^t+B^t]\sb{ij}$.

-   $[(\alpha A)^t]\sb{ij}=[\alpha A]\sb{ji}=\alpha a\sb{ji}=\alpha [A^t]\sb{ij}=[\alpha A^t]\sb{ij}$.
{{% /proof %}}

{{% theorem name="Propiedades de la matriz traspuesta conjugada" %}}
Sean $A,B\in \MatK{m}{n}{\C}$. Se tiene:

-   $(A^{\ast})^{\ast} = A$.

-   $(A+B)^{\ast} = A^{\ast} + B^{\ast}$.

-   $(\alpha A)^{\ast} = \overline{\alpha} A^{\ast}$.
{{% /theorem %}}

{{% proof %}}
La demostración es análoga a la del resultado anterior, utilizando que
la conjugación de números complejos preserva la suma y el producto.
Observemos que, en el último caso, el escalar $\alpha$ queda conjugado.
{{% /proof %}}

{{% definition %}}
Simetrı́as

Sea $A = (a\sb{ij})$ una matriz cuadrada.

-   Decimos que $A$ es **simétrica** si $A = A^t$, esto es,
    $a\sb{ij} = a\sb{ji}$.

-   Decimos que $A$ es **antisimétrica** si $A = -A^t$, esto es,
    $a\sb{ij} = -a\sb{ji}$.

Supongamos ahora que $A\in \MatK{n}{n}{\C}$.

-   Decimos que $A$ es **hermitiana** o **hermı́tica** si $A = A^{\ast}$, esto
    es, $a\sb{ij} = \overline{a\sb{ji}}$.

-   Decimos que $A$ es **antihermitiana** o **antihermı́tica** si
    $A = -A^{\ast}$, esto es, $a\sb{ij} = -\overline{a\sb{ji}}$.
{{% /definition %}}

{{% example name="Ejemplo" %}}
Sean $$A =  \left( \begin{array}{ccc} 1&0&5\cr{}0&-3&2
\cr{}5&2&-3\end{array} \right), \qquad B =  \left( \begin{array}{ccc} 1&1&5\cr{}0&-3&2
\cr{}5&2&-3\end{array} \right), \qquad C = \left( \begin{array}{cc} 1 & 1+i \cr 1-i & 3 \end{array} \right).$$
Entonces $A$ es simétrica, $B$ no es simétrica y $C$ es hermitiana.
{{% /example %}}

Un número importante asociado a una matriz cuadrada es la **traza**.

{{% definition %}}
Traza

Sea $A$ una matriz cuadrada de orden $n$ (es decir, de orden
$n\times n$).

La **diagonal principal** de $A$ es el conjunto de las posiciones
$(1,1), (2,2), \ldots, (n,n)$ de la matriz $A$.

La **traza** de $A$ es la suma de los elementos de su diagonal
principal:
$$\traza(A)=a\sb{11}+a\sb{22}+\cdots+a\sb{nn}=\sum\sb{i=1}^{n}{a\sb{ii}}.$$
{{% /definition %}}

{{% example name="Ejemplo" %}}
$$\text{Si} \quad A =  \left( \begin{array}{ccc} 1&1&5\cr{}1&-3&2
\cr{}5&2&-3\end{array} \right), \quad \text{entonces} \quad \traza(A) = 1 + (-3) + (-3) = -5.$$
{{% /example %}}

## Multiplicación de matrices

La multiplicación de matrices no está definida para dos matrices
cualesquiera; exigiremos que se satisfaga alguna condición entre las
matrices a multiplicar.

{{% definition %}}
Matrices ajustadas para la multiplicación Dos matrices $A$ y $B$ se
dicen **ajustadas para multiplicación en el orden** $AB$ cuando el
número de columnas de $A$ es igual al número de filas de $B$, esto es,
si $A$ es de orden $m \times p$ y $B$ es de orden $p \times n$.
{{% /definition %}}

Obsérvese que puede ocurrir que dos matrices $A$ y $B$ estén ajustadas
para la multiplicación en el orden $AB$ pero no en el orden $BA$.

{{% example name="Ejemplo" %}}
Las matrices $$A = \left( \begin{array}{rr} 1 & 2\cr
                              3 & 4 \end{array} \right),\quad
                              B = \left(
 \begin{array}{r} 5 \cr 6 \end{array} \right),$$ están ajustadas para la
multiplicación en el orden $AB$ pero no en el orden $BA$.
{{% /example %}}

{{% definition %}}
Vector fila por vector columna Sean
$$\vec{u}=\begin{pmatrix}u\sb{1} & u\sb{2} & \cdots & u\sb{n} \end{pmatrix},\qquad \vec{v}=\begin{pmatrix}v\sb{1} \cr v\sb{2} \cr \vdots \cr v\sb{n} \end{pmatrix}$$
un vector fila de orden $1\times n$ y un vector columna de orden
$n\times 1$. Se define el **producto** de $\vec{u}$ y $\vec{v}$ como el
escalar
$$\vec{u} \vec{v} = u\sb{1}v\sb{1}+u\sb{2}v\sb{2}+\cdots +u\sb{n}v\sb{n} = \sum\sb{i=1}^{n}{u\sb{i}v\sb{i}}.$$
{{% /definition %}}

Siendo precisos, el producto $\vec{u}\vec{v}$ es una matriz $1\times 1$
pero, como dijimos anteriormente, identificaremos las matrices
$1\times 1$ con los escalares.

Este producto de vectores fila por vectores columna se generaliza a
pares de matrices ajustadas para la multiplicación.

{{% definition %}}
Multiplicación de matrices Sean $A\sb{m\times p}$ y $B\sb{p\times n}$ dos
matrices ajustadas para multiplicación en el orden $AB$. La **matriz
producto** $AB$ es la matriz de orden $m\times n$ que verifica:
$$[AB]\sb{ij}=A\sb{i\ast} B\sb{\ast j}.$$
{{% /definition %}}

Según la definición, la componente $(i,j)$ de la matriz $AB$ es el
producto de la fila $i$ de $A$ por la columna $j$ de $B$. Esto se puede
escribir utilizando las entradas de la matriz, como sigue:
$$[AB]\sb{ij}=a\sb{i1}b\sb{ij}+a\sb{i2}b\sb{2j}+\cdots+a\sb{ip}b\sb{pj}.$$ Más
resumidamente, en forma de sumatorio:
$$[AB]\sb{ij}=\sum\sb{k=1}^{p}{a\sb{ik}b\sb{kj}}.$$

{{% example name="Ejemplo" %}}
Dadas $$A = \left( \begin{array}{rr} 1 & 2\cr
                              3 & 4 \end{array} \right),\quad
                              B = \left(
 \begin{array}{r} 5 \cr 6 \end{array} \right),$$ se tiene
$$AB=\left(\begin{array}{cc} 1 & 2 \cr 3 & 4 \end{array}\right)\left(\begin{array}{c} 5 \cr 6 \end{array}\right) =
\left(\begin{array}{c} 17 \cr 39 \end{array}\right)$$
{{% /example %}}

El ejemplo anterior nos muestra que existe el producto $AB$, pero no
$BA$. Aunque existan ambos productos, estos no tienen por qué ser
iguales y ni siquiera tienen que ser del mismo orden.

Considere lo que ocurre al tomar
$$A = \left( \begin{array}{rr} 1 & 2 \end{array} \right),\qquad  B = \left( \begin{array}{r} 3 \cr 4 \end{array} \right),$$
y calcular $AB$ y $BA$. Pero aunque fueran matrices cuadradas el
producto no es conmutativo en general. Por ejemplo, sean las matrices
$$C=\left(\begin{array}{cc}
            2 & 1\cr
            0 & -1
           \end{array}\right),\qquad
   D=\left(\begin{array}{cc}
            1 & 1\cr
            2 & 3
           \end{array}\right) .$$ Entonces
$$CD =  \left( \begin{array}{cc} 4&5\cr{}-2&-3\end{array}
 \right),\qquad  DC =  \left( \begin{array}{cc} 2&0\cr{}4&-1\end{array}
 \right).$$

Las siguientes propiedades son inmediatas a partir de la definición de
producto de matrices:

{{% theorem name="Filas y columnas de un producto" %}}
Supongamos que $A$ y $B$ son matrices ajustadas para multiplicación en
el orden $AB$. Se tiene:

-   $[AB]\sb{i\ast} = A\sb{i\ast} B$; esto es, la fila $i$ de $AB$ es la fila $i$
    de $A$ multiplicada por $B$.

-   $[AB]\sb{\ast j} = A B\sb{\ast j}$; esto es, la columna $j$ de $AB$ es $A$
    multiplicada por la columna $j$ de $B$.
{{% /theorem %}}

Estas igualdades se pueden escribir de manera más visual como
$$\begin{gathered}
 AB=\left( \begin{array}{c} A\sb{1\ast} \cr A\sb{2\ast} \cr \vdots \cr A\sb{m\ast} \end{array} \right) B = \left( \begin{array}{c} A\sb{1\ast} B \cr A\sb{2\ast} B \cr \vdots \cr A\sb{m\ast}B \end{array} \right), \cr
 AB= A \left( \begin{array}{cccc} B\sb{\ast 1} & B\sb{\ast 2} & \cdots & B\sb{\ast n} \end{array} \right) = \left( \begin{array}{cccc} A B\sb{\ast 1} & A B\sb{\ast 2} & \cdots & A B\sb{\ast n} \end{array} \right).
\end{gathered}$$

Otra propiedad del producto de matrices que será muy importante en
nuestro estudio de los sistemas de ecuaciones lineales es la siguiente:

{{% theorem name="Matriz por vector columna" %}}
 Si $A\sb{m\times n}$ es una matriz y
$\vec{v}\sb{n\times 1}$ es un vector columna, el producto $A\vec{v}$ es una
**combinación lineal de las columnas** de $A$, donde los coeficientes
son las componentes de $\vec v$.
$$A\vec{v} = A \begin{pmatrix}v\sb{1} \cr \vdots \cr v\sb{n} \end{pmatrix} = A\sb{\ast 1}v\sb{1}+A\sb{\ast 2}v\sb{2}+\cdots+A\sb{\ast n}v\sb{n}.$$
{{% /theorem %}}

{{% proof %}}
Sabemos que $A\vec v$ es un vector columna $m\times 1$, y que el
resultado de la combinación lineal
$A\sb{\ast 1}v\sb{1}+A\sb{\ast 2}v\sb{2}+\cdots+A\sb{\ast n}v\sb{n}$ también es un vector columna
$m\times 1$. Por tanto, sólo tenemos que comprobar que sus componentes
coinciden. Observemos que sólo hay posiciones de la forma $(i,1)$. Se
tiene: $$\begin{aligned}
_{i1} & =  a\sb{i1}v\sb{1}+a\sb{i2}v\sb{2}+\cdots+a\sb{in}v\sb{n}
  \cr & =  [A\sb{\ast 1}]\sb{i1}v\sb{1}+[A\sb{\ast 2}]\sb{i1}v\sb{2}+\cdots+[A\sb{\ast n}]\sb{i1}v\sb{n}
  \cr & =  [A\sb{\ast 1}v\sb{1}+A\sb{\ast 2}v\sb{2}+\cdots+A\sb{\ast n}v\sb{n}]\sb{i1}.
\end{aligned}$$
{{% /proof %}}

Del mismo modo se puede demostrar que si $\vec{u}\sb{1\times m}$ es un
vector fila y $A\sb{m\times n}$ es una matriz, entonces $\vec{u}A$ es una
**combinación lineal de las filas** de $A$, donde los coeficientes son
las componentes de $\vec{u}$.

Uniendo los dos últimos resultados, se tiene:

{{% theorem name="Filas y columnas de un producto" %}}
Supongamos que $A\sb{m\times p}=(a\sb{ij})$ y $B\sb{p\times n}=(b\sb{ij})$.

-   Las filas de $AB$ son combinaciones lineales de las filas de $B$.
    $$[AB]\sb{i\ast}=a\sb{i1}B\sb{1\ast}+a\sb{i2}B\sb{2\ast}+\cdots+a\sb{ip}B\sb{p\ast}.$$

-   Las coumnas de $AB$ son combinaciones lineales de las columnas de
    $A$. $$[AB]\sb{\ast j}=A\sb{\ast 1}b\sb{1j}+A\sb{\ast 2}b\sb{2j}+\cdots+A\sb{\ast p}b\sb{pj}.$$
{{% /theorem %}}

{{% proof %}}
Sabemos que $[AB]\sb{i\ast}= A\sb{i\ast}B$, y por tanto este producto es una
combinación lineal de las filas de $B$, cuyos coeficientes son las
componentes del vector fila $A\sb{i\ast}$. Esto es precisamente lo que dice
el enunciado.

Por otro lado, sabemos que $[AB]\sb{\ast j}=AB\sb{\ast j}$, y por tanto este
producto es una combinación lineal de las columnas de $A$, cuyos
coeficientes son las componentes del vector columna $B\sb{\ast j}$, como
querı́amos demostrar.
{{% /proof %}}

Otra forma muy usada para el producto de matrices es $$\begin{aligned}
  AB & = \left( \begin{array}{cccc} A\sb{\ast 1} & A\sb{\ast 2} & \ldots & A\sb{\ast p} \end{array} \right) \left( \begin{array}{c} B\sb{1\ast} \cr B\sb{2\ast} \cr \vdots \cr B\sb{p\ast} \end{array} \right) \cr
  & = A\sb{\ast 1} B\sb{1\ast} + A\sb{\ast 2} B\sb{2\ast} + \cdots + A\sb{\ast p} B\sb{p\ast}.
\end{aligned}$$ Observemos que la última expresión es una suma de
matrices de orden $m \times n$, cada una con todas sus filas (y sus
columnas) proporcionales. Se denomina **expansión mediante el producto
exterior**.

{{% remark %}}
El producto de matrices nos va a permitir expresar cualquier sistema de
ecuaciones lineales como una igualdad de matrices. Todo sistema de $m$
ecuaciones y $n$ incógnitas
$$\begin{array}{ccccccccc} a\sb{11} x\sb{1} & + & a\sb{12} x\sb{2} & + & \cdots & + & a\sb{1n} x\sb{n} & = & b\sb{1},
\cr a\sb{21} x\sb{1} & + & a\sb{22} x\sb{2} & + & \cdots & + & a\sb{2n} x\sb{n} & = & b\sb{2}, \cr & & & & \vdots \cr
a\sb{m1} x\sb{1} & + & a\sb{m2} x\sb{2} & + & \cdots & + & a\sb{mn} x\sb{n} & = & b\sb{m},
\end{array}$$ se puede escribir en forma matricial como
$A \vec{x} = \vec{b}$, donde
$$A = \left( \begin{array}{cccc} a\sb{11} & a\sb{12} & \ldots & a\sb{1n} \cr a\sb{21} & a\sb{22} & \ldots & a\sb{2n} \cr \vdots & \vdots & \ddots &
\vdots \cr a\sb{m1} & a\sb{m2} & \ldots & a\sb{mn} \end{array} \right), \qquad \vec{x} = \left( \begin{array}{c} x\sb{1} \cr x\sb{2} \cr \vdots \cr x\sb{n}
\end{array} \right), \qquad \vec{b} = \left( \begin{array}{c} b\sb{1} \cr b\sb{2} \cr \vdots \cr b\sb{m} \end{array} \right).$$
Por otro lado, toda ecuación matricial
$A\sb{m\times n} \vec{x}\sb{n \times 1} = \vec{b}\sb{m \times 1}$ representa un
sistema lineal de $m$ ecuaciones y $n$ incógnitas.

Si el sistema tiene una solución $\vec{u} = (u\sb{i})$, entonces podemos
escribir $$A\sb{\ast 1}u\sb{1} +  A\sb{\ast 2}u\sb{2} + \cdots +  A\sb{\ast n}u\sb{n} = \vec{b},$$ es
decir, el vector $\vec{b}$ se expresa como combinación lineal de las
columnas de $A$.

Por tanto, resolver un sistema de ecuaciones lineales es equivalente a
encontrar una combinación lineal de las columnas de la matriz de
coeficientes que dé como resultado el vector de los términos
independientes.
{{% /remark %}}

{{% example name="Ejemplo" %}}
El sistema de ecuaciones
$$\left\\{ \begin{array}{rrrrcr} & & x\sb{3} & + x\sb{4} & = & 1, \cr -2x\sb{1} & -4x\sb{2} & + x\sb{3} & & = & -1, \cr 3x\sb{1} & + 6x\sb{2} & -x\sb{3} & + x\sb{4} & = & 2. \end{array} \right.$$
se escribe como $A \vec{x} = \vec{b}$, donde
$$A = \left( \begin{array}{rrrr} 0 & 0 & 1 & 1 \cr -2 & -4 & 1 & 0 \cr 3 & 6 & -1 & 1 \end{array} \right),\qquad \vec{x} = \left( \begin{array}{c} x\sb{1} \cr x\sb{2} \cr x\sb{3} \cr x\sb{4} \end{array} \right),\qquad \vec{b} = \left( \begin{array}{r} 1 \cr -1 \cr 2 \end{array} \right).$$
Una solución del sistema es $\vec{u} = (-1,1,1,0)^t$, porque
$$\left( \begin{array}{r} 0 \cr -2 \cr 3 \end{array} \right)(-1) +
\left( \begin{array}{r} 0 \cr -4 \cr 6 \end{array} \right) 1 +
\left( \begin{array}{r} 1 \cr 1 \cr -1 \end{array} \right) 1 +
\left( \begin{array}{r} 1 \cr 0 \cr 1 \end{array} \right) 0 =
\left( \begin{array}{r} 1 \cr -1 \cr 2 \end{array} \right).$$
{{% /example %}}

## Propiedades de la multiplicación matricial

{{% theorem name="Propiedades distributiva y asociativa" %}}
Para matrices ajustadas se verifica

-   $A(B+C) = AB + AC$.

-   $(D+E)F = DF + EF$.

-   $A(BC) = (AB)C$.
{{% /theorem %}}

{{% proof %}}
-   Supongamos que $A\sb{m \times p}, B\sb{p \times n}, C\sb{p \times n}$. Las
    matrices $A(B+C)$ y $AB+AC$ son ambas de orden $m\times n$. Además
    son iguales, ya que sus entradas coinciden: $$\begin{aligned}
    _{ij} & = \sum\sb{k=1}^{p}{a\sb{ik}[B+C]\sb{kj}}
            =  \sum\sb{k=1}^{p}{a\sb{ik}(b\sb{kj}+c\sb{kj})}
            = \sum\sb{k=1}^{p}{(a\sb{ik}b\sb{kj}+a\sb{ik}c\sb{kj})}
    \cr    &    = \sum\sb{k=1}^{p}{a\sb{ik}b\sb{kj}}+\sum\sb{k=1}^{p}{a\sb{ik}c\sb{kj}}
            =  [AB]\sb{ij}+[BC]\sb{ij}  = [AB+BC]\sb{ij}.
\end{aligned}$$

-   La segunda afirmación se prueba de manera similar a la anterior.

-   Supongamos que $A\sb{m \times p}, B\sb{p \times q}, C\sb{q \times n}$. Las
    matrices $A(BC)$ y $(AB)C$ son ambas de orden $m\times n$.
    Comprobamos que tienen las mismas entradas, y por tanto son iguales:
    $$\begin{aligned}
    _{ij} & = \sum\sb{k=1}^{p}{a\sb{ik}[BC]\sb{kj}} = \sum\sb{k=1}^{p}{a\sb{ik}\left(\sum\sb{l=1}^{q}{b\sb{kl}c\sb{lj}}\right)}
     \cr & = \sum\sb{k=1}^{p}{\left(\sum\sb{l=1}^{q}{a\sb{ik}b\sb{kl}c\sb{lj}}\right)}
          = \sum\sb{l=1}^{q}{\left(\sum\sb{k=1}^{p}{a\sb{ik}b\sb{kl}c\sb{lj}}\right)}
     \cr & = \sum\sb{l=1}^{q}{\left(\left(\sum\sb{k=1}^{p}{a\sb{ik}b\sb{kl}}\right)c\sb{lj}\right)}
          = \sum\sb{l=1}^{q}{[AB]\sb{il}c\sb{lj}} = [(AB)C]\sb{ij}.
\end{aligned}$$
{{% /proof %}}

{{% definition %}}
Matriz identidad La matriz de orden $n \times n$ con unos en la diagonal
y ceros en el resto
$$I\sb{n} = \left( \begin{array}{cccc} 1 & 0 & \ldots & 0 \cr 0 & 1 & \ldots & 0 \cr \vdots & \vdots & \ddots & \vdots \cr 0 & 0 & \ldots & 1 \end{array} \right)$$
se denomina **matriz identidad** de orden $n$.
{{% /definition %}}

Para toda matriz $A$ de orden $m \times n$ se verifica
$$A I\sb{n} = A, \qquad I\sb{m} A = A.$$ El subı́ndice de $I\sb{n}$ se elimina cuando
el tamaño es obvio por el contexto. Las columnas de $I\sb{n}$ se representan
por los vectores $\vec{e}\sb{1}, \vec{e}\sb{2}, \ldots, \vec{e}\sb{n}$, es decir, el
vector $\vec{e}\sb{i}$ es el vector de $n$ componentes cuya componente
$i$-ésima es igual a $1$ y las restantes iguales a cero. Observemos que
la notación tiene la ambigüedad respecto al tamaño del vector, y se
deduce por los tamaños de las matrices que intervengan en la expresión.

{{% theorem name="Trasposición y producto" %}}
 Para matrices ajustadas $A\sb{m \times p}$ y
$B\sb{p \times n}$ se verifica: $$(AB)^t = B^t A^t.$$ Si $\K=\C$, se tiene
además: $$(AB)^{\ast} = B^{\ast} A^{\ast}$$
{{% /theorem %}}

{{% proof %}}
Probemos la primera propiedad. Las matrices $(AB)^t$ y $B^tA^t$ son
ambas de orden $n\times m$. Por tanto, sólo necesitamos comprobar que
sus componentes coinciden: $$\begin{aligned}
_{ij} & = [AB]\sb{ji} = \sum\sb{k=1}^{p}{a\sb{jk}b\sb{ki}} = \sum\sb{k=1}^{p}{b\sb{ki}a\sb{jk}}
 = \sum\sb{k=1}^{p}{[B^t]\sb{ik}[A^t]\sb{kj}}=[B^tA^t]\sb{ij}.
\end{aligned}$$

La segunda propiedad se demuestra de forma análoga, utilizando que la
conjugación de números complejos preserva la suma y el producto.
{{% /proof %}}

Gracias a las propiedades del producto y la trasposición, veamos que
podemos construir matrices simétricas (o hermitianas) asociadas a una
matriz cualquiera.

{{% lemma %}}
Dada una matriz cualquiera $A\sb{m,n}$, las matrices $AA^t$ y $A^tA$ son
simétricas.
{{% /lemma %}}

{{% proof %}}
Vemos que las matrices indicadas son simétricas demostrando que
coinciden con sus traspuestas: $$(AA^t)^t=(A^t)^tA^t = A A^t, \qquad
   (A^tA)^t = A^t(A^t)^t=A^tA.$$
{{% /proof %}}

{{% lemma %}}
Dada una matriz cualquiera $A\sb{m\times n}$ con entradas en $\C$, las
matrices $AA^{\ast}$ y $A^{\ast}A$ son hermitianas.
{{% /lemma %}}

{{% proof %}}
Se demuestra de forma análoga al resultado anterior.
{{% /proof %}}

La traza de una matriz también se comporta de forma muy especial con
respecto al producto de matrices.

{{% lemma %}}
Dadas matrices $A\sb{m \times n}$ y $B\sb{n \times m}$, se verifica
$$\traza(AB) = \traza(BA).$$
{{% /lemma %}}

{{% proof %}}
$$\begin{aligned}
 \traza(AB) & = \sum\sb{i=1}^{m}{[AB]\sb{ii}}= \sum\sb{i=1}^{m}{\left(\sum\sb{j=1}^{n}{a\sb{ij}b\sb{ji}}\right)}
 = \sum\sb{j=1}^{n}{\left(\sum\sb{i=1}^{m}{a\sb{ij}b\sb{ji}}\right)}
 \cr & = \sum\sb{j=1}^{n}{\left(\sum\sb{i=1}^{m}{b\sb{ji}a\sb{ij}}\right)}
      = \sum\sb{j=1}^{n}{[AB]\sb{jj}}=\traza(BA).
\end{aligned}$$
{{% /proof %}}

De lo anterior se deduce que, $\traza(ABC) = \traza(BCA) = \traza(CAB)$,
pero, en general, $\traza(ABC) \ne \traza(BAC)$. Además, dadas matrices
cuadradas $A$ y $B$, en general se tiene
$\traza(AB)\neq \traza(A)\traza(B)$.

{{% theorem name="Multiplicación por bloques" %}}
 Supongamos que $A$ y $B$ se particionan en
submatrices, también llamados bloques, como sigue:
$$A = \left( \begin{array}{cccc} A\sb{11} & A\sb{12} & \ldots & A\sb{1r} \cr A\sb{21} & A\sb{22} & \ldots & A\sb{2r} \cr \vdots & \vdots & \ddots &
\vdots \cr A\sb{s1} & A\sb{s2} & \ldots & A\sb{sr} \end{array} \right),
B = \left( \begin{array}{cccc} B\sb{11} & B\sb{12} & \ldots & B\sb{1t} \cr B\sb{21} & B\sb{22} & \ldots & B\sb{2t} \cr \vdots & \vdots & \ddots &
\vdots \cr B\sb{r1} & B\sb{r2} & \ldots & B\sb{rt} \end{array} \right).$$ Si
los pares $(A\sb{ik}, B\sb{kj})$ son ajustados para el producto, entonces
decimos que $A$ y $B$ tienen una **partición ajustada**. Para tales
matrices, el producto $AB$ se forma combinando los bloques exactamente
de la misma forma como se hace con los escalares en la multiplicación
ordinaria. Esto es, el bloque $(i,j)$ en $AB$ es
$$A\sb{i1} B\sb{1j} + A\sb{i2} B\sb{2j} + \cdots + A\sb{ir} B\sb{rj}.$$
{{% /theorem %}}

{{% example name="Ejemplo" %}}
Consideremos las matrices particionadas
$$A = \left( \begin{array}{rr|rr} 1 & 2 & 1 & 0 \cr 3 & 4 & 0 & 1 \cr \hline 1 & 0 & 0 & 0 \cr 0 & 1 & 0 & 0 \end{array} \right) = \left( \begin{array}{cc} C & I \cr I &  O  \end{array} \right), \quad
  B = \left( \begin{array}{rr|rr} 1 & 0 & 0 & 0 \cr 0 & 1 & 0 & 0 \cr \hline 1 & 2 & 1 & 2 \cr 3 & 4 & 3 & 4 \end{array} \right) = \left( \begin{array}{cc} I &  O  \cr C & C \end{array} \right),$$
donde
$$I = \left( \begin{array}{rr} 1 & 0 \cr 0 & 1 \end{array} \right) \quad \mbox{ y } \quad C = \left( \begin{array}{rr} 1 & 2 \cr 3 & 4
  \end{array} \right).$$ Mediante la multiplicación por bloques, el
producto $AB$ es fácil de obtener:
$$AB = \left( \begin{array}{cc} C & I \cr I &  O  \end{array} \right) \left( \begin{array}{cc} I &  O  \cr C & C \end{array} \right) = \left( \begin{array}{cc} 2C & C \cr I &  O  \end{array} \right) = \left( \begin{array}{rr|rr} 2 & 4 & 1 & 2 \cr 6 & 8 & 3 & 4 \cr \hline 1 & 0 & 0 & 0 \cr 0 & 1 & 0 & 0 \end{array} \right)$$
{{% /example %}}

## Operaciones elementales de filas

Una vez estudiadas las propiedades principales de las matrices, volvemos
a nuestro estudio de los sistemas de ecuaciones lineales, esta vez
estudiándolos de forma matricial.

La eliminación gaussiana se puede realizar sobre la matriz ampliada
$(A|\vec{b})$ mediante operaciones elementales aplicadas a las filas de
$(A|\vec{b})$. Estas operaciones elementales de filas se corresponden a
las tres operaciones elementales que vimos anteriormente.

Para una matriz de orden $m \times n$ de la forma
$$M = \left( \begin{array}{c} M\sb{1\ast} \cr \vdots \cr M\sb{i\ast} \cr \vdots
\cr M\sb{j\ast} \cr \vdots \cr M\sb{m\ast} \end{array} \right),$$ los tres tipos de
**operaciones elementales de filas** que podemos aplicar a $M$ son como
sigue.

{{% definition name="Operaciones elementales de filas" %}}
  
  |Tipo I|Tipo II|Tipo III|
  |:-:|:-:|:-:|
|Intercambio|Multiplicación por $\alpha \ne 0$|Combinación lineal|
|$M'=\left( \begin{array}{c} M\sb{1\ast} \cr \vdots \cr M\sb{j\ast} \cr \vdots\cr \vdots \cr M\sb{i\ast} \cr \vdots \cr M\sb{j\ast} + \alpha M\sb{i\ast}              \cr M\sb{i\ast} \cr \vdots \cr M\sb{m\ast} \end{array} \right)$|  $M'= \left( \begin{array}{c} M\sb{1\ast} \cr \vdots \cr \alpha M\sb{i\ast} \cr \vdots          \cr M\sb{m\ast} \end{array} \right)$                    |  $M'=\left( \begin{array}{c} M\sb{1\ast}  \cr \vdots \cr M\sb{m\ast} \end{array} \right)$|
|$M \mapright{F\sb{ij}} M'$|$M \mapright{\alpha F\sb{i}} M'$|$M \mapright{F\sb{j} + \alpha F\sb{i}} M'$|
  
Dos matrices $M$ y $M'$ se dicen **equivalentes por filas** si puede
transformarse una en otra mediante una sucesión finita de operaciones
elementales de filas. Por ejemplo, las matrices $$\begin{gathered}
   \left( \begin{array}{rrrr} 1 & 1 & -1 & 0 \cr 2 & 0 & 1 & 1 \end{array} \right) \mapright{F\sb{12}} \left( \begin{array}{rrrr} 2 & 0 & 1 & 1 \cr 1 & 1 & -1 & 0 \end{array} \right) \cr
   \left( \begin{array}{rrrr} 1 & 1 & -1 & 0 \cr 2 & 0 & 1 & 1 \end{array} \right) \mapright{F\sb{2} - 2 F\sb{1}} \left( \begin{array}{rrrr} 1 & 1 & -1 & 0 \cr 0 & -2 & 3 & 1 \end{array} \right), \cr
   \left( \begin{array}{rrrr} 1 & 1 & -1 & 0 \cr 2 & 0 & 1 & 1 \end{array} \right) \mapright{\frac{1}{2} F\sb{2}} \left( \begin{array}{rrrr} 1 & 1 & -1 & 0 \cr 1 & 0 & 1/2 & 1/2 \end{array} \right)
\end{gathered}$$ son equivalentes por filas. Usaremos esta notación para
indicar las transformaciones elementales.
{{% /definition %}} 

{{% remark %}}
Observemos que la relación "ser equivalentes por filas" es una relación
de equivalencia en el conjunto de matrices. Es de señalar el carácter
simétrico de la relación para cada una de las transformaciones. Para el
tipo I (intercambio), la misma operación transforma $M'$ en $M$. Para el
tipo II, la transformación $M' \mapright{\frac{1}{\alpha} F\sb{i}}M$, que
también es de tipo II, nos devuelve la matriz original. Para el tipo
III, con $M' \mapright{F\sb{j} - \alpha F\sb{i}}M$, que es de tipo III,
retornamos a $M$.
{{% /remark %}}

{{% example name="Ejemplo" %}}
Para resolver el sistema del ejemplo
[\[ap1:ej1\]](#ap1:ej1){reference-type="ref" reference="ap1:ej1"}
mediante operaciones elementales de filas, partimos de la matriz
ampliada $M=(A|\vec{b})$ y triangulamos la matriz de coeficientes $A$
realizando la secuencia de operaciones de filas correspondientes a las
operaciones elementales que antes realizamos sobre las ecuaciones.
$$\begin{aligned}
  \left( \begin{array}{rrr|r} \fbox{2} & 1 & 1 & 1 \cr 6 & 2 &
  1 & -1 \cr -2 & 2 & 1 & 7 \end{array} \right) & \mapright{\begin{array}{l} F\sb{2} - 3 F\sb{1} \cr F\sb{3} + F\sb{1} \end{array}} \left( \begin{array}{rrr|r} 2 & 1 & 1 & 1 \cr 0 & \fbox{-1} &
  -2 & -4 \cr 0 & 3 & 2 & 8 \end{array} \right) \cr & \mapright{F\sb{3} + 3 F\sb{2}} \left( \begin{array}{rrr|r} 2 & 1 & 1 & 1 \cr 0 & -1 &
  -2 & -4 \cr 0 & 0 & -4 & -4 \end{array} \right)
\end{aligned}$$ La matriz final representa el sistema triangular
$$\begin{array}{rrrrrcr} 2x\sb{1} & + & x\sb{2} & + & x\sb{3} & = & 1, \cr & - & x\sb{2}
& - & 2x\sb{3} & = & -4, \cr  &  &  & - & 4x\sb{3} & = & -4.
\end{array}$$ que se resuelve por sustitución hacia atrás, como
explicamos antes (ver ejemplo
[\[ap1:ej1\]](#ap1:ej1){reference-type="ref" reference="ap1:ej1"}).
{{% /example %}}

En general, si un sistema cuadrado $n \times n$ se triangula a la forma
$$\label{eq:triang}
\left( \begin{array}{cccc|c} t\sb{11} & t\sb{12} & \ldots & t\sb{1n} & c\sb{1} \cr 0 & t\sb{22} & \ldots & t\sb{2n} & c\sb{2} \cr \vdots & \vdots
& \ddots & \vdots & \vdots \cr 0 & 0 & \ldots & t\sb{nn} &
c\sb{n} \end{array} \right)$$ en donde cada $t\sb{ii} \ne 0$ (no hay pivotes
nulos), entonces el algoritmo general de sustitución hacia atrás es como
sigue.

{{% example name="Algoritmo de sustitución hacia atrás" %}}
 La solución del sistema triangular
se calcula mediante $x\sb{n} = c\sb{n}/t\sb{nn}$ y procede de manera recursiva
calculando
$$x\sb{i} = \frac{1}{t\sb{ii}} (c\sb{i} - t\sb{i,i+1} x\sb{i+1} - t\sb{i,i+2} x\sb{i+2} - \ldots - t\sb{in} x\sb{n})$$
para $i=n-1,n-2,\ldots,2,1$.
{{% /example %}}

## Forma escalonada por filas

Ya estamos preparados para analizar de forma general sistemas
rectangulares con $m$ ecuaciones y $n$ incógnitas
$$\begin{array}{ccccccccc} a\sb{11} x\sb{1} & + &
a\sb{12} x\sb{2} & + & \ldots & + & a\sb{1n} x\sb{n} & = & b\sb{1}
\cr a\sb{21} x\sb{1} & + & a\sb{22} x\sb{2} & + &
\ldots & + & a\sb{2n} x\sb{n} & = & b\sb{2} \cr & & & & \vdots \cr
a\sb{m1} x\sb{1} & + & a\sb{m2} x\sb{2} & + & \ldots & + & a\sb{mn} x\sb{n} & = &
b\sb{m},
\end{array}$$ donde $m$ puede ser diferente de $n$. Si no sabemos con
seguridad que $m$ y $n$ son iguales, entonces decimos que el sistema es
**rectangular**. El caso $m = n$ también queda comprendido en lo que
digamos.

El proceso de la eliminación gaussiana consta de una serie de pasos. El
paso $i$ consiste en determinar una posición pivote en la fila $i$,
conseguir un coeficiente no nulo en esa posición, y anular los
coeficientes que estén debajo. Se continúa hasta que todas las filas que
queden por considerar sean nulas.

En el caso de un sistema rectangular general, no siempre es posible
tener las posiciones pivote en la diagonal principal de la matriz de
coeficientes, como sucedı́a en el caso de los sistemas cuadrados con
solución única. Esto significa que el resultado final de la eliminación
gaussiana *no* será una forma triangular. Por ejemplo, consideremos el
siguiente sistema:
$$\begin{array}{rcrcrcrcrcr} x\sb{1} & + & 2x\sb{2} & + & x\sb{3} & + & 3x\sb{4} & +
& 3x\sb{5} & = & 5, \cr 2x\sb{1} & + & 4x\sb{2} &  &  & + & 4x\sb{4} & + & 4x\sb{5} & =
& 6, \cr x\sb{1} & + & 2x\sb{2} & + & 3x\sb{3} & + & 5x\sb{4} & + & 5x\sb{5} & = & 9,\cr
2x\sb{1} & + & 4x\sb{2} &  &  & + & 4x\sb{4} & + & 7x\sb{5} & = & 9.
\end{array}$$ Fijemos nuestra atención en la matriz ampliada
$$\label{eq:rref01}
B = (A|\vec{b}) = \left( \begin{array}{rrrrr|r} 1 & 2 & 1 & 3 & 3 & 5 \cr 2 & 4 & 0 & 4
& 4 & 6 \cr 1 & 2 & 3 & 5 & 5 & 9 \cr 2 & 4 & 0 & 4 & 7 & 9 \end{array} \right),$$
Aplicamos eliminación gaussiana a $B$ para obtener el siguiente
resultado:
$$\left( \begin{array}{rrrrr|r} \fbox{1} & 2 & 1 & 3 & 3 & 5 \cr 2 & 4 & 0
& 4 & 4 & 6
\cr 1 & 2 & 3 & 5 & 5 & 9 \cr 2 & 4 & 0 & 4 & 7 & 9 \end{array} \right) \to
\left( \begin{array}{rrrrr|r} 1 & 2 & 1 & 3 & 3 & 5 \cr 0 & \fbox{0} & -2
& -2 & -2 & -4
\cr 0 & 0 & 2 & 2 & 2 & 4 \cr 0 & 0 & -2 & -2 & 1 & -1 \end{array} \right).$$
En el proceso de eliminación que habı́amos considerado, la siguiente
posición pivote era la $(2,2)$. Si encontrábamos un cero en esta
posición, se efectuaba un intercambio con una fila inferior para llevar
un número no nulo a la posición pivote. Sin embargo, en este ejemplo, es
imposible llevar un elemento no nulo a la posición $(2,2)$ mediante el
intercambio de la segunda fila con una fila inferior.

Esto nos obliga a determinar las posiciones pivote de forma diferente.
El siguiente método funciona con cualquier sistema de ecuaciones
lineales. Consta de un máximo de $m-1$ pasos, uno por cada fila (salvo
la última). Al principio, la matriz $U$ mencionada en el método será
simplemente la matriz $A$.

{{% example name="Eliminación gaussiana" %}}
Sea $A\sb{m \times n}$ una matriz y supongamos que $U$ es la matriz
obtenida a partir de $A$ tras haber completado $i-1$ pasos de
eliminación gaussiana. Para ejecutar el $i$-ésimo paso, procedemos como
sigue:

-   Consideramos las filas $U\sb{i\ast},\cdots, U\sb{m\ast}$. Si son todas nulas,
    la eliminación gaussiana ha terminado y la matriz $U$ es el
    resultado. Si no, sea $j$ la primera columna que tenga un elemento
    no nulo en alguna de esas filas.

-   La posición pivote para el $i$-ésimo paso es la posición $(i,j)$.

-   Si es necesario, intercambiamos la fila $i$ con una fila inferior
    para llevar un número no nulo a la posición $(i,j)$, y entonces
    anulamos todas las entradas por debajo de este pivote mediante
    transformaciones elementales de tipo III.
{{% /example %}}

{{% example name="Ejemplo" %}}
[]{#ejem:rref02 label="ejem:rref02"} Aplicamos la eliminación gaussiana
a la matriz
$$B = \left( \begin{array}{rrrrr|r} 1 & 2 & 1 & 3 & 3 & 5 \cr 2 & 4 & 0 & 4
& 4 & 6 \cr 1 & 2 & 3 & 5 & 5 & 9 \cr 2 & 4 & 0 & 4 & 7 & 9 \end{array} \right),$$
y marcamos los elementos no nulos encontrados:

$$\begin{aligned}
 \left( \begin{array}{rrrrr|r} \fbox{1} & 2 & 1 & 3 & 3 & 5 \cr 2 & 4 & 0 & 4 &
4 & 6 \cr 1 & 2 & 3 & 5 & 5 & 9 \cr 2 & 4 & 0 & 4 & 7 & 9\end{array} \right)
\to \left( \begin{array}{rrrrr|r} \fbox{1} & 2 & 1 & 3 & 3 & 5 \cr 0 & 0
& \fbox{-2} & -2 & -2 &  -4 \cr 0 & 0 & 2 & 2 & 2 & 4 \cr 0 & 0 & -2 & -2 & 1 & -1
\end{array} \right) \cr \to \left( \begin{array}{rrrrr|r} \fbox{1} & 2 & 1 & 3 & 3 & 5 \cr 0 & 0
& \fbox{-2} & -2 & -2 & -4 \cr 0 & 0 & 0 & 0 & 0 & 0 \cr 0 & 0 & 0 & 0
& \fbox{3} & 3 \end{array} \right) \to \left( \begin{array}{rrrrr|r} \fbox{1} &
2 & 1 & 3 & 3 & 5 \cr 0 & 0 & \fbox{-2} & -2 & -2 & -4 \cr 0 & 0 & 0 & 0 &
\fbox{3} & 3 \cr 0 & 0 & 0 & 0 & 0 & 0 \end{array} \right).
\end{aligned}$$
{{% /example %}}

Observemos que el resultado final de aplicar la eliminación gaussiana en
el ejemplo anterior no es una forma triangular exactamente, sino un tipo
escalonado de forma triangular. Por su importancia, le damos un nombre
especial.

{{% definition %}}
Forma escalonada por filas

Se dice que una matriz está en **forma escalonada por filas** si
verifica lo siguiente:

-   Si la fila $i>1$ es no nula, entonces la fila $i-1$ también es no
    nula, y el primer elemento no nulo de la fila $i-1$ está más a la
    izquierda que el primer elemento no nulo de la fila $i$.
{{% /definition %}}

Observemos que, en una matriz en forma escalonada por filas, todas las
filas de ceros (si las hay) van al final, porque no podemos tener una
fila no nula debajo de una fila de ceros.

El algoritmo de eliminación gaussiana transforma una matriz cualquiera
en una que está en forma escalonada por filas, utilizando
transformaciones elementales de filas. Por tanto tenemos el siguiente
resultado:

{{% theorem name="Equivalencia con una forma escalonada" %}}
 Toda matriz es equivalente por
filas a una en forma escalonada por filas.
{{% /theorem %}}

Una estructura tı́pica de una forma escalonada por filas, con los pivotes
marcados, es $$\left( \begin{array}{cccccccc}
  \fbox{$\ast$} & * & * & * & * & * & * & * \cr
  0 & 0 & \fbox{$\ast$} & * & * & * & * & * \cr
  0 & 0 & 0 & \fbox{$\ast$} & * & * & * & * \cr
  0 & 0 & 0 & 0 & 0 & 0 & \fbox{$\ast$} & * \cr
  0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \cr
  0 & 0 & 0 & 0 & 0 & 0 & 0 & 0
\end{array} \right)$$ Los pivotes son las primeras entradas no nulas en
cada fila. El resto de entradas marcadas con asterisco pueden ser nulas
o no. Podemos tener también columnas de ceros a la izquierda de la
matriz.

Como hay flexibilidad para elegir las operaciones por filas que reducen
una matriz $A$ a una forma escalonada $E$, las entradas de $E$ no están
unı́vocamente determinadas por $A$. Sin embargo, probaremos que las
posiciones de los pivotes son siempre las mismas.

Terminamos esta sección con una observación muy importante. Aunque las
operaciones elementales de filas modifican las filas de una matriz,
veamos que las relaciones entre sus columnas permanecen inalteradas.

{{% theorem name="Relaciones entre columnas" %}}
 Sean $A$ y $B$ matrices equivalentes por
filas. Supongamos que una columna de $A$ es combinación lineal de las
anteriores: $$A\sb{\ast k} = \sum\sb{j=1}^{k-1} \alpha\sb{j} A\sb{\ast j},$$ Entonces se
tiene la misma relación en la matriz $B$, es decir,
$$B\sb{\ast k} = \sum\sb{j=1}^n \alpha\sb{j} B\sb{\ast j},$$ con los mismos coeficientes.
{{% /theorem %}}

{{% proof %}}
Consideremos la matriz $C$ formada por las primeras $k$ columnas de $A$,
y la matriz $D$ formada por las primeras $k$ columnas de $B$. Si
consideramos $C$ como la matriz ampliada de un sistema, vemos que
$(\alpha\sb{1},\ldots,\alpha\sb{k-1})^t$ es solución del sistema, porque la
columna $k$ es combinación lineal, con esos coeficientes, de las
columnas anteriores.

Como $A$ y $B$ son equivalentes por filas, podemos convertir $A$ en $B$
mediante una sucesión de transformaciones elementales de filas. Esa
misma sucesión transforma $C$ en $D$. Por tanto, el sistema de
ecuaciones correspondiente a $C$ es equivalente al sistema de ecuaciones
correspondiente a $D$. Esto significa que
$(\alpha\sb{1},\ldots,\alpha\sb{k-1})^t$ es solución de este último, y por
tanto la columna $k$ de la matriz $B$ es combinación lineal de las
anteriores, con estos mismos coeficientes.
{{% /proof %}}

{{% example name="Ejemplo" %}}
Recordemos la matriz del
Ejemplo [\[ejem:rref02\]](#ejem:rref02){reference-type="ref"
reference="ejem:rref02"}, y la forma escalonada por filas que obtuvimos
mediante eliminación gaussiana:
$$B = \left( \begin{array}{rrrrr|r} 1 & 2 & 1 & 3 & 3 & 5 \cr 2 & 4 & 0 & 4
& 4 & 6 \cr 1 & 2 & 3 & 5 & 5 & 9 \cr 2 & 4 & 0 & 4 & 7 & 9 \end{array} \right),
  \qquad E= \left( \begin{array}{rrrrr|r} \fbox{1} &
2 & 1 & 3 & 3 & 5 \cr 0 & 0 & \fbox{-2} & -2 & -2 & -4 \cr 0 & 0 & 0 & 0 &
\fbox{3} & 3 \cr 0 & 0 & 0 & 0 & 0 & 0 \end{array} \right).$$ Estas
matrices son equivalentes por filas. Vemos que en $B$ la segunda columna
es el doble de la primera: $B\sb{\ast 2}=2B\sb{\ast 1}$. Por tanto, la misma
relación se cumple en $E$, es decir, $E\sb{\ast 2}=2E\sb{\ast 1}$.

También vemos que $B\sb{\ast 4}=2B\sb{\ast 1}+B\sb{\ast 3}$, y por tanto se tiene la misma
relación en la matriz $E$, es decir, $E\sb{\ast 4}=2E\sb{\ast 1}+E\sb{\ast 3}$.

Por último, se tiene $B\sb{\ast 6}=B\sb{\ast 1}+B\sb{\ast 3}+B\sb{\ast 5}$, y por tanto también
se tiene $E\sb{\ast 6}=E\sb{\ast 1}+E\sb{\ast 3}+E\sb{\ast 5}$.

No es casualidad, como veremos más adelante, que las columnas que
dependen de las anteriores sean las que no tienen pivote. De hecho, como
la última columna no tiene pivote, hemos podido escribirla como
combinación lineal de las anteriores, y eso nos da una solución del
sistema asociado, en este caso $(1,0,1,0,1)^t$.

En general, el resultado anterior implica que las relaciones entre las
columnas de una matriz se mantienen en cualquiera de sus formas
escalonadas por filas.
{{% /example %}}

## Forma escalonada reducida por filas

Se puede avanzar en la simplificación de una forma escalonada por filas.
Por ejemplo, mediante la multiplicación de una fila por un escalar no
nulo (transformación elemental de tipo II), podemos conseguir que cada
pivote sea igual a $1$. Además, se pueden anular las entradas que se
encuentran por *encima* de cada pivote. El siguiente ejemplo ilustra
este proceso.

{{% example name="Ejemplo" %}}
Obtenemos una forma escalonada de la matriz
$$\left( \begin{array}{rrrrrr} 1 & 2 & 1 & 3 & 3 & 5 \cr 2 & 4 & 0 & 4 & 4 & 6
\cr 1 & 2 & 3 & 5 & 5 & 9 \cr 2 & 4 & 0 & 4 & 7 & 9
\end{array} \right)$$ mediante transformaciones elementales y hacemos,
en cada paso, los pivotes iguales a $1$, anulando además las entradas
superiores a cada pivote. $$\begin{gathered}
\left( \begin{array}{rrrrrr} \fbox{1} & 2 & 1 & 3 & 3 & 5 \cr 2 & 4 &
0 & 4 & 4 & 6 \cr 1 & 2 & 3 & 5 & 5 & 9 \cr 2 & 4 & 0 & 4 & 7 & 9
\end{array} \right) \to
\left( \begin{array}{rrrrrr} \fbox{1} & 2 & 1 & 3 & 3 & 5 \cr 0 & 0 & \fbox{-2} & -2 & -2 & -4 \cr
0 & 0 & 2 & 2 & 2 & 4 \cr 0 & 0 & -2 & -2 & 1 & -1
\end{array} \right) \to \cr
\left( \begin{array}{rrrrrr} \fbox{1} & 2 & 1 & 3 & 3 & 5 \cr 0 & 0 &
\fbox{1} & 1 & 1 & 2 \cr 0 & 0 & 2 & 2 & 2 & 4 \cr 0 & 0 & -2 & -2 & 1 & -1
\end{array} \right) \to
\left( \begin{array}{rrrrrr} \fbox{1} & 2 & 0 & 2 & 2 & 3 \cr 0 & 0 & \fbox{1} & 1 & 1 & 2 \cr
0 & 0 & 0 & 0 & 0 & 0 \cr 0 & 0 & 0 & 0 & 3 & 3 \end{array} \right)
\to \cr
\left(
\begin{array}{rrrrrr} \fbox{1} & 2 & 0 & 2 & 2 & 3 \cr 0 & 0 & \fbox{1} & 1 & 1 & 2 \cr 0 & 0 & 0 &
0 & \fbox{3} & 3 \cr 0 & 0 & 0 & 0 & 0 & 0
\end{array} \right)  \to
\left( \begin{array}{rrrrrr} \fbox{1} & 2 & 0 & 2 & 2 & 3  \cr 0 & 0 &
\fbox{1} & 1 & 1 & 2
\cr 0 & 0 & 0 & 0 & \fbox{1} & 1 \cr 0 & 0 & 0 & 0 & 0 & 0 \end{array}
\right) \to \cr
 \left( \begin{array}{rrrrrr} \fbox{1} & 2 & 0 & 2 & 0 & 1\cr 0 & 0 & \fbox{1} & 1 & 0 & 1 \cr
0 & 0 & 0 & 0 & \fbox{1} & 1 \cr 0 & 0 & 0 & 0 & 0 & 0
\end{array} \right).
\end{gathered}$$
{{% /example %}}

Este proceso es una extensión del conocido como método de Gauss-Jordan.
Comparamos este ejemplo con el resultado del ejemplo
[\[ejem:rref02\]](#ejem:rref02){reference-type="ref"
reference="ejem:rref02"} (pág. ), y vemos que las posiciones pivote son
las mismas en ambos casos. La única diferencia es el valor numérico de
algunas entradas. Por la naturaleza de la eliminación, cada pivote es 1
y todas las entradas por encima y por debajo son nulas. Por tanto, la
forma escalonada por filas que produce esta reducción contiene un número
reducido de entradas no nulas. Esto nos permite dar la siguiente
definición.

{{% definition %}}
Forma escalonada reducida por filas

Una matriz está en **forma escalonada reducida por filas** si se
verifican las siguientes condiciones:

-   Está en forma escalonada por filas.

-   Si una fila es no nula, su primera entrada no nula (el pivote) es
    $1$.

-   Todas las entradas por encima (y por debajo) del pivote son cero.
{{% /definition %}}

Una estructura tı́pica de una matriz en forma escalonada reducida por
filas es $$\left( \begin{array}{cccccccc}
  \fbox{1} & * & 0 & 0 & * & * & 0 & * \cr
  0 & 0 & \fbox{1} & 0 & * & * & 0 & * \cr
  0 & 0 & 0 & \fbox{1} & * & * & 0 & * \cr
  0 & 0 & 0 & 0 & 0 & 0 & \fbox{1} & * \cr
  0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \cr
  0 & 0 & 0 & 0 & 0 & 0 & 0 & 0
\end{array} \right)$$ Observemos que, por construcción, las columnas con
pivote son precisamente las primeras columnas de la matriz identidad.
Además, una columna que contenga un pivote no puede ser combinación
lineal de las anteriores. esto último se aplica a una forma escalonada
cualquiera. Como comentamos antes, puede haber distintas matrices en
forma escalonada por filas asociadas a una matriz $A$. Sin embargo, la
forma escalonada reducida tiene una propiedad especial.

{{% theorem name="Unicidad de la forma escalonada reducida por filas y pivotes" %}}
 Toda matriz
$A$ es equivalente por filas a una *única* forma escalonada reducida por
filas $E\sb{A}$. Además, toda forma escalonada de $A$ tiene los pivotes en
las mismas posiciones.
{{% /theorem %}}

{{% proof %}}
[]{#thm:red label="thm:red"} Sea $A$ una matriz $m \times n$. Probaremos
el teorema por inducción sobre $n$.

Supongamos que $A$ tiene una sola columna (caso $n=1$). Sabemos que el
método de Gauss-Jordan transforma una matriz en otra que está en forma
escalonada reducida por filas. Pero sólo hay dos posibles matrices en
forma escalonada reducida por filas de orden $m\times 1$, dependiendo de
si tienen o no tienen pivote. concretamente
$$\left( \begin{array}{c} 1 \cr 0 \cr \vdots \cr 0 \end{array} \right) \quad \text{ o bien }\quad \left( \begin{array}{c} 0 \cr 0 \cr \vdots \cr 0 \end{array} \right).$$
Es fácil ver que la segunda matriz sólo es equivalente por filas a ella
misma, porque cualquier transformación elemental de filas la deja
inalterada. Por tanto, todas las demás matrices de una sola columna (las
que sean no nulas) son equivalentes por filas a la primera de las dos
matrices, y no pueden ser equivalentes por filas a la segunda. Esto
implica que la forma escalonada reducida por filas de una matriz $A$ con
una sola columna es única: Si $A$ es no nula, $E\sb{A}$ es la primera de las
dos matrices anteriores, y si $A= O $, $E\sb{A}= O $.

Supongamos ahora que $n>1$ y que el teorema es cierto para matrices con
menos de $n$ columnas. Sean $B$ y $C$ dos formas escalonadas reducidas
por filas obtenidas a partir de $A$. Vamos a probar que $B=C$, y con
esto habremos demostrado la unicidad.

Sean $A', B'$ y $C'$ las matrices obtenidas mediante el borrado de la
última columna de $A, B$ y $C$, respectivamente. Entonces $B'$ y $C'$
son equivalentes por filas a $A'$. Más aún, $B'$ y $C'$ están en forma
escalonada reducida por filas. Como $A'$ tiene $n-1$ columnas, la
hipótesis de inducción nos garantiza que $B' = C'$. Por tanto, $B$ y $C$
únicamente podrı́an ser diferentes en la última columna.

Si $B$ no contiene un pivote en la última columna, esta columna será de
la forma
$$B\sb{\ast n}=\left(\begin{array}{c} \alpha\sb{1} \cr \vdots \cr \alpha\sb{r} \cr 0 \cr \vdots \cr 0 \end{array} \right),$$
donde $r$ es el número de pivotes que hay en $B$ (y en $B'$). Entonces
tenemos $$B\sb{\ast n}= \alpha\sb{1} \vec{e}\sb{1}+\cdots +\alpha\sb{r} \vec{e}\sb{r},$$ donde
$\vec{e}\sb{i}$ es la columna $i$ de la matriz identidad. Pero sabemos que
las columnas de $B$ que tienen pivotes son precisamente iguales a
$\vec{e}\sb{1},\ldots,\vec{e}\sb{r}$. Por tanto, la última columna de $B$ es
combinación lineal de las columnas anteriores con pivote, con
coeficientes $\alpha\sb{1},\ldots,\alpha\sb{r}$.

Como $C$ es equivalente por filas a $B$, obtenemos que la última columna
de $C$ es combinación lineal esas mismas columnas, con coeficientes
$\alpha\sb{1},\ldots,\alpha\sb{r}$. Pero las columas de $C$ anteriores a la
última son precisamente las de $C'$, que es igual a $B'$, luego son
exactamente $\vec{e}\sb{1},\ldots,\vec{e}\sb{r}$. Esto implica que la última
columna de $C$ en este caso, también es
$$C\sb{\ast n}=\left(\begin{array}{c} \alpha\sb{1} \cr \vdots \cr \alpha\sb{r} \cr 0 \cr \vdots \cr 0 \end{array} \right),$$
y por tanto $B=C$.

Observemos que, si la última columna de $C$ no tiene pivote, podemos
aplicar el mismo razonamiento para deducir que la última columna de $B$
tampoco lo tiene, y que ambas colummnas son iguales.

Por tanto, el único caso que queda se da cuando la última columna de $B$
tiene pivote, y la última colummna de $C$ también lo tiene. En este caso
ambas columnas son iguales a $\vec{e}\sb{r+1}$, y por tanto volvemos a
tener $B=C$. Esto demuestra la unicidad de la forma escalonada reducida
por filas.

Para la segunda parte, supongamos que $U$ es una forma escalonada por
filas obtenida a partir de $A$. Para llegar de $U$ a $E\sb{A}$, podemos
hacer los pivotes iguales a 1 y anular las entradas por encima de dicho
pivote. Por tanto, las posiciones pivote de $U$ coinciden con las de
$E\sb{A}$. Esto implica que cualquier forma escalonada por filas de $A$
tiene los pivotes en las mismas posiciones que $E\sb{A}$.
{{% /proof %}}

{{% remark %}}
Para una matriz $A$, el sı́mbolo $E\sb{A}$ denotará la única forma escalonada
reducida por filas derivada de $A$ mediante transformaciones elementales
de filas. También escribiremos
$$A \mapright{\rref} E\sb{A} \quad \text{ o bien }\quad  A \mapright{\GJ} E\sb{A},$$
porque el procedimiento anterior se conoce también como algoritmo de
Gauss-Jordan. El uso de `rref` responde a que es el comando que se usa
en programas como [Matlab, Octave]{.smallcaps} o [Scilab]{.smallcaps}
para el cálculo de la forma escalonada reducida por filas (**r**educed
**r**ow **e**chelon **f**orm).
{{% /remark %}}

Como las posiciones pivote son únicas, se sigue que el número de
pivotes, que es el mismo que el número de filas no nulas de $E\sb{A}$ (o de
cualquier forma escalonada de $A$), también está unı́vocamente
determinado por $A$. Este número se denomina **rango** de $A$, y es uno
de los conceptos fundamentales del curso.

{{% definition %}}
Rango de una matriz Supongamos que una matriz $A$ de orden ${m\times n}$
se reduce mediante operaciones elementales por filas a una forma
escalonada $E$. El **rango** de $A$ es el número $$\begin{array}{rcl}
  \rg(A) & = & \mbox{ número de pivotes } \cr
         & = & \mbox{ número de filas no nulas de } E \cr
         & = & \mbox{ número de columnas básicas de } A,
\end{array}$$ donde las columnas básicas de $A$ son aquellas columnas de
$A$ que contienen las posiciones pivote.
{{% /definition %}}

{{% example name="Ejemplo" %}}
Determinemos $E\sb{A}$, calculemos $\rg(A)$ e identifiquemos las columnas
básicas de
$$A = \left( \begin{array}{rrrrr} 1& 2 & 2 & 3 & 1 \cr 2 & 4 & 4 &
  6 & 2 \cr 3 & 6 & 6 & 9 & 6 \cr 1 & 2 & 4 & 5 & 3 \end{array}
  \right).$$ Identificamos los pivotes en cada paso. $$\begin{aligned}
    \left( \begin{array}{rrrrr} \fbox{1}& 2 & 2 & 3 & 1 \cr 2 & 4 & 4 &
  6 & 2 \cr 3 & 6 & 6 & 9 & 6 \cr 1 & 2 & 4 & 5 & 3 \end{array}
  \right) \to \left( \begin{array}{rrrrr} \fbox{1}& 2 & 2 & 3 & 1 \cr 0 & 0 & \fbox{0} &
  0 & 0 \cr 0 & 0 & 0 & 0 & 3 \cr 0 & 0 & 2 & 2 & 2 \end{array}
  \right) \to \left( \begin{array}{rrrrr} \fbox{1}& 2 & 2 & 3 & 1 \cr 0 & 0 & \fbox{2} &
  2 & 2 \cr 0 & 0 & 0 & 0 & 3 \cr 0 & 0 & 0 & 0 & 0 \end{array}
  \right) \cr \to \left( \begin{array}{rrrrr} \fbox{1}& 2 & 2 & 3 & 1 \cr 0 & 0 & \fbox{1} &
  1 & 1 \cr 0 & 0 & 0 & 0 & 3 \cr 0 & 0 & 0 & 0 & 0 \end{array}
  \right) \to \left( \begin{array}{rrrrr} \fbox{1}& 2 & 0 & 1 & -1 \cr 0 & 0 & \fbox{1} &
  1 & 1 \cr 0 & 0 & 0 & 0 & \fbox{3} \cr 0 & 0 & 0 & 0 & 0 \end{array}
  \right) \cr \to \left( \begin{array}{rrrrr} \fbox{1}& 2 & 0 & 1 & -1 \cr 0 & 0 & \fbox{1} &
  1 & 1 \cr 0 & 0 & 0 & 0 & \fbox{1} \cr 0 & 0 & 0 & 0 & 0 \end{array}
  \right) \to \left( \begin{array}{rrrrr} \fbox{1}& 2 & 0 & 1 & 0 \cr 0 & 0 & \fbox{1} &
  1 & 0 \cr 0 & 0 & 0 & 0 & \fbox{1} \cr 0 & 0 & 0 & 0 & 0 \end{array}
  \right) = E\sb{A}
\end{aligned}$$ Por tanto, $\rg(A) = 3$, y
$\{ A\sb{\ast 1}, A\sb{\ast 3}, A\sb{\ast 5} \}$ son las tres columnas básicas.
{{% /example %}}

{{% example name="Ejemplo" %}}
Determinemos el rango y columnas básicas de la matriz
$$A = \left( \begin{array}{rrrr} 1 & 2 & 1 & 1 \cr 2 & 4 & 2 & 2 \cr
  3 & 6 & 3 & 4 \end{array} \right).$$ Reducimos $A$ a forma escalonada
por filas.
$$\left( \begin{array}{rrrr} \fbox{1} & 2 & 1 & 1 \cr 2 & 4 & 2 & 2 \cr
  3 & 6 & 3 & 4 \end{array} \right) \to
  \left( \begin{array}{rrrr} \fbox{1} & 2 & 1 & 1 \cr 0 & 0 & 0 & \fbox{0} \cr
  0 & 0 & 0 & 1 \end{array} \right) \to
  \left( \begin{array}{rrrr} \fbox{1} & 2 & 1 & 1 \cr 0 & 0 & 0 & \fbox{1} \cr
  0 & 0 & 0 & 0 \end{array} \right) = E$$ Por tanto, $\rg(A) = 2$. Las
posiciones pivote están en la primera y cuarta columna, por lo que las
columnas básicas de $A$ son $A\sb{\ast 1}$ y $A\sb{\ast 4}$. Esto es,
$$\mbox{Columnas básicas } = \left\\{ \left( \begin{array}{r} 1 \cr
  2 \cr 3 \end{array} \right), \left( \begin{array}{r} 1 \cr 2 \cr 4
  \end{array} \right) \right\\}.$$ Es importante resaltar que las
columnas básicas de $A$ son columnas de $A$, y no de la forma escalonada
$E$.
{{% /example %}}

## Compatibilidad de sistemas

Recordemos que un sistema de $m$ ecuaciones y $n$ incógnitas se dice
**compatible** si posee al menos una solución. Si no tiene soluciones,
decimos que el sistema es **incompatible**.

Un sistema lineal **compatible** se dice **determinado** si tiene una
única solución, en otro caso se dice **indeterminado**.

El propósito de esta sección es determinar las condiciones bajo las que
un sistema es compatible. Establecer dichas condiciones para un sistema
de dos o tres incógnitas puede hacerse de forma geométrica. Una ecuación
lineal con dos incógnitas representa una recta en el plano, y una
ecuación lineal con tres incógnitas es un plano en el espacio de tres
dimensiones. Por tanto, un sistema lineal de $m$ ecuaciones con dos
incógnitas es compatible si y solamente si las $m$ rectas definidas por
las $m$ ecuaciones tienen al menos un punto común de intersección. Lo
mismo ocurre para $m$ planos en el espacio. Sin embargo, para $m$
grande, estas condiciones geométricas pueden ser difı́ciles de verificar
visualmente, y cuando $n > 3$ no es posible tal cosa.

Mejor que depender de la geometrı́a para establecer la compatibilidad,
usaremos la eliminación gaussiana. Si la matriz ampliada asociada
$(A|\vec{b})$ se reduce mediante transformaciones elementales de filas a
una forma escalonada reducida por filas $[E|\vec{c}]$, entonces la
compatibilidad o no del sistema es evidente.

Si la última columna $\vec{c}$ contiene un pivote, entonces no puede
escribirse como combinación lineal de las anteriores, y por tanto la
columna $\vec{b}$ no puede escribirse como combinación lineal de las
columnas de $A$, es decir, el sistema no tiene solución.

Si, por el contrario, la cúltima olumna $\vec{c}$ no contiene un pivote,
entonces es combinación lineal de las columnas de $E$ que tengan pivote
(que son iguales a las columnas de la matriz identidad). Por tanto, la
columna $\vec{b}$ será combinación lineal de las columnas de $A$, es
decir, el sistema sı́ tiene solución.

En realidad, en muchas ocasiones no es necesario calcular la forma
escalonada reducida por filas, puesto que la condición para que el
sistema tenga solución o no la tenga depende sólo de las posiciones
pivote. Basta por tanto con calcular una forma escalonada por filas, y
ver si hay pivote en la última columna.

A veces ni siquiera es necesario calcular una forma escalonada.
Supongamos que en un momento del proceso de reducción de $(A|\vec{b})$ a
$[E|\vec{c}]$ llegamos a una situación en la que la única entrada no nula
de una fila aparece en la última columna, como mostramos a continuación:
$$\begin{array}{l} \cr \cr \mbox{Fila } i \to \end{array}
\left( \begin{array}{cccccc|c} * & * & * & * & * & *& *
\cr 0 & 0 & 0 & * & * & * & * \cr 0 & 0 & 0 & 0 & * & * & *
\cr \colorbox{whiteblue}{0} & \colorbox{whiteblue}{0} & \colorbox{whiteblue}{0} & \colorbox{whiteblue}{0} &
 \colorbox{whiteblue}{0} & \colorbox{whiteblue}{0} & \alpha \cr
\ldots & \ldots & \ldots & \ldots & \ldots & \ldots & \ldots
\end{array} \right) \begin{array}{l} \cr \cr \leftarrow \alpha
\ne 0. \end{array}$$ Si esto ocurre en la $i$-ésima fila, entonces la
$i$-ésima ecuación del sistema asociado es
$$0\cdot x\sb{1} + 0 \cdot x\sb{2} + \ldots + 0 \cdot x\sb{n} = \alpha.$$ Para
$\alpha \ne 0$, esta ecuación no tiene solución, y el sistema original
es incompatible (recordemos que las operaciones por filas no alteran el
conjunto de soluciones). El recı́proco también se verifica. Esto es, si
el sistema es incompatible, entonces en algún momento del proceso de
eliminación llegamos a una fila de la forma $$\label{eq:comp01}
\left( \begin{array}{cccc|c} 0 & 0 & \ldots & 0 & \alpha
\end{array} \right), \alpha \ne 0.$$ En el caso extremo, llegarı́amos a
esta situación en el último paso, ya que si el sistema no tiene
solución, la última columna tendrá un pivote.

Recordemos que las columnas de $A$ que contienen las posiciones pivote
se denominan *columnas básicas*. El razonamiento anterior nos dice lo
siguiente:

* $(A|\vec{b})$ es compatible si y solamente si $\vec{b}$ no es columna
básica.


Decir que $\vec{b}$ no es columna básica en $(A|\vec{b})$ es equivalente a
decir que todas las columnas básicas de $(A|\vec{b})$ están en la matriz
de coeficientes $A$. Como el número de columnas básicas es el rango, la
compatibilidad puede ser caracterizada diciendo que un sistema es
compatible si y sólo si $\rg(A|\vec{b}) = \rg(A)$. Este enunciado en
función del rango se conoce como **teorema de Rouché-Frobenius**, del
que más adelante daremos una ampliación.

Resumimos todas estas condiciones.

{{% theorem name="Compatibilidad de un sistema" %}}
Cada uno de las siguientes enunciados es equivalente a que el sistema
lineal determinado por la matriz $(A|\vec{b})$ es compatible.

-   En la reducción por filas de $(A|\vec{b})$, nunca aparece una fila de
    la forma
    $$\left( \begin{array}{cccc|c} 0 & 0 & \ldots & 0 & \alpha \end{array} \right), \alpha \ne 0.$$

-   $\vec{b}$ es una columna no básica de $(A|\vec{b})$.

-   $\vec{b}$ es combinación lineal de las columnas básicas de $A$.

-   $\rg(A|\vec{b}) = \rg(A)$.
{{% /theorem %}}

{{% example name="Ejemplo" %}}
Determinemos si el sistema
$$\begin{array}{rcrcrcrcrcr} x\sb{1} & + & x\sb{2} & + & 2x\sb{3} & + & 2x\sb{4} & + & x\sb{5} & = & 1, \cr 2x\sb{1} & + & 2x\sb{2} & + & 4x\sb{3} & + & 4x\sb{4} & + & 3x\sb{5} & = & 1, \cr 2x\sb{1} & + & 2x\sb{2} & + & 4x\sb{3} & + & 4x\sb{4} & + & 2x\sb{5} & = & 2, \cr 3x\sb{1} & + & 5x\sb{2} & + & 8x\sb{3} & + & 6x\sb{4} & + & 5x\sb{5} & = & 3,
\end{array}$$ 
es compatible. Aplicamos eliminación gaussiana a la
matriz ampliada $(A|\vec{b})$. $$\begin{aligned}
    \left( \begin{array}{rrrrr|r} \fbox{1} & 1 & 2 & 2 & 1 & 1
    \cr 2 & 2 & 4 & 4 & 3 & 1 \cr 2 & 2 & 4 & 4 & 2 & 2 \cr
    3 & 5 & 8 & 6 & 5 & 3 \end{array} \right) & \to &
    \left( \begin{array}{rrrrr|r} \fbox{1} & 1 & 2 & 2 & 1 & 1
    \cr 0 & \fbox{0} & 0 & 0 & 1 & -1 \cr 0 & 0 & 0 & 0 & 0 & 0 \cr
    0 & 2 & 2 & 0 & 2 & 0 \end{array} \right) \cr & \to &
    \left( \begin{array}{rrrrr|r} \fbox{1} & 1 & 2 & 2 & 1 & 1
    \cr 0 & \fbox{2} & 0 & 0 & 2 & 0 \cr 0 & 0 & 0 & 0 & \fbox{1} & -1 \cr
    0 & 0 & 0 & 0 & 0 & 0 \end{array} \right).
\end{aligned}$$ Como no hay ninguna fila de la forma
$\left( \begin{array}{cccc|c} 0 & 0 & \ldots & 0 & \alpha \end{array} \right)$,
con $\alpha \ne 0$, el sistema es compatible. También observamos que
$\vec{b}$ no es una columna básica en $(A|\vec{b})$, por lo que
$\rg(A|\vec{b}) = \rg(A)$.
{{% /example %}}

Luego un sistema es compatible si y solamente si
$\rg (A|\vec{b})=\rg (A)$. El objetivo ahora es determinar cuándo es
compatible determinado o indeterminado. Para ello, vamos a estudiar
antes un tipo especial de sistemas.

## Sistemas homogéneos

{{% definition %}}
Sistemas homogéneos Un sistema de $m$ ecuaciones y $n$ incógnitas
$$\begin{array}{ccccccccc} a\sb{11} x\sb{1} & + &
a\sb{12} x\sb{2} & + & \ldots & + & a\sb{1n} x\sb{n} & = & 0
\cr a\sb{21} x\sb{1} & + & a\sb{22} x\sb{2} & + &
\ldots & + & a\sb{2n} x\sb{n} & = & 0 \cr & & & & \vdots \cr
a\sb{m1} x\sb{1} & + & a\sb{m2} x\sb{2} & + & \ldots & + & a\sb{mn} x\sb{n} & = & 0,
\end{array}$$ en el que los términos independientes son todos cero se
denomina **homogéneo**. Si al menos uno de los términos independientes
es no nulo, decimos que es **no homogéneo**.
{{% /definition %}}

En esta sección vamos a examinar algunas de las propiedades más
elementales de los sistemas homogéneos.

Un sistema homogéneo **siempre es compatible**, pues
$x\sb{1} = x\sb{2} = \ldots = x\sb{n} = 0$ siempre es una solución del sistema,
independientemente de los coeficientes. Esta solución se denomina
**solución trivial**. La pregunta es si hay otras soluciones además de
la trivial, y cómo podemos describirlas. Como antes, la eliminación
gaussiana nos dará la respuesta.

Mientras reducimos la matriz ampliada $[A|\vec{0}]$ de un sistema
homogéneo a una forma escalonada mediante la eliminación gaussiana, la
columna de ceros de la derecha no se ve alterada por ninguna de las
operaciones elementales. Ası́, cualquier forma escalonada derivada de
$[A|\vec{0}]$ tendrá la forma $[E|\vec{0}]$. Esto significa que no es
necesario tener en cuenta la columna de ceros a la hora de efectuar los
cálculos. Simplemente reducimos la matriz $A$ a una forma escalonada
$E$, y recordamos que los términos independientes son todos cero cuando
procedamos a la sustitución hacia atrás. El proceso se comprende mejor
con un ejemplo.

{{% example name="Ejemplo" %}}
Vamos a examinar las soluciones del sistema homogéneo
$$\label{eq:homog00}
  \begin{array}{rcrcrcrcr} x\sb{1} & + & 2x\sb{2} & + & 2x\sb{3} & + & 3x\sb{4} &
  = & 0, \cr 2x\sb{1} & + & 4x\sb{2} & + & x\sb{3} & + & 3x\sb{4} & = & 0, \cr 3x\sb{1}
  & + & 6x\sb{2} & + & x\sb{3} & + & 4x\sb{4} & = & 0. \end{array}$$ Reducimos la
matriz de coeficientes a una forma escalonada por filas:
$$A = \left( \begin{array}{rrrr} 1 & 2 & 2 & 3 \cr 2 & 4 & 1 & 3 \cr
  3 & 6 & 1 & 4 \end{array} \right) \to \left( \begin{array}{rrrr} 1 & 2 & 2 & 3 \cr 0 & 0 & -3 & -3 \cr
  0 & 0 & -5 & -5 \end{array} \right) \to \left( \begin{array}{rrrr} 1 & 2 & 2 & 3 \cr 0 & 0 & -3 & -3 \cr
  0 & 0 & 0 & 0 \end{array} \right) = E.$$ Entonces, el sistema
homogéneo inicial es equivalente al sistema homogéneo
$$\begin{array}{rcrcrcrcr} x\sb{1} & + & 2x\sb{2} & + & 2x\sb{3} & + & 3x\sb{4} &
  = & 0, \cr & & & & -3x\sb{3} & - & 3x\sb{4} & = & 0.
  \end{array}$$ Como hay cuatro incógnitas y solamente dos ecuaciones,
veremos que no habrá una solución única para cada incógnita. En este
caso podremos elegir dos incógnitas básicas, que llamaremos **variables
básicas**, y resolver el sistema en función de las otras dos, que
llamaremos **variables libres**. Aunque hay distintas posibilidades para
escoger las variables básicas, el convenio es escoger las incógnitas que
se encuentran en las posiciones pivote.

En este ejemplo, los pivotes están en la primera y tercera posición (las
columnas básicas), ası́ que $x\sb{1}$ y $x\sb{3}$ seán las variables básicas, y
$x\sb{2}$ y $x\sb{4}$ serán las variables libres. Esto significa que $x\sb{2}$ y
$x\sb{4}$ pueden tomar cualquier valor, y cada par de valores que elijamos
nos dará lugar a una solución del sistema.

Llamemos $\alpha\sb{1}$ y $\alpha\sb{2}$ a los valores que tomen las dos
variables libres. Escribimos entonces $x\sb{2}=\alpha\sb{1}$ y $x\sb{4}=\alpha\sb{2}$.
Sustiuimos estoa valores en las ecuaciones del sistema, y ya podemos
realizar la sustitución hacia atrás de las varialbes básicas, puesto que
el sistema, que ahora sólo tiene las variables básicas, está
triangulado.

Despejamos $x\sb{3}$ de la segunda ecuación y obtenemos $$x\sb{3} = -\alpha\sb{2}$$
Como ya tenemos $x\sb{2}$, $x\sb{3}$ y $x\sb{4}$ escritos en función de los
parámetros $\alpha\sb{1}$ y $\alpha\sb{2}$, sustituimos en la primera ecuación y
tenemos $$\begin{aligned}
    x\sb{1} & = & -2\alpha\sb{1}-2(-\alpha\sb{2})-3\alpha\sb{2} \cr & = & -2\alpha\sb{1} - \alpha\sb{2}.
\end{aligned}$$ Las soluciones al sistema homogéneo original pueden ser
descritas como $$\begin{aligned}
    x\sb{1} & = & -2\alpha\sb{1} - \alpha\sb{2}, \cr
    x\sb{2} & = & \alpha\sb{1}, \cr
    x\sb{3} & = & -\alpha\sb{2}, \cr
    x\sb{4} & = & \alpha\sb{2}.
\end{aligned}$$ Como las variables libres $x\sb{2}$ y $x\sb{4}$ pueden tomar
todos los valores posibles, y las variables básicas deben tomar los
valores descritos en función de esos parámetros (para cumplir las
ecuaciones del sistema), las expresiones anteriores describen todas las
soluciones del sistema.

Mejor que describir las soluciones de esta forma, es más conveniente
expresarlas de forma vectorial:
$$\left( \begin{array}{c} x\sb{1} \cr x\sb{2} \cr x\sb{3} \cr x\sb{4} \end{array}
  \right) = \left( \begin{array}{c} -2\alpha\sb{1} - \alpha\sb{2} \cr \alpha\sb{1} \cr -\alpha\sb{2} \cr
  \alpha\sb{2} \end{array} \right) = \alpha\sb{1} \left( \begin{array}{r} -2 \cr 1 \cr
  0 \cr 0 \end{array} \right) + \alpha\sb{2} \left( \begin{array}{r} -1 \cr 0
  \cr -1 \cr 1 \end{array} \right),$$ entendiendo que $\alpha\sb{1}$ y
$\alpha\sb{2}$ son parámetros que pueden tomar cualquier valor. Esta
representación se denominará **solución general** del sistema homogéneo
y enfatiza que la solución general del sistema es combinación lineal de
las dos soluciones $$\vec{h}\sb{1} = \left( \begin{array}{r} -2 \cr 1 \cr
  0 \cr 0 \end{array} \right) \quad \mbox{ y }\quad  \vec{h}\sb{2} = \left( \begin{array}{r} -1 \cr 0
  \cr -1 \cr 1 \end{array} \right).$$ Observemos que $\vec{h}\sb{i}$ es la
solución del sistema que se obtiene con los valores $\alpha\sb{i}=1$ y
$\alpha\sb{j}=0$ para $j\neq i$.

En la práctica, a la hora de resolver el sistema de ecuaciones, hay dos
opciones: hallar una forma escalonada por filas del sistema y proceder
como hemos hecho, o hallar la forma escalonada reducida por filas y
evitar ası́ la sustitución hacia atrás.

En este ejemplo, si calculamos la forma escalonada reducida de $A$
obtenemos:
$$A = \left( \begin{array}{rrrr} 1 & 2 & 2 & 3 \cr 2 & 4 & 1 & 3 \cr
  3 & 6 & 1 & 4 \end{array} \right) \to \left( \begin{array}{rrrr} 1 & 2 & 0 & 1
  \cr 0 & 0 & 1 & 1 \cr 0 & 0 & 0 & 0  \end{array} \right) = E\sb{A},$$ y el
sistema a resolver es
$$\begin{array}{rcrcrcrcr} x\sb{1} & + & 2x\sb{2} & & & + & x\sb{4} & = & 0, \cr & & & &
  x\sb{3} & + & x\sb{4} & = & 0. \end{array}$$ Dando valores $x\sb{2}=\alpha\sb{1}$ y
$x\sb{4}=\alpha\sb{2}$ a las variables libres, podemos despejar directamente las
variables básicas $$x\sb{1}=-2\alpha\sb{1} -\alpha\sb{2},\qquad x\sb{3}=-\alpha\sb{2}.$$ con
lo que obtenemos el mismo resultado que antes. Por ello, y para evitar
la sustitución hacia atrás, puede resultar más conveniente usar el
procedimiento de Gauss-Jordan para calcular la forma escalonada reducida
por filas $E\sb{A}$ y construir directamente la solución general a partir de
las entradas de $E\sb{A}$.
{{% /example %}}

Consideremos ahora un sistema homogéneo general $[A|\vec{0}]$ de $m$
ecuaciones y $n$ incógnitas. Si la matriz de coeficientes $A$ es de
rango $r$, entonces, por lo que hemos visto antes, habrá $r$ variables
básicas, correspondientes a las posiciones de las columnas básicas de
$A$, y $n-r$ variables libres, que se corresponden con las columnas no
básicas de $A$. Mediante la reducción de $A$ a la forma escalonada
reducida por filas, expresamos las variables básicas en función de las
variables libres (que sustituimos por parámetros) y obtenemos la
**solución general**, de la forma
$$\vec{x} = \alpha\sb{1} \vec{h}\sb{1} + \alpha\sb{2} \vec{h}\sb{2} + \cdots + \alpha\sb{n-r} \vec{h}\sb{n-r},$$
donde $\alpha\sb{1},\ldots, \alpha\sb{n-r}$ son parámetros que pueden tomar
cualquier valor, y donde $\vec{h}\sb{1}, \ldots, \vec{h}\sb{n-r}$ son vectores
columna que representan soluciones particulares del sistema.

Si llamamos $d\sb{1},\ldots,d\sb{n-r}$ a los ı́ndices de las variables libres,
observamos que el vector $\vec{h}\sb{1}$ tiene un 1 en la posición $d\sb{1}$, y
los restantes vectores $\vec{h}\sb{j}$ tienen un cero en esa posición. Lo
mismo se aplica a todos los vectores $\vec{h}\sb{i}$ : tienen un valor 1 en
la posición $d\sb{i}$ y los restantes vectores $\vec{h}\sb{j}$ tienen un cero en
esa posición. Es decir, si formamos una matriz con las columnas
$\vec{h}\sb{1}, \vec{h}\sb{2}, \ldots, \vec{h}\sb{n-r}$, la submatriz formada por las
filas $d\sb{1},\ldots,d\sb{n-r}$ es la matriz identidad de orden $n-r$ (como
podemos comprobar en el ejemplo anterior). Más adelante necesitaremos
esta caracterı́stica.

Resumamos lo deducido hasta ahora sobre la resoluci\
'on de un sistema homogéneo.

{{% example name="Solución general de un sistema homogéneo" %}}
-   Usamos eliminación gaussiana para reducir la matriz ampliada
    $(A|\vec{0})$ a una forma escalonada (o escalonada reducida) por
    filas $(E|\vec{0})$.

-   Identificamos las variables básicas y libres. Si el rango es $r$,
    entonces habrá $r$ variables básicas, correspondientes a las
    posiciones de las columnas básicas de $(A|\vec{0})$, y $n-r$
    variables libres, que se corresponden con las columnas no básicas de
    $(A|\vec{0})$.

-   Sustituimos las variables libres por parámetros, y expresamos las
    variables básicas en función de esos parámetros.

-   Escribimos el resultado de la forma
    $$\vec{x} = \alpha\sb{1} \vec{h}\sb{1} + \alpha\sb{2} \vec{h}\sb{2} + \cdots + \alpha\sb{n-r} \vec{h}\sb{n-r},$$
    donde $\alpha\sb{1},\alpha\sb{2},\ldots,\alpha\sb{n-r}$ son los parámetros que
    corresponden a las variables libres, y
    $\vec{h}\sb{1}, \vec{h}\sb{2}, \ldots, \vec{h}\sb{n-r}$ son vectores columna que
    representan soluciones particulares. Esta será la **solución
    general** del sistema homogéneo.
{{% /example %}}

[]{#metodo:sol-sist-hom label="metodo:sol-sist-hom"}

{{% example name="Ejemplo" %}}
Calculemos la solución general del sistema
$$\begin{array}{rcrcrcr} x\sb{1} &+ & 2x\sb{2} &+ & 2x\sb{3} & = & 0, \cr
    2x\sb{1} & + & 5x\sb{2} & + & 7x\sb{3} & = & 0, \cr 3x\sb{1} & + & 6x\sb{2} & + &
    6x\sb{3} & = & 0.
    \end{array}$$ Se tiene que
$$A = \left( \begin{array}{rrr} 1 & 2 & 2 \cr 2 & 5 & 7 \cr 3 & 6
    & 6 \end{array} \right) \to \left( \begin{array}{rrr} 1 & 0 & -4 \cr 0 & 1 & 3 \cr 0 &
    0 & 0 \end{array} \right) = E\sb{A},$$ de donde $\rg(A) = 2 < n = 3$.
Como las columnas básicas están en las posiciones uno y dos, $x\sb{1}$ y
$x\sb{2}$ son las variables básicas, y $x\sb{3}$ es libre. Escribimos
$x\sb{3}=\alpha$ y despejando de las dos ecuaciones determinadas por $E\sb{A}$
nos queda $x\sb{1} = 4\alpha$ y $x\sb{2} = -3\alpha$. La solución general es
$$\left( \begin{array}{c} x\sb{1} \cr x\sb{2} \cr x\sb{3} \end{array} \right)
    = \alpha \left( \begin{array}{r} 4 \cr -3 \cr 1 \end{array}
    \right), \quad \forall \alpha\in \K.$$
{{% /example %}}

Una última pregunta que nos planteamos es cuándo la solución trivial de
un sistema homogéneo es la única solución. Lo anterior nos muestra la
respuesta. Si hay al menos una variable libre, entonces el sistema
tendrá más de una solución. Por tanto, la solución trivial será la única
solución si y solamente si todas las variables son básicas, es decir,
$r = n$. En este caso, al despejar, todas las variables deberán tomar el
valor cero. Podemos reformular esto diciendo

{{% theorem name="Sistemas homogéneos: solución única" %}}
 Un sistema homogéneo de matriz $A$
es compatible determinado (tiene únicamente la solución trivial) si y
solamente si $\rg(A) = n$.
{{% /theorem %}}

[]{#thm:sist-homog-sol-unica label="thm:sist-homog-sol-unica"}

{{% example name="Ejemplo" %}}
El sistema homogéneo
$$\begin{array}{rcrcrcr} x\sb{1} &+ & 2x\sb{2} &+ & 2x\sb{3} & = & 0, \cr
    2x\sb{1} & + & 5x\sb{2} & + & 7x\sb{3} & = & 0, \cr 3x\sb{1} & + & 6x\sb{2} & + &
    8x\sb{3} & = & 0,
    \end{array}$$ tiene solamente la solución trivial porque
$$A = \left( \begin{array}{rrr} 1 & 2 & 2 \cr 2 & 5 & 7 \cr 3 & 6
    & 8 \end{array} \right) \to \left( \begin{array}{rrr} 1 & 2 & 2 \cr 0 & 1 & 3 \cr 0 &
    0 & 2 \end{array} \right) = E$$ prueba que $\rg(A) = 3 = n$. Se ve
fácilmente que la aplicación de la sustitución hacia atrás desde
$[E|\vec{0}]$ únicamente devuelve la solución trivial.
{{% /example %}}

## Sistemas no homogéneos

Recordemos que un sistema de $m$ ecuaciones y $n$ incógnitas
$$\begin{array}{ccccccccc} a\sb{11} x\sb{1} & + &
a\sb{12} x\sb{2} & + & \ldots & + & a\sb{1n} x\sb{n} & = & b\sb{1}
\cr a\sb{21} x\sb{1} & + & a\sb{22} x\sb{2} & + &
\ldots & + & a\sb{2n} x\sb{n} & = & b\sb{2} \cr & & & & \vdots \cr
a\sb{m1} x\sb{1} & + & a\sb{m2} x\sb{2} & + & \ldots & + & a\sb{mn} x\sb{n} & = &
b\sb{m},
\end{array}$$ es **no homogéneo** cuando $b\sb{i} \ne 0$ para algún $i$. A
diferencia de los sistemas homogéneos, los no homogéneos pueden ser
incompatibles y ya hemos visto que tienen solución si y sólo si la
última columna de la matriz ampliada no es básica.

Veamos que podemos resolver cualquier sistema de ecuaciones lineales (o
demostrar que no tiene solución) aplicando el mismo método que
utilizamos para resolver los sistemas homogéneos. La única diferencia es
que, si hay términos independientes no nulos, estos aparecerán al
expresar las variables básicas en función de las variables libres, como
vemos en el siguiente ejemplo:

{{% example name="Ejemplo" %}}
Consideremos el sistema no homogéneo $$\label{eq:sisnh00}
\begin{array}{rcrcrcrcr}
 x\sb{1} & + & 2x\sb{2} & + & 2x\sb{3} & + & 3x\sb{4} & = & 4, \cr 2x\sb{1} & + & 4x\sb{2}
 & + & x\sb{3} & + & 3x\sb{4} & = & 5, \cr 3x\sb{1} & + & 6x\sb{2} & + & x\sb{3} & + &
 4x\sb{4} & = & 7,
 \end{array}$$ en el que la matriz de coeficientes es la misma que la
matriz de coeficientes de
([\[eq:homog00\]](#eq:homog00){reference-type="ref"
reference="eq:homog00"}). Si $(A|\vec{b})$ se reduce por Gauss-Jordan a
$E\sb{(A|\vec{b})}$, tenemos
$$(A|\vec{b}) = \left( \begin{array}{rrrr|r} 1 & 2 & 2 & 3 & 4
 \cr 2 & 4 & 1 & 3 & 5 \cr 3 & 6 & 1 & 4 & 7 \end{array}
 \right) \mapright{\GJ} \left( \begin{array}{rrrr|r} 1 & 2 & 0 & 1 & 2
 \cr 0 & 0 & 1 & 1 & 1 \cr 0 & 0 & 0 & 0 & 0 \end{array}
 \right) = E\sb{(A|\vec{b})}.$$ Nos queda el sistema equivalente
$$\begin{array}{rcrcrcrcr} x\sb{1} & + & 2x\sb{2} & & & + & x\sb{4} & = & 2, \cr & & & &
 x\sb{3} & + & x\sb{4} & = & 1. \end{array}$$ sustituimos las variables libres
por parámetros, $x\sb{2}=\alpha\sb{1}$, $x\sb{4}=\alpha\sb{2}$, y despejamos las
variables básicas:
$$\begin{array}{rcl} x\sb{1} & = & 2-2\alpha\sb{1}-\alpha\sb{2}, \cr x\sb{2} & = & \alpha\sb{1}, \cr x\sb{3} & = & 1-\alpha\sb{2}, \cr x\sb{4} & = & \alpha\sb{2}.
 \end{array}$$ La solución general la obtenemos escribiendo estas
ecuaciones en la forma $$\begin{aligned}
 \label{eq:sisnh01}
 \left( \begin{array}{c} x\sb{1} \cr x\sb{2} \cr x\sb{3} \cr x\sb{4} \end{array} \right)
 = \left( \begin{array}{c} 2-\alpha\sb{1}-\alpha\sb{2} \cr \alpha\sb{1} \cr 1-\alpha\sb{2} \cr \alpha\sb{2}
 \end{array} \right)
 & = \left( \begin{array}{c} 2 \cr 0 \cr 1 \cr 0
 \end{array}\right)
 + \alpha\sb{1} \left( \begin{array}{r} -2 \cr 1 \cr 0 \cr 0 \end{array}
 \right) + \alpha\sb{2} \left( \begin{array}{r} -1 \cr 0 \cr -1 \cr 1
 \end{array}\right) \cr
 & = \vec{p} + \alpha\sb{1} \vec{h}\sb{1} + \alpha\sb{2} \vec{h}\sb{2} \notag.
\end{aligned}$$ La columna
$$\vec{p} = \left( \begin{array}{r} 2 \cr 0 \cr 1 \cr 0 \end{array}\right)$$
es una *solución particular* del sistema no homogéneo, que se obtiene
cuando $\alpha\sb{1} = 0, \alpha\sb{2} = 0$.

Además, la solución general del sistema homogéneo $$\label{eq:sisnh02}
 \begin{array}{rcrcrcrcr}
 x\sb{1} & + & 2x\sb{2} & + & 2x\sb{3} & + & 3x\sb{4} & = & 0, \cr 2x\sb{1} & + & 4x\sb{2}
 & + & x\sb{3} & + & 3x\sb{4} & = & 0, \cr 3x\sb{1} & + & 6x\sb{2} & + & x\sb{3} & + &
 4x\sb{4} & = & 0,
 \end{array}$$ es
$$\left( \begin{array}{r} x\sb{1} \cr x\sb{2} \cr x\sb{3} \cr x\sb{4} \end{array} \right) =
 \alpha\sb{1} \left( \begin{array}{r} -2 \cr 1 \cr 0 \cr 0 \end{array}
 \right) + \alpha\sb{2} \left( \begin{array}{c} -1 \cr 0 \cr -1 \cr 1
 \end{array}\right).$$ Ası́, vemos que la solución general del sistema
completo ([\[eq:sisnh01\]](#eq:sisnh01){reference-type="ref"
reference="eq:sisnh01"}) es la suma de una solución particular del
sistema completo y de la solución general del sistema homogéneo
([\[eq:sisnh02\]](#eq:sisnh02){reference-type="ref"
reference="eq:sisnh02"}).
{{% /example %}}

Lo anterior nos proporciona un método para resolver un sistema
compatible con matriz ampliada $(A|\vec{b})$, o para demostrar que es
incompatible.

{{% example name="Resolución de un sistema de ecuaciones lineales" %}}
-   Usamos eliminación gaussiana para reducir la matriz ampliada
    $(A|\vec{b})$ a una forma escalonada (o escalonada reducida) por
    filas $(E|\vec{c})$.

-   Si la última columna de $(E|\vec{c})$ es básica, el sistema es
    **incompatible** y el método ha terminado.

-   Si la última columna de $(E|\vec{c})$ no es básica, identificamos las
    variables básicas y las libres.

-   Sustituimos las variables libres por parámetros, y expresamos las
    variables básicas en función de esos parámetros.

-   Escribimos el resultado en la forma
    $$\vec{x} = \vec{p} + \alpha\sb{1} \vec{h}\sb{1} + \alpha\sb{2} \vec{h}\sb{2} + \cdots + \alpha\sb{n-r} \vec{h}\sb{n-r},$$
    donde

    -   $\alpha\sb{1},\alpha\sb{2},\ldots,\alpha\sb{n-r}$ son parámetros que
        corresponden a las variables libres, y

    -   $\vec{p}, \vec{h}\sb{1}, \vec{h}\sb{2}, \ldots, \vec{h}\sb{n-r}$ son vectores
        columna de orden $n$.

    Entonces $\vec{p}$ es una solución particular del sistema de matriz
    $(A|\vec{b})$ y
    $$\alpha\sb{1} \vec{h}\sb{1} + \alpha\sb{2} \vec{h}\sb{2} + \cdots + \alpha\sb{n-r} \vec{h}\sb{n-r}$$
    es la solución general del sistema homogéneo de matriz $(A|\vec{0})$.
    Esta será la **solución general** del sistema original.
{{% /example %}}

Como los parámetros $\alpha\sb{i}$ pueden tomar todos los posibles valores
en el cuerpo $\K$, la solución general describe todas las posibles
soluciones del sistema $(A|\vec{b})$.

Veamos otro ejemplo de este método.

{{% example name="Ejemplo" %}}
Calculemos la solución general del sistema
$$\begin{array}{rcrcrcrcrcr} x\sb{1} & + & x\sb{2} & + & 2x\sb{3} & + & 2x\sb{4}
   & + & x\sb{5} & = & 1, \cr 2x\sb{1} & + & 2x\sb{2} & + & 4x\sb{3} & + & 4x\sb{4}
   & + & 3x\sb{5} & = & 1, \cr 2x\sb{1} & + & 2x\sb{2} & + & 4x\sb{3} & + & 4x\sb{4}
   & + & 2x\sb{5} & = & 2, \cr 3x\sb{1} & + & 5x\sb{2} & + & 8x\sb{3} & + & 6x\sb{4}
   & + & 5x\sb{5} & = & 3,
   \end{array}$$ y la comparamos con la solución general del sistema
homogéneo asociado.

En primer lugar, calculamos la forma escalonada reducida por filas de la
matriz ampliada $(A|\vec{b})$.
$$\begin{array}{rclcl} (A|\vec{b}) & = & \left( \begin{array}{rrrrr|r}
   1 & 1 & 2 & 2 & 1 & 1 \cr
   2 & 2 & 4 & 4 & 3 & 1 \cr
   2 & 2 & 4 & 4 & 2 & 2 \cr
   3 & 5 & 8 & 6 & 5 & 3 \end{array} \right) & \to & \left( \begin{array}{rrrrr|r}
   1 & 1 & 2 & 2 & 1 & 1 \cr
   0 & 0 & 0 & 0 & 1 & -1 \cr
   0 & 0 & 0 & 0 & 0 & 0 \cr
   0 & 2 & 2 & 0 & 2 & 0 \end{array} \right) \cr & \to & \left( \begin{array}{rrrrr|r}
   1 & 1 & 2 & 2 & 1 & 1 \cr
   0 & 2 & 2 & 0 & 2 & 0 \cr
   0 & 0 & 0 & 0 & 1 & -1 \cr
   0 & 0 & 0 & 0 & 0 & 0 \end{array} \right) & \to &
   \left( \begin{array}{rrrrr|r}
   1 & 1 & 2 & 2 & 1 & 1 \cr
   0 & 1 & 1 & 0 & 1 & 0 \cr
   0 & 0 & 0 & 0 & 1 & -1 \cr
   0 & 0 & 0 & 0 & 0 & 0 \end{array} \right) \cr
   & \to & \left( \begin{array}{rrrrr|r}
   1 & 0 & 1 & 2 & 0 & 1 \cr
   0 & 1 & 1 & 0 & 1 & 0 \cr
   0 & 0 & 0 & 0 & 1 & -1 \cr
   0 & 0 & 0 & 0 & 0 & 0 \end{array} \right) & \to & \left( \begin{array}{rrrrr|r}
   1 & 0 & 1 & 2 & 0 & 1 \cr
   0 & 1 & 1 & 0 & 0 & 1 \cr
   0 & 0 & 0 & 0 & 1 & -1 \cr
   0 & 0 & 0 & 0 & 0 & 0 \end{array} \right) = E\sb{(A|\vec{b})}.
   \end{array}$$ El sistema es compatible, pues la última columna es no
básica (no tiene pivote). Las variables básicas son $x\sb{1},x\sb{2},x\sb{5}$ y las
variables libres son $x\sb{3},x\sb{4}$. Sustituimos las variables libres por
parámetros, $x\sb{3}=\alpha\sb{1}$, $x\sb{4}=\alpha\sb{2}$, y despejamos las variables
básicas en función de estos parámetros para obtener
$$\begin{array}{rcrrr} x\sb{1} & = & 1 & -\alpha\sb{1} & -2\alpha\sb{2} \cr x\sb{2} & = & 1& -\alpha\sb{1} \cr
   x\sb{3} & = & & \alpha\sb{1} \cr x\sb{4} & = & & & \alpha\sb{2} \cr x\sb{5}
   & = & -1
   \end{array}$$ La solución general del sistema no homogéneo es
$$\begin{aligned}
   \vec{x}
   = \left( \begin{array}{c} x\sb{1} \cr x\sb{2} \cr x\sb{3} \cr x\sb{4} \cr
   x\sb{5} \end{array} \right)
   = \left( \begin{array}{c} 1-\alpha\sb{1}-2\alpha\sb{2} \cr 1-\alpha\sb{1} \cr \alpha\sb{1} \cr \alpha\sb{2} \cr
   -1 \end{array} \right)
   & = \left( \begin{array}{r} 1 \cr 1 \cr 0 \cr 0 \cr
   -1 \end{array} \right) + \alpha\sb{1} \left( \begin{array}{r} -1 \cr -1 \cr 1 \cr 0 \cr
   0 \end{array} \right) + \alpha\sb{2} \left( \begin{array}{r} -2 \cr 0 \cr 0 \cr 1
   \cr 0 \end{array} \right)
   \cr & = \vec{p} + \alpha\sb{1} \vec{h}\sb{1} + \alpha\sb{2} \vec{h}\sb{2}.
\end{aligned}$$ La solución general del sistema homogéneo asociado es
$$\vec{x} = \left( \begin{array}{c} x\sb{1} \cr x\sb{2} \cr x\sb{3} \cr x\sb{4} \cr
   x\sb{5} \end{array} \right) = \left( \begin{array}{c} -\alpha\sb{1}-2\alpha\sb{2} \cr -\alpha\sb{1} \cr \alpha\sb{1} \cr \alpha\sb{2} \cr
   0 \end{array} \right) = \alpha\sb{1} \left( \begin{array}{r} -1 \cr -1 \cr 1 \cr 0 \cr
   0 \end{array} \right) + \alpha\sb{2} \left( \begin{array}{r} -2 \cr 0 \cr 0 \cr 1
   \cr 0 \end{array} \right)=\alpha\sb{1} \vec{h}\sb{1} + \alpha\sb{2} \vec{h}\sb{2}.$$
{{% /example %}}

El método que descrito nos da una solución particular $\vec{p}$ del
sistema completo, a la que sumamos la solución general del sistema
homogéneo para obtener la solución general del sistema completo. Veamos
que, en realidad, podrı́amos tomar cualquier otra solución particular del
sistema completo.

{{% theorem name="Soluciones de un sistema compatible" %}}
 Sea ${\cal S}$ un sistema lineal
compatible de matriz ampliada $(A|\vec{b})$ y sea ${\cal S}^h$ el sistema
lineal homogéneo de matriz ampliada $(A|\vec{0})$. Si $\vec{p}$ es una
solución particular de ${\cal S}$ entonces el conjunto de las soluciones
de ${\cal S}$ es
$$\left\\{\vec{p}+\vec{h}\mid\ \vec{h}\mbox{ es solución del sistema homogéneo }{\cal S}^h\right\\} .$$
{{% /theorem %}}

{{% proof %}}
Supongamos que el sistema $\mathcal{S}$ está representado por la
ecuación matricial
$A\sb{m \times n} \vec{x}\sb{n \times 1} = \vec{b}\sb{m \times 1}$, y llamemos
$\mathcal{S}^h$ al sistema homogéneo asociado, $A\vec{x}=\vec{0}$.

Consideremos un elemento $\vec{p}+\vec{h}$ del conjunto descrito en el
enunciado. Como $\vec{p}$ es una solución de $\mathcal{S}$, se tiene
$A \vec{p} = \vec{b}$. Y como $\vec{h}$ es una solución de $\mathcal{S}^h$,
entonces $A \vec{h} = \vec{0}$. Por tanto,
$$A(\vec{p} + \vec{h}) = A \vec{p} + A \vec{h} = \vec{b} + \vec{0} = \vec{b},$$
lo que implica que $\vec{p} + \vec{h}$ es una solución de $\mathcal{S}$.

Recı́procamente, sea $\vec{q}$ una solución del sistema $\mathcal{S}$.
Entonces
$$A (\vec{q} - \vec{p}) = A \vec{q} - A \vec{p} = \vec{b} - \vec{b} = \vec{0},$$
por lo que $\vec{q} - \vec{p}$ es una solución del sistema homogéneo
$\mathcal{S}^h$. Tenemos entonces $\vec{q} = \vec{p} + (\vec{q} - \vec{p})$,
es decir, $\vec{q}$ es de la forma $\vec{p} + \vec{h}$, donde $\vec{h}$ es
una solución del sistema homogéneo $\mathcal S^h$.

En consecuencia, el conjunto de soluciones del sistema $\mathcal S$ es
el que describe el enunciado.
{{% /proof %}}

Ahora nos hacemos una pregunta análoga al caso homogéneo: ¿cuándo un
sistema compatible tiene solución única?. Por el resultado anterior,
vemos que el sistema no homogéneo tendrá solución única si y sólo si el
conjunto descrito tiene un único elemento, es decir, si y sólo si el
sistema homogéneo asociado tiene solución única (la solución trivial).
Por tanto:

{{% theorem name="Sistemas no homogéneos: solución única" %}}
 Un sistema *compatible* de matriz
ampliada $(A|\vec{b})$ tendrá una única solución si y solamente si no hay
variables libres, esto es, si y solamente si $\rg (A)=n$. Esto es lo
mismo que decir que el sistema homogéneo asociado $(A|\vec{0})$ tiene
únicamente la solución trivial.
{{% /theorem %}}

{{% example name="Ejemplo" %}}
Consideremos el siguiente sistema no homogéneo:
$$\begin{array}{rcrcrcr} 2x\sb{1} & + & 4x\sb{2} & + & 6x\sb{3} & = & 2,\cr
  x\sb{1} & + & 2x\sb{2} & + & 3x\sb{3} & = & 1, \cr x\sb{1} & & & + & x\sb{3} & = &
  -3, \cr 2x\sb{1} & + & 4x\sb{2} & & & = & 8.
  \end{array}$$ La forma escalonada reducida por filas de $(A|\vec{b})$
es $$(A|\vec{b}) = \left( \begin{array}{rrr|r} 2 & 4 & 6 & 2 \cr 1
  & 2 & 3 & 1 \cr 1 & 0 & 1 & -3 \cr 2 & 4 & 0 & 8
  \end{array} \right) \to \left( \begin{array}{rrr|r} 1 & 0 & 0 & -2 \cr 0
  & 1 & 0 & 3 \cr 0 & 0 & 1 & -1 \cr 0 & 0 & 0 & 0
  \end{array} \right) = E\sb{(A|\vec{b})}.$$ El sistema es compatible
porque la última columna no es básica. Como además $\rg(A) = 3$ es igual
al número de incógnitas (no hay variables libres), el sistema homogéneo
asociado tiene únicamente la solución trivial, y la solución del sistema
original es
$$\vec{p} = \left( \begin{array}{r} -2 \cr 3 \cr -1 \end{array} \right).$$
{{% /example %}}

Como resumen de todo lo anterior, hemos probado el siguiente teorema:

{{% theorem name="Teorema de Rouché-Frobenius" %}}
Dado un sistema lineal con $m$ ecuaciones y $n$ incógnitas y matriz
ampliada $(A|\vec{b})$, se tiene:

-   El sistema es *incompatible* si y solamente si
    $\rg (A)<\rg (A|\vec{b})$.

-   El sistema es *compatible determinado* si y solamente si
    $\rg (A)=\rg (A|\vec{b})=n$.

-   El sistema es *compatible indeterminado* si y solamente si
    $\rg (A)=\rg (A|\vec{b})<n$.
{{% /theorem %}}
