+++
title = "Subespacios vectoriales"
weight = 60
+++



## Definiciones

Una vez que tenemos la estructura de $\K$-espacio vectorial sobre un
conjunto $V$, el siguiente paso es estudiar los subconjuntos de $V$ que
tienen esas mismas propiedades. Es análogo a lo que hacemos con grupos y
subgrupos. En este capı́tulo veremos cómo estos subconjuntos, que
llamaremos *variedades lineales vectoriales* o *subespacios
vectoriales*, también son espacios vectoriales, y estudiaremos sus
propiedades. La definición precisa es la siguiente:

{{% definition %}}
Subespacio vectorial Sea $V$ un espacio vectorial sobre un cuerpo $\K$,
y sea $W$ un subconjunto de $V$. Diremos que $W$ es un **subespacio
vectorial** o una **variedad lineal** de $V$ si, con las mismas
operaciones de suma y producto por escalar, $W$ es un espacio vectorial
sobre $\K$.
{{% /definition %}}

{{% example name="Ejemplo" %}}
1.  Dado un espacio vectorial $V$, los conjuntos $\{ \vec{0} \}$ y $V$
    son subespacios vectoriales. Decimos que un subespacio vectorial es
    **propio** si es distinto de estos casos.

2.  Dada $A\sb{m \times n}$ con coeficientes en un cuerpo $\K$, el
    conjunto de soluciones del sistema homogéneo $A \vec{x} = \vec{0}$ es
    un subespacio vectorial de $\K^n$. Se denota como $\nulo(A)$.

3.  El conjunto de polinomios de $\K[x]$ de grado menor o igual que $n$
    es un subespacio vectorial de $\K[x]$.

4.  Sea $V$ un espacio vectorial de dimensión finita $n$ y $\mathcal{B}$
    una base. Si $W \subset V$ es un subespacio vectorial, entonces
    $c\sb{\mathcal{B}}(W)$ es un subespacio vectorial de $\K^n$.
{{% /example %}}

En la práctica, para saber si un subconjunto $W$ de un espacio vectorial
$V$ es un subespacio vectorial, no es necesario comprobar que cumple
todas las propiedades de los $\K$-espacios vectoriales. Muchas de estas
propiedades se cumplen de forma inmediata porque los elementos de $W$
son también elementos de $V$. En realidad, lo único que es necesario
comprobar, si $W$ es no vacío, es que la suma y el producto por escalar
en $V$, cuando se restringen a vectores de $W$, son una suma y un
producto por escalar en $W$:

{{% theorem name="Caracterización de subespacio vectorial" %}}
Dado un $\K$-espacio vectorial $V$, un subconjunto $W\subset V$ es un
subespacio vectorial de $V$ si y solo si se cumplen las siguientes
propiedades:

1.  $W$ es no vacío.

2.  Para todo $\vec{v},\vec{w} \in W$, se tiene que $\vec{v}+\vec{w} \in W$.

3.  Para todo $\alpha \in \K$ y $\vec{v} \in W$, se tiene que
    $\alpha \vec{v}\in W$.
{{% /theorem %}}

{{% proof %}}
Es evidente que, si $W$ es un subespacio vectorial de $V$, cumple las
tres propiedades del enunciado.

Supongamos que $W$ es un subconjunto que cumple esas tres propiedades.
En primer lugar, la suma y el producto por escalar definidos para los
vectores de $V$ son también operaciones que, aplicadas a vectores de
$W$, dan como resultado vectores de $W$ (por las propiedades 2 y 3). Por
tanto, son operaciones bien definidas, luego $W$ será espacio vectorial
sobre $\K$ si esas dos operaciones cumplen todas las propiedades
requeridas en la definición de espacio vectorial.

Observemos que la tercera propiedad del enunciado, aplicada tomando
$\alpha=0$ y un vector cualquiera de $W$ (recordemos que $W$ es no
vacío), nos dice que el elemento neutro $\vec{0}\in V$ pertenece a $W$,
luego $W$ admite un elemento neutro de la suma de vectores: el mismo
vector $\vec{0}$ de $V$. Además, la misma propiedad aplicada tomando
$\alpha=-1$ nos dice que el opuesto de un vector cualquiera de $W$ es un
vector de $W$. El resto de propiedades de la definición de espacio
vectorial se obtienen de forma inmediata, puesto que los vectores de $W$
son también vectores de $V$.
{{% /proof %}}

En muchas ocasiones, cuando tenemos algunos vectores de un espacio $V$,
nos interesa encontrar un subespacio vectorial que los contenga y que
sea lo más pequeño posible: un subespacio determinado por esos vectores.
Por definición, si unos vectores pertenecen a un subespacio vectorial
$W$, cualquier combinación vectorial de esos vectores debe pertenecer
también a $W$. Veamos que estas combinaciones lineales son los únicos
vectores que necesitamos tener en el subespacio que buscamos.

{{% definition %}}
Subespacio generado por un conjunto o sistema Sea $V$ un $\K$-espacio
vectorial y sea $S$ un sistema o subconjunto de $V$. El conjunto de
combinaciones lineales de los vectores de $S$, que notaremos
$\langle S\rangle$, se denomina **subespacio generado** por $S$. Es
decir, si $S$ es no vacío:
$$\langle S \rangle = \{ \lambda\sb{1}\vec{v}\sb{1}+\cdots +\lambda\sb{m}\vec{v}\sb{m} \ |\ \lambda\sb{1},\ldots \lambda\sb{m}\in \K,\ \vec{v}\sb{1},\ldots ,\vec{v}\sb{m}\in S\}.$$
Por convenio, si $S=\varnothing$ entonces
$\langle S\rangle = \{\vec{0}\}$.
{{% /definition %}}

El conjunto $\langle S \rangle$ es efectivamente un subespacio vectorial
de $V$. Sean $\vec{v}, \vec{w} \in \langle S \rangle$. Entonces estos
vectores tienen una expresión de la forma
$$\vec{v} = \sum\sb{i=1}^m \lambda\sb{i} \vec{v}\sb{i},\quad \vec{w} = \sum\sb{j=1}^k \mu\sb{j} \vec{w}\sb{j}, \quad \text{ donde } \lambda\sb{i}, \mu\sb{j} \in \K, \vec{v}\sb{i}, \vec{w}\sb{j} \in S.$$
La suma se puede escribir
$$\vec{v} + \vec{w} = \lambda\sb{1}\vec{v}\sb{1}+\cdots +\lambda\sb{m}\vec{v}\sb{m} + \mu\sb{1}\vec{w}\sb{1}+\cdots +\mu\sb{k}\vec{w}\sb{k},$$
que es una combinación lineal finita de elementos de $S$, es decir,
pertenece a $\langle S \rangle$. Análogamente,
$$\alpha \vec{v} = (\alpha \lambda\sb{1}) \vec{v}\sb{1}+\cdots + (\alpha \lambda\sb{m}) \vec{v}\sb{m},$$
que también es una combinación lineal finita de elementos de $S$.

{{% example name="Ejemplo" %}}
Dada una matriz $A\sb{m \times n}$ con coeficientes en un cuerpo $\K$, el
subespacio de $\K^m$ generado por las columnas de $A$ se denota como
$\col(A)$.
{{% /example %}}

{{% remark %}}
A partir de la definición de $\langle S \rangle$, tenemos que $\vec{v}$
depende linealmente de un sistema o conjunto $S$ si y solamente si
$\vec{v} \in \langle S \rangle$.
{{% /remark %}}

{{% theorem name="Propiedades de los conjuntos generadores" %}}
Sean $S$ y $T$ subconjuntos de $V$. Entonces:

1.  $S \subset \langle S \rangle$.

2.  $S = \langle S \rangle$ si y solamente si $S$ es un subespacio
    vectorial.

3.  Si $S \subset T$ entonces
    $\langle S \rangle \subset \langle T \rangle$.
{{% /theorem %}}

{{% proof %}}
1.  Todo elemento de $S$ es una combinación lineal de sí mismo, luego
    pertenece a $\langle S\rangle$.

2.  Si $S=\langle S\rangle$, como $\langle S\rangle$ es un subespacio
    vectorial, entonces $S$ es un subespacio vectorial. Recíprocamente,
    si $S$ es un subespacio vectorial, toda combinación lineal de
    elementos de $S$ pertenece a $S$, luego
    $\langle S\rangle \subset S$. La otra inclusión viene dada por el
    apartado anterior, luego $S=\langle S\rangle$.

3.  Si $\vec{v}\in \langle S\rangle$, entonces es combinación lineal de
    los vectores de $S$. Pero como $S\subset T$, $\vec{v}$ es combinación
    lineal de los vectores de $T$, es decir,
    $\vec{v} \in \langle T\rangle$.
{{% /proof %}}

{{% theorem name="Carácter minimal del subespacio generado" %}}
 Dado un sistema o conjunto de
vectores $S$ de un espacio vectorial $V$, el subespacio vectorial
$\langle S \rangle$ es el menor subespacio que contiene a $S$. Es decir,
si $W$ es un subespacio vectorial que contiene a $S$, entonces
$\langle S\rangle \subset W$.
{{% /theorem %}}

{{% proof %}}
Si un subespacio vectorial $W$ contiene a $S$, es decir, si
$S\subset W$, entonces $\langle S\rangle \subset \langle W\rangle =W$.
{{% /proof %}}

## Dimensión de subespacios

Una propiedad importante a tener en cuenta es la siguiente:

{{% theorem name="Independencia lineal en subespacios" %}}
 Sea $W$ un subespacio vectorial de
$V$. Un sistema de vectores $S\subset W$ es linealmente independiente en
$W$ si y solo si es linealmente independiente en $V$.
{{% /theorem %}}

{{% proof %}}
Dado un sistema finito de vectores
$\{\vec{w}\sb{1},\ldots,\vec{w}\sb{r}\}\subset W$, la ecuación
$$x\sb{1}\vec{w}\sb{1}+\cdots+x\sb{r}\vec{w}\sb{r}=\vec{0}$$ tiene las mismas soluciones
tanto si consideramos los vectores en el espacio vectorial $W$ como si
los consideramos en $V$. Por tanto, el sistema es linealmente
independiente en $W$ (la ecuación tiene solución única) si y solo si lo
es en $V$. Si $S$ es infinito, la independencia o dependencia lineal de
$S$ se obtiene a partir de sus subconjuntos finitos, por tanto también
se tiene el resultado en este caso.
{{% /proof %}}

{{% theorem name="Dimensión de subespacios" %}}
Sea $V$ un $\K$-espacio vectorial de dimensión finita y $W \subset V$ un
subespacio vectorial. Entonces

1.  $W$ es de dimensión finita y $\dim W \le \dim V$.

2.  Además, $\dim W = \dim V$ si y solamente si $W = V$.
{{% /theorem %}}

{{% proof %}}
Sea $n = \dim V$.

1.  Supongamos que $W$ tiene dimensión infinita. En este caso, ningún
    sistema finito de vectores de $W$ puede ser sistema de generadores.
    Por tanto, cualquier sistema finito de vectores de $W$ linealmente
    independiente puede ampliarse con un vector que no dependa
    linealmente del sistema. Esto implica que podemos tener sistemas de
    vectores linealmente independientes de tamaño tan grande como
    queramos. Tomemos un sistema de vectores $S\subset W$, linealmente
    independiente, de tamaño $n+1$. Por el resultado anterior, $S$ es un
    sistema de $n+1$ vectores de $V$ linealmente independiente, lo que
    es imposible porque $\dim(V)=n$. Por tanto, $\dim(W)<\infty$.

    Sea ahora $B$ una base (necesariamente finita) de $W$. Al ser base,
    $B$ es linealmente independiente en $W$, y por tanto es linealmente
    independiente en $V$. Por tanto $\#(S)\leq n$, es decir,
    $\dim(W)\leq \dim(V)$.

2.  Supongamos que $\dim W = \dim V= n$. Dada una base $B$ de $W$, será
    un sistema linealmente independiente en $W$ formado por $n$ vectores
    de $W$. Por tanto será un sistema linealmente independiente en $V$
    formado por $n$ vectores de $V$, luego es base de $V$ (porque
    $\dim(V)=n$). Esto implica que $W=\langle B\rangle =V$.

    El recíproco es evidente.
{{% /proof %}}

{{% remark %}}
Sea $V$ un $\K$-espacio vectorial de dimensión finita $n$, donde tomamos
una base $\mathcal{B}$ y $W \subset V$ un subespacio. Por lo anterior,
$W$ es de dimensión finita y existe una base $\mathcal{B}\sb{W}$, con
$r \le n$ elementos. Si un vector $\vec{w}$ pertenece a $W$, entonces
tenemos dos formas de describirlo mediante coordenadas. Por una parte,
mediante $[\vec{w}]\sb{\mathcal{B}}$, las coordenadas respecto a
$\mathcal{B}$, que es un vector columna de $\K^n$. Por otra, mediante
$[\vec{w}]\sb{\mathcal{B}\sb{W}}$, que es un vector columna de $\K^r$.
{{% /remark %}}

{{% theorem name="Cálculo de dimensión de un subespacio" %}}
 En un $\K$-espacio vectorial $V$
de dimensión finita, sea ${\mathcal B}$ una base de $V$, $S$ un sistema
finito de vectores de $V$, y $A\sb{S,{\mathcal B}}$ la matriz cuyas
columnas son las coordenadas de los vectores de $S$ respecto a
${\mathcal B}$. Entonces
$$\dim \langle S \rangle =\rg(A\sb{S,{\mathcal B}})$$ y una base del
subespacio $\langle S \rangle$ está determinada por las columnas básicas
de $A\sb{S,\mathcal{B}}$.
{{% /theorem %}}

{{% proof %}}
[]{#thm:calcdim label="thm:calcdim"} Sea
$S= \{ \vec{w}\sb{1}, \ldots, \vec{w}\sb{p} \}$. Si $\rg(A\sb{S,{\mathcal B}}) = r$,
sean $[\vec{w}\sb{i\sb{1}}]\sb{\mathcal B}, \ldots, [\vec{w}\sb{i\sb{r}}]\sb{\mathcal B}$
las columnas básicas de la matriz. Sabemos que estas columnas básicas
forman un sistema linealmente independiente en $\K^n$. Entonces el
sistema $\{ \vec{w}\sb{i\sb{1}}, \ldots, \vec{w}\sb{i\sb{r}} \}$ es linealmente
independiente en $V$, por el morfismo de coordenadas, luego es
linealmente independiente en $\langle S\rangle$. Por otro lado, las
columnas no básicas de la matriz son combinación lineal de las básicas,
por lo que todos los vectores de $S$ dependen linealmente de
$\{ \vec{w}\sb{i\sb{1}}, \ldots, \vec{w}\sb{i\sb{r}} \}$. Si
$\vec{w} \in \langle S \rangle$, entonces $\vec{w}$ depende linealmente de
$S$. Por el carácter transitivo de la dependencia, el vector $\vec{w}$
depende linealmente de $\{ \vec{w}\sb{i\sb{1}}, \ldots, \vec{w}\sb{i\sb{r}} \}$, y
tenemos que este conjunto es generador de $\langle S \rangle$.
{{% /proof %}}

{{% theorem name="Dimensión de $\col(A)$" %}}
 Sea $A\sb{m \times n}$ una matriz con coeficientes
en $\K$. Entonces $\dim \col(A) = \rg(A)$ y una base de $\col(A)$ está
formada por las columnas básicas.
{{% /theorem %}}

{{% proof %}}
Sea $V = \K^m$ y $\mathcal{S}$ su base estándar. Si $S$ es el conjunto
de columnas de $A$, entonces $\col(A) = \langle S \rangle$. Además, la
matriz de coordenadas de los vectores de $S$ respecto a $\mathcal{S}$ es
la propia matriz $A$. Por el teorema anterior, tenemos que
$$\dim \col(A) = \dim \langle S \rangle = \rg(A).$$ Además, una base de
$\col(A)$ está formada por las columnas básicas de $A$.
{{% /proof %}}

{{% remark %}}
[]{#rem:rango-izq label="rem:rango-izq"} Observemos que el rango de la
matriz $A\sb{S,{\mathcal B}}$ **no depende** de la base ${\mathcal B}$, ya
que es igual al rango del conjunto de vectores $S$, que está definido
sin tener que recurrir a ninguna base. Entonces, si $\mathcal{B}$ y
$\mathcal{B}'$ son dos bases distintas, cuya matriz de cambio de base es
$M({\mathcal B}, {\mathcal B}')$, entonces se tiene
$$A\sb{S,{\mathcal B}}= M({\mathcal B}',{\mathcal B}) A\sb{S,{\mathcal B}'}$$
y
$$\rg(A\sb{S, \mathcal{B}'}) = \dim(\langle S \rangle) = \rg( A\sb{S,\mathcal{B}}),$$
de donde
$\rg(M({\mathcal B}',{\mathcal B}) A\sb{S,{\mathcal B}'}) = \rg(A\sb{S,\mathcal{B}'})$.
En términos matriciales, se verifica que
$\rg(A\sb{m \times n}) = \rg(P A\sb{m \times n})$ para cualquier matriz $P$
no singular.
{{% /remark %}}

{{% example name="Ejemplo" %}}
Sea $S = \{ 1-x, 1+x^2, 1-2x-x^2 \} \subset V = \R[x]\sb{2}$, el espacio
vectorial de polinomios reales de grado menor o igual que $3$. Fijamos
la base estándar $\mathcal{B} = \{ 1, x, x^2 \}$ en $V$ y formamos la
matriz
$$A\sb{S,\mathcal{B}} = \left( \begin{array}{rrr} 1 & 1 & 1 \cr -1 & 0 & -2 \cr 0 & 1 & -1 \end{array} \right).$$
Dado que
$$A\sb{S, \mathcal{B}} \mapright{\GJ} \left( \begin{array}{rrr} 1 & 0 & 2 \cr 0 & 1 & -1 \cr 0 & 0 & 0 \end{array} \right),$$
tenemos que una base del subespacio $\langle S \rangle$ está formada por
$\{ 1-x, 1+x^2 \}$ y
$\dim \langle S \rangle = \rg(A\sb{S,\mathcal{B}}) = 2$.
{{% /example %}}

## Ecuaciones paramétricas de un subespacio

Hasta ahora solamente hemos visto una forma de determinar un subespacio
vectorial: mediante un sistema de generadores. Esta forma es equivalente
a dar unas *ecuaciones paramétricas*, que es una forma de ver el espacio
$W$ como su imagen en un cierto $\K^n$.

Sea $W$ un subespacio vectorial de un espacio vectorial $V$ de dimensión
$n$, y sea $G=\{\vec{v}\sb{1}, \ldots, \vec{v}\sb{p}\}$ un sistema de generadores
de $W$. Supongamos que las coordenadas de $\vec{v}\sb{i}$ respecto a una base
${\mathcal B}$ de $V$ son
$$[\vec{v}\sb{i}]\sb{\mathcal B} = \left( \begin{array}{c} \alpha\sb{1i} \cr \vdots \cr \alpha\sb{ni} \end{array} \right), \qquad i=1,\ldots,p.$$
Entonces, como todo vector $\vec{v} \in W$ se escribe como combinación
lineal de $G$, existirán unos escalares $\lambda\sb{1},\ldots,\lambda\sb{p}$
tales que $$\vec{v} = \lambda\sb{1} \vec{v}\sb{1} + \cdots + \lambda\sb{p} \vec{v}\sb{p},$$
por lo que si llamamos $[\vec{v}]\sb{\mathcal B} = (x\sb{1}, \ldots, x\sb{n})^t$ se
tienen las relaciones $$\label{eq:param}
\left\\{\begin{array}{ccccccc}
 x\sb{1} & = & \alpha\sb{11} \lambda\sb{1} &+& \cdots &+& \alpha\sb{1p}\lambda\sb{p} \cr
x\sb{2} & = & \alpha\sb{21} \lambda\sb{1} &+& \cdots &+& \alpha\sb{2p}\lambda\sb{p} \cr
\vdots & & \vdots & &  & & \vdots \cr
x\sb{n} & = & \alpha\sb{n1} \lambda\sb{1} &+& \cdots &+& \alpha\sb{np}\lambda\sb{p} \cr
\end{array}\right.$$ Unas ecuaciones de este tipo, donde los escalares
$\lambda\sb{i}$ son parámetros indeterminados, se llaman unas **ecuaciones
paramétricas** de $W$ respecto de la base $\mathcal B$. En otras
palabras, unas ecuaciones paramétricas nos dicen cómo son las
coordenadas de un vector cualquiera de $W$, dependiendo de los
coeficientes que tomemos en la combinación lineal de los generadores. Si
cambiamos los generadores, obtenemos otras ecuaciones paramétricas.

Recı́procamente, unas ecuaciones del tipo
([\[eq:param\]](#eq:param){reference-type="ref" reference="eq:param"})
definen un subespacio vectorial generado por los vectores de coordenadas
respecto de la base ${\mathcal B}$ que aparecen como coeficientes de los
parámetros $\lambda\sb{i}$.

{{% example name="Ejemplo" %}}
En $\R^3$ y con respecto a una base ${\mathcal B}$ fijada, consideremos
el subespacio vectorial $W$ de ecuaciones paramétricas
$$\left\\{\begin{array}{l}
 x\sb{1}= 2 \lambda _1 - 3\lambda _2 \cr
 x\sb{2} = \lambda _1 + 5\lambda _2\cr
 x\sb{3}= \lambda\sb{1}-\lambda\sb{2}
\end{array}\right.$$ En este caso se trata del subespacio generado por
los vectores con coordenadas $(2,1,1)^t$ y $(-3,5,-1)^t$.
{{% /example %}}

Las ecuaciones paramétricas, en el fondo, equivalen a definir un
subespacio vectorial dando un sistema de generadores. Veamos ahora cómo,
a partir de unas ecuaciones paramétricas de un subespacio vectorial,
podemos calcular la dimensión del subespacio. La idea es sencilla, pues
sabemos que a partir de unas ecuaciones paramétricas podemos calcular un
sistema generador y, dados unos generadores, podemos obtener la
dimensión mediante el cálculo del rango de una matriz. Formalicemos este
razonamiento.

Sea $V$ un espacio vectorial de dimensión $n$, con base fijada
${\mathcal B}$, donde tenemos unas ecuaciones paramétricas
$$\left\\{\begin{array}{ccc}
x\sb{1} & = & \alpha\sb{11} \lambda\sb{1} + \cdots + \alpha\sb{1p}\lambda\sb{p} \cr
x\sb{2} & = & \alpha\sb{21} \lambda\sb{1} + \cdots + \alpha\sb{2p}\lambda\sb{p} \cr
\vdots & & \vdots \cr
x\sb{n} & = & \alpha\sb{n1} \lambda\sb{1} + \cdots + \alpha\sb{np}\lambda\sb{p} \cr
\end{array}\right.$$ de un subespacio vectorial $W$. Construimos la
matriz $$A\sb{W}=\left(\begin{matrix}
 \alpha\sb{11} & \alpha\sb{12} & \cdots & \alpha\sb{1p} \cr
 \alpha\sb{21} & \alpha\sb{22} & \cdots & \alpha\sb{2p} \cr
 \vdots & \vdots & & \vdots \cr
\alpha\sb{n1} & \alpha\sb{n2} & \cdots & \alpha\sb{np} \cr
\end{matrix}\right),$$ que es la matriz de coordenadas de unos vectores
generadores de $W$ respecto de la base ${\mathcal B}$. Entonces
$$\dim W = \rg(A\sb{W}).$$ como ya sabemos del teorema de cálculo de la
dimensión de un subespacio (pág. ).

## Ecuaciones implı́citas de un subespacio

Fijemos una base ${\mathcal B}$ en un espacio vectorial $V$ de dimensión
$n$, con respecto a la cual tomamos coordenadas. Sabemos que el conjunto
de soluciones de un sistema lineal homogéneo de la forma
$$\left\\{\begin{array}{ccccccccl}
    a\sb{11} x\sb{1} & + & a\sb{12} x\sb{2} & + & \cdots & + & a\sb{1n} x\sb{n} & = & 0 \cr
    a\sb{21} x\sb{1} & + & a\sb{22} x\sb{2} & + & \cdots & + & a\sb{2n} x\sb{n} & = & 0 \cr
        \vdots  & &    \vdots   &  &         &   &   \vdots   &   & \vdots \cr
    a\sb{m1} x\sb{1} & + & a\sb{m2} x\sb{2} & + & \cdots & + & a\sb{mn} x\sb{n} & = & 0.
\end{array}\right.$$ es un subespacio vectorial de $\K^n$. Un sistema de
**ecuaciones implı́citas** de un subespacio vectorial $W$ respecto de la
base $\mathcal B$ es un sistema lineal homogéneo cuyas soluciones son
las coordenadas de los vectores de $W$ respecto de $\mathcal B$ (es
decir, cuyo conjunto de soluciones es $c\sb{\mathcal B}(W)$). Si
escribimos el sistema de la forma $A\vec{x}=\vec{0}$, entonces el
subespacio $c\sb{\mathcal B}(W)$ es precisamente $\operatorname{null}(A)$.

Veamos cómo calcular unas ecuaciones paramétricas de un subespacio
vectorial a partir de unas implı́citas. Recordemos que el conjunto de
soluciones del sistema anterior lo podemos calcular mediante una forma
escalonada de la matriz de coeficientes $A$, despejando las variables
básicas en función de las libres, para obtener al final una expresión
$$\vec{x} = \alpha\sb{1} \vec{h}\sb{1} + \alpha\sb{2} \vec{h}\sb{2} + \cdots + \alpha\sb{n-r} \vec{h}\sb{n-r},$$
donde $\alpha\sb{1}, \alpha\sb{2}, \ldots, \alpha\sb{n-r}$ son parámetros que
corresponden a las variables libres, digamos
$x\sb{d\sb{1}},x\sb{d\sb{2}},\ldots,x\sb{d\sb{n-r}}$, y
$\vec{h}\sb{1}, \vec{h}\sb{2}, \ldots, \vec{h}\sb{n-r}$ son vectores columna que
representan soluciones particulares.

También sabemos que el vector $\vec{h}\sb{1}$ tiene un 1 en la posición
$d\sb{1}$, y los restantes vectores $\vec{h}\sb{j}$ tienen un cero en esa
posición. Lo mismo se aplica a todos los vectores $\vec{h}\sb{i}$ : tienen un
valor 1 en la posición $d\sb{i}$ y los restantes vectores $\vec{h}\sb{j}$ tienen
un cero en esa posición. Hay que tener en cuenta que todas estas
igualdades se refieren a las coordenadas de los vectores respecto de la
base ${\mathcal B}$, pero no lo estamos incorporando en la notación por
claridad. Podemos deducir entonces los siguiente:

-   El sistema $\{ \vec{h}\sb{1}, \ldots, \vec{h}\sb{n-r} \}$ es un sistema
    generador de $W$. Es claro, pues la expresión anterior nos indica
    que toda solución del sistema es combinación lineal de estos
    vectores.

-   El sistema $\{ \vec{h}\sb{1}, \ldots, \vec{h}\sb{n-r} \}$ es linealmente
    independiente. En efecto, una combinación lineal de la forma
    $$\alpha\sb{1} \vec{h}\sb{1} + \alpha\sb{2} \vec{h}\sb{2} + \cdots + \alpha\sb{n-r} \vec{h}\sb{n-r} = \vec{0}$$
    proporciona un conjunto de igualdades, y nos fijamos en las
    correspondientes a las componentes $d\sb{1}, d\sb{2}, \ldots, d\sb{n-r}$. Para
    la componente $d\sb{1}$ se tiene la igualdad
    $$\alpha\sb{1} \cdot 1 + \alpha\sb{2} \cdot 0 + \cdots + \alpha\sb{n-r} \cdot 0 = 0,$$
    pues $\vec{h}\sb{1}$ tiene una componente igual a $1$ en esa posición, y
    los restantes vectores $\vec{h}\sb{i}$ tienen el valor cero.
    Análogamente, en la componente $d\sb{2}$ obtenemos
    $$\alpha\sb{1} \cdot 0 + \alpha\sb{2} \cdot 1 + \cdots + \alpha\sb{n-r} \cdot 0 = 0,$$
    por la misma razón. Finalmente, estas igualdades implican que
    $\alpha\sb{1} = \alpha\sb{2} = \ldots = \alpha\sb{n-r} = 0$.

Por tanto, el sistema $\{ \vec{h}\sb{1}, \ldots, \vec{h}\sb{n-r} \}$ es una base
de $W$, de donde $\dim W = n - \rg(A)$. Además, hemos dado un
procedimiento para pasar de ecuaciones implı́citas a paramétricas. En
particular, tenemos que

{{% theorem name="Dimensión del espacio nulo" %}}
 Sea $A\sb{m \times n}$ una matriz con
coeficientes en $\K$. Entonces $$\dim \nulo(A) = n - \rg(A).$$
{{% /theorem %}}

{{% example name="Ejemplo" %}}
En $\R^4$, y con respecto a la base estándar, consideremos el subespacio
vectorial definido por el sistema de ecuaciones
$$W: \left\\{ \begin{array}{l} x\sb{2} -x\sb{3}-x\sb{4} = 0, \cr x\sb{2} + x\sb{4} = 0. \end{array} \right.$$
Si calculamos la forma escalonada reducida por filas de la matriz de
coeficientes nos queda
$$A = \left( \begin{array}{rrrr} 0 & 1 & -1 & -1 \cr 0 & 1 & 0 & 1 \end{array} \right) \mapright{\rref} \left( \begin{array}{rrrr} 0 & 1 & 0 & 1 \cr 0 & 0 & 1 & 2 \end{array} \right).$$
Entonces el rango de la matriz de coeficientes es iguala $2$, de donde
$\dim W = 4-2 = 2$. Una base de este espacio la obtenemos al despejar
las variables básicas ($x\sb{2}, x\sb{3}$) en función de las libres ($x\sb{1}, x\sb{4}$)
en las ecuaciones paramétricas
$$W: \left\\{ \begin{array}{lcll} x\sb{1} & = & \alpha\sb{1}, \cr x\sb{2} & = & &-\alpha\sb{2}, \cr x\sb{3} & = & & -2\alpha\sb{2}, \cr x\sb{4} & = & & \alpha\sb{2}. \end{array} \right.$$
Por tanto,
$$W = \langle \vec{h}\sb{1} = \left( \begin{array}{l} 1 \cr 0 \cr 0 \cr 0 \end{array} \right), \vec{h}\sb{2} = \left( \begin{array}{l} 0 \cr -1 \cr -2 \cr 1 \end{array} \right) \rangle,$$
y $\{\vec{h}\sb{1},\vec{h}\sb{2}\}$ es base de $W$. En este caso, como estamos en
$\R^4$ con la base estándar, tenemos $W=c\sb{\mathcal B}(W)$.
{{% /example %}}

## Conversión entre paramétricas e implı́citas

Al final de la sección anterior hemos visto un método para transformar
un subespacio vectorial dado por unas ecuaciones implı́citas en una base
de dicho subespacio. Podemos resumirlo en el siguiente método.

{{% example name="Paso de implı́citas a paramétricas" %}}
Sea $V$ un $\K$-espacio vectorial donde se ha fijado una base
${\mathcal B}$. Dado un subespacio vectorial $W$ definido por un sistema
de ecuaciones homogéneo $A \vec{x} = \vec{0}$,

1.  Calculamos una forma escalonada (o reducida) por filas de $A$:
    $$A \mapright{\rref} E\sb{A}.$$

2.  Despejamos las variables básicas en función de las libres.

3.  Calculamos los vectores $\vec{h}\sb{i}$ a partir de los coeficientes de
    las variables libres.
{{% /example %}}

{{% example name="Ejemplo" %}}
Consideremos en $\R^4$ el subespacio $W$ dado por las soluciones del
siguiente sistema lineal homogéneo:
$$\left\\{ \begin{array}{rrrrcl} 2x\sb{1} & + 3x\sb{2} & -2x\sb{3} & - x\sb{4} & = & 0, \cr x\sb{1} & + 4x\sb{2} & & - x\sb{4} & = & 0, \cr 2x\sb{1} & -7 x\sb{2} & -6 x\sb{3} & + x\sb{4} & = & 0. \end{array} \right.$$
Tomamos la matriz $A$ de coeficientes del sistema, y calculamos su forma
escalonada reducida por filas:
$$A \mapright{\mbox{G-J}}   \left( \begin{array}{cccc} 1&0&-8/5&-1/5\cr{}0&1&2/5& -1/5\cr{}0&0&0&0\end{array} \right).$$
Despejamos las variables pivote en función de las libres:
$$\left\\{ \begin{array}{rcrr} x\sb{1} & = & \frac{8}{5} x\sb{3} & + \frac{1}{5} x\sb{4}, \cr x\sb{2} & = & - \frac{2}{5} x\sb{3} & + \frac{1}{5} x\sb{4}. \end{array} \right.$$
Cambiamos las variables libres por parámetros:
$$\left\\{ \begin{array}{rcrr} x\sb{1} & = & \frac{8}{5} \lambda\sb{1} & + \frac{1}{5} \lambda\sb{2}, \cr x\sb{2} & = & - \frac{2}{5} \lambda\sb{1} & + \frac{1}{5} \lambda\sb{2}, \cr x\sb{3} & = & \lambda\sb{1}, \cr x\sb{4} & = & & \lambda\sb{2}. \end{array} \right.$$
Una base de $W$ está formada por los vectores
$$\vec{w}\sb{1} = \left( \begin{array}{r} \frac{8}{5} \cr -\frac{2}{5} \cr 1 \cr 0 \end{array} \right), \vec{w}\sb{2} = \left( \begin{array}{r} \frac{1}{5} \cr \frac{1}{5} \cr 0 \cr 1 \end{array} \right).$$
Observemos que para este cálculo hemos partido de un conjunto de
ecuaciones que eran dependientes, pues la tercera ecuación se obtiene
tras sumar la primera multiplicada por $3$ y la segunda por $(-4)$.
{{% /example %}}

El problema que nos planteamos ahora es si todo subespacio vectorial
admite unas ecuaciones implı́citas. La respuesta es afirmativa y la
prueba se obtiene a partir del método para pasar de ecuaciones
paramétricas, es decir, de un sistema de generadores, a ecuaciones
implı́citas.

Vamos a explicar dos métodos diferentes para obtenerlas. Uno se basa en
el método del orlado y el segundo utiliza transformaciones elementales
por filas. Partimos de un espacio vectorial $V$ donde se ha fijado una
base ${\mathcal B}$ y $W$ es un subespacio vectorial dado por un sistema
finito de generadores $S$.

{{% example name="De paramétricas a implı́citas: orlado" %}}
1.  Sea $A\sb{S,{\mathcal B}}$ la matriz cuyas columnas son las
    coordenadas de los elementos de $S$ respecto de la base
    ${\mathcal B}$.

2.  Mediante el método del orlado, se identifican el máximo número
    posible de columnas independientes, con lo que se obtiene una base
    ${\mathcal B}\sb{1}$ del subespacio $W$. Digamos que ${\mathcal B}\sb{1}$
    tiene $r$ elementos, es decir, $\dim(W)=r$.

3.  Se considera la matriz
    $A\sb{{\mathcal B}\sb{1}, {\mathcal B}}\in \MatK{n}{r}{\K}$, cuyas
    columnas son una base de $W$, y la matriz $M$ que resulta al añadir
    a esta matriz una columna de incógnitas con las variables
    $x\sb{1},\ldots, x\sb{n}$:
    $$M=\left(\begin{array}{c|c} A\sb{{\mathcal B}\sb{1}, {\mathcal B}} & \begin{array}{c} x\sb{1}\cr\vdots\cr x\sb{n}\end{array} \end{array}\right).$$

4.  Con el método del orlado aplicado a un menor no nulo de orden $r$,
    se calculan $n-r$ determinantes que deben ser iguales a cero y
    definen unas ecuaciones implı́citas de $W$.
{{% /example %}}

La justificación del método anterior se basa en que un vector de
coordenadas
$\left( \begin{array}{c} x\sb{1} \cr \vdots \cr x\sb{n} \end{array} \right)$
pertenece a $W$ si y solamente si es combinación lineal de las columnas
de $A\sb{B\sb{1},B}$, esto es, si y solamente si la matriz $M$ tiene rango
$r$.

{{% example name="Ejemplo" %}}
En el espacio vectorial $\R^4$, respecto de la base estándar
$\mathcal{B}$, sea $W$ el subespacio vectorial generado por el sistema
$$S=\{ \vec{v}\sb{1} = \left( \begin{array}{r} 1 \cr 1 \cr 2 \cr -1 \end{array} \right), \vec{v}\sb{2} = \left( \begin{array}{r} 2 \cr 0 \cr 1 \cr 1 \end{array} \right), \vec{v}\sb{3} = \left( \begin{array}{r} 1 \cr -1 \cr -1 \cr 2 \end{array} \right) \}.$$
Aplicando el método del orlado averiguamos que $\rg (S)=2$ y que, de
hecho, el tercer vector es combinación lineal de los otros dos. Entonces
$$A\sb{S,\mathcal{B}}=\left(\begin{array}{cc}\fbox{$\begin{array}{cc}
         1 & 2\cr
         1 & 0
        \end{array}$} & \begin{array}{c} 1\cr -1\end{array}\cr
        \begin{array}{cc}
         2 & 1\cr
         -1 & 1
        \end{array} & \begin{array}{c} -1\cr 2\end{array}
        \end{array}\right).$$ Luego
$\mathcal{B}\sb{1}=\{ \vec{v}\sb{1}, \vec{v}\sb{2} \}$ es una base de $W$. La
condición a imponer es que un vector de coordenadas
$(x\sb{1},x\sb{2},x\sb{3},x\sb{4})^t$ está en $W$ si y solamente si
$$\rg\left( \begin{array}{cc}
  \fbox{$\begin{array}{cc} 1 & 2\cr 1 & 0 \end{array}$} & \begin{array}{c} x\sb{1}\cr x\sb{2}\end{array} \cr
        \begin{array}{cc}  2 & 1\cr -1 & 1 \end{array} & \begin{array}{c} x\sb{3}\cr x\sb{4}\end{array} \end{array}\right) = 2 \Leftrightarrow
  \left\\{\begin{array}{l}  \left|\begin{array}{ccc} 1 & 2 & x\sb{1}\cr  1 & 0 & x\sb{2}\cr  2 & 1 & x\sb{3} \end{array}\right| =0
    \equiv x\sb{1}+3x\sb{2}-2x\sb{3}=0\cr \cr
       \left|\begin{array}{ccc} 1 & 2 & x\sb{1}\cr 1 & 0 & x\sb{2}\cr -1 & 1 & x\sb{4} \end{array}\right| =0
       \equiv x\sb{1}-3x\sb{2}-2x\sb{4}=0
      \end{array}\right. .$$ Luego unas ecuaciones implı́citas de $W$
son: $$W\colon\left\\{\begin{array}{l}
                 x\sb{1}+3x\sb{2}-2x\sb{3}=0\cr
                 x\sb{1}-3x\sb{2}-2x\sb{4}=0
                \end{array}\right. .$$
{{% /example %}}

El método de eliminación es muy similar en cuanto al comienzo, aunque se
parte de la matriz
$M = \left( \begin{array}{cc} A\sb{S,{\mathcal B}} & \vec{x} \end{array} \right)$.

{{% example name="De paramétricas a implı́citas: transformaciones elementales" %}}
Suponemos fijada una base ${\mathcal B}$ del espacio vectorial $V$, y
sea $W$ el subespacio vectorial generado por un sistema $S$.

1.  Se considera la matriz $A\sb{S,{\mathcal B}}$, cuyas columnas son las
    coordenadas de los elementos de $S$ respecto de la base
    ${\mathcal B}$. Sea $M$ la matriz que resulta al añadir a esta
    matriz una columna de variables $x\sb{1},\ldots, x\sb{n}$.
    $$M=\left(\begin{array}{c|c} A\sb{S,{\mathcal B}} & \begin{array}{c} x\sb{1}\cr\vdots\cr x\sb{n}\end{array} \end{array}\right) .$$

2.  Calculamos $E$ una forma escalonada de la matriz
    $A\sb{S,{\mathcal B}}$, efectuando las mismas transformaciones en la
    última columna de $M$.

3.  Imponemos las condiciones sobre la última columna de $E$ para que no
    contenga ningún pivote.
{{% /example %}}

[]{#de-parm-a-impl label="de-parm-a-impl"} La justificación del método
se basa en la misma idea que el método anterior: un vector
$\left( \begin{array}{c} x\sb{1} \cr \vdots \cr x\sb{n} \end{array} \right)$
pertenece a $W$ si y solamente si es combinación lineal de las columnas
de $A\sb{S,\mathcal{B}}$. Es decir, si y solamente si en cualquier forma
escalonada por filas de la matriz $M$ la última columna no contiene un
pivote. Una forma escalonada por filas de $M$ es de la forma
$(E|\vec{c})$, donde $E$ es una forma escalonada por filas de
$A\sb{S,\mathcal{B}}$. Supongamos que $\rg(A\sb{S,\mathcal{B}})=r$, es decir
que $E$ tiene $r$ pivotes; entonces la forma escalonada por filas de $M$
debe ser del tipo $$\left(\begin{array}{c|c}
         E & \begin{array}{c}
              \text{eq}\sb{1}\cr\vdots\cr \text{eq}\sb{r}\cr
              a\sb{11}x\sb{1}+\cdots a\sb{1n}x\sb{n}\cr
              \vdots\cr
              a\sb{n-r,1}x\sb{1}+\cdots a\sb{n-r,n}x\sb{n}
             \end{array}
        \end{array}\right) ,$$ donde $\text{eq}\sb{1},\ldots , \text{eq}\sb{r}$
son expresiones lineales en $x\sb{1},\ldots ,x\sb{n}$. Que la última columna no
tenga pivote es equivalente a que se verifiquen las ecuaciones
$$\left\\{\begin{array}{l}
          a\sb{11}x\sb{1}+\cdots+a\sb{1n}x\sb{n}=0\cr
          \vdots\cr
          a\sb{n-r,1}x\sb{1}+\cdots+a\sb{n-r,n}x\sb{n}=0
         \end{array}\right. ,$$ que son unas ecuaciones implı́citas de
$W$.

{{% example name="Ejemplo" %}}
En el espacio vectorial $\R^4$, respecto de la base estándar
$\mathcal{S}$, sea $W$ el subespacio vectorial generado por el sistema
$$S=\{ \vec{v}\sb{1} = \left( \begin{array}{r} 1 \cr 1 \cr 2 \cr -1 \end{array} \right), \vec{v}\sb{2} = \left( \begin{array}{r} 2 \cr 0 \cr 1 \cr 1 \end{array} \right), \vec{v}\sb{3} = \left( \begin{array}{r} 1 \cr -1 \cr -1 \cr 2 \end{array} \right) \}.$$
Mediante transformaciones elementales por filas obtenemos
$$\left(\begin{array}{ccc|c}
         1 & 2 & 1 & x\sb{1} \cr
         1 & 0 & -1 & x\sb{2}\cr
         2 & 1 & -1 & x\sb{3}\cr
         -1 & 1 & 2 & x\sb{4}
        \end{array}\right) \rightsquigarrow
  \left(\begin{array}{ccc|c}
         1 & 2 & 1 & x\sb{1}\cr
         0 & -2 & -2 & -x\sb{1}+x\sb{2}\cr
         0 & 0 & 0 & -(1/2)x\sb{1}-(3/2)x\sb{2}+x\sb{3}\cr
         0 & 0 & 0 & -(1/2)x\sb{1}+(3/2)x\sb{2}+x\sb{4}
        \end{array}\right) .$$ Luego unas ecuaciones implı́citas de $W$
son $$W\colon\left\\{\begin{array}{l}
                 -(1/2)x\sb{1}-(3/2)x\sb{2}+x\sb{3}=0\cr
                 -(1/2)x\sb{1}+(3/2)x\sb{2}+x\sb{4}=0
                \end{array}\right. .$$
{{% /example %}}

Como consecuencia de los procedimientos anteriores podemos concluir con
el siguiente resultado:

{{% theorem name="Existencia de ecuaciones paramétricas e implı́citas" %}}
 Sea $V$ un
$\K$-espacio vectorial de dimensión finita y ${\mathcal B}$ una base.
Todo subespacio vectorial $W$ se puede representar, respecto a
${\mathcal B}$, mediante unas ecuaciones paramétricas y unas ecuaciones
implı́citas.
{{% /theorem %}}

{{% remark %}}
Cuando un subespacio vectorial $W$ está definido por unas ecuaciones
implı́citas $A\sb{p \times n} \vec{x} = \vec{0}$, puede ocurrir que la matriz
$A$ no sea de rango $p$, es decir, que alguna ecuación dependa
linealmente de las otras. Los métodos anteriores de paso de paramétricas
a implı́citas proporcionan lo que se denomina un sistema *independiente*
de ecuaciones.
{{% /remark %}}

Es recomendable hacer una pequeña variación en el método de cálculo de
unas ecuaciones implı́citas a partir de las paramétricas, con vistas a
aprovechar la capacidad de cálculo de algunos programas que no tratan
variables dentro de una matriz. Lo que necesitamos es la matriz no
singular $P$ tal que $PA = E$, con $E$ una forma escalonada. Entonces
sabemos que
$$\left( \begin{array}{c|c} A & I\sb{m} \end{array} \right) \rightsquigarrow \left( \begin{array}{c|c} E & P \end{array} \right),$$
con lo que obtenemos $P$. Si $\rg(A) = r$, entonces la condición sobre
la ausencia de pivotes en la última columna se traduce en que las
últimas $n - r$ componentes de $P \vec{x}$ sean nulas. Veamos lo que
ocurre en el ejemplo anterior.

Formamos la matriz
$$\left( \begin{array}{ccc|c} \vec{w}\sb{1} & \vec{w}\sb{2} & \vec{w}\sb{3} & I\sb{4} \end{array} \right) \rightsquigarrow  \left( \begin{array}{ccccccc} 1&2&1&1&0&0&0\cr{}0&-2&
-2&-1&1&0&0\cr{}0&0&0&-1/2&-3/2&1&0\cr{}0
&0&0&-1/2&3/2&0&1\end{array} \right).$$ Dado que el rango de la matriz
de vectores inicial es igual a $2$, consideremos las últimas $4-2 = 2$
componentes de $P \vec{x}$:
$$P \vec{x} =  \left( \begin{array}{c} x\sb{1} \cr{}- x\sb{1} + x\sb{2} \cr {} -1/2 x\sb{1} -3/2 x\sb{2} + x\sb{3} \cr {} -1/2x\sb{1} + 3/2 x\sb{2} + x\sb{4} \end{array}
 \right) \Rightarrow W\colon\left\\{\begin{array}{l}
                 -(1/2)x\sb{1}-(3/2)x\sb{2}+x\sb{3}=0\cr
                 -(1/2)x\sb{1}+(3/2)x\sb{2}+x\sb{4}=0
                \end{array}\right. .$$

{{% example name="De paramétricas a implı́citas: transformaciones elementales (2)" %}}
Suponemos fijada una base ${\mathcal B}$ del espacio vectorial $V$, y
sea $W$ el subespacio vectorial generado por un sistema $S$.

1.  Se considera la matriz $A\sb{S,{\mathcal B}}$, de orden $m \times n$,
    cuyas columnas son las coordenadas de los elementos de $S$ respecto
    de la base ${\mathcal B}$.

2.  Calculamos $E$ una forma escalonada de la matriz
    $A\sb{S,{\mathcal B}}$, y una matriz no singular $P$ tal que $PA = E$.
    $$\left( \begin{array}{c|c} A & I\sb{n} \end{array} \right) \mapright{\text{Gauss}} \left( \begin{array}{c|c} E & P \end{array} \right).$$

3.  Si $\rg(A) = r$, esto es, las primeras $r$ filas de $E$ son no
    nulas, las ecuaciones implı́citas de $W$ son las $(n-r)$ últimas
    componentes de $P \vec{x}$.
{{% /example %}}

[]{#de-parm-a-impl-2 label="de-parm-a-impl-2"}

## Operaciones con subespacios

### Intersección y suma de subespacios

Ya hemos visto cómo se puede determinar un subespacio vectorial usando
ecuaciones paramétricas o implı́citas y cómo calcular su dimensión.
Continuaremos con las operaciones básicas entre subespacios vectoriales,
que permiten formar nuevos subespacios a partir de unos dados. Dados dos
subespacios $W\sb{1}$ y $W\sb{2}$ tiene sentido considerar la intersección como
conjuntos. Veamos que tiene una estructura adicional.

{{% theorem name="Intersección de subespacios vectoriales" %}}
 Si $W\sb{1}$ y $W\sb{2}$ son dos
subespacios vectoriales de un espacio vectorial $V$, entonces
$W\sb{1}\cap W\sb{2}$ es un subespacio vectorial.
{{% /theorem %}}

{{% proof %}}
Sean $\vec{v}\sb{1}, \vec{v}\sb{2}\in W\sb{1}\cap W\sb{2}$. Como pertenecen a $W\sb{1}$,
entonces $\vec{v}\sb{1}+\vec{v}\sb{2}\in W\sb{1}$, al ser $W\sb{1}$ subespacio vectorial.
Pero como también pertenecen a $W\sb{2}$, entonces
$\vec{v}\sb{1}+\vec{v}\sb{2}\in W\sb{2}$. Por tanto,
$\vec{v}\sb{1}+\vec{v}\sb{2}\in W\sb{1}\cap W\sb{2}$.

Análogamente se demuestra que si $\alpha \in \K$ y
$\vec{v}\in W\sb{1}\cap W\sb{2}$, entonces $\alpha \vec{v}\in W\sb{1}\cap W\sb{2}$. Por
tanto, $W\sb{1}\cap W\sb{2}$ satisface las dos propiedades necesarias y
suficientes para ser un subespacio vectorial.
{{% /proof %}}

Una forma directa de obtener la expresión de la intersección de dos
subespacios vectoriales consiste en tomar como punto de partida unas
ecuaciones implı́citas de ambos. El sistema formado por *todas* las
ecuaciones forman unas ecuaciones implı́citas de la intersección.

{{% example name="Intersección de subespacios (ecuaciones implícitas)" %}}
 Sea $V$ un
$\K$-espacio vectorial de dimensión finita y ${\mathcal B}$ una base.
Sean $W\sb{1}, W\sb{2}$ subespacios dados por unas ecuaciones implı́citas
$A\sb{1} \vec{x} = \vec{0}$, $A\sb{2} \vec{x} = \vec{0}$, respectivamente, respecto
de la base ${\mathcal B}$. Entonces unas ecuaciones implı́citas del
subespacio $W\sb{1} \cap W\sb{2}$ son
$$W\sb{1} \cap W\sb{2} : \left\\{ \begin{array}{rcl} A\sb{1} \vec{x} & = & \vec{0}, \cr A\sb{2} \vec{x} & = & \vec{0}. \end{array} \right..$$
{{% /example %}}

{{% example name="Ejemplo" %}}
En el espacio vectorial $\R^4$ la intersección de los subespacios
$$W\sb{1}\colon\left\\{\begin{array}{l}
                  x\sb{1}-x\sb{3}=0\cr
                  x\sb{2}+x\sb{4}=0
                 \end{array}\right.\ \mbox{ y }\
  W\sb{2}\colon x\sb{1}+x\sb{4}=0$$ es $$W\sb{1}\cap W\sb{2}\colon\left\\{\begin{array}{l}
                  x\sb{1}-x\sb{3}=0\cr
                  x\sb{2}+x\sb{4}=0\cr
                  x\sb{1}+x\sb{4}=0
                 \end{array}\right. .$$
{{% /example %}}

Si uno de los espacios viene dado por unas ecuaciones paramétricas, y el
otro por unas implícitas, la nitersección se puede calcular como sigue.

{{% example name="Intersección de subespacios (ecuaciones paramétricas e implícitas)" %}}
Sea $V$ un $\K$-espacio vectorial de dimensión finita y ${\mathcal B}$
una base. Sean $W\sb{1}$ un subespacio dado por unas ecuaciones paramétricas
y $W\sb{2}$ un subespacio dado por unas ecuaciones implı́citas, respecto de
la base ${\mathcal B}$. Entonces unas ecuaciones paramétricas del
subespacio $W\sb{1} \cap W\sb{2}$ se calculan como sigue:

1.  Sustituimos las ecuaciones paramétricas de $W\sb{1}$ en las implícitas
    de $W\sb{2}$.

2.  Resolvemos el sistema obtenido (donde las incógnitas son los
    parámetros de $W\sb{1}$).

3.  Sustituimos las soluciones del sistema en las paramétricas de $W\sb{1}$.
{{% /example %}}

{{% example name="Ejemplo" %}}
En el espacio vectorial $\R^4$ la intersección de los subespacios
$$W\sb{1}\colon\left\\{\begin{array}{l}
                  x\sb{1}=\alpha\sb{1}\cr
                  x\sb{2}=\alpha\sb{2} \cr
                  x\sb{3}=\alpha\sb{1}\cr
                  x\sb{4}=-\alpha\sb{2}
                 \end{array}\right.\ \mbox{ y }\
  W\sb{2}\colon x\sb{1}+x\sb{4}=0$$ se obtiene sustituyendo las ecuaciones
paramétricas de $W\sb{1}$ en las implícitas de $W\sb{2}$, con lo que se obtiene:
$$\alpha\sb{1}-\alpha\sb{2}=0.$$ Este es un sistema de ecuaciones lineales (con
una sola ecuación), cuyas incógnitas son $\alpha\sb{1}$ y $\alpha\sb{2}$. La
primera variable es pivote y la segunda es libre, luego su solución
general es: $$\left\\{\begin{array}{l}
                  \alpha\sb{1}=t\cr
                  \alpha\sb{2}=t
                 \end{array}\right.$$ Sustituyendo esta solución general
en las ecuaciones paramétricas de $W\sb{1}$, obtenemos unas ecuaciones
paramétricas de $W\sb{1}\cap W\sb{2}$: $$\left\\{\begin{array}{l}
                  x\sb{1}=t\cr
                  x\sb{2}=t \cr
                  x\sb{3}=t\cr
                  x\sb{4}=-t
                 \end{array}\right.$$ Por tanto
$W=\left\langle\left(\begin{array}{r} 1 \cr 1 \cr 1 \cr -1 \end{array}\right)\right\rangle$.

Este método se justifica puesto que los vectores de $W\sb{1}$ son los de la
forma dada por sus ecuaciones paramétricas, pero solo estárán además en
$W\sb{2}$ aquellos que cumplan las ecuaciones implícitas de $W\sb{2}$, lo que
impone unas restricciones sobre los parámetros de $W\sb{1}$ (en este caso
$\alpha\sb{1}-\alpha\sb{2}=0$). Los vectores que $W\sb{1}$ cuyos parámetros cumplan
estas restricciones son los vectores de $W\sb{1}\cap W\sb{2}$.
{{% /example %}}

Si los dos subespacios $W\sb{1}$ y $W\sb{2}$ vienen dados por ecuaciones
paramétricas, podemos proceder de la siguiente manera.

{{% example name="Intersección de subespacios (ecuaciones paramétricas)" %}}
Sea $V$ un $\K$-espacio vectorial de dimensión finita y ${\mathcal B}$
una base. Sean $W\sb{1}$ y $W\sb{2}$ subespacios dados por unas ecuaciones
paramétricas respecto de la base ${\mathcal B}$. Entonces unas
ecuaciones paramétricas del subespacio $W\sb{1} \cap W\sb{2}$ se calculan como
sigue:

1.  Igualamos las ecuaciones paramétricas de $W\sb{1}$ a las de $W\sb{2}$.

2.  Resolvemos el sistema obtenido (donde las incógnitas son los
    parámetros de $W\sb{1}$ y $W\sb{2}$).

3.  Sustituimos las soluciones del sistema, bien en las paramétricas de
    $W\sb{1}$, bien en las de $W\sb{2}$.
{{% /example %}}

{{% example name="Ejemplo" %}}
En el espacio vectorial $\R^4$ la intersección de los subespacios
$$W\sb{1}\colon\left\\{\begin{array}{l}
                  x\sb{1}=\alpha\sb{1}\cr
                  x\sb{2}=\alpha\sb{2} \cr
                  x\sb{3}=\alpha\sb{1}\cr
                  x\sb{4}=-\alpha\sb{2}
                 \end{array}\right.\ \mbox{ y }\
  W\sb{2}\colon \left\\{\begin{array}{l}
                  x\sb{1}=\beta\sb{1}\cr
                  x\sb{2}=\beta\sb{2} \cr
                  x\sb{3}=\beta\sb{3}\cr
                  x\sb{4}=-\beta\sb{1}
                 \end{array}\right.$$ se obtiene igualando las
ecuaciones paramétricas de $W\sb{1}$ a las de $W\sb{2}$ (observemos que los
parámetros deben tener distintos nombre), con lo que se obtiene:
$$\left\\{\begin{array}{l}
   \alpha\sb{1}=\beta\sb{1} \cr
   \alpha\sb{2}=\beta\sb{2} \cr
   \alpha\sb{1}=\beta\sb{3} \cr
   -\alpha\sb{2}=-\beta\sb{1}
\end{array}\right.$$ Este es un sistema de ecuaciones lineales cuyas
incógnitas son $\alpha\sb{1},\alpha\sb{2},\beta\sb{1},\beta\sb{2},\beta\sb{3}$. Resolviendo
el sistema, vemos que su solución general es: $$\left\\{\begin{array}{l}
                  \alpha\sb{1}=t\cr
                  \alpha\sb{2}=t \cr
                  \beta\sb{1}=t \cr
                  \beta\sb{2}=t \cr
                  \beta\sb{3}=t
                 \end{array}\right.$$ Sustituyendo esta solución general
en las ecuaciones paramétricas de $W\sb{1}$ (también podría hacerse en las
de $W\sb{2}$), obtenemos unas ecuaciones paramétricas de $W\sb{1}\cap W\sb{2}$:
$$\left\\{\begin{array}{l}
                  x\sb{1}=t\cr
                  x\sb{2}=t \cr
                  x\sb{3}=t\cr
                  x\sb{4}=-t
                 \end{array}\right.$$ Por tanto
$W=\left\langle\left(\begin{array}{r} 1 \cr 1 \cr 1 \cr -1 \end{array}\right)\right\rangle$.

La justificación de este método viene dada por el hecho de que un vector
estará a la vez en $W\sb{1}$ y en $W\sb{2}$ si y solo si se puede escribir como
indican las ecuaciones paramétricas de $W\sb{1}$ y también como indican las
ecuaciones paramétricas de $W\sb{2}$. Al igualar las dos ecuaciones
obtenemos las condiciones que deben cumplir estos parámetros para que
los vectores que determinen estén a la vez en los dos subespacios.
{{% /example %}}

Una vez que hemos visto que la intersección de subespacios es un
subespacio, y hemos estudiado algunas de sus propiedades, podrı́amos
intentar hacer lo mismo con la unión de subespacios vectoriales. Pero
hay que ser cuidadoso. Aunque la intersección de dos subespacios
vectoriales es un subespacio, la *unión* de dos subespacios *no es un
subespacio*, en general. Por ejemplo, en $\R^3$, la unión de dos rectas
que pasan por el origen no tiene por qué ser una recta, y por supuesto
no es un punto, ni un plano, ni todo el espacio.

De todas formas, aunque $W\sb{1}\cup W\sb{2}$ no tenga estructura de subespacio,
podemos considerar uno que contenga a $W\sb{1}$ y a $W\sb{2}$. Para ello, basta
tomar $\langle W\sb{1}\cup W\sb{2}\rangle$. Tenemos entonces la siguiente
definición:

{{% definition %}}
Suma de subespacios Sean $W\sb{1}$ y $W\sb{2}$ dos subespacios de un espacio
vectorial $V$. Se llama **suma** de $W\sb{1}$ y $W\sb{2}$ al subespacio
$$W\sb{1}+W\sb{2}=\langle W\sb{1}\cup W\sb{2}\rangle.$$
{{% /definition %}}

Por definición, si conocemos $W\sb{1}$ y $W\sb{2}$ y queremos hallar $W\sb{1}+W\sb{2}$,
basta tomar un sistema de generadores $S\sb{1}$ de $W\sb{1}$ y un sistema de
generadores $S\sb{2}$ de $W\sb{2}$. La concatenación de estos dos sistemas,
$S\sb{1}\cup S\sb{2}$, será un sistema de generadores de $W\sb{1}+W\sb{2}$.

{{% example name="Ejemplo" %}}
En el espacio vectorial $\R^4$ la suma de los subespacios
$$W\sb{1} = \langle \vec{v}\sb{1} = \left( \begin{array}{r} 1 \cr 0 \cr -1 \cr 0 \end{array} \right), \vec{v}\sb{2} = \left( \begin{array}{r} 0 \cr 1 \cr 0 \cr 1 \end{array} \right) \rangle, \qquad W\sb{2} = \langle \vec{w}\sb{1} = \left( \begin{array}{r} 1 \cr 0 \cr 0 \cr 1 \end{array} \right) \rangle$$
es el subespacio
$W\sb{1}+W\sb{2} = \langle \vec{v}\sb{1}, \vec{v}\sb{2}, \vec{w}\sb{1} \rangle$.
{{% /example %}}

{{% example name="Suma de subespacios" %}}
 Sean $W\sb{1}, W\sb{2}$ subespacios dados por los sistemas
de generadores respectivos $S\sb{1}$ y $S\sb{2}$. Entonces el subespacio suma
$W\sb{1} + W\sb{2}$ tiene como sistema generador el sistema $S\sb{1} \cup S\sb{2}$.
{{% /example %}}

{{% remark %}}
La suma $W\sb{1}+W\sb{2}$ es, en realidad, el subespacio más pequeño que
contiene a $W\sb{1}\cup W\sb{2}$.
{{% /remark %}}

### Propiedades de la suma de subespacios

Dados dos subespacios $W\sb{1}$ y $W\sb{2}$ en un espacio vectorial de dimensión
finita $V$, hemos definido la variedad suma $W\sb{1}+W\sb{2}$. La causa de que
este subespacio vectorial se llame *suma*, se encuentra en el siguiente
resultado:

{{% theorem name="Caracterización de la suma de subespacios" %}}
 Sean $W\sb{1}$ y $W\sb{2}$ dos
subespacios de un espacio vectorial $V$. Entonces
$$W\sb{1}+W\sb{2}=\{ \vec{v}\sb{1}+\vec{v}\sb{2} ~|~ \vec{v}\sb{1}\in W\sb{1}, \vec{v}\sb{2}\in W\sb{2}\}.$$
{{% /theorem %}}

{{% proof %}}
Si $\vec{v}\in W\sb{1}+W\sb{2}$, entonces es combinación lineal de los vectores
de $W\sb{1}\cup W\sb{2}$. Separemos esta combinación lineal en dos sumandos
$\vec{v}=\vec{v}\sb{1}+\vec{v}\sb{2}$, donde en $\vec{v}\sb{1}$ están todos los términos
en que aparece un vector de $W\sb{1}$, y $\vec{v}\sb{2}$ contiene el resto de los
términos, que necesariamente consta de vectores de $W\sb{2}$. Entonces
$\vec{v}\sb{1}\in \langle W\sb{1}\rangle =W\sb{1}$, y
$\vec{v}\sb{2}\in \langle W\sb{2}\rangle =W\sb{2}$.

La otra inclusión es trivial.
{{% /proof %}}

Se puede definir de manera inductiva la suma de un número finito de
subespacios vectoriales a partir del teorema de caracterización:
$$W\sb{1} + W\sb{2} + \cdots + W\sb{m} = \langle W\sb{1} \cup W\sb{2} \cup \cdots \cup W\sb{m} \rangle.$$
Y los elementos de esta suma están caracterizados por
$$W\sb{1}+\cdots+W\sb{m}= \{ \vec{v}\sb{1}+\cdots+\vec{v}\sb{m} ~|~ \vec{v}\sb{i}\in W\sb{i}, i=1,\ldots,m \}.$$

{{% example name="Ejemplo" %}}
La forma de obtener un sistema de generadores de la suma finita de
subespacios consiste en concatenar los sistemas de generadores de cada
uno de los sumandos. Por ejemplo, en $\R^5$ consideremos los subespacios
$W\sb{1} = \langle \vec{w}\sb{11} \rangle, W\sb{2} = \langle \vec{w}\sb{21}, \vec{w}\sb{22} \rangle, W\sb{3} = \langle \vec{w}\sb{31} \rangle$,
donde
$$\vec{w}\sb{11} =  \left( \begin{array}{c} -1\cr{}2\cr{}3
\cr{}-2\cr{}-1\end{array} \right), \vec{w}\sb{21} =  \left( \begin{array}{c} -1\cr{}-2\cr{}
1\cr{}4\cr{}0\end{array} \right), \vec{w}\sb{22} =  \left( \begin{array}{c} 4\cr{}3\cr{}-2
\cr{}-4\cr{}-3\end{array} \right), \vec{w}\sb{31} =  \left( \begin{array}{c} 0\cr{}-5\cr{}-
4\cr{}-5\cr{}-2\end{array} \right).$$
Entonces
$W\sb{1} + W\sb{2} + W\sb{3} = \langle \vec{w}\sb{11}, \vec{w}\sb{21}, \vec{w}\sb{22}, \vec{w}\sb{31} \rangle$.
En este caso, los cuatro vectores son independientes, y forman una base
del espacio suma.
{{% /example %}}

### Fórmula de la dimensión

Veamos ahora uno de los teoremas más importantes del álgebra lineal, que
relaciona las dimensiones de dos subespacios cualesquiera, su suma y su
intersección. Este teorema es muy útil para calcular dimensiones de
subespacios vectoriales.

{{% theorem name="Fórmula de la dimensión" %}}
 Sean $W\sb{1}$ y $W\sb{2}$ dos subespacios vectoriales
de un espacio vectorial $V$ de dimensión finita. Entonces
$$\dim W\sb{1} + \dim W\sb{2} = \dim (W\sb{1}+W\sb{2}) + \dim (W\sb{1}\cap W\sb{2}).$$
{{% /theorem %}}

{{% proof %}}
Sea ${\mathcal B}\sb{0}=\{ \vec{u}\sb{1},\ldots,\vec{u}\sb{r} \}$ una base de
$W\sb{1}\cap W\sb{2}$. Por el teorema de la base incompleta (pág. ), podemos
ampliar ${\mathcal B}\sb{0}$ hasta una base de $W\sb{1}$, y también la podemos
ampliar hasta una base de $W\sb{2}$. Es decir, existen dos sistemas de
vectores, $S\sb{1}=\{\vec{v}\sb{1},\ldots,\vec{v}\sb{s}\}$ y
$S\sb{2}=\{\vec{w}\sb{1},\ldots,\vec{w}\sb{t}\}$ tales que
${\mathcal B}\sb{1} = {\mathcal B}\sb{0}\cup S\sb{1}$ es una base de $W\sb{1}$, y
${\mathcal B}\sb{2} = {\mathcal B}\sb{0}\cup S\sb{2}$ es una base de $W\sb{2}$.

Sea ${\mathcal B} = {\mathcal B}\sb{0} \cup S\sb{1} \cup S\sb{2}$, la concatenación
de estos tres sistemas. Vamos a demostrar que ${\mathcal B}$ es base de
$W\sb{1}+W\sb{2}$, y con eso habremos probado el teorema, ya que $\dim W\sb{1}=r+s$,
$\dim W\sb{2} =r+t$, $\dim (W\sb{1}\cap W\sb{2})=r$, y en este caso
$\dim(W\sb{1}+W\sb{2})=r+s+t$.

${\mathcal B}$ es un sistema generador de $W\sb{1}+W\sb{2}$, ya que
${\mathcal B}$ contiene a todos los vectores de ${\mathcal B}\sb{1}$ y de
${\mathcal B}\sb{2}$. Por tanto, basta comprobar que es linealmente
independiente. Consideremos una combinación lineal de la forma
$$\sum\sb{i=1}^r{\alpha\sb{i}}\vec{u}\sb{i}   +\sum\sb{j=1}^s{\beta _j}\vec{v}\sb{j}  +\sum\sb{k=1}^t{\gamma\sb{k}\vec{w}\sb{k}}= \vec{0}.$$
Hay que demostrar que todos los coeficientes deben ser nulos. Sea
$$\vec{v} = \sum\sb{i=1}^r{\alpha\sb{i}}\vec{u}\sb{i}   +\sum\sb{j=1}^s{\beta _j}\vec{v}\sb{j}  = - \sum\sb{k=1}^t{\gamma\sb{k}\vec{w}\sb{k}}.$$
De la primera forma de escribir $\vec{v}$ se obtiene que $\vec{v}\in W\sb{1}$,
y de la segunda, que $\vec{v}\in W\sb{2}$. Por tanto,
$\vec{v}\in W\sb{1}\cap W\sb{2}$, y ası́ $\vec{v}$ se escribe de forma única como
combinación lineal de los vectores de ${\mathcal B}\sb{0}$. Como también se
escribe de forma única como combinación lineal de los vectores de
${\mathcal B}\sb{1}$ (la fórmula anterior), y
${\mathcal B}\sb{0} \subset {\mathcal B}\sb{1}$, estas dos formas de escribirlo
deben ser la misma. Por tanto, $\beta\sb{1}=\cdots=\beta\sb{s}=0$.

Después de esto, nos queda
$$\sum\sb{i=1}^r{\alpha\sb{i}}\vec{u}\sb{i}  +\sum\sb{k=1}^t{\gamma\sb{k}\vec{w}\sb{k}}= \vec{0},$$
pero esta es una combinación lineal de los vectores de ${\mathcal B}\sb{2}$,
que es linealmente independiente, luego todos los coeficientes son
nulos.
{{% /proof %}}

### Suma directa

Como vimos en la sección precedente, los subespacios vectoriales se
pueden intersecar o sumar. En esta sección veremos una suma con una
propiedad especial que permite dividir un espacio vectorial en trozos.

{{% definition %}}
Suma directa Sean $W\sb{1}, \ldots, W\sb{r}$ subespacios de un espacio vectorial
$V$. Decimos que $W = W\sb{1} + \cdots + W\sb{r}$ es **suma directa** de
$W\sb{1}, \ldots, W\sb{r}$ si todo vector $\vec{w} \in W$ se expresa de una única
forma como
$$\vec{w} = \vec{u}\sb{1} + \cdots + \vec{u}\sb{r}, \quad \textrm{con }\vec{u}\sb{i} \in W\sb{i}, \quad i=1,\ldots, r.$$
Lo notaremos como
$$W = W\sb{1} \oplus \cdots \oplus W\sb{r} = \bigoplus\sb{i=1}^r W\sb{i}.$$
{{% /definition %}}

{{% example name="Ejemplo" %}}
[]{#sumdir label="sumdir"} En el espacio vectorial $\R^4$ consideramos
los subespacios vectoriales
$$W\sb{1} = \langle \vec{v}\sb{1} = \left( \begin{array}{r} 1 \cr 0 \cr 1 \cr 0 \end{array} \right), \vec{v}\sb{2} = \left( \begin{array}{r} 1 \cr 0 \cr 0 \cr 1 \end{array} \right) \rangle, \qquad 
W\sb{2} = \langle \vec{v}\sb{3} = \left( \begin{array}{r} 0 \cr 1 \cr 0 \cr 1 \end{array} \right), \vec{v}\sb{4} = \left( \begin{array}{r} 0 \cr 0 \cr 1 \cr 1 \end{array} \right) \rangle.$$
Entonces $W = W\sb{1} + W\sb{2} = \R^4$ y como
$\{ \vec{v}\sb{1}, \vec{v}\sb{2}, \vec{v}\sb{3}, \vec{v}\sb{4} \}$ es base de $\R^4$,
tenemos la unicidad de la expresión de todo vector de $W$ como suma de
vectores de $W\sb{1}$ y $W\sb{2}$. Por tanto, $\R^4 = W\sb{1} \oplus W\sb{2}$.
{{% /example %}}

Existen otras caracterizaciones de la suma directa que nos serán de
utilidad posteriormente.

{{% theorem name="Propiedades de la suma directa" %}}
Sean $W\sb{1}, \ldots, W\sb{r} \subset V$ subespacios vectoriales y
$W = W\sb{1} + \cdots + W\sb{r}$.

1.  Si ${\mathcal B}\sb{i}, i=1,\ldots, r$ son bases respectivas de cada
    $W\sb{i}$, entonces $W$ es suma directa de $W\sb{1}, \ldots, W\sb{r}$ si y
    solamente si la concatenación
    ${\mathcal B} = \bigcup\sb{i=1}^r {\mathcal B}\sb{i}$ es base de $W$.

2.  $W$ es suma directa de $W\sb{1}, \ldots, W\sb{r}$ si y solamente si una
    expresión de la forma
    $$\vec{0} = \vec{u}\sb{1} + \cdots + \vec{u}\sb{r}, \text{ con cada } \vec{u}\sb{i} \in W\sb{i}$$
    implica que $\vec{u}\sb{1} = \cdots = \vec{u}\sb{r} = \vec{0}$.

3.  Si $W$ es de dimensión finita, $W$ es suma directa de
    $W\sb{1}, \ldots, W\sb{r}$ si y solamente si
    $\dim W = \sum\sb{i=1}^r \dim W\sb{i}$.
{{% /theorem %}}

{{% proof %}}
1.  Supongamos que $W$ es suma directa de $W\sb{1}, \ldots, W\sb{r}$. Es claro,
    por la definición de suma, que ${\mathcal B}$ es un sistema
    generador de $W$ y por el teorema de coordenadas únicas (pág. ) es
    una base de $W$. El recı́proco se sigue de nuevo por la unicidad de
    la expresión de un vector de $W$ con respecto a una base del
    subespacio.

2.  Si $W$ es suma directa de $W\sb{1}, \ldots, W\sb{r}$, el vector $\vec{0}$
    admite una expresión *única* como suma de vectores de los
    subespacios $W\sb{i}$. Tal expresión es
    $$\vec{0} = \underbrace{\vec{0}}\sb{\in W\sb{1}} + \cdots + \underbrace{\vec{0}}\sb{\in W\sb{r}}$$
    de donde tenemos la implicación. Recı́procamente, sea $\vec{w} \in W$
    y supongamos que tenemos las expresiones $$\begin{aligned}
          \vec{w} & = \vec{u}\sb{1} + \cdots + \vec{u}\sb{r} & \vec{u}\sb{i} \in W\sb{i}, i=1, \ldots, r \cr & = \vec{u}'_1 + \cdots + \vec{u}'_r, & \vec{u}'_i \in W\sb{i}, i=1, \ldots, r.
\end{aligned}$$ Entonces
    $$\vec{0} = \underbrace{\vec{u}\sb{1} - \vec{u}'_1}\sb{\in W\sb{1}} + \cdots + \underbrace{\vec{u}\sb{r} - \vec{u}'_r}\sb{\in W\sb{r}},$$
    y por la hipótesis esto implica que
    $\vec{u}\sb{i} = \vec{u}'_i, i=1,\ldots,r$, es decir, la expresión es
    única.

3.  Es consecuencia de la primera propiedad.
{{% /proof %}}

{{% remark %}}
En el caso de suma de dos subespacios ($r=2$), existe una
caracterización clásica de la suma directa: $W = W\sb{1} \oplus W\sb{2}$ si y
solamente si $W = W\sb{1} + W\sb{2}$ y $W\sb{1} \cap W\sb{2} = \vec{0}$. En efecto, si
$W = W\sb{1} \oplus W\sb{2}$ y $\vec{v} \in W\sb{1} \cap W\sb{2}$, entonces tenemos las
expresiones
$$\vec{v} = \underbrace{\vec{v}}\sb{\in W\sb{1}} + \underbrace{\vec{0}}\sb{\in W\sb{2}} = \underbrace{\vec{0}}\sb{\in W\sb{1}} + \underbrace{\vec{v}}\sb{\in W\sb{2}}.$$
Entonces $\vec{v} = \vec{0}$ por la unicidad de la expresión. De manera
recı́proca, sea $\vec{w} \in W\sb{1} + W\sb{2}$ que se puede expresar como
$$\vec{w} = \vec{u}\sb{1} + \vec{u}\sb{2} = \vec{u}'_1 + \vec{u}'_2, \text{ con } \vec{u}\sb{1}, \vec{u}'_1 \in W\sb{1}, \vec{u}\sb{2}, \vec{u}'_2 \in W'_2.$$
Entonces $\vec{u}\sb{1} - \vec{u}'_1 = \vec{u}'_2 - \vec{u}\sb{2}$ es un vector que
pertenece a $W\sb{1} \cap W\sb{2}$. Por tanto, $\vec{u}\sb{1} = \vec{u}'_1$ y
$\vec{u}\sb{2} = \vec{u}'_2$.
{{% /remark %}}

{{% remark %}}
Si $V$ es un espacio vectorial de dimensión finita y
$W\sb{1} \varsubsetneq V$ un subespacio propio, existe $W\sb{2} \subset V$
subespacio vectorial tal que $V = W\sb{1} \oplus W\sb{2}$. Basta construir una
base ${\mathcal B}\sb{1}$ de $W\sb{1}$ y ampliarla a una base de $V$. Los
vectores de dicha ampliación generan a $W\sb{2}$.
{{% /remark %}}

## Espacio cociente

Estudiamos ahora una noción que es básica en muchas ramas de las
matemáticas, en particular en el álgebra lineal: el *espacio cociente*.
Fijaremos a partir de ahora un espacio vectorial $V$ y un subespacio
vectorial $W\subset V$. Básicamente, se puede pensar en el espacio
cociente de $V$ sobre $W$ como si fuera el espacio $V$, pero donde los
vectores de $W$ no tienen ningún valor: es decir, cualquier vector de
$W$ representa el vector $\vec{0}$ del espacio cociente; y si sumamos a
cualquier vector del cociente un vector de $W$, éste se queda igual.
Vamos a definirlo de forma rigurosa.

Decimos que dos vectores $\vec{u}, \vec{v} \in V$ son **$W$-equivalentes**
si $\vec{u} - \vec{v} \in W$, y lo notaremos como $\vec{u} \sim\sb{W} \vec{v}$.
La $W$-equivalencia define una relación de equivalencia en $V$. En
efecto:

-   Propiedad reflexiva: todo vector $\vec{u}$ es $W$-equivalente a sı́
    mismo, porque $\vec{u} - \vec{u} = \vec{0} \in W$.

-   Propiedad simétrica: si $\vec{u} \sim\sb{W} \vec{v}$, entonces
    $\vec{u} - \vec{v} \in W$. Como $W$ es subespacio vectorial, el
    opuesto de este vector también pertenece a $W$, es decir,
    $-\vec{u} + \vec{v} \in W$, lo que implica que $\vec{v} \sim\sb{W} \vec{u}$.

-   Propiedad transitiva: si $\vec{u} \sim\sb{W} \vec{v}$ y
    $\vec{v} \sim\sb{W} \vec{w}$, entonces $\vec{u} - \vec{v} \in W$ y
    $\vec{v} - \vec{w} \in W$. Si sumamos estos vectores, obtendremos un
    nuevo vector de $W$:
    $$(\vec{u} - \vec{v}) + (\vec{v} - \vec{w}) =  \vec{u} - \vec{w} \in W,$$
    lo que significa que $\vec{u} \sim\sb{W} \vec{w}$.

{{% example name="Ejemplo" %}}
Consideremos en $\R^3$ el subespacio
$W = \langle \vec{u}\sb{1}, \vec{u}\sb{2} \rangle$, donde
$$\vec{u}\sb{1} =  \left( \begin{array}{c} 3\cr{}-2\cr{}-
1\end{array} \right), \vec{u}\sb{2} =  \left( \begin{array}{c} 0\cr{}-1\cr{}2 \end{array} \right).$$
Entonces
$$\vec{u} =  \left( \begin{array}{c} -2\cr{}1\cr{}4
\end{array} \right) \sim\sb{W} \vec{v} =  \left( \begin{array}{c} -11\cr{}3\cr{}
15\end{array} \right),$$ porque
$\vec{u} - \vec{v} = 3 \vec{u}\sb{1} - 4 \vec{u}\sb{2} \in W$. Observemos que para
comprobar si dos vectores son $W$-equivalentes, basta resolver un
sistema de ecuaciones.
{{% /example %}}

Dado un vector $\vec{v} \in V$, la clase de equivalencia de $\vec{v}$ es
el *conjunto* formado por todos los vectores $W$-equivalentes con
$\vec{w}$. Este conjunto se puede definir como
$\{ \vec{v} + \vec{w} ~|~ \vec{w} \in W \}$ y, por ese motivo, la clase de
equivalencia de denota como $\vec{v} + W$.

Si $\vec{u} + W = \vec{v}+W$ entonces existe $\vec{w} \in W$ tal que
$\vec{u} = \vec{v} + \vec{w}$, de donde $\vec{u} - \vec{v} \in W$, o lo que
es lo mismo, $\vec{u} \sim\sb{W} \vec{v}$. El recı́proco es inmediato. Por
tanto,
$$\vec{u} + W = \vec{v} + W \quad \text{ es equivalente a } \quad \vec{u} \sim\sb{W} \vec{v},  \quad  \text{ o bien } \quad \vec{u}-\vec{v}\in W.$$
Una consecuencia de este hecho es que dos clases de equivalencia
$\vec{u} + W$ y $\vec{v} + W$, o bien son disjuntas, o bien son iguales.

{{% definition %}}
Espacio cociente Sea $W$ un subespacio vectorial de un espacio vectorial
$V$. Llamaremos **espacio cociente** de $V$ sobre $W$, y lo denotaremos
$V/W$, al conjunto formado por las clases de equivalencia definidas por
$W$.
{{% /definition %}}

{{% example name="Ejemplo" %}}
En $\R^3$ consideremos el subespacio vectorial
$W = \langle \vec{e}\sb{1} \rangle$. Entonces $\R^3/W$ está formado por las
clases de equivalencia $\vec{u} + W$, en cada una de las cuales hay un
representante de la forma
$$\left( \begin{array}{c} 0 \cr \alpha\sb{2} \cr \alpha\sb{3} \end{array} \right) + W.$$
{{% /example %}}

Nuestro interés en este conjunto es que nos permite crear un nuevo
espacio vectorial.

{{% theorem name="Espacio cociente como espacio vectorial" %}}
Sea $W$ un subespacio vectorial de un $\K$-espacio vectorial $V$. El
espacio cociente $V/W$, con las operaciones

-   **Suma:** $(\vec{u} + W)+ (\vec{v}+W) = (\vec{u}+\vec{v})+W$,

-   **Producto por escalar:** $\alpha (\vec{u}+W)= (\alpha \vec{u})+W$,

es un espacio vectorial sobre $\K$. Además, si $V$ es de dimensión
finita, se tiene: $$\dim(V/W)=\dim(V)-\dim(W).$$
{{% /theorem %}}

{{% proof %}}
Debemos probar, en primer lugar, que las operaciones están bien
definidas. Esto es, que si $\vec{u}+W= \vec{u}'+W$ y además
$\vec{v}+W=\vec{v}'+W$, entonces las clases de equivalencia
$(\vec{u}+\vec{v})+W$ y $(\vec{u}'+\vec{v}')+W$ son iguales. Pero sabemos
que $\vec{u}\sim\sb{W} \vec{u}'$, luego $\vec{u}-\vec{u}'\in W$. Análogamente
$\vec{v}-\vec{v}'\in W$. Por tanto,
$(\vec{u}-\vec{u}')+(\vec{v}-\vec{v}')=(\vec{u}+\vec{v})-(\vec{u}'+\vec{v}')\in W$.
Es decir, $(\vec{u}+\vec{v})\sim\sb{W} (\vec{u}'+\vec{v}')$, luego
$(\vec{u}+\vec{v})+W=(\vec{u}'+\vec{v}')+W$ como querı́amos demostrar.

Por otro lado, si $\vec{u}+W=\vec{u}'+W$ y $\alpha \in \K$, entonces
$(\vec{u}-\vec{u}')\in W$, luego
$\alpha (\vec{u} -\vec{u}')=\alpha \vec{u}-\alpha \vec{u}'\in W$. Por tanto
$(\alpha \vec{u})+W=(\alpha \vec{u}')+W$, y se obtiene el resultado. La
demostración de que $V/W$ es un espacio vectorial es directa. Observemos
que el elemento neutro de la suma de clases es la clase $\vec{0}+W$.

Para probar la fórmula que relaciona sus dimensiones, tomemos una base
${\mathcal B}\sb{1}=\{\vec{u}\sb{1},\ldots,\vec{u}\sb{r}\}$ de $W$. Esta base se puede
ampliar a una base
$${\mathcal B} =  \{\vec{u}\sb{1},\ldots,\vec{u}\sb{r},\vec{u}\sb{r+1},\ldots,\vec{u}\sb{n}\}$$
de $V$. Vamos a probar que
${\mathcal B}\sb{2} = \{ \vec{u}\sb{r+1}+W, \ldots, \vec{u}\sb{n}+W \}$ es una base
de $V/W$, y esto demostrará el resultado.

Probemos primero que ${\mathcal B}\sb{2}$ es un sistema generador. Sea
$\vec{v}+W$ una clase de equivalencia cualquiera. Como $\vec{v}\in V$,
podremos escribirlo como combinación lineal de los elementos de
${\mathcal B}$. Es decir,
$\vec{v}=\alpha _1\vec{u}\sb{1}+\cdots +\alpha _n \vec{u}\sb{n}$. Sea
$\vec{u}=\alpha _1\vec{u}\sb{1}+\cdots+\alpha _r\vec{u}\sb{r}$. Claramente
$\vec{u}\in W$, luego $\vec{u}'=\vec{v}-\vec{u} \sim\sb{W} \vec{v}$, donde
$\vec{u}'=\alpha\sb{r+1}\vec{u}\sb{r+1}+\cdots +\alpha _n \vec{u}\sb{n}$. Pero en
ese caso
$\vec{v}+W=\vec{u}'+W= \alpha\sb{r+1}(\vec{u}\sb{r+1}+W)+\cdots+ \alpha\sb{n} (\vec{u}\sb{n}+W)$.
Es decir, cualquier clase de equivalencia, $\vec{v}+W$, puede escribirse
como combinación lineal de los elementos de ${\mathcal B}\sb{2}$.

La demostración estará completa si probamos que ${\mathcal B}\sb{2}$ es un
sistema linealmente independiente. Supongamos que tenemos una
combinación lineal
$$\alpha\sb{r+1}(\vec{u}\sb{r+1}+W)+\cdots+ \alpha _n(\vec{u}\sb{n}+W)=\vec{0}+W.$$
Esto implica que
$$(\alpha\sb{r+1}\vec{u}\sb{r+1}+\cdots+\alpha _n \vec{u}\sb{n})+W=\vec{0}+W,$$ es
decir, $(\alpha\sb{r+1}\vec{u}\sb{r+1}+\cdots+\alpha _n \vec{u}\sb{n})\in W$. Pero
el subespacio vectorial
$$W\sb{1} = \langle \vec{u}\sb{r+1},\ldots,\vec{u}\sb{n} \rangle \text{ verifica que } V = W \oplus W\sb{1},$$
ya que ${\mathcal B}$ es una base. Luego la única posibilidad es que
$$(\alpha\sb{r+1}\vec{u}\sb{r+1}+\cdots+\alpha _n \vec{u}\sb{n})=\vec{0}, \text{ por lo que } \alpha _{r+1}=\cdots=\alpha _n=0.$$
Esto nos dice que los elementos de ${\mathcal B}\sb{2}$ son linealmente
independientes.
{{% /proof %}}

Sea $V$ un espacio vectorial sobre $\K$, de dimensión finita, en el que
hemos fijado una base respecto de la que tomamos coordenadas.

{{% example name="Base de $V/W$ y coordenadas" %}}
-   Calculamos una base ${\mathcal B}\sb{1}=\{\vec{u}\sb{1},\ldots,\vec{u}\sb{r}\}$ de
    $W$.

-   Completamos a una base del espacio completo
    ${\mathcal B}=\{\vec{u}\sb{1},\ldots,\vec{u}\sb{r},\vec{u}\sb{r+1},\ldots,\vec{u}\sb{n} \}$
    (método de ampliación, p. ).

-   Una base de $V/W$ es
    ${\mathcal B}\sb{2}= \{ \vec{u}\sb{r+1}+W,\ldots,\vec{u}\sb{n}+W \}$.

Sea $\vec{v}\in V$ un vector cualquiera.

-   Expresamos $\vec{v}$ como combinación lineal de los vectores de la
    base ${\mathcal B}$:
    $$\vec{v}=\alpha\sb{1}\vec{u}\sb{1}+\cdots +\alpha\sb{r}\vec{u}\sb{r}+\alpha\sb{r+1}\vec{u}\sb{r+1}+\cdots +\alpha\sb{n}\vec{u}\sb{n}.$$

-   Entonces
    $\vec{v} + W = \alpha\sb{r+1} (\vec{u}\sb{r+1} + W) + \cdots + \alpha\sb{n} (\vec{u}\sb{n} + W)$,
    de donde
    $$(\vec{v}+W)\sb{{\mathcal B}\sb{2}}= \left( \begin{array}{c} \alpha\sb{r+1} \cr \vdots \cr \alpha\sb{n} \end{array} \right).$$
{{% /example %}}

[]{#metodo:base-cociente label="metodo:base-cociente"}

{{% example name="Ejemplo" %}}
En $V=\R^3$ consideremos el subespacio $W$ definido por las ecuaciones
$$W\colon\left\\{\begin{array}{l}
                 x\sb{1}-2x\sb{2}=0\cr
                 x\sb{3}=0
                \end{array}\right. \mbox{ y el vector } \vec{v} = \left( \begin{array}{r} 1 \cr 1 \cr 1 \end{array} \right).$$
Vamos a calcular una base de $V/W$ y las coordenadas del vector
$\vec{v}+W$ respecto a dicha base.

Una base de $W$ es ${\mathcal B}\sb{1}=\{ \vec{u}\sb{1} \}$, donde
$$\vec{u}\sb{1} = \left( \begin{array}{r} 2 \cr 1 \cr 0 \end{array} \right).$$
Ampliamos a una base de $V$: $$\left(\begin{array}{c|ccc}
         2 & 1 & 0 & 0\cr
         1 & 0 & 1 & 0\cr
         0 & 0 & 0 & 1
        \end{array}\right)\mapright{\text{Gauss}}
  \left(\begin{array}{c|ccc}
         2 & 1 & 0 & 0\cr
         0 & -1/2 & 1 & 0\cr
         0 & 0 & 0 & 1
        \end{array}\right) .$$ Luego
${\mathcal B} = \{ \vec{u}\sb{1}, \vec{e}\sb{1}, \vec{e}\sb{3} \}$ es una base de $V$,
y una base de $V/W$ es ${\mathcal B}\sb{2}=\{ \vec{e}\sb{1}+W,\vec{e}\sb{3}+W\}$.

Para hallar las coordenadas de $\vec{v} + W$ respecto de ${\mathcal B}\sb{2}$
hacemos lo siguiente $$\left(\begin{array}{ccc|c}
         2 & 1 & 0 & 1\cr
         1 & 0 & 0 & 1\cr
         0 & 0 & 1 & 1
        \end{array}\right)\mapright{\text{G-J}}
  \left(\begin{array}{ccc|c}
         1 & 0 & 0 & 1\cr
         0 & 1 & 0 & -1\cr
         0 & 0 & 1 & 1
        \end{array}\right) .$$ Entonces las coordenadas de $\vec{v}$
respecto de la base ${\mathcal B}$, y las de $\vec{v}+W$ respecto de
${\mathcal B}\sb{2}$ son
$$\vec{v}\sb{\mathcal B}= \left( \begin{array}{r} 1 \cr -1 \cr 1 \end{array} \right), \quad\mbox{ y }\quad (\vec{v} + W)\sb{{\mathcal B}\sb{2}} = \left( \begin{array}{r} -1 \cr 1 \end{array} \right).$$
Otra forma es partir de la expresión
$\vec{v} + W = \alpha\sb{1} (\vec{e}\sb{1} + W) + \alpha\sb{2} (\vec{e}\sb{2} + W)$, que
implica que
$$\left( \begin{array}{c} 1- \alpha\sb{1} \cr 1 \cr 1- \alpha\sb{2} \end{array} \right) \in W\colon\left\\{\begin{array}{l}
                 x\sb{1}-2x\sb{2}=0\cr
                 x\sb{3}=0
                \end{array}\right. \text{ de donde } \left\\{ \begin{array}{l} 1-\alpha\sb{1} - 2 = 0, \cr 1-\alpha\sb{3} = 0. \end{array} \right.$$
Entonces $\alpha\sb{1} = -1, \alpha\sb{2} = 1$ y llegamos a la misma conclusión.
{{% /example %}}

Al igual que podemos construir una base de $V/W$ y las coordenadas de un
vector $\vec{v} + W$, también tenemos un criterio para decidir si un
sistema $T = \{ \vec{v}\sb{1} + W, \ldots, \vec{v}\sb{s} + W \}$ es linealmente
independiente en el espacio cociente $V/W$. Una expresión de la forma
$$\alpha\sb{1} (\vec{v}\sb{1} + W) + \cdots + \alpha\sb{s} (\vec{v}\sb{s} + W) = \vec{0} + W$$
es equivalente a $\alpha\sb{1} \vec{v}\sb{1} + \cdots + \alpha\sb{s} \vec{v}\sb{s} \in W$.
Tiene solución no trivial (algún $\alpha\sb{i}$ no nulo) si y solamente si
$\langle \vec{v}\sb{1}, \ldots, \vec{v}\sb{s} \rangle \cap W \ne \vec{0}$. Tenemos
ası́ dos métodos para resolver el problema:

1.  Mediante las ecuaciones implı́citas de $W$. Sustituimos las
    ecuaciones paramétricas
    $\vec{x}=\alpha\sb{1}\vec{v}\sb{1}+\cdots+\alpha\sb{s}\vec{v}\sb{s}$ de
    $\langle \vec{v}\sb{1},\ldots,\vec{v}\sb{s}\rangle$ en las ecuaciones
    implı́citas de $W$, lo que proporciona un sistema lineal homogéneo en
    las incógnitas $\alpha\sb{1}, \ldots, \alpha\sb{s}$. El sistema $T$ es
    linealmente independiente si y solamente si la única solución es la
    trivial.

2.  Mediante un sistema generador
    $W=\langle \vec{w}\sb{1}, \ldots, \vec{w}\sb{r}\rangle$. Formamos la matriz
    $$B = \left( \begin{array}{ccc|ccc} \vec{w}\sb{1} & \cdots & \vec{w}\sb{r} & \vec{v}\sb{1} & \cdots & \vec{v}\sb{s} \end{array} \right),$$
    y calculamos una forma escalonada
    $E\sb{B} = \left( \begin{array}{c|c} C\sb{1} & C\sb{2} \end{array} \right)$. El
    sistema $T$ es linealmente independiente si y solamente si todas las
    columnas de $C\sb{2}$ aportan pivote.

{{% example name="Independencia lineal en $V/W$" %}}
Sea $T = \{ \vec{v}\sb{1} + W, \ldots, \vec{v}\sb{s} + W \} \subset V/W$ y
$S\sb{W} = \{ \vec{w}\sb{1}, \ldots, \vec{w}\sb{r} \}$ un sistema generador de $W$,
con $\dim V = n$.

-   Formamos la matriz
    $$B = \left( \begin{array}{ccc|ccc} \vec{w}\sb{1} & \ldots & \vec{w}\sb{r} & \vec{v}\sb{1} & \ldots & \vec{v}\sb{s} \end{array} \right)\sb{n \times (r+s)}.$$

-   Calculamos
    $B \mapright{\text{Gauss}} E\sb{B} = \left( \begin{array}{c|c} C\sb{1} & C\sb{2} \end{array} \right)$,
    donde $C\sb{1}$ es una matriz $n \times r$ y $C\sb{2}$ es una matriz
    $n \times s$.

-   El sistema $T$ es linealmente independiente si y solamente si todas
    las columnas de $C\sb{2}$ aportan pivote.
{{% /example %}}

{{% example name="Ejemplo" %}}
En el $\R$-espacio vectorial $V=\R^3$, y fijada la base estándar,
consideramos $W$ el subespacio vectorial definido por
$$W \equiv \left\\{ \begin{array}{ll}
 x\sb{1} -x\sb{3} = 0 \cr  x\sb{2} - x\sb{3} = 0
\end{array}\right.$$ Sea $T = \{ \vec{v}\sb{1} + W, \vec{v}\sb{2} + W \}$, donde
$\vec{v}\sb{1} = (2,1,3)^t, \vec{v}\sb{2} = (0,-1,1)^t$. De la forma habitual,
calculamos una base de $W$, que es
$\mathcal{B}\sb{W} = \{ \vec{w}\sb{1} = (1,1,1)^t \}$. Formamos la matriz
$$B = \left( \begin{array}{c|cc} \vec{w}\sb{1} & \vec{v}\sb{1} & \vec{v}\sb{2} \end{array} \right) \mapright{\text{Gauss}} \left( \begin{array}{r|rr} 1 & -1 & 1 \cr 0 & 2 & -1 \cr 0 & 0 & 0 \end{array} \right).$$
La tercera columna no aporta pivote, por lo que el sistema $T$ es
linealmente dependiente en $V/W$.

Otra forma de ver que $T$ es linealmente dependiente es aprovechando la
definición de $W$ mediante ecuaciones implı́citas para resolver el
problema. Consideramos unas ecuaciones paramétricas de
$\langle \vec{v}\sb{1},\vec{v}\sb{2}\rangle$: $$\left\\{\begin{array}{l}
     x\sb{1}=2 \alpha\sb{1} \cr x\sb{2}=\alpha\sb{1} - \alpha\sb{2} \cr x\sb{3}=3 \alpha\sb{1} + \alpha\sb{2}
   \end{array}\right.$$ Sustituyéndolas en las ecuaciones implícitas de
$W$, obtenemos:
$$\left\\{ \begin{array}{l} (2\alpha\sb{1}) - (3\alpha\sb{1} + \alpha\sb{2}) = 0, \cr (\alpha\sb{1} - \alpha\sb{2}) - (3\alpha\sb{1} + \alpha\sb{2}) = 0. \end{array} \right.$$
Es decir:
$$\left\\{ \begin{array}{l} -\alpha\sb{1} - \alpha\sb{2} = 0, \cr -2\alpha\sb{1} - 2\alpha\sb{2} = 0. \end{array} \right.$$
Este sistema tiene solución no trivial $\alpha\sb{1} = \alpha\sb{2} = t$, por lo
que $T$ es linealmente dependiente.
{{% /example %}}
