+++
title = "Espacios vectoriales"
weight = 50
+++



## Estructuras algebraicas.

En temas anteriores hemos definido matrices y vectores, estudiando
algunas de sus propiedades. También hemos trabajado con cuerpos de
escalares, suponiendo que se trataba de $\Q$, $\R$ o $\C$, pero sin dar
más detalles. Ahora vamos a estudiar con rigor estos conceptos.
Definiremos algunas de las principales estructuras que se utilizan en
álgebra, como son: grupos, anillos, cuerpos y espacios vectoriales. A
continuación nos centraremos en una de las estructuras que se estudian
en esta asignatura: los espacios vectoriales.

Las **estructuras algebraicas** son conjuntos donde hay definidas
ciertas operaciones, que satisfacen unas determinadas propiedades. Las
operaciones pueden ser de varios tipos. Por ejemplo, una **operación
binaria interna**, definida en un conjunto $X$, es una función que a dos
elementos de $X$ (dados en orden), le hace corresponder otro elemento de
$X$. Es decir, una función $$p:\: X\times X \rightarrow X.$$ Por
ejemplo, $p$ podrı́a ser la suma, la diferencia o la multiplicación de
números reales. Observemos que, en ocasiones (la diferencia de números
reales, por ejemplo) el orden en que se den los dos elementos implicados
influye en el resultado.

Cuando se trabaja con una operación interna, se suele utilizar un
sı́mbolo, por ejemplo $*$, de manera que el resultado de aplicar la
operación a dos elementos, $a$ y $b$, se escribe $a* b$. Un ejemplo
tı́pico es el sı́mbolo $+$ para la suma de números. En ocasiones, ni
siquiera se utiliza sı́mbolo alguno, como en el caso del producto de
números, donde $ab$ representa el producto de $a$ y $b$.

La primera estructura algebraica que estudiaremos, una de las más
básicas y utilizadas, es la de **grupo**:

{{% definition %}}
Grupo

Sea $G$ un conjunto no vacı́o, y sea $*$ una operación binaria interna
definida en $G$. Se dice que $(G,*)$ es un **grupo** si se cumplen las
siguientes propiedades:

1.  **Asociativa**: $(a*b)*c=a*(b*c)$, para todo $a,b,c\in G$.

2.  **Elemento neutro**: existe $e\in G$ tal que $a*e=e*a=a$, para todo
    $a\in G$.

3.  **Elemento opuesto**: Para todo $a\in G$, existe $a'\in G$ tal que
    $a*a'=a' *a = e$.
{{% /definition %}}

En un grupo hay dos resultados sencillos de unicidad:

-   El elemento neutro es único. Si $e, e' \in G$ verifican la condición
    de elemento neutro, entonces $e * e' = e$, por ser $e'$ elemento
    neutro, pero también $e*e' = e'$ por la misma razón para $e$. Por
    tanto, $e=e'$.

-   Para todo $a \in G$, el elemento opuesto de $a$ es único. Si
    $a', a'' \in G$ son elementos opuestos de $a$, entonces
    $a'= a'*e = a' *(a*a'')=(a'*a)*a'' = e*a'' =a''$.

Normalmente, la operación interna $*$ será la *suma* o el *producto* de
elementos. En la notación *aditiva*, el elemento neutro se denota $0$, y
el elemento opuesto a $a$ se denota $-a$. En la notación
*multiplicativa*, el elemento neutro se denota $1$, y el elemento
opuesto a $a$, que en este caso se llama el *inverso* de $a$, se suele
denotar $a^{-1}$, o bien $\displaystyle \frac{1}{a}$.

Un grupo es **abeliano** o **conmutativo** si la operación es
conmutativa, es decir, si para todo $a, b \in G$ se verifica
$a*b = b*a$.

{{% example name="Ejemplo" %}}
Algunos ejemplos de grupos son los siguientes:

-   $(\Z,+)$,$(\Q,+)$,$(\R,+)$ y $(\C,+)$ son grupos abelianos aditivos.

-   $(\Q\backslash\{0\},\cdot)$,$(\R\backslash\{0\},\cdot)$ y
    $(\C\backslash\{0\},\cdot)$,donde $\cdot$ se refiere al producto,
    son grupos abelianos multiplicativos.

-   El conjunto de matrices $m \times n$ con entradas en un cuerpo $\K$
    (ahora veremos la definición de cuerpo), junto con la suma de
    matrices, es un grupo abeliano aditivo.

-   El conjunto de matrices cuadradas $n\times n$ **invertibles** con
    entradas en un cuerpo $\K$, junto con la multiplicación de matrices,
    forma un grupo que se llama **grupo lineal** de orden $n$ sobre
    $\K$, y se denota $\Gl(n,\K)$. Este grupo **no es abeliano**.

-   El conjunto de matrices cuadradas $n\times n$ con entradas en un
    cuerpo $\K$, y **con determinante igual a 1**, junto con la
    multiplicación de matrices, forma un grupo que se llama **grupo
    especial lineal** de orden $n$ sobre $\K$, y se denota $\SL(n,\K)$.
    Tampoco es abeliano.

-   Los vectores de $n$ coordenadas, con la suma de vectores, forman un
    grupo abeliano.
{{% /example %}}

En ocasiones, se define más de una operación interna sobre un conjunto.
Existen estructuras que dependen de dos o más operaciones. Por ejemplo,
la más sencilla es la estructura de *anillo*. Usaremos las notaciones
tradicionales, $+$ y $\cdot$, para las dos operaciones internas, pero
debemos recordar que pueden ser operaciones cualesquiera verificando las
condiciones de la definición:

{{% definition %}}
Anillo

Sea $A$ un conjunto no vacı́o, y sean $+,\cdot$ dos operaciones binarias
internas, que llamaremos *suma* y *producto*, definidas en $A$. Se dice
que $(A,+,\cdot)$ es un **anillo**, si se cumplen las siguientes
propiedades:

1.  $(A,+)$ es un grupo abeliano.

2.  **Propiedad asociativa del producto:**
    $(a\cdot b)\cdot c=a\cdot (b\cdot  c)$, para todo $a,b,c\in A$.

3.  **Propiedad distributiva del producto respecto a la suma:**
    $$\begin{cases}
     a\cdot (b+c)= a\cdot b + a\cdot c,  & \text{para todo } a,b,c\in A, \cr \cr
     (a+b)\cdot c= a\cdot c + b\cdot c, & \text{para todo } a,b,c\in A.
    \end{cases}$$
{{% /definition %}}

Si se verifica alguna propiedad más, tenemos tipos especiales de
anillos:

-   Un anillo $(A,+,\cdot)$, se dice que es **unitario**, o que tiene
    **elemento unidad**, si existe $u \in A$ tal que
    $a \cdot u = u \cdot a = a$ para todo $a \in A$.

-   Un anillo $(A,+,\cdot)$, se dice que es **conmutativo** si
    $a\cdot b=b\cdot a$, para todo $a,b\in A$.

{{% example name="Ejemplo" %}}
Algunos ejemplos de anillo son los siguientes:

-   $(\Z,+,\cdot)$, $(\Q,+,\cdot)$, $(\R,+,\cdot)$ y $(\C,+,\cdot)$ son
    anillos conmutativos.

-   Si $\Z[x]$ es el conjunto de los polinomios en la variable $x$, con
    coeficientes en $\Z$, y definimos naturalmente la suma $(+)$ y el
    producto $(\cdot)$ de dos polinomios, entonces $(\Z[x],+,\cdot)$ es
    un anillo conmutativo.

-   De igual modo, $(\Q[x],+,\cdot)$, $(\R[x],+,\cdot)$, y
    $(\C[x],+,\cdot)$ son anillos conmutativos.

-   El conjunto de matrices $n\times n$ con entradas en un cuerpo $\K$,
    con la suma y el producto de matrices, es un anillo **no
    conmutativo**.
{{% /example %}}

En resumen, si $(A,+,\cdot)$ es un anillo, entonces $(A,+)$ es un grupo,
y $(A,\cdot)$ es *casi* un grupo: sólo le falta que todo elemento tenga,
y que exista el elemento unidad.

Hay elementos, como el $0$ (el elemento neutro de la suma), que no
pueden tener inverso multiplicativo. Pero si cualquier otro elemento
puede invertirse, es decir, si $(A\backslash\{0\},\cdot)$ fuera un
grupo, y aún más, un grupo abeliano, entonces estarı́amos ante un
*cuerpo*.

{{% definition %}}
Cuerpo

Sea $\K$ un conjunto no vacı́o, y sean $+,\cdot$ dos operaciones
internas, que llamaremos *suma* y *producto*, definidas en $\K$. Se dice
que $(\K,+,\cdot)$ es un **cuerpo** si se cumplen las siguientes
propiedades:

1.  $(\K,+)$ es un grupo abeliano.

2.  $(\K\backslash\{0\},\cdot)$ es un grupo abeliano, donde $0$ es el
    elemento neutro de la suma.

3.  **Propiedad distributiva del producto respecto a la suma:**
    $$a\cdot (b+c)= a\cdot b + a\cdot c,  \quad  \text{ para todo } a,b,c\in \K.$$
{{% /definition %}}

Dicho de otra forma, un cuerpo es un anillo conmutativo, con elemento
unidad, donde todo elemento no nulo tiene inverso.

Observemos que la propiedad distributiva solamente tiene una condición.
Esto es porque el producto es conmutativo, luego la otra condición es
consecuencia de la primera.

{{% example name="Ejemplo" %}}
Algunos ejemplos de cuerpo son los siguientes:

-   $(\Q,+,\cdot)$, $(\R,+,\cdot)$ y$(\C,+,\cdot)$ son cuerpos.

-   Notaremos por $\Gl(n,\K)$ al conjunto de matrices invertibles y
    $\SL(n,\K)$ al conjunto de matrices cuyo determinante es 1. Estos
    anillos **no son cuerpos**, ya que el producto de matrices no es
    conmutativo.
{{% /example %}}

Los cuerpos tienen multitud de propiedades, que no se estudiarán en esta
asignatura. Los usaremos para definir estructuras más complejas, que
generalicen las propiedades de los vectores, que hemos visto en los
temas anteriores.

Para ello debemos definir las *operaciones externas*. Consideremos un
conjunto $X$, y otro conjunto $\K$ que llamaremos *conjunto de
escalares*. Llamaremos **operación binaria externa** sobre $X$, a una
función que tome un elemento de $\K$ y un elemento de $X$, y dé como
resultado un elemento de $X$. Es decir, una función:
$$p: \K \times X \rightarrow X.$$ Normalmente, a una operación externa
de este tipo la denotaremos $\cdot$ y la llamaremos *multiplicación por
escalar*; y al resultado de aplicarla a un escalar $\alpha\in \K$ y a un
elemento $x\in X$, lo denotaremos $\alpha \cdot x$, o simplemente
$\alpha x$, y lo llamaremos producto de $\alpha$ por $x$.

Por tanto, si tenemos un conjunto $X$ y otro conjunto de escalares $\K$,
podemos tener operaciones internas en cada uno de esos conjuntos, y
operaciones externas entre ellos. Usando estas dos posibilidades, se
definen los *espacios vectoriales*.

{{% example name="Ejemplo" %}}
Consideremos el conjunto $\R[X]\sb{4}$ de polinomios en $\R[X]$ de grado
menor o igual que $4$. La operación suma es interna, y dota de
estructura de grupo abeliano a este conjunto. Una operación binaria
externa es el producto por elementos de $\R$. Sean $p(X) \in \R[X]\sb{4}$, y
$\alpha \in \R$. Entonces si
$$p(X) = a\sb{4} X^4 + a\sb{3} X^3 + a\sb{2} X^2 + a\sb{1} X + a\sb{0}$$ definimos
$$\alpha \cdot p(X) = (\alpha a\sb{4}) X^4 + (\alpha a\sb{3}) X^3 + (\alpha a\sb{2}) X^2 + (\alpha a\sb{1}) X + (\alpha a\sb{0}).$$
{{% /example %}}

Sean entonces $V$ y $\K$ conjuntos no vacı́os, con $+$ una operación
interna sobre $V$, y $\cdot$ una operación externa sobre $V$ con
conjunto de escalares $\K$, que llamaremos *producto por un escalar*.

{{% definition %}}
Espacio vectorial

Sea $\K$ un cuerpo. Diremos que $(V, +, \cdot)$ es un **espacio
vectorial** sobre $\K$ si se cumplen las siguientes propiedades:

1.  $(V,+)$ es un grupo abeliano.

2.  Si $\alpha \in \K$ y $\vec{v} \in V$, entonces
    $\alpha \cdot \vec{v} \in V$.

3.  El producto por escalar verifica las siguientes propiedades, para
    todo $\alpha, \beta \in \K$ y para todo $\vec{v}, \vec{w} \in V$:

    1.  $(\alpha +\beta )\vec{v} = \alpha \vec{v} +\beta \vec{v}$.

    2.  $\alpha (\vec{v} +\vec{w})= \alpha \vec{v} +\alpha \vec{w}$.

    3.  $\alpha (\beta \vec{v})= (\alpha \beta )\vec{v}$.

    4.  $1\vec{v} =\vec{v}$, donde $1$ es el elemento neutro de la
        multiplicación de $\K$.
{{% /definition %}}

A los elementos de un espacio vectorial los llamaremos **vectores**, y
los escribiremos **en negrita**. En un $\K$-espacio vectorial hay, por
tanto, cuatro operaciones: la suma y producto de escalares (operaciones
en el cuerpo base), la suma de vectores (operación interna en el espacio
vectorial) y el producto de vectores por escalares.

En las condiciones anteriores, también nos referiremos a $V$ como un
$\K$-espacio vectorial.

{{% example name="Ejemplo" %}}
Algunos ejemplos de espacios vectoriales son los siguientes:

-   Los vectores columna del producto cartesiano $\K^n$ que vimos
    anteriormente forman un espacio vectorial. El espacio vectorial de
    los vectores de $n$ coordenadas sobre un cuerpo $\K$, se denota
    $\K^n$. La suma se realiza coordenada a coordenada, y el producto
    por escalar también. Ejemplos de este tipo son $\R^2$ o $\R^3$. Los
    vectores fila de $\K^n$ también forman un $\K$-espacio vectorial.
    Podemos tener
    $$\K^{1 \times n} = \{ \left( \begin{array}{cccc} x\sb{1} & x\sb{2} & \ldots & x\sb{n} \end{array} \right), x\sb{i} \in \K \} \quad \mbox{ y } \quad \K^{n \times 1} = \left\\{ \left( \begin{array}{c} x\sb{1} \cr x\sb{2} \cr \vdots \cr x\sb{n} \end{array} \right), x\sb{i} \in \K \right\\}.$$
    En el contexto de los espacios vectoriales, no hay diferencia si se
    trata un elemento de $\K^n$ como fila o como columna. Cuando la
    distinción entre un vector fila o un vector columna sea irrelevante,
    o el contexto sea claro, usaremos la notación $\K^n$ para designar
    este espacio vectorial. En otros casos, cuando hablemos de
    coordenadas respecto a una base, usaremos la notación por columnas.

-   El conjunto $\MatK{m}{n}{\K}$ de las matrices $m\times n$ con
    entradas en un cuerpo $\K$, con la suma de matrices y el producto
    por escalar, forman un espacio vectorial. Observemos que el producto
    de matrices no se utiliza aquı́. En general, no tiene por qué existir
    una multiplicación de vectores en un espacio vectorial.

-   Dado $V$ un $\K$-espacio vectorial, sea $W =\{ \vec{0} \}$ el
    subconjunto de $V$ formado únicamente por el elemento neutro de la
    suma de $V$. Cualquier operación donde intervenga algún vector da
    como resultado el único elemento $\vec{0}$, por lo que las
    operaciones están bien definidas. Tiene estructura de espacio
    vectorial, y lo denominamos espacio vectorial **trivial**.

-   Los conjuntos de polinomios $\mathbb{Q}[x]$, $\mathbb{R}[x]$ y
    $\mathbb{C}[x]$ son espacios vectoriales con cuerpo de escalares,
    respectivamente, $\mathbb{Q}$, $\mathbb{R}$ y $\mathbb{C}$.

-   Los conjuntos $\mathbb{Q}[x]\sb{n}$, $\mathbb{R}[x]\sb{n}$ y
    $\mathbb{C}[x]\sb{n}$, formados por polinomios de grado menor o igual
    a $n$, son espacios vectoriales con cuerpo de escalares,
    respectivamente, $\mathbb{Q}$, $\mathbb{R}$ y $\mathbb{C}$.

-   El conjunto de soluciones de un sistema lineal homogéneo con
    coeficientes en un cuerpo $\K$ es un espacio vectorial sobre $\K$.
    Sea $A\sb{m \times n}$ una matriz, y llamemos
    $V = \{ \vec{v} \in \K^n ~|~ A \vec{v} = \vec{0} \}$. Es decir, $V$ es
    el conjunto de soluciones del sistema lineal homogéneo
    $A\vec{x}=\vec{0}$. Vamos a probar que $V$ es un espacio vectorial. En
    primer lugar, veamos la estructura de grupo. Es claro que
    $\vec{0} \in V$. Sean $\vec{v}, \vec{w} \in V$. Entonces $-\vec{v}$ es
    solución del sistema, y $\vec{v} + \vec{w}$ es un elemento de $V$,
    pues $$A(\vec{v} + \vec{w} ) = A \vec{v} + A \vec{w} = \vec{0}.$$ Ası́, la
    operación suma es interna, y hereda las propiedades de grupo de
    $\K^n$. El producto por un escalar verifica que
    $$A (\alpha \vec{v} ) = \alpha A \vec{v} = \alpha \vec{0} = \vec{0},$$ y
    también se verifican las propiedades de la definición de espacio
    vectorial.

-   El cuerpo base es fundamental en la definición del espacio
    vectorial, por lo que llamaremos habitualmente a $V$ un $\K$-espacio
    vectorial. Por ejemplo, veremos más adelante que no es lo mismo
    $\C^2$ como $\C$-espacio vectorial que como $\R$-espacio vectorial.
{{% /example %}}

Terminamos esta sección con algunas consecuencias sencillas de la
definición de espacio vectorial.

{{% theorem name="Propiedades fundamentales de un espacio vectorial" %}}
Si $V$ es un $\K$-espacio vectorial, se tienen las siguientes
propiedades, para todo $\alpha, \beta\in \K$ y todo
$\vec{v}, \vec{w}\in V$:

1.  $\alpha \vec{0} = \vec{0}$, donde $\vec{0}$ es el elemento neutro de la
    suma en $V$.

2.  $0\vec{v} = \vec{0}$, donde $0$ es el elemento neutro de la suma en
    $\K$.

3.  Si $\alpha \vec{v}= \vec{0}$ entonces, o bien $\alpha =0$ o bien
    $\vec{v}=\vec{0}$.

4.  $(-\alpha )\vec{v} = \alpha (-\vec{v})= -(\alpha \vec{v})$.

5.  Si $\alpha \vec{v}=\beta \vec{v}$ y $\vec{v} \neq \vec{0}$, entonces
    $\alpha =\beta$.

6.  Si $\alpha \vec{v} = \alpha \vec{w}$ y $\alpha \neq 0$, entonces
    $\vec{v}=\vec{w}$.
{{% /theorem %}}

{{% proof %}}
Probemos en primer lugar la propiedad de cancelación en $V$ como grupo:
si $\vec{v} + \vec{u} = \vec{v} + \vec{w}$, entonces $\vec{u} = \vec{w}$. En
efecto, queda $$\begin{aligned}
 (-\vec{v}) + \vec{v} + \vec{u} & = (-\vec{v}) + \vec{v} + \vec{w} & \text{ al sumar el opuesto de } \vec{v}, \cr
 (-\vec{v} + \vec{v}) + \vec{u} & = (-\vec{v} + \vec{v}) + \vec{w} & \text{ por la propiedad asociativa, } \cr
 \vec{0} + \vec{u} & = \vec{0} + \vec{w} & \text{ por la definición de opuesto,} \cr
 \vec{u} & = \vec{w} & \text{ por la definición de elemento neutro.}
\end{aligned}$$

1.  De la igualdad
    $$\alpha \vec{0} = \alpha (\vec{0} + \vec{0}) = \alpha \vec{0} + \alpha \vec{0},$$
    tenemos que $\alpha \vec{0} = \vec{0}$ por la propiedad de
    cancelación.

2.  Análogamente,
    $$\vec{v} = 1 \cdot \vec{v} = (1+0) \vec{v} = 1 \cdot \vec{v} + 0 \vec{v} = \vec{v} + 0 \vec{v}.$$
    Por la propiedad de cancelación, $0 \vec{v} = \vec{0}$.

3.  Supongamos que $\alpha \vec{v} = \vec{0}$. Si $\alpha = 0$, ya lo
    tenemos. Si $\alpha \ne 0$, existe $\alpha^{-1} \in \K$ y
    $$\alpha^{-1} (\alpha \vec{v}) = \alpha^{-1} \vec{0} \stackrel{1)}= \vec{0}.$$
    Como
    $\alpha^{-1}(\alpha \vec{v}) = (\alpha^{-1} \alpha) \vec{v} = 1 \vec{v} = \vec{v}$,
    tenemos el resultado.

4.  Veamos en primer lugar que $-(\alpha \vec{v}) = (-\alpha) \vec{v}$, es
    decir, el elemento opuesto de $\alpha \vec{v}$ es $(-\alpha) \vec{v}$.
    En efecto,
    $$\alpha \vec{v} + (-\alpha) \vec{v} = (\alpha +(-\alpha)) \vec{v} = 0 \vec{v} = \vec{0},$$
    de donde tenemos esta parte. Para la segunda,
    $$\alpha \vec{v} + \alpha (-\vec{v}) = \alpha (\vec{v} + (-\vec{v})) = \alpha \vec{0} = \vec{0}.$$

Los dos últimos apartados son consecuencias inmediatas de los
anteriores.
{{% /proof %}}

## Dependencia lineal

A partir de este momento estudiaremos propiedades de conjuntos de
vectores. En muchas ocasiones necesitaremos considerar listas ordenadas
de vectores, donde puede incluso haber vectores repetidos (recordemos
que en un conjunto no puede haber elementos repetidos, y el orden no
importa). A estas listas ordenadas de vectores las llamaremos **sistemas
de vectores**. Es habitual denotar los sistemas de vectores como si
fueran un conjunto, por ejemplo
$S=\{\vec{v}\sb{1},\vec{v}\sb{2},\ldots,\vec{v}\sb{r}\}$, pero si decimos que $S$ es un
**sistema de vectores**, entonces estamos fijando el orden
$\vec{v}\sb{1},\vec{v}\sb{2},\ldots,\vec{v}\sb{r}$, y estamos permitiendo tener
$\vec{v}\sb{i}=\vec{v}\sb{j}$ incluso si $i\neq j$.

Por abuso de notación, si $S=\{\vec{v}\sb{1},\vec{v}\sb{2},\ldots,\vec{v}\sb{r}\}$ y
$T=\{\vec{w}\sb{1},\vec{w}\sb{2},\ldots,\vec{w}\sb{t}\}$, denotaremos $S\cup T$ al
sistema
$\{\vec{v}\sb{1},\vec{v}\sb{2},\ldots,\vec{v}\sb{r},\vec{w}\sb{1},\vec{w}\sb{2},\ldots,\vec{w}\sb{t}\}$,
que es la concatenación de las dos listas ordenadas. Por tanto, la
"unión\" de un sistema de $r$ vectores y un sistema de $t$ vectores
siempre tiene $r+t$ vectores (aunque haya vectores repetidos). También,
si $S=\{\vec{v}\sb{1},\vec{v}\sb{2},\ldots,\vec{v}\sb{r}\}$, denotaremos
$S\setminus \{\vec{v}\sb{i}\}$ al sistema obtenido a partir de $S$ al
eliminar el vector que ocupa la posición $i$, manteniendo el orden de
los demás vectores. Es decir,
$S\setminus\{\vec{v}\sb{i}\}=\{\vec{v}\sb{1},\ldots,\vec{v}\sb{i-1},\vec{v}\sb{i+1},\ldots,\vec{v}\sb{r}\}$.

El concepto de combinación lineal ya nos ha aparecido cuando estudiamos
las operaciones elementales entre filas y columnas de una matriz. Ese
caso, que se corresponde con un conjunto $\K^m$, lo generalizamos a
espacios vectoriales arbitrarios.

{{% definition %}}
Combinación lineal Sea $V$ un $\K$-espacio vectorial. Dados $r$ vectores
$\vec{v}\sb{1}, \ldots, \vec{v}\sb{r}\in V$, llamamos **combinación lineal** de
estos vectores a cualquier expresión de la forma
$$\alpha _1 \vec{v}\sb{1} + \alpha _2 \vec{v}\sb{2} +\cdots +\alpha _r \vec{v}\sb{r} \in V, \text{ donde  } \alpha _1,\ldots,\alpha _r\in \K.$$
{{% /definition %}}

{{% example name="Ejemplo" %}}
En el $\Q$-espacio vectorial $V = \Q[x]$, consideremos el conjunto de
vectores $\{ 1, 1+x, 1+x+x^2 \}$. Entonces
$$p(x) = 2 \cdot 1 + (-2) \cdot (1+x) + 3 \cdot(1+x+x^2) = 3+x+3x^2$$ es
una combinación lineal del conjunto dado.
{{% /example %}}

{{% remark %}}
Obsérvese que, por definición, las combinaciones lineales de vectores
son finitas. Es decir, podemos partir de un conjunto infinito de
vectores, pero en la suma solamente intervienen un número finito de
elementos.
{{% /remark %}}

{{% definition %}}
Dependencia lineal Sea $V$ un $\K$-espacio vectorial. Diremos que un
vector $\vec{v}$ **depende linealmente** de un conjunto o de un sistema
de vectores $S \subset V$ si se puede escribir como combinación lineal
de vectores de $S$.
{{% /definition %}}

{{% example name="Ejemplo" %}}
En el ejemplo anterior, vemos que el polinomio $3+x+3x^2$ depende
linealmente del conjunto $\{ 1, 1+x, 1+x+x^2 \}$, pero no depende
linealmente del conjunto $\{ 1, 1+x \}$.
{{% /example %}}

{{% definition %}}
Conjunto linealmente (in)dependiente

Sea $V$ un $\K$-espacio vectorial.

-   Un sistema finito de vectores $S=\{ \vec{v}\sb{1}, \ldots, \vec{v}\sb{r} \}$
    es **linealmente dependiente** si la ecuación
    $$x\sb{1} \vec{v}\sb{1} + \cdots + x\sb{r} \vec{v}\sb{r} = \vec{0}$$ tiene solución no
    trivial.

-   Un conjunto finito de vectores $S\subset V$ es **linealmente
    dependiente** si el sistema formado por sus vectores (en cualquier
    orden) es linealmente dependiente.

-   Un subconjunto infinito $S \subset V$ es **linealmente dependiente**
    si contiene un conjunto finito de vectores linealmente dependiente.

En caso contrario, decimos que el sistema o el conjunto $S$ es
**linealmente independiente** o **libre**.
{{% /definition %}}

Un caso extremo de la definición es $S = \emptyset$, que lo consideramos
linealmente independiente. Si el conjunto o sistema $S$ es de la forma
$S = \{ \vec{v}\sb{1}, \ldots, \vec{v}\sb{r} \}$, diremos de forma extendida que
los vectores $\vec{v}\sb{1}, \ldots, \vec{v}\sb{r}$ son linealmente dependientes o
independientes en lugar de hablar de la dependencia o independencia de
$S$.

{{% example name="Ejemplo" %}}
Consideremos el sistema
$S = \{ \vec{u}\sb{1}, \vec{u}\sb{2}, \vec{u}\sb{3}, \vec{u}\sb{4} \}$ de vectores en $\R^3$
dados por
$$\vec{u}\sb{1} = \left( \begin{array}{c} 4\cr{}3\cr{}-2 \end{array} \right), \vec{u}\sb{2} =  \left( \begin{array}{c}  -4 \cr {} -5 \cr {} -2\end{array} \right), \vec{u}\sb{3} = \left( \begin{array}{c} 1\cr{}0\cr{}-5 \end{array} \right), \vec{u}\sb{4} =
 \left( \begin{array}{c} 1\cr{}0\cr{}-2 \end{array} \right).$$
Tratemos de determinar si $S$ es un sistema linealmente dependiente.
Planteamos si existen escalares no todos nulos $\alpha\sb{i}, i=1,2,3,4$
tales que
$$\vec{0} = \alpha\sb{1} \vec{u}\sb{1} + \alpha\sb{2} \vec{u}\sb{2} + \alpha\sb{3} \vec{u}\sb{3} + \alpha\sb{4} \vec{u}\sb{4}.$$
Se trata entonces de verificar si el sistema lineal homogéneo
$$\left\\{ \begin{array}{rrrrcr} 4 \alpha\sb{1} & -4 \alpha\sb{2} & + \alpha\sb{3} & + \alpha\sb{4} & = & 0, \cr 3 \alpha\sb{1} & -5 \alpha\sb{2} & & & = & 0, \cr -2 \alpha\sb{1} & -2 \alpha\sb{2} & -5 \alpha\sb{3} & -2 \alpha\sb{4} & = & 0, \end{array} \right.$$
tiene alguna solución no trivial. Observemos que la matriz de
coeficientes del sistema de ecuaciones es
$$\left( \begin{array}{cccc} 4&-4&1&1\cr{}3&-5&0&0 \cr{}-2&-2&-5&-2\end{array} \right),$$
cuyas columnas son los vectores de partida. El sistema de ecuaciones lo
resolvemos mediante eliminación gaussiana:
$$\left( \begin{array}{cccc} 4&-4&1&1\cr{}3&-5&0&0 \cr{}-2&-2&-5&-2\end{array} \right) \mapright{\mbox{Gauss}}   \left( \begin{array}{cccc} 4&-4&1&1\cr{}0&-8&-3&-3
\cr{}0&0&24&0\end{array} \right)
 = E.$$ El rango de la matriz de coeficientes es $3$, que es menor que
el número de incógnitas. Por tanto, tiene solución no trivial. Al
resolver el sistema de ecuaciones deducimos una combinación lineal de
los vectores de $S$ igual al vector $\vec{0}$:
$$\frac{5}{8} \cdot \vec{u}\sb{1} + \frac{3}{8} \cdot \vec{u}\sb{2} + 0 \cdot \vec{u}\sb{3} + (-1) \vec{u}\sb{4} = \vec{0}.$$
A partir de los pivotes de la matriz $E$, deducimos que el sistema
$S' = \{ \vec{u}\sb{1}, \vec{u}\sb{2}, \vec{u}\sb{3} \}$ es linealmente independiente,
pues forman las columnas básicas de la matriz de coeficientes. No puede
haber dependencia lineal entre estos vectores, pues entonces la
tendrı́amos entre
$$\vec{e}\sb{1} = \left( \begin{array}{r} 1 \cr 0 \cr 0 \end{array} \right), \vec{e}\sb{2} = \left( \begin{array}{r} 0 \cr 1 \cr 0 \end{array} \right), \vec{e}\sb{3} = \left( \begin{array}{r} 0\cr 0 \cr 1 \end{array} \right),$$
y es fácil ver que estos vectores forman un sistema linealmente
independiente.
{{% /example %}}

Si el espacio vectorial es $\K^n$, podemos usar los cálculos matriciales
de los temas anteriores para saber si un sistema de vectores
$S = \{ \vec{v}\sb{1}, \ldots, \vec{v}\sb{r}\} \subset \K^n$ es linealmente
dependiente o independiente.

{{% example name="Dependencia e independencia lineal en $\K^n$" %}}
-   Sea
    $A\sb{S} = \left( \begin{array}{c|c|c} \vec{v}\sb{1} & \cdots & \vec{v}\sb{r} \end{array} \right)$.

-   Calculamos $A\sb{S} \mapright{\text{Gauss}} E$.

-   Si alguna columna de $E$ no tiene pivote, la columna correspondiente
    de $A\sb{S}$ es combinación lineal de las anteriores, y el sistema $S$
    es *linealmente dependiente*.

-   Si todas las columnas de $E$ tienen pivote, entonces $S$ es
    *linealmente independiente*.

En resumen,

* $S$ es linealmente independiente si y solamente si $\rg(A\sb{S}) = r$.

{{% /example %}}

{{% example name="Ejemplo" %}}
El sistema $S = \{ \vec{u}\sb{1}, \vec{u}\sb{2}, \vec{u}\sb{3} \}$ de $\R^3$ dado por
$$\vec{u}\sb{1} =  \left( \begin{array}{c} 1\cr{}1\cr{}2 \end{array} \right), \vec{u}\sb{2} =  \left( \begin{array}{c} -1\cr{}0\cr{}1 \end{array} \right), \vec{u}\sb{3} =  \left( \begin{array}{c} 2\cr{}-1\cr{}2
\end{array} \right),$$ es linealmente independiente, pues
$$A\sb{S} =  \left( \begin{array}{ccc} 1&-1&2\cr{}1&0&-1 \cr{}2&1&2\end{array} \right) \mapright{\mbox{Gauss}}   \left( \begin{array}{ccc} 1&-1&2\cr{}0&1&-3
\cr{}0&0&7\end{array} \right).$$ Sin embargo, el sistema
$T = \{ \vec{u}\sb{1}, \vec{u}\sb{2}, \vec{v}\sb{3} \}$, con
$$\vec{v}\sb{3} =  \left( \begin{array}{c} 5\cr{}2\cr{}1 \end{array} \right)$$
verifica que
$$A\sb{T} =  \left( \begin{array}{ccc} 1&-1&5\cr{}1&0&2 \cr{}2&1&1\end{array} \right) \mapright{\mbox{Gauss}}   \left( \begin{array}{ccc} 1&-1&5\cr{}0&1&-3
\cr{}0&0&0\end{array} \right),$$ por lo que $T$ es un
sistema linealmente dependiente.
{{% /example %}}

{{% theorem name="Relaciones entre dependencia y combinación lineal" %}}
1.  Un sistema $S$ de vectores es linealmente dependiente si y solamente
    si existe un vector $\vec{v} \in S$ que depende linealmente de
    $S \setminus\{ \vec{v} \}$.

2.  Si un vector $\vec{u}$ depende linealmente del conjunto o sistema $S$
    y cada vector de $S$ depende linealmente del conjunto o sistema $T$,
    entonces $\vec{u}$ depende linealmente de $T$.

3.  Sea $S \subset V$ un sistema linealmente independiente. Si $\vec{v}$
    es un vector que no depende linealmente de los vectores de $S$,
    entonces $S\cup \{\vec{v}\}$ es un sistema linealmente independiente.
{{% /theorem %}}

{{% proof %}}
[]{#thm:depcomblin label="thm:depcomblin"}

1.  Supongamos que el sistema $S$ es linealmente dependiente. Entonces
    existen vectores $\vec{v}\sb{1},\ldots, \vec{v}\sb{r} \in S$ y escalares
    $\alpha\sb{1},\ldots,\alpha\sb{r} \in \K$, no todos nulos, tales que
    $$\alpha\sb{1} \vec{v}\sb{1} + \alpha\sb{2} \vec{v}\sb{2} +\cdots +\alpha _r \vec{v}\sb{r} = \vec{0}.$$
    Sabemos que existe al menos un $\alpha\sb{i}\neq 0$ por lo que podemos
    despejar
    $$\alpha\sb{i} \vec{v}\sb{i} = -\alpha\sb{1}\vec{v}\sb{1} - \cdots - \alpha\sb{i-1} \vec{v}\sb{i-1} - \alpha\sb{i+1} \vec{v}\sb{i+1} - \cdots - \alpha\sb{r} \vec{v}\sb{r},$$
    y al ser $\alpha\sb{i}\neq 0$, obtenemos
    $$\vec{v}\sb{i} = -\frac{\alpha\sb{1}}{\alpha\sb{i}}\vec{v}\sb{1} - \cdots - \frac{\alpha\sb{i-1}}{\alpha\sb{i}} \vec{v}\sb{i-1} - \frac{\alpha\sb{i+1}}{\alpha\sb{i}} \vec{v}\sb{i+1} - \cdots - \frac{\alpha\sb{r}}{\alpha\sb{i}} \vec{v}\sb{r},$$
    que es una expresión de $\vec{v}\sb{i}$ como combinación lineal de los
    demás; por tanto $\vec{v}\sb{i}$ depende linealmente de los demás.

    Supongamos ahora que existe un vector $\vec{v}$ que depende
    linealmente de $S \setminus \vec{v}$. Esto quiere decir que existe
    una combinación lineal
    $$\vec{v} = \beta\sb{1} \vec{v}\sb{1} + \cdots + \beta\sb{r} \vec{v}\sb{r}, \qquad \vec{v}\sb{k} \in S \setminus \{ \vec{v} \}.$$
    De esta igualdad se obtiene
    $$\beta\sb{1}\vec{v}\sb{1} + \cdots + \beta\sb{r} \vec{v}\sb{r} - \vec{v} = \vec{0},$$
    que es una expresión del vector $\vec{0}$ como combinación lineal de
    los vectores $\vec{v}\sb{1},\ldots, \vec{v}\sb{r}, \vec{v}$ donde no todos los
    coeficientes son nulos (el coeficiente de $\vec{v}$ es $-1$). Por
    tanto, el sistema $\{\vec{v}\sb{1},\ldots, \vec{v}\sb{r}, \vec{v} \}$ es
    linealmente dependiente y $S$ también lo es.

2.  Por hipótesis, podemos escribir $$\begin{gathered}
     \vec{u} = \alpha\sb{1}\vec{v}\sb{1}+\cdots + \alpha\sb{p} \vec{v}\sb{p}, \qquad \vec{v}\sb{i} \in S \text{ y además} \cr
     \vec{v}\sb{i} = \beta\sb{i,1}\vec{w}\sb{i1}+ \cdots +\beta\sb{i,q\sb{i}}\vec{w}\sb{iq\sb{i}}, \qquad \text{ para } i=1,\ldots, p, \quad \vec{w}\sb{ij} \in T.
\end{gathered}$$ Consideremos el sistema formado por todos los
    vectores $\vec{w}\sb{ij}$ que aparecen en las expresiones anteriores.
    Los etiquetamos como $\vec{w}\sb{1}, \ldots, \vec{w}\sb{q}$, con ı́ndice que no
    depende de $i$, y podemos escribir entonces que, para cada $i$,
    $$\vec{v}\sb{i} = \sum\sb{j=1}^q \beta'_{ij} \vec{w}\sb{j},$$ con la asignación
    de escalares nulos en las expresiones correspondientes.

    Ahora sustituimos cada $\vec{v}\sb{i}$ por la combinación lineal
    anterior. $$\begin{aligned}
      \vec{u} & = \sum\sb{i=1}^p \alpha\sb{i} \vec{v}\sb{i} = \sum\sb{i=1}^p \alpha\sb{i} \left( \sum\sb{j=1}^q \beta'_{ij} \vec{w}\sb{j} \right) \cr & = \sum\sb{j=1}^q \underbrace{\left( \sum\sb{i=1}^p \alpha\sb{i} \beta'_{ij} \right) }\sb{\gamma\sb{j}} \vec{w}\sb{j} = \sum\sb{j=1}^q \gamma\sb{j} \vec{w}\sb{j},
    \end{aligned}$$ lo que implica que $\vec{u}$ depende linealmente de
    $\{\vec{w}\sb{1},\ldots, \vec{w}\sb{q}\}$ y, por tanto, de $T$.

3.  Por reducción al absurdo, supongamos que $S \cup \{\vec{v}\}$ es
    linealmente dependiente. Esto quiere decir que existe una
    combinación lineal no trivial de los vectores de $S\cup \{\vec{v}\}$
    igual a cero, es decir, se puede escribir
    $$\alpha\sb{1} \vec{u}\sb{1} + \cdots + \alpha\sb{r} \vec{u}\sb{r} + \beta \vec{v} = \vec{0}, \quad \vec{u}\sb{i} \in S \quad \forall i=1,\ldots, r,$$
    donde no todos los coeficientes $\alpha\sb{i}, \beta$ son nulos. Si se
    tiene que $\beta=0$, la expresión anterior serı́a una combinación
    lineal de los vectores de $S$ igual a $\vec{0}$, donde no todos los
    coeficientes serı́an nulos, lo cual no es posible porque $S$ es un
    sistema linealmente independiente. Por tanto, $\beta\neq 0$ y
    podemos entonces despejar $\vec{v}$ en la expresión anterior,
    obteniendo
    $$\vec{v}= -\frac{\alpha\sb{1}}{\beta} \vec{u}\sb{1} -\cdots -  \frac{\alpha\sb{r}}{\beta} \vec{u}\sb{r}.$$
    Ası́, $\vec{v}$ depende linealmente de $S$.
{{% /proof %}}

## Conjunto generador y base

En esta sección veremos cómo el concepto de dependencia lineal sirve
para expresar los elementos de un espacio vectorial utilizando sólo un
sistema (posiblemente finito) de vectores.

{{% definition %}}
Conjunto o sistema generador Sea $V$ un $\K$-espacio vectorial. Diremos
que un conjunto (o sistema) de vectores $S$ es un **conjunto (o sistema)
generador** de $V$ si todo vector de $V$ puede escribirse como
combinación lineal de los vectores de $S$. En este caso diremos que $V$
está generado por $S$, o por los vectores de $S$.
{{% /definition %}}

Si el espacio $V$ tiene un sistema generador con un número finito de
elementos decimos que es **finitamente generado**.

{{% example name="Ejemplo" %}}
En el $\R$-espacio vectorial $\R[x]$, el sistema
$S = \{ 1, x, x^2, \ldots \}$ es un sistema generador de $V$.
{{% /example %}}

{{% example name="Ejemplo" %}}
El sistema $S = \{ \vec{u}\sb{1}, \vec{u}\sb{2}, \vec{u}\sb{3}, \vec{u}\sb{4} \}$, con
$$\vec{u}\sb{1} = \left( \begin{array}{r} 1 \cr 1 \cr 2 \end{array} \right), \vec{u}\sb{2} = \left( \begin{array}{r} -1 \cr 0 \cr 1 \end{array} \right), \vec{u}\sb{3} = \left( \begin{array}{r} 2 \cr -1 \cr 2 \end{array} \right), \vec{u}\sb{4} = \left( \begin{array}{r} -1 \cr 3 \cr 5 \end{array} \right),$$
es un sistema generador de $\R^3$. Se trata de expresar un vector
arbitrario de $\R^3$ como combinación lineal de los elementos de $S$,
esto es, de resolver el sistema de ecuaciones
$$\left( \begin{array}{c} \alpha\sb{1} \cr \alpha\sb{2} \cr \alpha\sb{3} \end{array} \right) = x\sb{1} \vec{u}\sb{1} + x\sb{2} \vec{u}\sb{2} + x\sb{3} \vec{u}\sb{3} + x\sb{4} \vec{u}\sb{4}$$
para cualesquiera $\alpha\sb{1}, \alpha\sb{2}, \alpha\sb{3} \in \R$. La matriz de
coeficientes del sistema es
$$\left( \begin{array}{cccc} 1&-1&2&-1\cr{}1&0&-1&3 \cr{}2&1&2&5\end{array} \right),$$
con forma escalonada por filas igual a
$$E =   \left( \begin{array}{cccc} 1&-1&2&-1\cr{}0&1&-3&4
\cr{}0&0&7&-5\end{array} \right).$$ El rango es entonces
igual a tres, por lo que el sistema siempre tendrá solución,
independientemente de los valores de $\alpha\sb{1}, \alpha\sb{2}, \alpha\sb{3}$.
{{% /example %}}

{{% example name="Sistema generador en $\K^n$" %}}
Sea $G = \{ \vec{v}\sb{1}, \ldots, \vec{v}\sb{s} \} \subset \K^n$ un sistema de
vectores.

-   Construimos la matriz
    $A\sb{G} = \left( \begin{array}{c|c|c} \vec{v}\sb{1} & \cdots & \vec{v}\sb{s} \end{array} \right)$,
    que es de orden $n \times s$.

-   Calculamos $A\sb{G} \mapright{\mbox{Gauss}} E$ una forma escalonada.

-   Si $E$ tiene $n$ pivotes, entonces $G$ es un sistema generador. En
    caso contrario, no lo es.

En resumen,

* $G$ es sistema generador de $\K^n$ si y solamente si $\rg(A\sb{G}) = n$.

{{% /example %}}

Justifiquemos el método anterior. Si $G$ es un sistema generador,
entonces todo vector $\vec{v} \in \K^n$ se expresa como combinación
lineal de los vectores de $G$. En particular, cada uno de los vectores
$\vec{e}\sb{i}, i=1, \ldots, n$ es combinación lineal de los vectores de $G$.
Si lo traducimos a sistemas de ecuaciones, esto implica que cada uno de
los sistemas de ecuaciones de matriz ampliada $(A\sb{G}|\vec{e}\sb{i})$ es
compatible, de donde
$$\rg(A\sb{G}|\vec{e}\sb{i}) = \rg(A\sb{G}), \quad i=1,\ldots, n.$$ Sea
$T = (A\sb{G}|I\sb{n})$, que es una matriz $n \times (s+n)$. El bloque de la
derecha nos dice que $\rg(T) = n$ y el razonamiento anterior implica que
$\rg(T) = \rg(A\sb{G})$. Por tanto, $\rg(A\sb{G}) = n$.

Recı́procamente, si $\rg(A\sb{G}) = n$ y $\vec{v} \in \K^n$, la matriz
$(A\sb{G}|\vec{v})$ es de orden $n \times (s+1)$ y tiene rango igual a $n$,
de donde $\vec{v}$ es combinación lineal de los vectores de $G$.

Un espacio vectorial puede tener muchos sistemas de generadores
diferentes. Incluso puede haber sistemas de generadores donde algún
vector no sea necesario. En el ejemplo anterior, los tres primeros
vectores forman un sistema generador de $\R^3$. Esto nos va a llevar al
concepto de base.

{{% definition %}}
Base Sea $V$ un $\K$-espacio vectorial. Una **base** de $V$ es un
sistema $S$ de vectores de $V$ linealmente independiente y generador.
{{% /definition %}}

En otras palabras, una base es un sistema de generadores de un espacio
vectorial en el que no sobra ningún vector, ya que, al ser linealmente
independiente, ninguno de ellos puede escribirse como combinación lineal
de los demás.

{{% example name="Ejemplo" %}}
En $\K^n$ tenemos siempre la base estándar
${\mathcal S} = \{ \vec{e}\sb{1}, \ldots, \vec{e}\sb{n} \}$, donde el vector
$\vec{e}\sb{i}$ es la $n$-upla que tiene un 1 en la posición $i$ y cero en
las restantes. ${\mathcal S}$ es un sistema linealmente independiente,
pues si consideramos una combinación lineal
$$\alpha\sb{1} \vec{e}\sb{1} + \cdots + \alpha\sb{n} \vec{e}\sb{n} = \vec{0},$$ entonces,
al identificar componentes de cada lado, nos queda que
$\alpha\sb{i} = 0, i=1,\ldots,n$. Además, es un sistema generador, pues dado
$\vec{v} \in \K^n$ de componentes $v\sb{i}$, podemos escribir, de manera
inmediata, que $\vec{v} = v\sb{1} \vec{e}\sb{1} + \cdots + v\sb{n} \vec{e}\sb{n}$.
{{% /example %}}

{{% example name="Ejemplo" %}}
El sistema $\{ 1, x, x^2, \ldots \}$ es una base del espacio vectorial
$\R[x]$. Observemos que tiene un número infinito de elementos. Además,
$\R[x]$ no puede ser finitamente generado: supongamos que existe un
sistema finito de polinomios $g\sb{1}(x), g\sb{2}(x), \ldots, g\sb{r}(x)$ que sean
linealmente independientes y generadores. Existe una potencia $k$ de $x$
que es la mayor que aparece en estos polinomios. Entonces el polinomio
$x^{k+1}$ no se puede expresar como combinación lineal de
$g\sb{1}(x), \ldots, g\sb{r}(x)$.
{{% /example %}}

{{% example name="Ejemplo" %}}
El sistema $S = \{ \vec{u}\sb{1}, \vec{u}\sb{2}, \vec{u}\sb{3}, \vec{u}\sb{4} \}$, con
$$\vec{u}\sb{1} = \left( \begin{array}{r} 1 \cr 1 \cr 2 \end{array} \right), \vec{u}\sb{2} = \left( \begin{array}{r} -1 \cr 0 \cr 1 \end{array} \right), \vec{u}\sb{3} = \left( \begin{array}{r} 2 \cr -1 \cr 2 \end{array} \right), \vec{u}\sb{4} = \left( \begin{array}{r} -1 \cr 3 \cr 5 \end{array} \right),$$
es un sistema generador de $\R^3$, pero no es una base, pues el vector
$\vec{u}\sb{4}$ se puede expresar como combinación lineal de los restantes.
En concreto,
$$\vec{u}\sb{4} = \frac{16}{7} \vec{u}\sb{1} + \frac{13}{7} \vec{u}\sb{2} - \frac{5}{7} \vec{u}\sb{3}.$$
Sin embargo, el sistema
${\mathcal B} = \{ \vec{u}\sb{1}, \vec{u}\sb{2}, \vec{u}\sb{3} \}$ es base, pues la
matriz
$$A\sb{\mathcal B} = \left( \begin{array}{ccc} \vec{u}\sb{1} & \vec{u}\sb{2} & \vec{u}\sb{3} \end{array} \right)$$
tiene una forma escalonada con tres pivotes, es decir, su rango es tres.
Esto significa que es un sistema generador y los vectores son
independientes.
{{% /example %}}

{{% remark %}}
Las bases infinitas no tienen nada que ver con las combinaciones
lineales infinitas. En el ejemplo de $\R[x]$, no estamos considerando
expresiones de la forma $$\sum\sb{i=0}^\infty c\sb{i} x^i,$$ que es una serie
formal.
{{% /remark %}}

Ahora veamos que un espacio vectorial finitamente generado, que no sea
trivial, siempre tiene una base. Además veremos cómo se construye, a
partir de un sistema de generadores.

{{% theorem name="Existencia de base" %}}
 Sea $V\neq \{ \vec{0} \}$ un espacio vectorial
finitamente generado. Dado cualquier sistema finito de generadores
$G \subset V$, existe una base ${\mathcal B}$ de $V$ formada por
vectores de $G$.
{{% /theorem %}}

{{% proof %}}
[]{#thm:existbase label="thm:existbase"}

Sea $G = \{ \vec{v}\sb{1}, \ldots, \vec{v}\sb{p} \}$. Vamos a clasificar los
elementos del sistema $G$. Si $\vec{v}\sb{1} \ne \vec{0}$, lo etiquetamos como
básico. Si es cero, lo etiquetamos como no básico. Para cada $i \ge 2$,
si $\vec{v}\sb{i}$ no depende linealmente de los anteriores
$\{ \vec{v}\sb{1}, \ldots, \vec{v}\sb{i-1} \}$, etiquetamos a $\vec{v}\sb{i}$ como
básico y, en otro caso, lo etiquetamos como no básico. Sea $\mathcal{B}$
el sistema formado por todos los elementos básicos. Vamos a probar que
$\mathcal{B}$ es una base de $V$. El sistema $\mathcal{B}$ es no vacı́o,
porque no todos los $\vec{v}\sb{i}$ son nulos y el primer $\vec{v}\sb{i}$ no nulo
será básico.

Todo vector de $V$ depende linealmente de $G$, porque es un sistema
generador. Por la construcción, todos los elementos de $G$ dependen
linealmente de $\mathcal{B}$: los básicos porque están en $\mathcal{B}$
y los no básicos por la definición. En consecuencia, $\mathcal{B}$ es un
sistema generador.

Basta entonces comprobar que $\mathcal{B}$ es un sistema linealmente
independiente. Sea $\mathcal{B} = \{ \vec{u}\sb{1}, \ldots, \vec{u}\sb{s} \}$ y
consideremos una expresión de la forma
$$\alpha\sb{1} \vec{u}\sb{1} + \alpha\sb{2} \vec{u}\sb{2} + \cdots + \alpha\sb{s} \vec{u}\sb{s} = \vec{0}.$$
Entonces
$$\alpha\sb{1} \vec{u}\sb{1} + \alpha\sb{2} \vec{u}\sb{2} + \cdots + \alpha\sb{s-1} \vec{u}\sb{s-1} = -\alpha\sb{s} \vec{u}\sb{s}$$
Por la definición de elemento básico, el vector $\vec{u}\sb{s}$ no depende
linealmente de $\{ \vec{u}\sb{1}, \ldots, \vec{u}\sb{s-1} \}$, por lo que
$\alpha\sb{s} = 0$ (en otro caso, podríamos despejar $\vec{u}\sb{s}$ como
combinaciópn lineal de los demás). Por una repetición de este argumento,
llegamos a que $\alpha\sb{s-1} = 0$ y, en general, que todos los
$\alpha\sb{i} = 0, 1 \le i \le s$. Por tanto, $\mathcal{B}$ es un sistema
linealmente independiente.
{{% /proof %}}

{{% example name="Extracción de una base en $\K^n$" %}}
Sea $G=\{\vec{v}\sb{1},\ldots,\vec{v}\sb{p}\}$ un sistema de generadores del
espacio vectorial $\K^n$.

-   Formamos la matriz
    $A\sb{G} = \left( \begin{array}{ccc} \vec{v}\sb{1} & \cdots & \vec{v}\sb{p} \end{array} \right)$.

-   Calculamos $A\sb{G} \mapright{\text{Gauss}} E$.

-   Las columnas básicas de $A\sb{G}$, que se corresponden con las columnas
    con pivotes de $E$, forman una base de $\K^n$.
{{% /example %}}

{{% example name="Ejemplo" %}}
En $\R^3$ consideremos el sistema
$$G= \left\\{ \vec{v}\sb{1}=  \left( \begin{array}{c} 1\cr{}1\cr{}2 \end{array} \right),
\vec{v}\sb{2}= \left( \begin{array}{c} -1\cr{}0\cr{}1 \end{array} \right),
\vec{v}\sb{3}=  \left( \begin{array}{c} 5\cr{}2\cr{}1 \end{array} \right),
\vec{v}\sb{4}= \left( \begin{array}{c} 0\cr{}1\cr{}1 \end{array} \right),
\vec{v}\sb{5}=  \left( \begin{array}{c} 2\cr{}-1\cr{}2 \end{array} \right).
\right\\} .$$ Mediante la forma escalonada por filas, obtenemos
$$\left( \begin{array}{ccccc} \vec{v}\sb{1} & \vec{v}\sb{2} & \vec{v}\sb{3} & \vec{v}\sb{4} & \vec{v}\sb{5} \end{array} \right) \mapright{\mbox{Gauss}}   \left( \begin{array}{ccccc} 1&-1&5&0&2\cr{}0&1&-3&1&-
3\cr{}0&0&0&-2&7\end{array} \right).$$ Luego $G$ es un
sistema generador de $\R^3$ y
${\mathcal B}=\{\vec{v}\sb{1},\vec{v}\sb{2},\vec{v}\sb{4}\}$ es una base.
{{% /example %}}

## Dimensión

En esta sección definiremos un concepto esencial del álgebra lineal: la
*dimensión* de un espacio vectorial. Necesitamos primero el siguiente
resultado:

{{% theorem name="Relación entre sistemas independientes y generadores" %}}
 Sea $V$ un
$\K$-espacio vectorial. Si $G=\{\vec{u}\sb{1},\ldots,\vec{u}\sb{m}\}$ es un
sistema generador de $V$ y $S=\{\vec{v}\sb{1},\ldots,\vec{v}\sb{r}\}$ un sistema
linealmente independiente, entonces $r \leq m$.
{{% /theorem %}}

{{% proof %}}
[]{#independientes<generadores label="independientes<generadores"} Como
$G = \{ \vec{u}\sb{1}, \ldots, \vec{u}\sb{m} \}$ es un sistema generador, cada
vector $\vec{v}\sb{i}$ de $S$ se puede escribir como combinación lineal de
los vectores de $G$, esto es,
$$\vec{v}\sb{i} = a\sb{1i} \vec{u}\sb{1} + \cdots + a\sb{mi} \vec{u}\sb{m}, \qquad a\sb{ji} \in \K, \quad i=1, \ldots, r.$$
Como $S=\{\vec{v}\sb{1},\ldots,\vec{v}\sb{r}\}$ un linealmente independiente, la
ecuación vectorial $$x\sb{1} \vec{v}\sb{1} + \cdots + x\sb{r} \vec{v}\sb{r} = \vec{0},$$
tiene solución única $x\sb{1} = \cdots = x\sb{r} = 0$. Sustituimos en dicha
expresión los valores de $\vec{v}\sb{i}$ en función de los $\vec{u}\sb{j}$:
$$x\sb{1} (a\sb{11} \vec{u}\sb{1} + \cdots + a\sb{m1} \vec{u}\sb{m} ) + \cdots + x\sb{r} (a\sb{1r} \vec{u}\sb{1} + \cdots + a\sb{mr} \vec{u}\sb{m} ) = \vec{0}.$$
Reordenando:
$$(a\sb{11} x\sb{1} + \cdots + a\sb{1r} x\sb{r}) \vec{u}\sb{1} + \cdots + (a\sb{m1} x\sb{1} + \cdots + a\sb{mr} x\sb{r}) \vec{u}\sb{m} = \vec{0}.$$
Una solución de esta ecuación vectorial se obtendría si todos los
coeficientes fueran nulos, es decir, si se tuviera:
$$\left\\{ \begin{array}{cl} a\sb{11} x\sb{1} + \cdots + a\sb{1r} x\sb{r} & = 0, \cr & \vdots \cr a\sb{m1} x\sb{1} + \cdots + a\sb{mr} x\sb{r} & = 0. \end{array} \right.$$
Si este último sistema de ecuaciones tuviera solución no trivial,
obtendríamos una solución no trivial de la ecuación vectorial anterior,
y sabemos que esto es imposible (tiene solución única). Por tanto, el
último sistema de ecuaciones tiene solución única, por lo que el rango
de la matriz de coeficientes $A\sb{m \times r} = (a\sb{ij})$ tiene que ser
igual al número de incógnitas, es decir, $\rg(A) = r$. Esto implica que
$A$ debe tener al menos $r$ filas, y por tanto $r\leq m$.
{{% /proof %}}

Este resultado implica que una base de un espacio vectorial finitamente
generado tiene que contener un número finito de elementos, pues su
cardinal está acotado por el de cualquier sistema generador.
Identificamos ahora un invariante de un espacio vectorial finitamente
generado.

{{% theorem name="Teorema de la base" %}}
 Sea $V$ un $\K$-espacio vectorial finitamente
generado. Entonces todas las bases de $V$ tienen el mismo número de
elementos.
{{% /theorem %}}

{{% proof %}}
Como $V$ es finitamente generado, existe una base del espacio. Sean
${\mathcal B}\sb{1}$ y ${\mathcal B}\sb{2}$ dos bases de $V$, de $m$ y $n$
vectores respectivamente. Como ${\mathcal B}\sb{1}$ es un sistema generador
y ${\mathcal B}\sb{2}$ es libre, entonces $n\leq m$ por la relación entre
sistemas independientes y generadores. Pero como ${\mathcal B}\sb{2}$ es un
sistema generador y ${\mathcal B}\sb{1}$ es libre, se tiene $m\leq n$. Por
tanto, $m=n$.
{{% /proof %}}

{{% definition %}}
Dimensión de un espacio vectorial

La dimensión de un $\K$-espacio vectorial $V$ que denotamos $\dim(V)$,
se define como sigue:

-   Si $V = \{ \vec{0} \}$, entonces $\dim (V)=0$.

-   Si $V$ es finitamente generado, su dimensión es el número de
    elementos de cualquier base de $V$.

-   Si $V$ no es finitamente generado, diremos que tiene dimensión
    infinita, y escribiremos $\dim V=\infty$.
{{% /definition %}}

{{% example name="Ejemplo" %}}
-   El $\K$-espacio vectorial $\K^n$ tiene dimensión $n$, pues la base
    estándar tiene $n$ elementos.

-   El sistema de polinomios, $\K[x]$, es un $\K$-espacio vectorial de
    dimensión infinita y una base es $\{ x^i ~|~ i=0,1,2,\ldots \}$. Si
    $V$ es un espacio vectorial que no es de generación finita, no hemos
    probado la existencia de una base. Esto requiere otros métodos que
    hacen uso del axioma de elección.

-   El sistema de matrices $\MatK{m}{n}{\K}$ es un $\K$-espacio
    vectorial de dimensión $m \cdot n$. Una base está formada por las
    matrices $E(i,j)$, que tienen ceros en todas sus posiciones, salvo
    en el valor $1$ en la posición $(i,j)$.

-   El cuerpo $\C$ es un $\R$-espacio vectorial de dimensión $2$.

-   El cuerpo $\R$ es un $\Q$-espacio vectorial de dimensión infinita.
{{% /example %}}

La dimensión de un espacio vectorial nos impone restricciones sobre el
tamaño que pueden tener los sistemas libres o generadores.

{{% theorem name="Acotación de sistemas generadores e independientes" %}}
Sea $V$ un $\K$-espacio vectorial de dimensión finita y
$S=\{\vec{v}\sb{1},\ldots,\vec{v}\sb{m}\} \subset V$.

1.  Si $S$ es un sistema generador, entonces $m\geq \dim V$.

2.  Si $S$ es linealmente independiente, entonces $m \leq \dim V$.

3.  Si $S$ es un sistema generador y $m=\dim V$, entonces $S$ es base de
    $V$.

4.  Si $S$ es linealmente independiente, y $m=\dim V$, entonces $S$ es
    base de $V$.
{{% /theorem %}}

{{% proof %}}
Sea $n = \dim(V)$. Los dos primeros apartados los conocemos ya.
Supongamos entonces que $S$ es un sistema generador con $m = \dim(V)$.
Por el teorema de existencia de base (pág. ), existe una base formada
por elementos de $S$, que debe tener $m=\dim(V)$ elementos, luego debe
ser igual a $S$. Por tanto $S$ es una base.

Supongamos ahora que $S$ es un sistema linealmente independiente, con
$m = \dim(V)$. Si un vector $\vec{v} \in V$ no se puede expresar como
combinación lineal de $S$, entonces $S \cup \{ \vec{v} \}$ es linealmente
independiente (relación entre combinación y dependencia lineal, pág. ),
lo que implica que $m+1 \le \dim V$, que es una contradicción. Por
tanto, todo vector de $V$ depende linealmente de $S$, luego $S$ es un
sistema de generadores. Como ya sabemos que es linealmente
independiente, es base.
{{% /proof %}}

{{% example name="Base en $\K^n$" %}}
-   Sea ${\mathcal B} = \{ \vec{v}\sb{1}, \ldots, \vec{v}\sb{n} \}$ y
    $A\sb{\mathcal B} = \left( \begin{array}{c|c|c} \vec{v}\sb{1} & \cdots & \vec{v}\sb{n} \end{array} \right)$.

-   Calculamos $A\sb{\mathcal B} \mapright{\mbox{Gauss}} E$.

-   Si $E$ tiene tiene $n$ pivotes, entonces ${\mathcal B}$ es una base.
    En caso contrario, no lo es.

En resumen,

* ${\mathcal B}$ es base de $\K^n$ si y solamente si $A\sb{\mathcal B}$ es
una matriz cuadrada $n\times n$ de rango $n$.

{{% /example %}}

Al igual que de un sistema generador podemos extraer una base, un
sistema linealmente independiente se puede ampliar a una base.

{{% theorem name="Ampliación a una base" %}}
 Sea $V$ un $\K$-espacio vectorial de dimensión
finita $n$ y $S=\{\vec{v}\sb{1},\ldots,\vec{v}\sb{r}\}$ un sistema linealmente
independiente, con $r < n$. Entonces existen $n-r$ vectores
$\vec{v}\sb{r+1},\ldots,\vec{v}\sb{n}\in V$ tales que el sistema
$\{\vec{v}\sb{1},\ldots,\vec{v}\sb{n}\}$ es base de $V$. Además, los vectores
$\vec{v}\sb{r+1},\ldots,\vec{v}\sb{n}$ pueden tomarse de cualquier base de
$V$.
{{% /theorem %}}

{{% proof %}}
[]{#thm:incompleta label="thm:incompleta"} Sea
$\mathcal{B} = \{ \vec{u}\sb{1}, \ldots, \vec{u}\sb{n} \}$ una base cualquiera de
$V$ y formemos el sistema
$G = \{ \vec{v}\sb{1}, \ldots, \vec{v}\sb{r}, \vec{u}\sb{1}, \ldots, \vec{u}\sb{n}  \}$.
Como $\mathcal{B}$ es un sistema generador, también lo es $G$.
Apliquemos el proceso descrito en la prueba del teorema de existencia de
base (página ), donde clasificamos a los vectores de $G$ como básicos o
no. Como $S$ es un sistema linealmente independiente, todos los vectores
$\{ \vec{v}\sb{1}, \ldots, \vec{v}\sb{r} \}$ son básicos y la base resultante del
proceso los contiene.
{{% /proof %}}

[]{#metodo:incompleta label="metodo:incompleta"}

{{% example name="Ampliación a una base de $\K^n$" %}}
Sea $S = \{\vec{v}\sb{1},\ldots,\vec{v}\sb{m}\}$ un sistema linealmente
independiente de $\K^n$, con $m\leq n$, y
$\{ \vec{e}\sb{1}, \ldots, \vec{e}\sb{n} \}$ la base estándar de $\K^n$.

-   Formamos la matriz
    $$A=\left( \begin{array}{ccc|ccc} \vec{v}\sb{1} & \cdots & \vec{v}\sb{m} & \vec{e}\sb{1} & \cdots & \vec{e}\sb{n} \end{array}\right).$$

-   Calculamos una forma escalonada por filas de $A$, que tiene $n$
    pivotes.

-   Las columnas básicas de $A$ constituyen una ampliación de $S$.
{{% /example %}}

El método anterior es válido, porque los $m$ primeros pivotes están en
las $m$ primeras columnas, pues $S$ es un sistema libre, y el resto
entre las siguientes columnas. Por tanto, las columnas básicas de la
matriz $A$ contienen a los vectores de partida
$\vec{v}\sb{1}, \ldots, \vec{v}\sb{m}$ y forman una base de $\K^n$ que completa a
$S$.

{{% example name="Ejemplo" %}}
En el espacio vectorial $\R^4$ consideramos el sistema libre
$$S= \left\\{ \vec{v}\sb{1}= \left( \begin{array}{r} 1 \cr -1 \cr 2 \cr 1 \end{array} \right),\vec{v}\sb{2} = \left( \begin{array}{r} 2 \cr -1 \cr 2 \cr 1 \end{array} \right) \right\\}.$$
Completaremos $S$ con vectores de la base estándar hasta obtener una
base de $\R^4$:
$$\left( \begin{array}{cc|cccc}  1 & 2 & 1 & 0 & 0 & 0\cr -1 & -1 & 0 & 1 & 0 & 0\cr 2 & 2 & 0 & 0 & 1 & 0\cr 1 & 1 & 0 & 0 & 0 & 1 \end{array}\right) \rightsquigarrow  \left(\begin{array}{cc|cccc}   1 & 2 & 1 & 0 & 0 & 0\cr  0 & 1 & 1 & 1 & 0 & 0\cr 0 & 0 & 0 & 2 & 1 & 0\cr 0 & 0 & 0 & 0 & -1/2 & 1 \end{array}\right).$$

Luego una base de $\R^4$ que completa a $S$ es
$\left\\{ \vec{v}\sb{1},\vec{v}\sb{2},\vec{e}\sb{2},\vec{e}\sb{3} \right\\}$
{{% /example %}}

En el método anterior hemos seleccionado la base estándar de $\K^n$,
pero es posible realizar el mismo procedimiento con cualquier base que
tengamos inicialmente.

## \* Espacio producto

El producto cartesiano de espacios vectoriales sobre un mismo cuerpo
$\K$ es un sistema que admite la estructura de espacio vectorial.
Además, en el caso de dimensión finita, podemos calcular una base de
este nuevo espacio.

{{% theorem name="Espacio producto" %}}
Dados dos espacios vectoriales $V\sb{1}$ y $V\sb{2}$ sobre un mismo cuerpo $\K$,
el producto cartesiano
$$V\sb{1}\times V\sb{2} =\{(\vec{v}\sb{1},\vec{v}\sb{2}) ~|~ \vec{v}\sb{1}\in V\sb{1}, \vec{v}\sb{2} \in V\sb{2} \},$$
tiene estructura de $\K$-espacio vectorial con las operaciones

-   **Suma:**
    $(\vec{u}\sb{1}, \vec{u}\sb{2})+(\vec{v}\sb{1},\vec{v}\sb{2})= (\vec{u}\sb{1}+\vec{v}\sb{1},\vec{u}\sb{2}+\vec{v}\sb{2})$.

-   **Producto por escalar:**
    $\alpha (\vec{v}\sb{1},\vec{v}\sb{2})=(\alpha \vec{v}\sb{1},\alpha \vec{v}\sb{2})$.
{{% /theorem %}}

{{% proof %}}
Es una sencilla verificación de las propiedades que definen un
$\K$-espacio vectorial.
{{% /proof %}}

{{% theorem name="Dimensión del espacio producto" %}}
 Dados dos $\K$-espacios vectoriales $V\sb{1}$
y $V\sb{2}$ de dimensión finita, el espacio producto $V\sb{1}\times V\sb{2}$ es un
espacio vectorial de dimensión
$\dim(V\sb{1}\times V\sb{2})=\dim(V\sb{1})+\dim(V\sb{2})$.
{{% /theorem %}}

{{% proof %}}
Tomemos una base ${\mathcal B}\sb{1} = \{ \vec{u}\sb{1},\ldots,\vec{u}\sb{m} \}$ de
$V\sb{1}$ y una base $\mathcal{B}\sb{2}=\{ \vec{v}\sb{1},\ldots, \vec{v}\sb{n}  \}$ de
$V\sb{2}$. Se prueba de forma directa que el sistema de vectores
$${\mathcal B} = \left((\vec{u}\sb{1}, \vec{0}),\ldots,(\vec{u}\sb{m},\vec{0}),(\vec{0},\vec{v}\sb{1}),\ldots,(\vec{0},\vec{v}\sb{n})\right)$$
es base de $V\sb{1}\times V\sb{2}$. Por tanto,
$\dim(V\sb{1}\times V\sb{2})=m+n=\dim(V\sb{1})+\dim(V\sb{2})$.
{{% /proof %}}

## Coordenadas

La principal ventaja de la existencia de bases, en los espacios
vectoriales de dimensión finita, es que vamos a poder estudiarlos, sea
cual sea el espacio vectorial, como si fuera $\K^n$. Esto lo vamos a
conseguir mediante el uso de *coordenadas*.

Recordemos que una base es un sistema de vectores, y por tanto sus
vectores están ordenados, lo que nos permite hablar del $i$-ésimo vector
de una base.

{{% theorem name="Unicidad de la expresión" %}}
 Sea $V$ un $\K$-espacio vectorial finitamente
generado y sea ${\mathcal B}$ un sistema de vectores de $V$. Entonces
${\mathcal B}$ es una base si y solamente si todo vector de $V$ se puede
expresar **de una única manera** como combinación lineal de los vectores
de ${\mathcal B}$.
{{% /theorem %}}

{{% proof %}}
[]{#T:coor\sb{u}nicas label="T:coor\sb{u}nicas"} Supongamos que
${\mathcal B} = \{\vec{u}\sb{1},\ldots, \vec{u}\sb{n}\}$ es una base de $V$. Dado
un vector $\vec{v}\in V$, como ${\mathcal B}$ es sistema de generadores,
podremos escribir
$\vec{v}=\alpha\sb{1}\vec{u}\sb{1} + \cdots + \alpha\sb{n} \vec{u}\sb{n}$. Si existiera
otra forma de expresar $\vec{v}$, digamos
$\vec{v} = \beta\sb{1} \vec{u}\sb{1} + \cdots + \beta\sb{n} \vec{u}\sb{n}$, entonces
tendrı́amos
$$\vec{0} = \vec{v} - \vec{v} = (\alpha\sb{1}-\beta\sb{1}) \vec{u}\sb{1} + \cdots + (\alpha\sb{n} - \beta\sb{n}) \vec{u}\sb{n}.$$
Pero como ${\mathcal B}$ es un sistema linealmente independiente, los
coeficientes de la expresión anterior deben ser todos nulos. Es decir,
$\alpha\sb{i}-\beta\sb{i}=0$, o lo que es lo mismo, $\alpha\sb{i}=\beta\sb{i}$ para todo
$i=1,\ldots, n$. Por tanto, la forma de expresar $\vec{v}$ como
combinación lineal de los elementos de ${\mathcal B}$ es única.

Recı́procamente, sea ${\mathcal B}=\{\vec{u}\sb{1},\ldots, \vec{u}\sb{n}\}$ un
sistema de vectores tal que todo vector $\vec{v}\in V$ se puede expresar
de forma única como combinación lineal de los vectores de
${\mathcal B}$. Por un lado, ${\mathcal B}$ es sistema de generadores,
puesto que todo vector de $V$ se puede expresar como combinación lineal
de ${\mathcal B}$. Por otra parte, consideremos el vector $\vec{0}\in V$.
Sabemos que siempre se tiene la combinación lineal obvia:
$$\vec{0} = 0 \cdot \vec{u}\sb{1} + \cdots + 0 \cdot \vec{u}\sb{n}.$$ Por la
propiedad que le suponemos a ${\mathcal B}$, esta es la única forma de
escribir $\vec{0}$ como combinación lineal de los vectores de
${\mathcal B}$. Por tanto, ${\mathcal B}$ es un sistema linealmente
independiente y es una base.
{{% /proof %}}

{{% definition %}}
Coordenadas Sea $V$ un $\K$-espacio vectorial de dimensión $n$. Dada una
base ${\mathcal B}= \{\vec{u}\sb{1},\ldots,\vec{u}\sb{n} \}$, para todo vector
$\vec{v}\in V$, existe una única combinación lineal
$$\vec{v}=\alpha\sb{1} \vec{u}\sb{1} + \cdots +\alpha\sb{n} \vec{u}\sb{n}.$$ Los escalares
$\alpha\sb{1},\ldots,\alpha\sb{n}$ definen, por tanto, al vector $\vec{v}$, y los
llamaremos **coordenadas** de $\vec{v}$ respecto a ${\mathcal B}$.
Escribiremos:
$$\vec{v}\sb{\mathcal B} = \left( \begin{array}{c} \alpha\sb{1} \cr \vdots \cr\alpha\sb{n} \end{array} \right).$$
{{% /definition %}}

Cuando la base ${\mathcal B}$ esté clara por el contexto, escribiremos
simplemente
$$\vec{v} = \left( \begin{array}{c} \alpha\sb{1} \cr \vdots \cr\alpha\sb{n} \end{array} \right).$$

Por tanto, no importa cómo sea $V$ como espacio vectorial; si fijamos
una base, vamos a poder representar los elementos de $V$ como elementos
del conocido espacio vectorial $\K^n$. Hay que hacer notar unas
cuestiones sencillas, pero muy importantes.

-   Un vector $\vec{v} \in \K^n$ viene dado por una $n$-upla de números,
    que coincide con las coordenadas de $\vec{v}$ con respecto a la base
    estándar ${\mathcal S}$ de $\K^n$. Esto es, si
    $$\vec{v} = \left( \begin{array}{c} \alpha\sb{1} \cr \vdots \cr \alpha\sb{n} \end{array} \right), \mbox{ entonces } \vec{v}\sb{\mathcal S} = \left( \begin{array}{c} \alpha\sb{1} \cr \vdots \cr \alpha\sb{n} \end{array} \right).$$

-   Dada una base ${\mathcal B} = \{ \vec{u}\sb{1}, \ldots, \vec{u}\sb{n} \}$ de
    un espacio vectorial $V$, se tiene que
    $$[\vec{u}\sb{1}]\sb{\mathcal B} = \left( \begin{array}{c} 1 \cr 0 \cr \vdots \cr 0 \end{array} \right), \ldots, [\vec{u}\sb{n}]\sb{\mathcal B} = \left( \begin{array}{c} 0 \cr \vdots \cr 0 \cr 1 \end{array} \right),$$
    pues
    $\vec{u}\sb{i} = 0 \cdot \vec{u}\sb{1} + \cdots + 1 \cdot \vec{u}\sb{i} + \cdots + 0 \cdot \vec{u}\sb{n}$.

El problema que se plantea ahora es cómo calcular las coordenadas de un
vector $\vec{v}$ con respecto a una base dada. En un espacio vectorial
$V$, hay que tratar de escribir $\vec{v}$ como combinación lineal de los
elementos de la base. En $\K^n$, podemos hacerlo resolviendo un sistema
de ecuaciones.

{{% example name="Cálculo de las coordenadas en $\K^n$" %}}
Sea ${\mathcal B}=\{\vec{u}\sb{1},\ldots,\vec{u}\sb{n}\}$ una base de $\K^n$, y
$\vec{v}$ un vector, definido por una $n$-upla.

-   Formamos la matriz
    $\left( \begin{array}{c|c} A\sb{\mathcal B} & \vec{v} \end{array} \right) = \left( \begin{array}{ccc|c} \vec{u}\sb{1} & \cdots & \vec{u}\sb{n} & \vec{v} \end{array} \right)$.

-   Calculamos
    $\left( \begin{array}{c|c} A\sb{\mathcal B} & \vec{v} \end{array} \right) \mapright{\mbox{\scriptsize G-J}} \left( \begin{array}{c|c} I\sb{n} & \vec{w} \end{array} \right)$
    la forma escalonada reducida por filas de la matriz
    $(A\sb{\mathcal B}|\vec{v})$.

-   La columna $\vec{w}$ contiene las coordenadas de $\vec{v}$ respecto de
    la base ${\mathcal B}$, porque es la solución del sistema
    $A\sb{\mathcal B}\vec{x}=\vec{v}$, que nos dice cómo escribir $\vec{v}$
    como combinación lineal de las columnas de $A$.
{{% /example %}}

{{% example name="Ejemplo" %}}
En $\R^4$, consideremos el vector $\vec{v}$, y la base
${\mathcal B} = \{ \vec{u}\sb{1}, \vec{u}\sb{2}, \vec{u}\sb{3}, \vec{u}\sb{4} \}$, dados por
$$\vec{v} = \left( \begin{array}{r} 1 \cr 2 \cr 3 \cr 4 \end{array} \right), \vec{u}\sb{1}= \left( \begin{array}{r} 1 \cr 1 \cr 1 \cr 0 \end{array} \right), \vec{u}\sb{2} = \left( \begin{array}{r} 1 \cr 1 \cr 0 \cr 1 \end{array} \right), \vec{u}\sb{3}= \left( \begin{array}{r} 1 \cr 0 \cr 1 \cr 1 \end{array} \right), \vec{u}\sb{4}= \left( \begin{array}{r} 0 \cr 1 \cr 1 \cr 1 \end{array} \right).$$
Entonces
$$A = \left( \begin{array}{cccc|c} \vec{u}\sb{1} & \vec{u}\sb{2} & \vec{u}\sb{3} & \vec{u}\sb{4} & \vec{v} \end{array} \right) = \left(\begin{array}{cccc|c}
         1 & 1 & 1 & 0 & 1\cr
         1 & 1 & 0 & 1 & 2\cr
         1 & 0 & 1 & 1 & 3\cr
         0 & 1 & 1 & 1 & 4
        \end{array}\right) \mapright{\mbox{G-J}}
  \left(\begin{array}{cccc|c}
         1 & 0 & 0 & 0 & -2/3\cr
         0 & 1 & 0 & 0 & 1/3\cr
         0 & 0 & 1 & 0 & 4/3\cr
         0 & 0 & 0 & 1 & 7/3
        \end{array}\right) .$$ Luego
$$\vec{v}\sb{\mathcal B}=\left( \begin{array}{r} -\frac{2}{3} \cr {} \frac{1}{3} \cr {} \frac{4}{3} \cr {} \frac{7}{3} \end{array} \right).$$
Podemos comprobar que
$\vec{v}=-\frac{2}{3}\vec{u}\sb{1}+\frac{1}{3}\vec{u}\sb{2}+\frac{4}{3}\vec{u}\sb{3}+\frac{7}{3}\vec{u}\sb{4}$.
{{% /example %}}

{{% theorem name="Coordenadas y operaciones con vectores" %}}
Sea $V$ un $\K$-espacio vectorial de dimensión $n$ y ${\mathcal B}$ una
base de $V$. Sea $$c\sb{\mathcal B} : V \rightarrow \K^n$$ la aplicación
que a cada elemento de $V$ le hace corresponder el vector de sus
coordenadas: $c\sb{\mathcal B}(\vec{v}) = \vec{v}\sb{\mathcal B}$. Entonces
$c\sb{\mathcal B}$ es una aplicación biyectiva que verifica

1.  $c\sb{\mathcal B}(\vec{u} + \vec{v}) = c\sb{\mathcal B}(\vec{u}) + c\sb{\mathcal B}(\vec{v})$,

2.  $c\sb{\mathcal B}(\alpha \vec{u}) = \alpha c\sb{\mathcal B}(\vec{u})$,

para todo $\alpha \in \K$ y $\vec{u}, \vec{v} \in V$.
{{% /theorem %}}

{{% proof %}}
[]{#thm:morf-coord label="thm:morf-coord"} La aplicación es biyectiva
por el teorema de la unicidad de la expresión (pág. ). Si
${\mathcal B} = \{ \vec{u}\sb{1}, \ldots, \vec{u}\sb{n} \}$ y
$$\vec{v}\sb{\mathcal B} = \left( \begin{array}{c} a\sb{1} \cr a\sb{2} \cr \vdots \cr a\sb{n} \end{array} \right), \qquad \vec{w}\sb{\mathcal B} = \left( \begin{array}{c} b\sb{1} \cr b\sb{2} \cr \vdots \cr b\sb{n} \end{array} \right),$$
entonces
$$\vec{v} = a\sb{1} \vec{u}\sb{1} + a\sb{2} \vec{u}\sb{2} + \cdots + a\sb{n} \vec{u}\sb{n}, \vec{w} = b\sb{1} \vec{u}\sb{1} + b\sb{2} \vec{u}\sb{2} + \cdots + b\sb{n} \vec{u}\sb{n},$$
de donde $$\begin{aligned}
  \vec{v} + \vec{w} & = (a\sb{1} + b\sb{1}) \vec{u}\sb{1} + (a\sb{2} + b\sb{2}) \vec{u}\sb{2} + \cdots + (a\sb{n} + b\sb{n}) \vec{u}\sb{n}, \cr
  \alpha \vec{v} & = (\alpha a\sb{1}) \vec{u}\sb{1} + (\alpha a\sb{2}) \vec{u}\sb{2} + \cdots + (\alpha a\sb{n}) \vec{u}\sb{n}.
\end{aligned}$$ Por tanto,
$$[\vec{v} + \vec{w}]\sb{\mathcal B} = \left( \begin{array}{c} a\sb{1} + b\sb{1} \cr a\sb{2} + b\sb{2} \cr \vdots \cr a\sb{n} + b\sb{n} \end{array} \right), \qquad [\alpha \vec{u}]\sb{\mathcal B} = \left( \begin{array}{c} \alpha a\sb{1} \cr \alpha a\sb{2} \cr \vdots \cr \alpha a\sb{n} \end{array} \right).$$
{{% /proof %}}

Esta aplicación $c\sb{\mathcal B}$ recibe el nombre de **morfismo de
coordenadas** respecto de la base ${\mathcal B}$. La encontraremos más
adelante como un caso especialmente importante de aplicación entre
espacios vectoriales.

{{% remark %}}
La aplicación $c\sb{\mathcal B}$ tiene gran importancia para el cálculo
efectivo en espacios vectoriales de dimensión finita. Sea
$S = \{ \vec{v}\sb{1}, \ldots, \vec{v}\sb{r} \}$ un sistema finito de vectores de
$V$ y ${\mathcal B}$ una base *cualquiera* de $V$, con $\dim V = n$.
Entonces es fácil comprobar los siguientes resultados:

-   $S$ es un sistema linealmente independiente si y solamente si el
    sistema
    $\{ [\vec{v}\sb{1}]\sb{\mathcal B}, \ldots, [\vec{v}\sb{n}]\sb{\mathcal B} \}$ es
    linealmente independiente en $\K^n$.

-   $S$ es un sistema generador si y solamente si
    $\{ [\vec{v}\sb{1}]\sb{\mathcal B}, \ldots, [\vec{v}\sb{n}]\sb{\mathcal B} \}$ es
    un sistema generador en $\K^n$.

-   $S$ es una base de $V$ si y solamente si
    $\{ [\vec{v}\sb{1}]\sb{\mathcal B}, \ldots, [\vec{v}\sb{n}]\sb{\mathcal B} \}$ es
    una base de $\K^n$.

Observemos también que la ampliación de un sistema linealmente
independiente a una base de $V$ se puede hacer a través de $\K^n$. En
resumen, todos los procedimientos que hemos visto en $\K^n$ pueden ser
aplicados a espacios vectoriales de dimensión finita una vez fijada una
base.
{{% /remark %}}

## Cambio de base

Observemos que las coordenadas de un vector de $V$ dependen de la base
${\mathcal B}$ que hayamos elegido. Si tuviéramos otra base
${\mathcal B}'$, las coordenadas del mismo vector serı́an diferentes.
Vamos a ver entonces cómo están relacionados estos dos tipos de
coordenadas.

Supongamos que tenemos un espacio vectorial $V$ de dimensión $n$, y
consideremos dos bases de $V$ dadas por
${\mathcal B} = \{ \vec{u}\sb{1},\ldots, \vec{u}\sb{n} \}$ y
${\mathcal B}'= \{ \vec{u}'_1,\ldots, \vec{u}'_n \}$. Como ${\mathcal B}$
es base, podremos escribir cada vector de ${\mathcal B}'$ respecto a
${\mathcal B}$, es decir, tendremos:

$$\begin{aligned}
   \vec{u}'_1 & = a\sb{11} \vec{u}\sb{1} + a\sb{21} \vec{u}\sb{2} + \cdots + a\sb{n1} \vec{u}\sb{n}, & [\vec{u}'_1]\sb{\mathcal B} & = \left( \begin{array}{c} a\sb{11} \cr a\sb{21} \cr \vdots \cr a\sb{n1} \end{array} \right), \cr
   \vec{u}'_2 & = a\sb{12} \vec{u}\sb{1} + a\sb{22} \vec{u}\sb{2} + \cdots + a\sb{n2} \vec{u}\sb{n}, & [\vec{u}'_2]\sb{\mathcal B} & = \left( \begin{array}{c} a\sb{12} \cr a\sb{22} \cr \vdots \cr a\sb{n2} \end{array} \right), \cr
   \vdots & & \vdots \cr
   \vec{u}'_n & = a\sb{1n} \vec{u}\sb{1} + a\sb{2n} \vec{u}\sb{2} + \cdots + a\sb{nn} \vec{u}\sb{n}, & [\vec{u}'_n]\sb{\mathcal B} & = \left( \begin{array}{c} a\sb{1n} \cr a\sb{2n} \cr \vdots \cr a\sb{nn} \end{array} \right).
\end{aligned}$$ Definimos la **matriz de cambio de base** o **matriz de
paso** de ${\mathcal B}'$ a ${\mathcal B}$ como
$$M({\mathcal B}', {\mathcal B}) = (a\sb{ij}) = \left( \begin{array}{cccc} [\vec{u}'_1]\sb{\mathcal B} & [\vec{u}'_2]\sb{\mathcal B} & \ldots & [\vec{u}'_1]\sb{\mathcal B} \end{array} \right),$$
es decir, la **columna** $i$ de $M({\mathcal B}',{\mathcal B})$ contiene
las coordenadas del vector $\vec{u}'_i$ de ${\mathcal B}'$ respecto de la
base ${\mathcal B}$. Con esta notación, se tiene lo siguiente:

{{% theorem name="Ecuaciones del cambio de base" %}}
 Si las coordenadas de $\vec{v}\in V$
respecto a ${\mathcal B}$ y ${\mathcal B}'$ son, respectivamente
$$\vec{v}\sb{\mathcal B} = \left( \begin{array}{c} x\sb{1} \cr \vdots \cr x\sb{n} \end{array} \right) \quad \text{ y } \quad  \vec{v}\sb{\mathcal B'} = \left( \begin{array}{c} x'_1 \cr \vdots \cr x'_n \end{array} \right),$$
entonces se tiene la relación $$\begin{aligned}
   x\sb{1} & = a\sb{11} x'_1 + a\sb{12} x'_2 + \cdots + a\sb{1n} x'_n, \cr
   x\sb{2} & = a\sb{21} x'_1 + a\sb{22} x'_2 + \cdots + a\sb{2n} x'_n, \cr
       &   \vdots \cr
   x\sb{n} & = a\sb{n1} x'_1 + a\sb{n2} x'_2 + \cdots + a\sb{nn} x'_n,
\end{aligned}$$ que en forma matricial es
$\vec{v}\sb{\mathcal B} = M({\mathcal B'}, {\mathcal B}) \vec{v}\sb{\mathcal B'}$.
{{% /theorem %}}

{{% proof %}}
En las igualdades
$$\vec{v}= x\sb{1}\vec{u}\sb{1}+\cdots +x\sb{n} \vec{u}\sb{n}, \qquad \vec{v} = x'_1 \vec{u}'_1 + \cdots + x'_n \vec{u}'_n,$$
sustituimos cada $\vec{u}'_i$ por
$a\sb{1i} \vec{u}\sb{1} + a\sb{2i} \vec{u}\sb{2} + \cdots + a\sb{ni} \vec{u}\sb{n}$ y
agrupamos coeficientes, obtendremos:
$$\vec{v} = (a\sb{11}x'_1 + \cdots + a\sb{1n} x'_n) \vec{u}\sb{1} + \cdots + (a\sb{n1}x'_1 + \cdots + a\sb{nn} x'_n) \vec{u}\sb{n}.$$
Como la forma de expresar $\vec{v}$ como combinación lineal de
${\mathcal B}$ es única, los coeficientes de esta última combinación
lineal han de ser iguales a $x\sb{1},\ldots, x\sb{n}$, lo que demuestra el
resultado.

Otra forma de probar la igualdad es mediante el uso del morfismo de
coordenadas $c\sb{\mathcal{B}}$. Se tiene que $$\begin{aligned}
  \vec{v}\sb{\mathcal{B}} & = x'_1 [\vec{u}'_1]\sb{\mathcal{B}} + \cdots + x'_n [\vec{u}'_n]\sb{\mathcal{B}} \cr
                       & = \left( \begin{array}{ccc} [\vec{u}'_1]\sb{\mathcal{B}} & \ldots & [\vec{u}'_n]\sb{\mathcal{B}} \end{array} \right) \left( \begin{array}{c} x'_1 \cr \vdots \cr x'_n \end{array} \right) \cr
                       & = M(\mathcal{B}', \mathcal{B}) \vec{v}\sb{\mathcal{B}'}.
\end{aligned}$$
{{% /proof %}}

Como es natural, se podı́an haber invertido los papeles y tendrı́amos una
matriz de cambio de base $M({\mathcal B},{\mathcal B}')$, cuyas columnas
están formadas por las coordenadas de los vectores de ${\mathcal B}$
respecto a la base ${\mathcal B}'$. El siguiente resultado establece la
conexión entre estas dos matrices de cambio de base.

{{% theorem name="Inversa de la matriz de cambio de base" %}}
 Dadas dos bases ${\mathcal B}$ y
${\mathcal B}'$ de un espacio vectorial de dimensión $n$, la matriz de
cambio de base $M({\mathcal B}', {\mathcal B})$ es invertible, y su
inversa es $M({\mathcal B},{\mathcal B}')$.
{{% /theorem %}}

{{% proof %}}
Consideremos la matriz $M({\mathcal B},{\mathcal B}')$. Para cualquier
vector $\vec{v} \in V$, se verifica que
$$\vec{v}\sb{\mathcal B} = M({\mathcal B}', {\mathcal B}) \vec{v}\sb{\mathcal B'}, \qquad \vec{v}\sb{\mathcal B'} = M({\mathcal B}, {\mathcal B}') \vec{v}\sb{\mathcal B},$$
de donde
$$\vec{v}\sb{\mathcal B} = M({\mathcal B}', {\mathcal B}) M({\mathcal B}, {\mathcal B}') \vec{v}\sb{\mathcal B} \text{ para todo vector } \vec{v} \in V.$$
Si $\vec{v}$ recorre todos los vectores de la base ${\mathcal B}$,
obtenemos las relaciones en $\K^n$ dadas por $$\begin{aligned}
  \vec{e}\sb{1} & = M({\mathcal B}', {\mathcal B}) M({\mathcal B}, {\mathcal B}') \vec{e}\sb{1}, \cr
  \vec{e}\sb{2} & = M({\mathcal B}', {\mathcal B}) M({\mathcal B}, {\mathcal B}') \vec{e}\sb{2}, \cr
  & \vdots \cr
  \vec{e}\sb{n} & = M({\mathcal B}', {\mathcal B}) M({\mathcal B}, {\mathcal B}') \vec{e}\sb{n}.
\end{aligned}$$ Por tanto,
$I\sb{n} = M({\mathcal B}', {\mathcal B}) M({\mathcal B}, {\mathcal B}')$ y
tenemos el resultado.
{{% /proof %}}

{{% example name="Cálculo de la matriz de cambio de base en $\K^n$" %}}
Consideremos dos bases ${\mathcal B}=\{\vec{v}\sb{1},\ldots,\vec{v}\sb{n}\}$ y
${\mathcal B}'=\{\vec{v}'_1,\ldots,\vec{v}'_n\}$ del espacio vectorial
$\K^n$, respecto de la base estándar, y sean $A\sb{\mathcal B}$ y
$A\sb{{\mathcal B}'}$ las matrices de coordenadas respectivas de ambas
bases.

-   Calculamos la forma escalonada reducida por filas de la matriz
    $\left( \begin{array}{c|c} A\sb{\mathcal B} & A\sb{{\mathcal B}'} \end{array} \right)$.
    $$\left( \begin{array}{c|c} A\sb{\mathcal B} & A\sb{{\mathcal B}'} \end{array} \right) \mapright{\mbox{G-J}} \left( \begin{array}{c|c} I\sb{n} & P \end{array} \right).$$

-   Entonces $P$ es la matriz $M({\mathcal B}',{\mathcal B})$ de cambio
    de base de ${\mathcal B}'$ a ${\mathcal B}$, porque para todo $i$,
    la columna $i$ de $P$ es la solución del sistema de matriz ampliada
    $(A\sb{\mathcal B}|\vec{v}'_i)$, es decir, es
    $[\vec{v}'_i]\sb{\mathcal B}$.
{{% /example %}}

{{% example name="Ejemplo" %}}
En $\R^3$ consideramos las bases
${\mathcal B}= \{ \vec{u}\sb{1}, \vec{u}\sb{2}, \vec{u}\sb{3} \}$ y
${\mathcal B}'= \{ \vec{v}\sb{1}, \vec{v}\sb{2}, \vec{v}\sb{3} \}$, donde
$$\vec{u}\sb{1} = \left( \begin{array}{c} 1 \cr 1 \cr 0 \end{array} \right), \vec{u}\sb{2}= \left( \begin{array}{c} 1 \cr 0 \cr 1 \end{array} \right), \vec{u}\sb{3}= \left( \begin{array}{c} 0 \cr 1 \cr 1 \end{array} \right),
 \vec{v}\sb{1} = \left( \begin{array}{c} 1 \cr -1 \cr 0 \end{array} \right), \vec{v}\sb{2}= \left( \begin{array}{c} 1 \cr 0 \cr -1 \end{array} \right), \vec{v}\sb{3}= \left( \begin{array}{c} 1\cr 0 \cr 0 \end{array} \right).$$
Entonces
$$\left( \begin{array}{c|c} A\sb{\mathcal B} & A\sb{{\mathcal B}'} \end{array} \right) = \left(\begin{array}{ccc|ccc}
         1 & 1 & 0 & 1 & 1 & 1\cr
         1 & 0 & 1 & -1 & 0 & 0\cr
         0 & 1 & 1 & 0 & -1 & 0
        \end{array}\right) \mapright{\mbox{G-J}}
  \left(\begin{array}{ccc|ccc}
         1 & 0 & 0 & 0 & 1 & 1/2\cr
         0 & 1 & 0 & 1 & 0 & 1/2\cr
         0 & 0 & 1 & -1 & -1 & -1/2
        \end{array}\right) .$$ Luego la matriz del cambio de base es
$$M({\mathcal B}',{\mathcal B})=\left(\begin{array}{ccc} 0 & 1 & 1/2\cr 1 & 0 & 1/2\cr -1 & -1 & -1/2 \end{array}\right).$$
{{% /example %}}
