+++
title = "Álgebra matricial"
weight = 20
+++



## Inversa de una matriz

En el tema anterior definimos la suma y el producto de matrices (cuando
las dimensiones de las matrices lo permiten), y sus propiedades básicas.
Ahora veremos otras propiedades, principalmente del producto, y la
relación de esta operación con las transformaciones elementales y con el
rango.

{{% definition %}}
Inversa de una matriz

Una matriz cuadrada $A\sb{n\times n}$ se dice **invertible** (o
**regular**) se existe una matriz $B\sb{n \times n}$ tal que
$$AB = I\sb{n} \quad \mbox{ y }\quad  BA = I\sb{n}.$$ En este caso, la matriz
$B$ se denomina **inversa** de $A$, y la notaremos por $B = A^{-1}$.

No todas la matrices cuadradas tienen inversa. Si $A$ no tiene inversa,
diremos que es **singular**.
{{% /definition %}}

{{% example name="Ejemplo" %}}
Consideremos las matrices
$$A\sb{1} = \left( \begin{array}{rr} 1 & 2 \cr 1 & 3 \end{array} \right),\quad  A\sb{2} = \left( \begin{array}{rr} 1 & 2 \cr 1 & 2 \end{array} \right).$$
La matriz
$B\sb{1} = \left( \begin{smallmatrix} 3 & -2 \cr -1 & 1 \end{smallmatrix} \right)$
verifica que $A\sb{1} B\sb{1} = B\sb{1} A\sb{1} = I\sb{2}$, por lo que $A\sb{1}$ tiene inversa.

Sin embargo, supongamos que existe
$B = \left( \begin{smallmatrix} b\sb{11} & b\sb{12} \cr b\sb{21} & b\sb{22} \end{smallmatrix} \right)$
tal que $A\sb{2} B = I\sb{2}$. Entonces se tienen que verificar las relaciones
$$\left\\{ \begin{array}{l} b\sb{11} + 2 b\sb{21} = 1, \cr b\sb{12} + 2 b\sb{22} = 0, \cr b\sb{11} + 2 b\sb{21} = 0, \cr b\sb{12} + 2 b\sb{22} = 1, \end{array} \right.$$
que es un sistema incompatible en las incógnitas $b\sb{ij}$. Por tanto,
$A\sb{2}$ no tiene inversa.
{{% /example %}}

{{% theorem name="Unicidad de inversa" %}}
 Si una matriz cuadrada $A$ es invertible, su inversa
es única.
{{% /theorem %}}

{{% proof %}}
Spupongamos que $B\sb{1}$ y $B\sb{2}$ son matrices inversas de $A$. Entonces
$$B\sb{1} = B\sb{1} I = B\sb{1} (A B\sb{2}) = (B\sb{1} A) B\sb{2} = I B\sb{2} = B\sb{2}.$$ Por tanto, no
pueden existir dos inversas de $A$ que sean distintas.
{{% /proof %}}

De aquı́ deducimos el siguiente resultado:

{{% theorem name="Ecuaciones matriciales" %}}
-   Si $A$ es una matriz invertible, entonces existe una única solución
    para $X$ en la ecuación matricial
    $A\sb{n \times n} X\sb{n \times p} = C\sb{n \times p}$, que es
    $X = A^{-1}C$.

-   Un sistema de $n$ ecuaciones y $n$ incógnitas se puede escribir como
    una ecuación matricial
    $A\sb{n \times n} \vec{x}\sb{n \times 1} =  \vec{b}\sb{n \times 1}$. Por lo
    anterior, si $A$ es invertible, el sistema tiene solución única
    igual a $\vec{x} = A^{-1} \vec{b}$.
{{% /theorem %}}

{{% proof %}}
La ecuación matricial $AX=C$ tiene solución puesto que, como $A$ es
invertible, podemos considerar la matriz inversa $A^{-1}$, y
sustituyendo $X=A^{-1}C$ en la ecuación vemos que se verifica:
$A(A^{-1}C)=(AA^{-1})C=IC=C$.

Por otra parte, la solución es única puesto que, dada una solución
cualquiera $X=M$, tendrı́amos $AM=C$, y multiplicando esta igualdad por
la izquierda por $A^{-1}$, obtenemos: $A^{-1}(AM)=A^{-1}C$, donde
$A^{-1}(AM)=(A^{-1}A)M=IM=M$. Es decir, tenemos $M=A^{-1}C$. Como la
inversa de $A$ es única, la solución $M$ es también única.

El caso del sistema de ecuaciones es un caso particular de lo anterior,
donde el vector de términos independientes $\vec{b}$ es una matriz
$n\times 1$.
{{% /proof %}}

A pesar de lo anterior, debemos hacer hincapié en que la representación
de la solución como $\vec{x} = A^{-1} \vec{b}$ es conveniente desde el
punto de vista teórico o de notación. En la práctica, un sistema
invertible $A \vec{x} = \vec{b}$ no se resuelve calculando $A^{-1}$ y
después el producto $A^{-1} \vec{b}$. Se realizan más operaciones ası́ que
aplicando las técnicas de eliminación descritas en el tema anterior.

Como no todas las matrices cuadradas tienen inversa, se necesitan
métodos para distinguir entre matrices singulares e invertibles. Los más
importantes son los que siguen.

{{% theorem name="Existencia de inversa" label="eq:inv" %}}
Sea $A$ una matriz cuadrada de orden $n$. Son equivalentes:

1.  $A$ es invertible.

2.  El sistema $A \vec{x} = \vec{0}$ tiene solución única
    ($\vec{x} = \vec{0}$).

3.  $\rg(A) = n$.

4.  $A \mapright{\GJ} I\sb{n}$.
{{% /theorem %}}

{{% proof %}}
$(1) \Rightarrow (2)$. Si $A$ es invertible, ya vimos que el sistema
$A \vec{x} = \vec{0}$ tiene solución única $\vec{x}=A^{-1}\vec{0}= \vec{0}$.

$(2) \Leftrightarrow (3)$. Por el Teorema de Rouché-Frobenius, sabemos
que el sistema lineal homogéneo $A \vec{x} = \vec{0}$ tiene solución única
si y solamente si $\rg(A) = \rg(A|\vec{0}) = n$. Como siempre se tiene
$\rg(A) = \rg(A|\vec{0})$, el sistema tiene solución única si y sólo si
$\rg(A)=n$.

$(3) \Leftrightarrow (4)$. Si $\rg(A) = n$, la forma escalonada reducida
por filas $E\sb{A}$ es una matriz diagonal cuadrada de orden $n$, con $n$
pivotes. La única forma posible es $E\sb{A} = I\sb{n}$. El recı́proco es
inmediato.

$(4) \Rightarrow (1)$. Supongamos que $\rg(A) = n$. Sea $\vec{e\sb{j}}$ la
columna $j$ de la matriz identidad $n\times n$. Entonces cada sistema
$A \vec{x} = \vec{e}\sb{j}$ es compatible determinado, porque
$\rg(A) = \rg(A|\vec{e}\sb{j}) = n$ (la matriz ampliada no puede tener rango
mayor que $n$, porque sólo tiene $n$ filas). Si $B$ es la matriz cuyas
columnas son las soluciones de cada sistema $A \vec{x} = \vec{e}\sb{j}$, por
las propiedades del producto de matrices tenemos: $AB=I$.

Falta demostrar que también se tiene $BA=I$. Es decir, queremos
demostrar que $BA-I=\vec{0}$. Para ello, observemos que:
$$A(BA-I) = ABA - A = IA - A =  O .$$ Por tanto, cada columna de
$BA-I$ es una solución del sistema homogéneo $A\vec{x} = \vec{0}$. Como
$\rg(A)=n$, la única solución de este sistema es $\vec{0}$, por lo que
todas las columnas de $BA-I$ son nulas, esto es, $BA-I =  O $, y por
tanto $BA = I = AB$. Luego $A$ es invertible.
{{% /proof %}}

Gracias al resultado anterior podemos demostrar que, para comprobar que
una matriz es la inversa de otra, sólo es necesitamos hacer una
multiplicación (y no las dos que dice la definición).

{{% theorem name="Comprobación de matriz inversa" %}}
 Si $A\sb{n\times n}$, $B\sb{n\times n}$ son
matrices tales que $AB=I$, entonces también se tiene $BA=I$, y por tanto
$A$ y $B$ son invertibles, $B=A^{-1}$ y $A=B^{-1}$.
{{% /theorem %}}

{{% proof %}}
Consideremos el sistema $B\vec{x}=\vec{0}$. Multiplicando por la izquierda
por $A$, se tiene $AB\vec{x}=A\vec{0}$, por tanto $I\vec{x}=\vec{x}=\vec{0}$.
Es decir, el sistema $B\vec{x}=\vec{0}$ tiene solución única
($\vec{x}=\vec{0}$). Por el resultado anterior, $B$ es una matriz
invertible, y podemos considerar su matriz inversa $B^{-1}$. Pero
entonces tenemos $B^{-1}=IB^{-1}=ABB^{-1}=A$. Es decir, $A$ es la matriz
inversa de $B$, lo que implica que $AB=BA=I$, como queríamos demostrar.
{{% /proof %}}

Aunque evitaremos el cálculo de la inversa de una matriz, hay veces que
debemos hacerlo. Para construir un algoritmo que nos devuelva $A^{-1}$
cuando $A\sb{n \times n}$ es invertible, usaremos lo que acabamos de ver:
determinar $A^{-1}$ es equivalente a resolver la ecuación matricial
$AX = I$, que es lo mismo que resolver los $n$ sistemas de ecuaciones
definidos por $$A \vec{x} = \vec{e}\sb{j}, \quad j=1,2,\ldots,n,$$ donde
$\vec e\sb{i}$ es la columna $j$ de la matriz identidad. En otras palabras,
si $\vec c\sb{1}, \vec c\sb{2},\ldots, \vec c\sb{n}$ son las respectivas soluciones,
entonces la matriz formada por esas columnas
$$X = \left( \begin{array}{cccc} \vec c\sb{1} & \vec c\sb{2} & \cdots & \vec c\sb{n} \end{array} \right)$$
es la única solución de la ecuación $AX=I$ y de aquı́ $X = A^{-1}$.

Si $A$ es invertible, el método de Gauss-Jordan reduce la matriz
ampliada $(A|\vec e\sb{j})$ a $(I|\vec c\sb{j})$, donde $\vec c\sb{j}$ es la única
solución de $A\vec{x} = \vec e\sb{j}$, y por tanto es la columna $j$ de
$A^{-1}$. Por tanto, el método de Gauss-Jordan nos permite calcular las
columnas de $A^{-1}$. Pero mejor que resolver cada sistema
$A\vec{x} = \vec e\sb{j}$ de forma independiente, podemos resolverlos
simultáneamente aprovechando que todos tienen la misma matriz de
coeficientes, y que por tanto se utilizan exactamente las mismas
transformaciones elementales para resolver cada uno de los sistemas. En
otras palabras, si aplicamos Gauss-Jordan a la matriz
$(A|I)=(A|\vec e\sb{1}|\vec e\sb{2}| \cdots |\vec e\sb{n})$ obtenemos
$$\left(A|\vec e\sb{1}|\vec e\sb{2}| \cdots |\vec e\sb{n}\right) \mapright{\mbox{Gauss-Jordan}} \left(I|\vec c\sb{1}|\vec c\sb{2}| \cdots |\vec c\sb{n}\right),$$
o de manera más compacta
$$\left(A|I\right) \mapright{\mbox{Gauss-Jordan}} \left(I|A^{-1}\right).$$
¿Qué ocurre si intentamos invertir una matriz singular con este
procedimiento? El resultado anterior nos indica que una matriz singular
$A$ no puede ser reducida mediante Gauss-Jordan a la matriz $I$ porque
una fila de ceros aparecerá en algún momento en la zona correspondiente
a la matriz $A$. Por ello, no tenemos que saber a priori si la matriz
que tenemos es o invertible, pues resultará evidente en el proceso de
cálculo.

{{% example name="Cálculo de la inversa" %}}
 La eliminación de Gauss-Jordan se puede usar para
el cálculo de la inversa de una matriz $A$ mediante la reducción
$$\left(A|I\right) \ \mapright{\GJ} \ \left(I|A^{-1}\right).$$ La única
posibilidad de que este método falle es porque aparezca una fila de
ceros en el lado izquierdo de la matriz ampliada, y esto ocurre si y
solamente si la matriz $A$ es singular.
{{% /example %}}

{{% example name="Ejemplo" %}}
Calculemos, si existe, la inversa de la matriz
$$A = \left( \begin{array}{rrr} 1 & 1 & 1 \cr 1 & 2 & 2 \cr 1 & 2 & 3 \end{array} \right).$$
Aplicamos el método de Gauss-Jordan para obtener $$\begin{array}{rlcl}
    (A|I) = & \left( \begin{array}{rrr|rrr} 1 & 1 & 1 & 1 & 0 & 0 \cr 1 & 2 & 2 & 0 & 1 & 0 \cr 1 & 2 & 3 & 0 & 0 & 1
    \end{array} \right) &
    \to & \left( \begin{array}{rrr|rrr} 1 & 1 & 1 & 1 & 0 & 0 \cr 0 & 1 & 1 & -1 & 1 & 0 \cr 0 & 1 & 2 & -1 & 0 & 1 \end{array} \right) \cr {}
    \to & \left( \begin{array}{rrr|rrr} 1 & 0 & 0 & 2 & -1 & 0 \cr 0 & 1 & 1 & -1 & 1 & 0 \cr 0 & 0 & 1 & 0 & -1 & 1 \end{array} \right) &
    \to & \left( \begin{array}{rrr|rrr} 1 & 0 & 0 & 2 & -1 & 0 \cr 0 & 1 & 0 & -1 & 2 & -1 \cr 0 & 0 & 1 & 0 & -1 & 1 \end{array} \right).
  \end{array}$$ Por tanto, la matriz es invertible y
$$A^{-1} = \left( \begin{array}{rrr} 2 & -1 & 0 \cr -1 & 2 & -1 \cr 0 & -1 & 1 \end{array} \right).$$
Se comprueba fácilmente que, al multiplicar $A$ por esta matriz,
obtenemos la matriz identidad.
{{% /example %}}

{{% theorem name="Propiedades de la inversión de matrices" %}}
Para matrices invertibles $A$ y $B$, se verifica que $A^{-1}$, $AB$ y
$A^{t}$ son invertibles y, si $K=\mathbb C$, $A^{\ast}$ también es
invertible. Además:

-   $\left(A^{-1}\right)^{-1} = A$.

-   $(AB)^{-1} = B^{-1} A^{-1}$.

-   $\left(A^t\right)^{-1} = \left(A^{-1}\right)^t \quad$ y, si
    $K=\mathbb C$,
    $\quad \left(A^{\ast}\right)^{-1} =  \left(A^{-1}\right)^{\ast}$.
{{% /theorem %}}

{{% proof %}}
Recordemos que para demostrar que una matriz es la inversa de otra, sólo
hay que ver que su producto (en el orden que queramos) es $I$. Y esto
además demuestra que las dos matrices son invertibles.

Se tiene que la inversa de $A^{-1}$ es $A$ porque $A^{-1}A=I$. Para ver
que la inversa de $AB$ es $B^{-1}A^{-1}$ sólo hay que comprobar que
$(AB)(B^{-1}A^{-1})= ABB^{-1}A^{-1}=AA^{-1}=I$. La inversa de $A^t$ es
$\left(A^{-1}\right)^t$ porque
$A^{t}\left(A^{-1}\right)^t = \left(A^{-1}A\right)^t = I^t = I$.
Finalmente, si $K=\mathbb C$, vemos que la inversa de $A^{\ast}$ es
$\left(A^{-1}\right)^{\ast}$ ya que
$A^{\ast }\left(A^{-1}\right)^{\ast} = \left(A^{-1}A\right)^{\ast} = I^{\ast} = I$.
{{% /proof %}}

## Matrices elementales y equivalencia

{{% remark %}}
[Operaciones elementales por columnas]{.smallcaps}. Es evidente que las
mismas operaciones elementales por filas descritas en el tema anterior
pueden realizarse por columnas. Tenemos entonces tres tipos de
operaciones análogas a las operaciones elementales por filas:

-   **Tipo I.** Intercambiar las columnas $i$ y $j$.

-   **Tipo II.** Reemplazar la columna $i$ por un múltiplo no nulo de
    ella misma.

-   **Tipo III.** Reemplazar la columna $j$ por la suma de ella misma
    con un múltiplo de la columna $j$.

Análogamente a lo visto para las filas existen unas matrices especiales
llamadas *formas escalonadas por columnas* y *formas escalonadas
reducidas por columnas*. La trasposición de matrices nos permite definir
rápidamente estos conceptos:

-   Una matriz se dice que es una **forma escalonada por columnas** si
    su traspuesta es una forma escalonada por filas.

-   Una matriz se dice que es una **forma escalonada reducida por
    columnas** si su traspuesta es una forma escalonada reducida por
    filas.

Igualmente se puede comprobar que toda matriz puede ser transformada,
mediante operaciones por columnas, en una forma escalonada por columnas
y en una forma escalonada reducida por columnas. Por último, dos
matrices se dicen *equivalentes por columnas* si puede transformarse una
en otra mediante operaciones elementales por columnas.

Obsérvese que las operaciones elementales por columnas *no transforman*
la matriz de un sistema lineal en la matriz de otro sistema lineal
equivalente.

Si $A\sb{m \times n}$ es una matriz arbitraria, sabemos que la matriz
$A^t$ es equivalente por filas a una única forma escalonada reducida por
filas, por lo que $A$ es equivalente por columnas a *una única* forma
escalonada reducida por columnas.
{{% /remark %}}

{{% remark %}}
Para todo $j=1,\ldots,n$, sea $\vec{e}\sb{j}$, de orden $n\times 1$, la
columna $j$ de la matriz identidad $I\sb{n}$. El vector $\vec e\sb{j}$ tiene
todas sus entradas nulas salvo un $1$ en la posición $j$. Observemos que
la multiplicación de una matriz $A\sb{m\times n}$ por el vector columna
$\vec{e}\sb{j}$ resulta $A\vec{e}\sb{j}=A\sb{\ast j}$, la columna $j$ de la matriz $A$.
Análogamente, si ahora $\vec{e}\sb{i}$ es de orden $m\times 1$ (la columna
$i$ de la matriz $I\sb{m}$), la multiplicación del vector fila $\vec e\sb{i}^t$
por la matriz $A$ resulta $\vec{e}\sb{i}^t A=A\sb{i\ast}$, la fila $i$ de la
matriz $A$.
{{% /remark %}}

Vamos a ver que las operaciones elementales que usamos para la
eliminación gaussiana pueden interpretarse como productos por ciertas
matrices de estructura muy sencilla.

{{% definition %}}
Matriz elemental de tipo I Decimos que una matriz cuadrada $E\sb{ij}$ es
**elemental de tipo I** si se obtiene a partir de la matriz identidad
$I\sb{n}$ mediante una operación elemental por filas de tipo I. Es decir, si
$E\sb{ij}$ se obtiene de $I\sb{n}$ al intercambiar las filas $i$ y $j$.
{{% /definition %}}

En este caso todas las entradas de la matriz $E\sb{ij}$ son nulas, salvo
$$[E\sb{ij}]\sb{kk}=1, \text{ para } k\ne i,j, \quad  [E\sb{ij}]\sb{ij}=1, \quad [E\sb{ij}]\sb{ji}=1.$$
Si atendemos a las filas de $E$ observamos que
$$[E\sb{ij}]\sb{k\ast} = \vec{e}\sb{k}^t \text{ si } k\ne i,j, \quad [E\sb{ij}]\sb{i\ast} = \vec{e}\sb{j}^t, \quad [E\sb{ij}]\sb{j\ast}=\vec{e}\sb{i}^t.$$

{{% example name="Ejemplo" %}}
Si $n = 3$, entonces las matrices
$$E\sb{13} = \left( \begin{array}{rrr} 0 & 0 & 1 \cr 0 & 1 & 0 \cr 1 & 0 & 0 \end{array} \right),\quad  E\sb{23} = \left( \begin{array}{rrr} 1& 0 & 0 \cr 0 & 0 & 1 \cr 0 & 1 & 0 \end{array} \right)$$
son matrices elementales de tipo I.
{{% /example %}}

{{% theorem name="Propiedades de una matriz elemental de tipo I" %}}
-   La multiplicación de una matriz elemental de tipo I *a la izquierda*
    de una matriz produce en ésta una operación elemental *por filas* de
    tipo I.

-   La multiplicación de una matriz elemental de tipo I *a la derecha*
    de una matriz produce en ésta una operación elemental *por columnas*
    de tipo I.

-   Si $E\sb{ij}$ es una matriz elemental de tipo I entonces es invertible
    y su inversa es ella misma. Es decir, $E\sb{ij}E\sb{ij}=I$.
{{% /theorem %}}

{{% proof %}}
Basta probar el primer punto, pues el segundo, además de ser análogo, se
deduce del primero por trasposición de matrices.

Sean entonces $A\sb{m\times n}$ y $E\sb{m\times m}$ la matriz obtenida de la
identidad al intercambiar las filas $i$ y $j$. Vamos a describir la
matriz $EA$ por filas, recordando que la fila $k$ de $EA$ es igual a la
fila $k$ de $E$ multiplicada por la matriz $A$.

Si $k\ne i,j$ sabemos que $E\sb{k\ast}=\vec{e}\sb{k}^t$, de donde calculamos la
fila $k-$ésima de $EA$:
$$[EA]\sb{k\ast}=E\sb{k\ast}A=\vec{e}\sb{k}^tA=A\sb{k\ast},\mbox{ con }k\ne i,j.$$ Además
$E\sb{i\ast}=\vec{e}\sb{j}^t$ y $E\sb{j\ast}=\vec{e}\sb{i}^t$. Luego las filas $i-$ésima y
$j-$ésima de $EA$ son
$$[EA]\sb{i\ast}=E\sb{i\ast}A=\vec{e}\sb{j}^tA=A\sb{j\ast}\quad \mbox{ y }\quad [EA]\sb{j\ast}=E\sb{j\ast}A=\vec{e}\sb{i}^tA=A\sb{i\ast}.$$
Es decir, la matriz $EA$ es la que se obtiene de $A$ al intercambiar las
filas $i$ y $j$.

La demostración de la última proposición es consecuencia del resultado
anterior. Si $E$ es la matriz que se obtiene de la identidad al
intercambiar las filas $i$ y $j$, el producto a la izquierda de $E$ por
ella misma vuelve a intercambiar las filas $i$ y $j$, obteniendo $EE=I$.
{{% /proof %}}

{{% definition %}}
Matriz elemental de tipo II Decimos que una matriz cuadrada
$E\sb{i}(\alpha)$ es **elemental de tipo II** si se obtiene a partir de la
matriz identidad $I\sb{n}$ mediante una operación elemental por filas de
tipo II. Es decir, si $E\sb{i}(\alpha)$ se obtiene de $I\sb{n}$ al sustituir la
fila $i$ por un múltiplo no nulo de ella ($\alpha \ne 0$).
{{% /definition %}}

En este caso todas las entradas de la matriz $E\sb{i}(\alpha)$ son nulas,
salvo
$$[E\sb{i}(\alpha)]\sb{kk}=1,\ k\ne i, \quad [E\sb{i}(\alpha)]\sb{ii} = \alpha, \text{ con } \alpha \ne 0.$$
Si atendemos a las filas de $E$ observamos que
$$[E\sb{i}(\alpha)]\sb{k\ast} = \vec{e}\sb{k}^t \text{ si } k\ne i, \quad [E\sb{i}(\alpha)]\sb{i\ast} = \alpha \vec{e}\sb{i}^t \text{  con } \alpha \ne 0.$$

{{% example name="Ejemplo" %}}
Si $n = 3$, entonces las matrices
$$E\sb{1}(-3) = \left( \begin{array}{rrr} -3 & 0 & 0 \cr 0 & 1 & 0 \cr 0 & 0 & 1 \end{array} \right), \quad E\sb{3}({\textstyle \frac{1}{2}}) = \left( \begin{array}{rrr} 1 & 0 & 0 \cr 0 & 1 & 0 \cr 0 & 0 & \frac{1}{2} \end{array} \right)$$
son matrices elementales de tipo II.
{{% /example %}}

{{% theorem name="Propiedades de una matriz elemental de tipo II" %}}
-   La multiplicación de una matriz elemental de tipo II *a la
    izquierda* de una matriz produce en ésta una operación elemental
    *por filas* de tipo II.

-   La multiplicación de una matriz elemental de tipo II *a la derecha*
    de una matriz produce en ésta una operación elemental *por columnas*
    de tipo II.

-   Si $E\sb{i}(\alpha)$ es una matriz elemental de tipo II entonces es
    invertible y su inversa es $E\sb{i}(\frac{1}{\alpha})$, que también es
    una matriz elemental de tipo II.
{{% /theorem %}}

{{% proof %}}
La primera parte es similar a la proposición correspondiente de las
matrices elementales de tipo I. Es fácil comprobar que si $E\sb{i}(\alpha)$
es la matriz que se obtiene de la identidad al multiplicar la fila $i$
por $\alpha \ne 0$, entonces se obtendrá la identidad al volver a
multiplicar esa fila por $\frac{1}{\alpha}$. Esto equivale a decir que
$E\sb{i}(\frac{1}{\alpha})E\sb{i}(\alpha)=I$, luego
$(E\sb{i}(\alpha))^{-1}=E\sb{i}(\frac{1}{\alpha})$.
{{% /proof %}}

{{% definition %}}
Matriz elemental de tipo III Decimos que una matriz cuadrada
$E\sb{ij}(\alpha)$ es **elemental de tipo III** si se obtiene a partir de
la matriz identidad $I\sb{n}$ mediante una operación elemental por filas de
tipo III. Es decir, si $E\sb{ij}(\alpha)$ se obtiene de $I\sb{n}$ al sumar, a
la fila $i$, la fila $j$ multiplicada por $\alpha$.
{{% /definition %}}

En este caso todas las entradas de la matriz $E\sb{ij}(\alpha)$ son nulas,
salvo
$$[E\sb{ij}(\alpha)]\sb{ii} = 1 \text{ para todo } i=1,\ldots n,\quad  [E\sb{ij}(\alpha)]\sb{ij} = \alpha.$$
Si atendemos a las filas de $E\sb{ij}(\alpha)$ observamos que
$$[E\sb{ij}(\alpha)]\sb{k\ast} = \vec{e}\sb{k}^t  \text{ si }  k\ne i,\quad  [E\sb{ij}(\alpha)]\sb{i\ast} = \vec{e}\sb{i}^t + \alpha \vec{e}\sb{j}^t.$$

{{% example name="Ejemplo" %}}
Si $n = 3$, las matrices
$$E\sb{12}(-4) = \left( \begin{array}{rrr} 1 & -4 & 0 \cr 0 & 1 & 0 \cr 0 & 0 & 1 \end{array} \right), \quad E\sb{31}(-7) = \left( \begin{array}{rrr} 1 & 0 & 0 \cr 0 & 1 & 0 \cr -7 & 0 & 1 \end{array} \right)$$
son matrices elementales de tipo III.
{{% /example %}}

{{% theorem name="Propiedades de una matriz elemental de tipo III" %}}
-   La multiplicación de una matriz elemental de tipo III *a la
    izquierda* de una matriz produce en ésta una operación elemental
    *por filas* de tipo III.

-   La multiplicación de una matriz elemental de tipo III *a la derecha*
    de una matriz produce en ésta una operación elemental *por columnas*
    de tipo III.

-   Si $E\sb{ij}(\alpha)$ es una matriz elemental de tipo III entonces es
    invertible y su inversa es $E\sb{ij}(-\alpha)$, que es también una
    matriz elemental de tipo III.
{{% /theorem %}}

{{% proof %}}
Las dos primeras afirmaciones se demuestran de manera similar a las de
tipo $I$. La tercera de demuestra comprobando que, si $E\sb{ij}(\alpha)$
se ha obtenido sumando, a la fila $i$ de la matriz identidad, la fila
$j$ multiplicada por $\alpha$, entonces volveremos a obtener la matriz
identidad si le restamos, a la fila $i$, la fila $j$ multiplicada por
$\alpha$ (es decir, si le sumamos la fila $j$ multiplicada por
$-\alpha$). Por tanto $E\sb{ij}(-\alpha)E\sb{ij}(\alpha)=I$, de donde
$(E\sb{ij}(\alpha))^{-1}=E\sb{ij}(-\alpha)$.
{{% /proof %}}

Aunque no hemos hablado de dimensiones, lo anterior es válido para
matrices generales de orden $m \times n$.

{{% example name="Ejemplo" %}}
Consideremos la sucesión de operaciones para reducir
$$A = \left( \begin{array}{rrr} 1 & 2 & 4 \cr 2 & 4 & 8 \cr 3 & 6 & 13 \end{array} \right)$$
a su forma escalonada reducida por filas $E\sb{A}$. $$\begin{aligned}
    A & =  \left( \begin{array}{rrr} 1 & 2 & 4 \cr 2 & 4 & 8 \cr 3 & 6 & 13 \end{array} \right)
    \mapright{\begin{array}{c} F\sb{2} - 2F\sb{1} \cr F\sb{3} - 3F\sb{1} \end{array}}
    \left( \begin{array}{rrr} 1 & 2 & 4 \cr 0 & 0 & 0 \cr 0 & 0 & 1 \end{array} \right) \cr
    &  \mapright{\begin{array}{c} F\sb{23} \end{array}}
    \left( \begin{array}{rrr} 1 & 2 & 4 \cr 0 & 0 & 1 \cr 0 & 0 & 0 \end{array} \right)
    \mapright{\begin{array}{c} F\sb{1} - 4F\sb{2} \end{array}}
    \left( \begin{array}{rrr} 1 & 2 & 0 \cr 0 & 0 & 1 \cr 0 & 0 & 0 \end{array} \right) = E\sb{A}
\end{aligned}$$ La reducción se puede ver como una sucesión de
multiplicaciones a izquierda por las matrices elementales
correspondientes.
$$\left( \begin{array}{rrr} 1 & -4 & 0 \cr 0 & 1 & 0 \cr 0 & 0 & 1 \end{array} \right)
  \left( \begin{array}{rrr} 1 & 0 & 0 \cr 0 & 0 & 1 \cr 0 & 1 &  0 \end{array} \right)
  \left( \begin{array}{rrr} 1 & 0 & 0 \cr 0 & 1 & 0 \cr -3 & 0 & 1 \end{array} \right)
  \left( \begin{array}{rrr} 1 & 0 & 0 \cr -2 & 1 & 0 \cr 0 & 0 & 1 \end{array} \right) A = E\sb{A}.$$
Observemos el orden en que las matrices elementales deben ser
multiplicadas.
{{% /example %}}

{{% theorem name="Producto de matrices elementales" %}}
 Una matriz $A$ es invertible si y
solamente si $A$ es el producto de matrices elementales de tipos I, II,
o III.
{{% /theorem %}}

{{% proof %}}
Si $A$ es invertible, el método de Gauss-Jordan reduce $A$ a la matriz
$I$ mediante operaciones por fila. Si $G\sb{1}, G\sb{2}, \ldots, G\sb{k}$ son las
correspondientes matrices elementales, entonces
$$G\sb{k} \cdots G\sb{2} G\sb{1} A = I, \mbox{ o bien } A = G\sb{1}^{-1} G\sb{2}^{-1} \cdots G\sb{k}^{-1}.$$
Como la inversa de una matriz elemental es una matriz elemental, esto
prueba que $A$ se puede expresar como producto de matrices elementales.

Recı́procamente, si $A = E\sb{1} E\sb{2} \cdots E\sb{k}$ es un producto de matrices
elementales, entonces $A$ es invertible, pues es el producto de matrices
invertibles.
{{% /proof %}}

{{% example name="Ejemplo" %}}
Expresemos
$$A = \left( \begin{array}{rr} -2 & 3 \cr 1 & 0 \end{array} \right)$$
como producto de matrices elementales. Mediante la reducción a su forma
escalonada reducida por filas, comprobaremos que $A$ es invertible, y la
expresaremos como dicho producto. En efecto, $$\begin{array}{l|l}
    \begin{array}{cc} A & \mapright{-\frac{1}{2} F\sb{1} }  \left( \begin{array}{cc} 1&-3/2\cr{}1&0\end{array} \right)
    \cr & \mapright{F\sb{2} - F\sb{1}} \left( \begin{array}{cc} 1&-3/2\cr{}0&3/2\end{array} \right)
    \cr & \mapright{\frac{2}{3} F\sb{2}} \left( \begin{array}{cc} 1&-3/2\cr{}0&1\end{array} \right)
    \cr & \mapright{F\sb{1} + \frac{3}{2} F\sb{2}} \left( \begin{array}{cc} 1&0\cr{}0&1\end{array} \right).
    \end{array}
    &
    \begin{array}{c}
    \left( \begin{array}{rr} -\frac{1}{2} & 0 \cr 0 & 1 \end{array} \right) A = \left( \begin{array}{cc} 1&-3/2\cr{}1&0\end{array} \right) \cr
    \left( \begin{array}{rr} 1 & 0 \cr -1 & 1 \end{array} \right) \left( \begin{array}{cc} 1&-3/2\cr{}1&0\end{array} \right) = \left( \begin{array}{cc} 1&-3/2\cr{}0&3/2\end{array} \right) \cr
    \left( \begin{array}{rr} 1 & 0 \cr 0 & \frac{2}{3} \end{array} \right) \left( \begin{array}{cc} 1&-3/2\cr{}0&3/2\end{array} \right) = \left( \begin{array}{cc} 1&-3/2\cr{}0&1\end{array} \right) \cr
    \left( \begin{array}{rr} 1 & \frac{3}{2} \cr 0 & 1 \end{array} \right) \left( \begin{array}{cc} 1&-3/2\cr{}0&1\end{array} \right) = \left( \begin{array}{cc} 1&0\cr{}0&1\end{array} \right).
    \end{array}
  \end{array}$$ Entonces
$$\left( \begin{array}{rr} 1 & \frac{3}{2} \cr 0 & 1 \end{array} \right)
  \left( \begin{array}{rr} 1 & 0 \cr 0 & \frac{2}{3} \end{array} \right)
  \left( \begin{array}{rr} 1 & 0 \cr -1 & 1 \end{array} \right)
  \left( \begin{array}{rr} -\frac{1}{2} & 0 \cr 0 & 1 \end{array} \right) A = I\sb{2},$$
de donde
$$A = \left( \begin{array}{rr} -2 & 0 \cr 0 & 1 \end{array} \right)
  \left( \begin{array}{rr} 1 & 0 \cr 1 & 1 \end{array} \right)
  \left( \begin{array}{rr} 1 & 0 \cr 0 & \frac{3}{2} \end{array} \right)
  \left( \begin{array}{rr} 1 & -\frac{3}{2} \cr 0 & 1 \end{array} \right).$$

Esta forma de descomponer $A$ como producto de matrices elementales no
es única. Por ejemplo, podrı́amos haber hecho:
$$A= \left(\begin{array}{rr} -2 & 3 \cr 1 & 0 \end{array}\right) \mapright{F\sb{12}}
    \left(\begin{array}{rr}  1 & 0 \cr -2 & 3 \end{array}\right) \mapright{F\sb{2}+2F\sb{1}}
    \left(\begin{array}{rr} 1 & 0 \cr 0 & 3 \end{array}\right) \mapright{\frac{1}{3}F\sb{2}}
    \left(\begin{array}{rr} 1 & 0 \cr 0 & 1 \end{array}\right),$$ con lo
que habrı́amos obtenido:
$$\left(\begin{array}{rr} 1 & 0 \cr 0 & \frac{1}{3} \end{array}\right)
    \left(\begin{array}{rr} 1 & 0 \cr 2 & 1 \end{array}\right)
    \left(\begin{array}{rr} 0 & 1 \cr 1 & 0 \end{array}\right)A=I\sb{2},$$ y
de aquı́ tendrı́amos otra posible descomposiciń de $A$:
$$A=\left(\begin{array}{rr} 0 & 1 \cr 1 & 0 \end{array}\right)
   \left(\begin{array}{rr} 1 & 0 \cr -2 & 1 \end{array}\right)
   \left(\begin{array}{rr} 1 & 0 \cr 0 & 3 \end{array}\right).$$
{{% /example %}}

{{% definition label="def:equiv-mat" %}}
Equivalencia de matrices Cuando una matriz $B$ se obtiene de una matriz
$A$ mediante operaciones elementales (que pueden ser de filas, de
columnas, o de ambas), escribiremos $A \sim B$ y diremos que $A$ y $B$
son **matrices equivalentes**. Otra forma de expresarlo es que
$$A \sim B \quad \Leftrightarrow \quad \exists P,Q \text{ invertibles, tales que } PAQ=B.$$
{{% /definition %}}

{{% example name="Ejemplo" %}}
Las matrices $$A = \left(
\begin{array}{cccc}
 -1 & 4 & 0 & 4 \cr
 -3 & 2 & -4 & 2 \cr
 -2 & -2 & -2 & -4 \cr
\end{array}
\right)\quad \text{ y }\quad  B = \left(
\begin{array}{cccc}
 86 & -15 & -16 & -33 \cr
 -6 & 51 & -8 & -43 \cr
 28 & 33 & -12 & -43 \cr
\end{array}
\right)$$ son equivalentes porque $P A Q = B$ para las matrices
invertibles $$P = \left(
\begin{array}{ccc}
 2 & 3 & 2 \cr
 2 & -3 & -2 \cr
 2 & -1 & -1 \cr
\end{array}
\right),\quad  Q = \left(
\begin{array}{cccc}
 0 & 3 & 2 & 3 \cr
 2 & 3 & 1 & -3 \cr
 -3 & 0 & -1 & -3 \cr
 3 & 0 & -2 & -1 \cr
\end{array}
\right)$$
{{% /example %}}

{{% definition %}}
Equivalencia por filas y columnas

-   Decimos que dos matrices $A, B$ de la misma dimensión $m \times n$
    son **equivalentes por filas** si $B$ se obtiene de $A$ mediante
    operaciones elementales de filas. Es decir, existe una matriz $P$ de
    orden $m$ invertible tal que $PA = B$. Lo notamos como
    $A \stackrel{\mbox{f}} \sim B$.

-   Decimos que dos matrices $A, B$ de la misma dimensión $m \times n$
    son **equivalentes por columnas** si $B$ se obtiene de $A$ mediante
    operaciones elementales de columnas. Es decir, existe una matriz $Q$
    de orden $n$ invertible tal que $AQ = B$. Lo notamos como
    $A \stackrel{\mbox{c}} \sim B$.
{{% /definition %}}

Estas relaciones son de equivalencia.

{{% example name="Ejemplo" %}}
Toda matriz $A$ es equivalente por filas a su forma escalonada reducida
por filas $E\sb{A}$.

La matriz $$\begin{gathered}
  B = \left( \begin{array}{cccc}  1 & 0 & 0 & 0 \cr  0 & 1 & 0 & 0 \cr  0 & 0 & 0 & 0 \cr \end{array} \right) \text{ es equivalente por columnas a la matriz } \cr BQ=B \cdot \left( \begin{array}{cccc}  0 & 3 & 2 & 3 \cr  2 & 3 & 1 & -3 \cr  -3 & 0 & -1 & -3 \cr  3 & 0 & -2 & -1 \cr \end{array} \right) = \left(
\begin{array}{cccc}  0 & 3 & 2 & 3 \cr   2 & 3 & 1 & -3 \cr  0 & 0 & 0 & 0 \cr \end{array} \right),
\end{gathered}$$ porque la matriz $Q$ es invertible.
{{% /example %}}

Veamos ahora un resultado que nos debe sonar del tema anterior, y que
ahora podemos demostrar con la ayuda de la multiplicación de matrices.

{{% theorem name="Relaciones entre filas y columnas" %}}
-   Si $A \stackrel{\mbox{f}} \sim B$, entonces las relaciones que
    existen entre las columnas de $A$ también se tienen entre las
    columnas de $B$. Esto es, dados escalares
    $\alpha\sb{1},\ldots,\alpha\sb{n}\in K$,
    $$A\sb{\ast k} = \sum\sb{j=1}^n \alpha\sb{j} A\sb{\ast j} \quad \Leftrightarrow \quad  B\sb{\ast k} = \sum\sb{j=1}^n \alpha\sb{j} B\sb{\ast j}.$$

-   Si $A \stackrel{\mbox{c}} \sim B$, entonces las relaciones que
    existen entre las filas de $A$ también se tienen entre las filas de
    $B$.
{{% /theorem %}}

En particular, las relaciones entre columnas en $A$ y $E\sb{A}$ deben ser
las mismas, por lo que las columnas no básicas de $A$ son combinación
lineal de las básicas.

{{% proof %}}
Si $A \stackrel{\mbox{f}} \sim B$, entonces $PA = B$ para una matriz $P$
invertible. Tal como vimos en el producto de matrices,
$$B\sb{\ast j} = (PA)\sb{\ast j} = P A\sb{\ast j}.$$ Por tanto, si
$A\sb{\ast k} = \sum\sb{j=1}^n \alpha\sb{j} A\sb{\ast j}$, la multiplicación a la
izquierda por $P$ produce $B\sb{\ast k} = \sum\sb{j=1}^n \alpha\sb{j} B\sb{\ast j}$. El
recı́proco se obtiene con $P^{-1}$.

El resultado para las columnas se deduce inmediatamente a partir de lo
anterior aplicado a $A^t$ y $B^t$.
{{% /proof %}}

Veamos ahora que el rango de una matriz no cambia bajo transformaciones
elementales de filas o de columnas.

{{% theorem name="Ivariancia del rango para matrices equivalentes por filas" %}}
 Sean
$A\sb{m\times n}$ y $B\sb{m\times n}$. Se tiene:
$$A\stackrel{\mbox{f}}{\sim} B \quad \Rightarrow \quad \rg(A)=\rg(B).$$
Es decir, dadas $P\sb{m\times m}$ y $A\sb{m\times n}$, se tiene:
$$P \textrm{ invertible} \quad \Rightarrow \quad \rg(A)=\rg(PA).$$
{{% /theorem %}}

{{% proof %}}
Las dos afirmaciones son claramente equivalentes, ası́ que probaremos la
primera. Supongamos que $A\stackrel{\mbox{f}}{\sim} B$. Por la unicidad
de la forma escalonada reducida por filas, la aplicación de Gauss-Jordan
a $A$ y $B$ nos lleva a la misma matriz, es decir $E\sb{A}=E\sb{B}$. Como el
rango de una matriz es el número de pivotes de su escalonada reducida
por filas, se sigue que $\rg(A) = \rg(B)$.
{{% /proof %}}

{{% theorem name="Ivariancia del rango para matrices equivalentes por columnas" %}}
 Sean
$A\sb{m\times n}$ y $B\sb{m\times n}$. Se tiene:
$$A\stackrel{\mbox{c}}{\sim} B \quad \Rightarrow \quad \rg(A)=\rg(B).$$
Es decir, dadas $A\sb{m\times n}$ y $Q\sb{n\times n}$, se tiene:
$$Q \textrm{ invertible} \quad \Rightarrow \quad \rg(A)=\rg(AQ).$$
{{% /theorem %}}

{{% proof %}}
Como antes, las dos afirmaciones son claramente equivalentes. En este
caso probaremos la segunda. Supongamos que $Q$ es invertible, y veamos
que $\rg(A)=\rg(AQ)$.

Sabemos que existe una matriz invertible $P$ (que se puede obtener por
el método de Gauss-Jordan), tal que $PA=E\sb{A}$, la reducida por filas de
$A$. Por el resultado anterior, como $P$ es invertible, tenemos:
$$\rg(AQ)=\rg(PAQ)=\rg(E\sb{A}Q).$$ Sea $r = \rg(A)$. Entonces $E\sb{A}$ tiene
$r$ filas no nulas y $m-r$ filas nulas al final. Esto implica que la
matriz $E\sb{A}Q$ tiene $m-r$ filas nulas al final, de donde
$\rg(AQ) = \rg(E\sb{A} Q) \le r = \rg(A)$.

Si ahora llamamos $B$ a la matriz $AQ$, y observamos que $Q^{-1}$ es
invertible, el argumento anterior nos dice que $\rg(BQ^{-1})\le \rg(B)$.
Pero esto quiere decir que $\rg(A)\le \rg(AQ)$. Por tanto tenemos las
dos desigualdades, luego $\rg(A)=\rg(AQ)$.
{{% /proof %}}

{{% theorem name="Invariancia del rango para matrices equivalentes" %}}
 Sean $A\sb{m\times n}$ y
$B\sb{m\times n}$. Se tiene:
$$A \sim B \quad \Rightarrow \quad \rg(A)=\rg(B).$$ Es decir, dadas
$P\sb{m\times m}$, $A\sb{m\times n}$ y $Q\sb{n\times n}$, se tiene:
$$P, Q \textrm{ invertibles} \quad \Rightarrow \quad \rg(A)=\rg(PAQ).$$
{{% /theorem %}}

{{% proof %}}
Es inmediato a partir de los dos resultados anteriores, ya que
$\rg(PAQ)=\rg(AQ)=\rg(A)$, al ser $P$ y $Q$ matrices invertibles.
{{% /proof %}}

## Forma normal del rango

La forma escalonada reducida por filas $E\sb{A}$ es lo más lejos que podemos
llegar mediante transformaciones por filas. Sin embargo, si permitimos
además el uso de transformaciones por columnas, la reducción es mucho
mayor.

{{% theorem name="Forma normal de rango" label="thm:forma-normal-rango" %}}
 Si $A$ es una matriz de orden $m \times n$ y
$\rg(A) = r$, entonces
$$A \sim N\sb{r} = \left( \begin{array}{cc} I\sb{r} &  O  \cr  O  &  O  \end{array} \right).$$
$N\sb{r}$ se denomina **forma normal de rango** de $A$.
{{% /theorem %}}

{{% proof %}}
Como
$A \stackrel{\mbox{f}} \sim E\sb{A}$, existe una matriz invertible $P$ tal
que $PA = E\sb{A}$. Si $\rg(A) = r$, entonces las columnas básicas de $E\sb{A}$
son las $r$ columnas unitarias. Mediante intercambio de columnas
aplicados a $E\sb{A}$, podemos poner estas $r$ columnas en la parte superior
izquierda. Si $Q\sb{1}$ es el producto de las matrices elementales que hacen
estos intercambios, entonces
$$PAQ\sb{1} = E\sb{A} Q\sb{1} = \left( \begin{array}{cc} I\sb{r} & J \cr  O  &  O  \end{array} \right).$$
Ahora multiplicamos a la derecha ambos lados de esta expresión por la
matriz invertible
$$Q\sb{2} = \left( \begin{array}{cc} I\sb{r} & -J \cr  O  & I \end{array} \right),$$
y nos queda
$$PAQ\sb{1} Q\sb{2} = \left( \begin{array}{cc} I\sb{r} &  O  \cr  O  &  O  \end{array} \right).$$
Entonces $A \sim N\sb{r}$.
{{% /proof %}}

{{% example name="Ejemplo" %}}
Dadas $A\sb{m\times n}$ y $B\sb{p\times q}$, veamos que
$$\rg \left( \begin{array}{cc} A &  O  \cr  O  & B \end{array} \right) = \rg(A) + \rg(B).$$
Si $\rg(A) = r$ y $\rg(B) = s$, entonces $A \sim N\sb{r}$ y $B \sim N\sb{s}$.
Observemos que las transformaciones elementales que efectuemos sobre las
primeras $m$ filas y $n$ columnas de la matriz inicial, sólo afectarán a
$A$, y las que efectuemos sobre las últimas $p$ filas y $q$ columnas,
sólo afectarán a $B$. Por tanto, aplicando dichas transformaciones a las
filas y columnas adecuadas, tendremos
$$\left( \begin{array}{cc} A &  O  \cr  O  & B \end{array} \right) \sim \left( \begin{array}{cc} N\sb{r} &  O  \cr  O  & N\sb{s} \end{array} \right).$$
Intercambiando ahora filas y columnas para llevar todos los pivotes a
las primeras filas y columnas, obtenemos finalmente
$$\left( \begin{array}{cc} A &  O  \cr  O  & B \end{array} \right) \sim \left( \begin{array}{cc} N\sb{r} &  O  \cr  O  & N\sb{s} \end{array} \right)\sim \left(\begin{array}{cc} N\sb{r+s} &  O  \cr  O  &  O \end{array}\right),$$
de donde
$$\rg \left( \begin{array}{cc} A &  O  \cr  O  & B \end{array} \right) = r+s.$$
{{% /example %}}

Dadas matrices $A$ y $B$, ¿cómo decidimos si
$A \sim B, A \stackrel{\mbox{f}} \sim B$ o
$A \stackrel{\mbox{c}} \sim B$?

{{% theorem name="Test de equivalencia" %}}
Sean $A$ y $B$ matrices de orden $m \times n$. Entonces

-   $A \sim B \quad \Leftrightarrow \quad \rg(A) = \rg(B)$.

-   $A \stackrel{\mbox{f}} \sim B \quad \Leftrightarrow \quad  E\sb{A} = E\sb{B}$.

-   $A \stackrel{\mbox{c}} \sim B \quad \Leftrightarrow \quad  E\sb{A^t} = E\sb{B^t}$.
{{% /theorem %}}

{{% proof %}}
Si $\rg(A) = \rg(B)$, entonces $A \sim N\sb{r}$ y $B \sim N\sb{r}$, de donde
$A \sim N\sb{r} \sim B$. Recı́procamente, si $A \sim B$, entonces $B = PAQ$
con $P$ y $Q$ invertibles, de donde $\rg(B) = \rg(A)$.

Supongamos ahora que $A \stackrel{\mbox{f}} \sim B$. Como
$B \stackrel{\mbox{f}} \sim E\sb{B}$, entonces
$A \stackrel{\mbox{f}} \sim E\sb{B}$, y dado que la forma escalonada
reducida por filas es única, se sigue que $E\sb{B} = E\sb{A}$. Recı́procamente,
si $E\sb{A} = E\sb{B}$, entonces
$$A \stackrel{\mbox{f}} \sim E\sb{A} = E\sb{B} \stackrel{\mbox{f}} \sim B.$$

Para las columnas, basta considerar que $A \stackrel{\text{c}} \sim B$
si y solamente si $A^t \stackrel{\text{f}} \sim B^t$.
{{% /proof %}}

{{% theorem %}}

Rango y trasposición
$$\rg(A) = \rg(A^t) \quad \mbox{ y } \quad \rg(A) = \rg(A^{\ast}).$$
{{% /theorem %}}

{{% proof %}}
Sea $\rg(A) = r$, y sean $P$ y $Q$ matrices invertibles tales que
$$PAQ = N\sb{r} = \left( \begin{array}{cc} I\sb{r} &  O \sb{r \times (n-r)} \cr  O \sb{(m-r) \times r} &  O \sb{(m-r) \times (n-r)} \end{array} \right).$$
Entonces $N\sb{r}^t = Q^t A^t P^t$. Como $Q^t$ y $P^t$ son invertibles, se
sigue que $A^t \sim N\sb{r}^t$, y entonces
$$\rg(A^t) = \rg(N\sb{r}^t) = \rg \left( \begin{array}{cc} I\sb{r} &  O \sb{r \times (m-r)} \cr  O \sb{(n-r) \times r} &  O \sb{(n-r) \times (m-r)}
  \end{array} \right) = r = \rg(A).$$ De forma análoga,
$N\sb{r}^{\ast} = Q^{\ast} A^{\ast} P^{\ast}$, donde $Q^{\ast}, P^{\ast}$ son matrices invertibles. Como
$$N\sb{r}^{\ast} = \left( \begin{array}{cc} I\sb{r} &  O \sb{r \times (m-r)} \cr  O \sb{(n-r) \times r} &  O \sb{(n-r) \times (m-r)}
  \end{array} \right)=N\sb{r}^t,$$ se tiene que $\rg(N\sb{r}^{\ast}) = r$, y como
$\rg(A^{\ast}) = \rg(N\sb{r}^{\ast})$ por equivalencia de matrices, tenemos que
$\rg(A^{\ast}) = r = \rg(A)$.
{{% /proof %}}

## Producto directo de matrices

{{% definition %}}
Producto directo o de Kronecker Sean $A\sb{m \times n}, B\sb{p \times q}$
matrices. El **producto directo o de Kronecker** de $A$ y $B$ es la
matriz $C\sb{mp \times nq}$ definida como
$$C = A \otimes B = a\sb{ij} B, i=1,2,\ldots,m, j=1, 2, \ldots, n.$$
{{% /definition %}}

{{% example name="Ejemplo" %}}
El producto de Kronecker de las matrices
$$A = \left( \begin{array}{rr} 1 & 2 \cr 3 & 4 \end{array} \right), B = \left( \begin{array}{rr} 2 &1 \cr -1 & 0 \cr 3 & 2 \end{array} \right)$$
es
$$A \otimes B = \left( \begin{array}{rr|rr} 2 & 1 & 4 & 2 \cr -1 & 0 & -2 & 0 \cr 3 & 2 & 6 & 4 \cr \hline 6 & 3 & 8 & 4 \cr -3 & 0 & -4 & 0 \cr 9 & 6 & 12 & 8  \end{array} \right) \text{ y } B \otimes A = \left( \begin{array}{rr|rr} 2 & 4 & 1 & 2 \cr 6 & 8 & 3 & 4 \cr \hline -1 & -2 & 0 & 0 \cr -3 & -4 & 0 & 0 \cr \hline -3 & 6 & 2 & 4 \cr 9 & 12 & 6 & 8 \end{array} \right).$$
{{% /example %}}

{{% theorem name="Propiedades del producto de Kronecker" %}}
1.  Sean $A$ y $B$ matrices del mismo tamaño. Entonces
    $(A + B) \otimes C = A \otimes C + B \otimes C$ y
    $C \otimes (A+B) = C \otimes A + C \otimes B$.

2.  Si $A, C$ y $B,D$ son parejas de matrices ajustadas para el
    producto, entonces $(A \otimes B)(C \otimes D) = AC \otimes BD$.

3.  Si $A$ y $B$ son matrices invertibles, entonces
    $(A \otimes B)^{-1} = A^{-1} \otimes B^{-1}$.

4.  $(A \otimes B)^t = A^t \otimes B^t$.
{{% /theorem %}}
