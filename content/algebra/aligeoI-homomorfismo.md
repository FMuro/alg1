+++
title = "Homomorfismos"
weight = 70
+++



Hasta ahora la única relación que hemos visto entre espacios vectoriales
diferentes es la de inclusión de un subespacio vectorial en otro. En
este tema estudiaremos una nueva relación: los homomorfismos, que como
su propio nombre da a entender son las aplicaciones entre espacios
vectoriales que respetan las operaciones entre un espacio y otro. Desde
el punto de vista de estructuras, es el paso natural tras haber definido
una clase de objetos: qué relaciones hay entre ellos.

## Definición y propiedades

Los homomorfismos de espacios vectoriales son aplicaciones entre
espacios vectoriales (sobre el mismo cuerpo) que respetan las dos
operaciones básicas: la suma de vectores y el producto de vectores por
escalares.

{{% definition %}}
Homomorfismos

Sean $V$ y $V'$ espacios vectoriales sobre un cuerpo $\K$ y
$f\colon V \to V'$ una aplicación. Diremos que $f$ es un
**homomorfismo** o **aplicación lineal** si para todos
$\vec{u}, \vec{v} \in V$ y $\alpha \in \K$ se cumplen las condiciones
siguientes:

-   $f(\vec{u}+\vec{v})=f(\vec{u})+f(\vec{v})$.

-   $f(\alpha \vec{v})=\alpha f(\vec{v})$.
{{% /definition %}}

{{% example name="Ejemplo" %}}
Veamos algunos ejemplos de homomorfismos y de aplicaciones que no lo
son.

1.  Si $V$ es un $\K$-espacio vectorial, la **aplicación identidad**,
    $\id\sb{V}\colon V\rightarrow V$, definida por $\id\sb{V}(\vec{v})=\vec{v}$
    para todo $\vec{v}\in V$, es un homomorfismo.

2.  Dados dos espacios vectoriales cualesquiera $V$ y $V'$, la
    aplicación $${\mathcal O}\sb{V,V'}\colon V\rightarrow V',$$ definida
    por ${\mathcal O}\sb{V,V'}(\vec{v})=\vec{0}$ para todo $\vec{v}\in V$, es
    un homomorfismo denominado **homomorfismo trivial**.

3.  Si $V$ es un $\K$-espacio vectorial de dimensión finita $\dim V=n$
    con base $\mathcal{B}\subset V$, la aplicación de **asignación de
    coordenadas** $c\sb{\mathcal{B}}\colon V\rightarrow \K^n$, definida
    por $c\sb{\mathcal{B}}(\vec{v})=\vec{v}\sb{\mathcal{B}}$, es un
    homomorfismo.

4.  Si $W\subset V$ es un subespacio vectorial, la **inclusión**
    $i\colon W\rightarrow V$, $i(\vec{w})=\vec{w}$, es un homomorfismo.

5.  Si $W\subset V$ es un subespacio vectorial, la **proyección
    natural** sobre el cociente $p\colon V\rightarrow V/W$,
    $p(\vec{v})=\vec{v} + W$, también es un homomorfismo.

6.  Las propiedades elementales de la suma y el producto de matrices
    demuestran que toda matriz $A\sb{m \times n}$ da lugar a un
    homomorfismo $$\begin{aligned}
     \K^n&\longrightarrow \K^m,\cr
    \vec{v}&\mapsto  A\vec{v}.
    \end{aligned}$$

7.  La aplicación $f\colon \K \rightarrow \K$ definida por $f(x)=x+1$ no
    es un homomorfismo de $\K$-espacios vectoriales ya que
    $$f(1+2) = (1+2)+1 = 4 \neq 5 = 2 + 3 = f(1) + f(2) = 2 + 3.$$

8.  Si $\K=\Q, \R$ o $\C$, la aplicación $f\colon \K \rightarrow \K$,
    $f(x)=x^2$, tampoco es un homomorfismo de $\K$ espacios vectoriales,
    puesto que $$f(2 + 3) = 25 \neq 4 + 9 = f(2) + f(3).$$
{{% /example %}}

Algunas propiedades básicas de los homomorfismos son las siguientes.

{{% theorem name="Propiedades de los homomorfismos" %}}
Sea $f\colon V\rightarrow V'$ un homomorfismo de $\K$-espacios
vectoriales.

1.  $f(\vec{0}\sb{V})=\vec{0}\sb{V'}$.

2.  $f(-\vec{v})=-f(\vec{v})$ para todo $\vec{v}\in V$.

3.  Dados vectores $\vec{v}\sb{1},\ldots,\vec{v}\sb{n}\in V$ y escalares
    $\alpha _1,\ldots,\alpha _n\in \K$ cualesquiera, se verifica que
    $$f(\alpha _1\vec{v}\sb{1}+\cdots+\alpha _n \vec{v}\sb{n})=\alpha _1f(\vec{v}\sb{1})+\cdots+\alpha\sb{n} f(\vec{v}\sb{n}).$$
{{% /theorem %}}

{{% proof %}}
1.  Como $\vec{0}\sb{V} + \vec{0}\sb{V} =\vec{0}\sb{V}$,
    $$f(\vec{0}\sb{V})=f(\vec{0}\sb{V}+\vec{0}\sb{V})=f(\vec{0}\sb{V})+f(\vec{0}\sb{V}).$$
    Despejando en esta ecuación obtenemos que $f(\vec{0}\sb{V})=\vec{0}\sb{V'}$.

2.  Sabemos que el vector opuesto $-\vec{v}$ se puede obtener a partir de
    $\vec{v}$ multiplicando por el escalar $-1\in \K$, es decir
    $-\vec{v}=(-1)\vec{v}$, por tanto
    $$f(-\vec{v})=f((-1)\vec{v})=(-1)f(\vec{v}) = -f(\vec{v}).$$

3.  Por inducción sobre $n$. Para $n=1$ se sigue de la propia definición
    de homomorfismo que $f(\alpha\sb{1}\vec{v}\sb{1})=\alpha\sb{1} f(\vec{v}\sb{1})$. Si
    suponemos que la ecuación es cierta para $n-1$, aplicando la
    hipótesis de inducción junto con la definición de homomorfismo
    obtenemos lo siguiente: $$\begin{aligned}
     f(\alpha _1\vec{v}\sb{1}+\cdots+\alpha _n \vec{v}\sb{n}) & = f(\alpha _1\vec{v}\sb{1}+\cdots+\alpha _{n-1} \vec{v}\sb{n-1} +\alpha\sb{n} \vec{v}\sb{n})\cr
    & = f(\alpha _1\vec{v}\sb{1}+\cdots+\alpha _{n-1} \vec{v}\sb{n-1}) +f(\alpha\sb{n} \vec{v}\sb{n})\cr
    & = \alpha\sb{1} f(\vec{v}\sb{1})+\cdots+\alpha _{n-1} f(\vec{v}\sb{n-1}) +\alpha\sb{n} f(\vec{v}\sb{n}).
    \end{aligned}$$
{{% /proof %}}

{{% remark %}}
Los homomorfismos preservan sistemas linealmente dependientes. Sea
$f\colon V\rightarrow V'$ un homomorfismo entre $\K$-espacios
vectoriales. Si
$$S = \{\vec{v}\sb{1}, \ldots ,\vec{v}\sb{n}\}\subset V \text{ es un sistema linealmente dependiente,}$$
entonces
$$f(S)=\{f(\vec{v}\sb{1}), \ldots ,f(\vec{v}\sb{n})\}\subset V' \text{ es también un sistema linealmente dependiente.}$$

Por ser $S$ linealmente dependiente existen escalares
$\alpha\sb{1},\ldots, \alpha\sb{n}$, no todos nulos, tales que
$$\alpha\sb{1}\vec{v}\sb{1}+\cdots + \alpha\sb{n}\vec{v}\sb{n} = \vec{0}\sb{V}.$$ Si aplicamos
$f$ a ambos términos de esta ecuación se obtiene
$$\alpha _1 f(\vec{v}\sb{1})+\cdots+\alpha _n f(\vec{v}\sb{n})=f(\alpha _1\vec{v}\sb{1}+\cdots+\alpha _n \vec{v}\sb{n}) =  f(\vec{0})= \vec{0}\sb{V'},$$
luego $f(S)$ es linealmente dependiente.
{{% /remark %}}

{{% remark %}}
No todos los homomorfismos preservan los sistemas linealmente
independientes. Sea $f \colon \R^3 \to \R^2$ dada por
$f(\vec{v}) = A \vec{v}$, con
$$A = \left( \begin{array}{rrr} 1 & 0 & 0 \cr 0 & 1 & 0 \end{array} \right).$$
La base estándar $\{ \vec{e}\sb{1}, \vec{e}\sb{2}, \vec{e}\sb{3} \}$ es un sistema
linealmente independiente, y
$$f(\vec{e}\sb{1}) = \left( \begin{array}{r} 1 \cr 0 \end{array} \right), f(\vec{e}\sb{2}) = \left( \begin{array}{r} 0 \cr 1 \end{array} \right), f(\vec{e}\sb{3}) = \left( \begin{array}{r} 0 \cr 0 \end{array} \right).$$
Es claro que $\{ f(\vec{e}\sb{1}), f(\vec{e}\sb{2}), f(\vec{e}\sb{3}) \}$ es un sistema
linealmente dependiente, pues contiene al vector nulo.
{{% /remark %}}

Para definir un homomorfismo, basta con dar la imagen de los vectores de
una base del espacio vectorial de partida.

{{% theorem name="Existencia y unicidad de homomorfismo" %}}
 Sean $V$ y $V'$ dos $\K$-espacios
vectoriales. Supongamos que $V$ es de dimensión finita $n$ con base
${\mathcal B} = \{ \vec{v}\sb{1}, \ldots, \vec{v}\sb{n} \} \subset V$. Sea
$S = \{ \vec{v}'_1, \ldots, \vec{v}'_n \} \subset V'$ un sistema de $n$
vectores cualesquiera de $V'$. Entonces existe un único homomorfismo
$f\colon V\rightarrow V'$ tal que $f(\vec{v}\sb{i})=\vec{v}'_i$ para todo
$1\leq i\leq n$.
{{% /theorem %}}

[]{#thm:ex-unic label="thm:ex-unic"}

{{% proof %}}
La aplicación $f$ se define de la siguiente manera. Dado un vector
$\vec{u} \in V$, existen unos únicos escalares $a\sb{1}, \ldots, a\sb{n} \in \K$
(sus coordenadas respecto a ${\mathcal{B}}$) tales que
$\vec{u} = \sum\sb{i=1}^n a\sb{i} \vec{v}\sb{i}$. Entonces definimos $f(\vec{u})$
mediante la fórmula
$$\vec{u} \mapsto f(\vec{u}) = {a}\sb{1} \vec{v}'_1 + \cdots + {a}\sb{n} \vec{v}'_n.$$
Veamos que $f$ es un homomorfismo. Si $\vec{v} \in V$ se expresa en
función de la base ${\mathcal B}$ como
$\vec{v} = b\sb{1} \vec{v}\sb{1} + \cdots + b\sb{n} \vec{v}\sb{n}$ entonces
$$\vec{u} + \vec{v} = (a\sb{1} + b\sb{1}) \vec{v}\sb{1} + \cdots + (a\sb{n} + b\sb{n}) \vec{v}\sb{n},$$
y la definición de $f$ nos lleva a $$\begin{aligned}
f(\vec{u} + \vec{v})&=({a}\sb{1}+{b}\sb{1})\vec{v}'_1+\cdots+({a}\sb{n}+{b}\sb{n})\vec{v}'_n\cr
&=({a}\sb{1}\vec{v}'_1+{b}\sb{1}\vec{v}'_1)+\cdots+({a}\sb{n}\vec{v}'_n+{b}\sb{n}\vec{v}'_n)\cr
&=({a}\sb{1}\vec{v}'_1+\cdots+{a}\sb{n}\vec{v}'_n)+({b}\sb{1}\vec{v}'_1+\cdots+{b}\sb{n}\vec{v}'_n)\cr &= f(\vec{u})+f(\vec{v}).
\end{aligned}$$

Si $\alpha \in \K$, entonces
$\alpha \vec{u} = \alpha a\sb{1} \vec{v}\sb{1} + \cdots + \alpha a\sb{n} \vec{v}\sb{n}$,
luego $$\begin{aligned}
f(\alpha \vec{u})&=(\alpha {a}\sb{1})\vec{v}'_1+\cdots+(\alpha {a}\sb{n})\vec{v}'_n\cr
&=\alpha ({a}\sb{1}\vec{v}'_1)+\cdots+\alpha( {a}\sb{n}\vec{v}'_n)\cr
&=\alpha({a}\sb{1}\vec{v}'_1+\cdots+{a}\sb{n}\vec{v}'_n)\cr &= \alpha f(\vec{u}).
\end{aligned}$$ Con esto concluye la demostración de que $f$ es un
homomorfismo. Para ver la unicidad de $f$ razonamos del siguiente modo.
Sea $g\colon V\rightarrow V'$ un homomorfismo que también satisface
$g(\vec{v}\sb{i})=\vec{v}'_i$ para todo $1\leq i\leq n$. Como
$\vec{u}= {a}\sb{1}\vec{v}\sb{1}+\cdots+{a}\sb{n}\vec{v}\sb{n}$, tenemos que
$$g(\vec{u}) =   {a}\sb{1} g(\vec{v}\sb{1})+\cdots+{a}\sb{n} g(\vec{v}\sb{n}) = {a}\sb{1} \vec{v}'_1+\cdots+{a}\sb{n}\vec{v}'_n = f(\vec{u}),$$
luego la aplicación $g$ es igual a $f$.
{{% /proof %}}

## Matriz de un homomorfismo

{{% definition %}}
Matriz de un homomorfismo Sea $f\colon V\rightarrow V'$ un homomorfismo
entre $\K$-espacios vectoriales de dimensión finita con bases
$$\mathcal{B}=\{\vec{v}\sb{1},\ldots,\vec{v}\sb{n}\}\subset V, \quad \mathcal{B}'=\{\vec{v}'_1,\ldots,\vec{v}'_m\}\subset V'.$$
La **matriz** de $f$ **respecto de** ${\mathcal B}$ y ${\mathcal B}'$ es
la matriz de orden $m \times n$ dada por
$$M\sb{\mathcal{B}, \mathcal{ B} '}(f)= \left( \begin{array}{c|c|c} f(\vec{v}\sb{1})\sb{\mathcal{B} '} & \cdots & f(\vec{v}\sb{n})\sb{\mathcal{B} '} \end{array} \right),$$
cuyas columnas son las coordenadas de los vectores de $f({\mathcal B})$
respecto de ${\mathcal B}'$.
{{% /definition %}}

Observemos que si $f, g:V \to V'$ son homomorfismos de espacios
vectoriales tales que
$$M\sb{{\mathcal B},{\mathcal B}'}(f) = M\sb{{\mathcal B},{\mathcal B}'}(g),$$
entonces $f = g$. Esto se tiene porque si
${\mathcal B} = \{ \vec{v}\sb{1}, \ldots, \vec{v}\sb{n} \}$, entonces las
coordenadas de los vectores $f(\vec{v}\sb{i}), i=1,\ldots,n$, respecto de la
base ${\mathcal B}'$, coinciden con las coordenadas de los vectores
$g(\vec{v}\sb{i}), i=1,\ldots,n$. Por la unicidad establecida en el teorema
de existencia y unicidad de homomorfismos (pág. ), tenemos la igualdad.

{{% example name="Ejemplo" %}}
Los siguientes ejemplos de matrices asociadas a homomorfismos son ya
conocidos:

1.  Si $V$ es un espacio vectorial de dimensión finita y
    $\mathcal{B},\mathcal{B}'\subset V$ son dos bases de $V$ entonces la
    matriz del homomorfismo identidad $\mbox{id}\sb{V}\colon V\rightarrow V$
    respecto de $\mathcal{B}$ en la partida y de $\mathcal{B}'$ en la
    llegada es la matriz de cambio de base,
    $$M\sb{\mathcal{B}, \mathcal{ B} '}(\mbox{id}\sb{V})=M(\mathcal{B}, \mathcal{ B} ').$$

2.  Sea $A\sb{m \times n}$ una matriz con coeficientes en $\K$ y
    $f\colon \K^n\rightarrow \K^m$ el homomorfismo definido a partir de
    $A$ en los ejemplos de la sección anterior, $f(\vec{v})=A\vec{v}$.
    Entonces, si $\mathcal{S}\subset \K^n$ y $\mathcal{S}'\subset \K^m$
    son las respectivas bases estándar, tenemos que
    $M\sb{\mathcal{S}, \mathcal{S}'}(f)=A$.
{{% /example %}}

{{% theorem name="Interpretación matricial de la imagen de un vector" %}}
 Sea
$f\colon V\rightarrow V'$ un homomorfismo entre $\K$-espacios
vectoriales de dimensión finita, con bases
$$\mathcal{B}=\{\vec{v}\sb{1},\ldots,\vec{v}\sb{n}\}\subset V, \quad \mathcal{B}'=\{\vec{v}'_1,\ldots,\vec{v}'_m\}\subset V'.$$
Para todo $\vec{v}\in V$ se satisface la siguiente ecuación:
$$f(\vec{v})\sb{\mathcal{B}'} = M\sb{\mathcal{B}, \mathcal{ B} '}(f) \cdot \vec{v}\sb{\mathcal{B}}.$$
{{% /theorem %}}

{{% proof %}}
Si denotamos
$$\vec{v}\sb{\mathcal{B}} = \left( \begin{array}{c} a\sb{1} \cr \vdots \cr {a}\sb{n} \end{array} \right),$$
tenemos que $\vec{v}={a}\sb{1}\vec{v}\sb{1}+\cdots+ {a}\sb{n}\vec{v}\sb{n}.$ Aplicando $f$
obtenemos $$f(\vec{v})= {a}\sb{1}f(\vec{v}\sb{1})+\cdots+ {a}\sb{n}f(\vec{v}\sb{n}).$$ Si
ahora tomamos coordenadas respecto de ${\mathcal B}'$, como esta
operación es lineal, deducimos que $$\begin{aligned}
f(\vec{v})\sb{\mathcal{B}'} & = {a}\sb{1}f(\vec{v}\sb{1})\sb{\mathcal{B}'}+\cdots+ {a}\sb{n}f(\vec{v}\sb{n})\sb{\mathcal{B}'} \cr
 & = \left( \begin{array}{c|c|c} f(\vec{v}\sb{1})\sb{\mathcal{B} '} & \ldots & f(\vec{v}\sb{n})\sb{\mathcal{B} '} \end{array} \right)
\left( \begin{array}{c} a\sb{1}\cr \vdots\cr a\sb{n} \end{array} \right)\cr
&= M\sb{\mathcal{B}, \mathcal{ B} '}(f) \cdot \vec{v}\sb{\mathcal{B}}.
\end{aligned}$$
{{% /proof %}}

{{% example name="Ejemplo" %}}
Sea $f:\R^3 \to \R^2$ definida como
$$f\left( \begin{array}{c} x\sb{1} \cr x\sb{2} \cr x\sb{3} \end{array} \right) = \left( \begin{array}{c} x\sb{1} + x\sb{2} -x\sb{3} \cr 2x\sb{1} +x\sb{2} + 3x\sb{3} \end{array} \right).$$
Sea $\mathcal{S} = \{ \vec{e}\sb{1}, \vec{e}\sb{2}, \vec{e}\sb{3} \}$ la base estándar
en $\R^3$ y $\mathcal{S}' = \{ \vec{e}'_1, \vec{e}'_2 \}$ la base estándar
en $\R^2$. Entonces $$\begin{aligned}
    f(\vec{e}\sb{1}) & = \left( \begin{array}{r} 1 \cr 2 \end{array} \right), & [f(\vec{e}\sb{1})]\sb{\mathcal{S}'} & = \left( \begin{array}{r} 1 \cr 2 \end{array} \right), \cr
    f(\vec{e}\sb{2}) & = \left( \begin{array}{r} 1 \cr 1 \end{array} \right), & [f(\vec{e}\sb{2})]\sb{\mathcal{S}'} & = \left( \begin{array}{r} 1 \cr 1 \end{array} \right), \cr
    f(\vec{e}\sb{3}) & = \left( \begin{array}{r} -1 \cr 3 \end{array} \right), & [f(\vec{e}\sb{3})]\sb{\mathcal{S}'} & = \left( \begin{array}{r} -1 \cr 3 \end{array} \right),
\end{aligned}$$ de donde
$$M\sb{\mathcal{S}, \mathcal{S}'}(f) = \left( \begin{array}{rrr} 1 & 1 & -1 \cr 2 & 1 & 3 \end{array} \right),$$
lo que es lógico, pues es fácil ver que se tiene la igualdad
$$f\left( \begin{array}{c} x\sb{1} \cr x\sb{2} \cr x\sb{3} \end{array} \right) = \left( \begin{array}{rrr} 1 & 1 & -1 \cr 2 & 1 & 3 \end{array} \right) \left( \begin{array}{c} x\sb{1} \cr x\sb{2} \cr x\sb{3} \end{array} \right).$$
Consideremos ahora las bases respectivas de $\R^3$ y $\R^2$ dadas por
$$\mathcal{B} = \{ \vec{u}\sb{1} = \left( \begin{array}{r} 1 \cr 1 \cr 1 \end{array} \right), \vec{u}\sb{2} = \left( \begin{array}{r} 1 \cr -1 \cr 1 \end{array} \right), \vec{u}\sb{3} = \left( \begin{array}{r} -4 \cr 5 \cr 1 \end{array} \right) \}, \quad \mathcal{B}' = \{ \vec{u}'_1 = \left( \begin{array}{r} 1 \cr 2 \end{array} \right), \vec{u}'_2 = \left( \begin{array}{r} 1 \cr 1 \end{array} \right) \}.$$
Entonces $$\begin{aligned}
    f(\vec{u}\sb{1}) & = \left( \begin{array}{r} 1 \cr 6 \end{array} \right), \cr
    f(\vec{u}\sb{2}) & = \left( \begin{array}{r} -1 \cr 4 \end{array} \right), \cr
    f(\vec{u}\sb{3}) & = \left( \begin{array}{r} 0 \cr 0 \end{array} \right).
\end{aligned}$$ Para calcular la matriz
$M\sb{\mathcal{B},\mathcal{B}'}(f)$ necesitamos obtener las coordenadas de
los vectores $f(\vec{u}\sb{i}), 1 \le i \le 3$ respecto de la base
$\mathcal{B}'$. Para ello, debemos resolver tres sistemas de ecuaciones
con la misma matriz de coeficientes y diferentes términos
independientes. En este caso, se tiene que
$$\left( \begin{array}{rr|rrr} 1 & 1 & 1 & -1 & 0 \cr 2 & 1 & 6 & 4 & 0 \end{array} \right) \mapright{\GJ} \left( \begin{array}{rr|rrr} 1 & 0 & 5 & 5 & 0 \cr 0 & 1 & -4 & -6 & 0 \end{array} \right).$$
Por tanto, $$\begin{aligned}
    f(\vec{u}\sb{1}) & = 5 \vec{u}'_1 - 4 \vec{u}'_2, \cr
    f(\vec{u}\sb{2}) & = 5 \vec{u}'_1 - 6 \vec{u}'_2, \cr
    f(\vec{u}\sb{3}) & = 0 \vec{u}'_1 - 0 \vec{u}'_2,
\end{aligned}$$ y
$$M\sb{\mathcal{B}, \mathcal{B}'}(f) = \left( \begin{array}{rrrr} 5 & 5 & 0 \cr -4 & -6 & 0 \end{array} \right).$$
{{% /example %}}

La relación entre homomorfismos y matrices es incluso más estrecha, como
vamos a ver a partir de la composición de homomorfismos. Dados dos
homomorfismos de $\K$-espacios vectoriales
$$V \stackrel{f} \longrightarrow V' \stackrel{g}\longrightarrow V'',$$
su composición $g\circ f\colon V \rightarrow V''$ es un homomorfismo. Si
$\vec{u}, \vec{v}\in V$ se tiene que $$\begin{aligned}
(g\circ f)(\vec{u}+\vec{v}) & = g(f(\vec{u} + \vec{v}))\cr
& = g(f(\vec{u})+g(\vec{v}))\cr
& = g(f(\vec{u}))+ g(f(\vec{v}))\cr
& = (g\circ f) (\vec{u} )+ (g\circ f) (\vec{v}).
\end{aligned}$$ Por otra parte, si $\alpha\in \K$, $$\begin{aligned}
(g\circ f)(\alpha \vec{v}) & =
g(f(\alpha \vec{v}))\cr
& = g(\alpha f(\vec{v}))\cr
& = \alpha g(f(\vec{v}))\cr
& =
\alpha \: (g\circ f)(\vec{v}),
\end{aligned}$$ luego $g\circ f$ es un homomorfismo.

La fórmula que define la multiplicación de matrices tiene su origen
histórico (consulte este
[[enlace]{.underline}](http://math.stackexchange.com/questions/271927/why-historically-do-we-multiply-matrices-as-we-do))
en el siguiente resultado:

{{% theorem name="Composición y multiplicación de matrices" %}}
 Dados dos homomorfismos de
$\K$-espacios vectoriales de dimensión finita
$$V\stackrel{f}\longrightarrow V' \stackrel{g}\longrightarrow V'',$$ con
bases $\mathcal{B}\subset V$, $\mathcal{B}'\subset V'$ y
$\mathcal{B}''\subset V''$, se satisface la siguiente ecuación:
$$M\sb{\mathcal{B}, \mathcal{ B} ''}(g\circ f)= M\sb{\mathcal{B}', \mathcal{ B} ''}(g) \cdot M\sb{\mathcal{B}, \mathcal{ B} '}(f).$$
{{% /theorem %}}

{{% proof %}}
Sea $\mathcal{B}=\{\vec{u}\sb{1},\ldots,\vec{u}\sb{n}\}$ la base de $V$. Entonces,
aplicando la proposición anterior, $$\begin{aligned}
 M\sb{\mathcal{B}', \mathcal{ B} ''}(g) \cdot M\sb{\mathcal{B}, \mathcal{ B} '}(f) & = M\sb{\mathcal{B}', \mathcal{ B} ''}(g) \cdot \left( \begin{array}{c|c|c} f(\vec{u}\sb{1})\sb{\mathcal{B} '} & \cdots & f(\vec{u}\sb{n})\sb{\mathcal{B} '} \end{array} \right) \cr &=
\left( \begin{array}{c|c|c} M\sb{\mathcal{B}', \mathcal{ B} ''}(g) \cdot f(\vec{u}\sb{1})\sb{\mathcal{B} '} & \cdots & M\sb{\mathcal{B}', \mathcal{ B} ''}(g) \cdot f(\vec{u}\sb{n})\sb{\mathcal{B} '} \end{array} \right)\cr
&=
\left( \begin{array}{c|c|c} g(f(\vec{u}\sb{1}))\sb{\mathcal{B} ''} & \cdots & g(f(\vec{u}\sb{n}))\sb{\mathcal{B} ''} \end{array} \right)\cr
&= \left( \begin{array}{c|c|c} (g\circ f)(\vec{u}\sb{1})\sb{\mathcal{B} ''} & \cdots & (g\circ f)(\vec{u}\sb{n})\sb{\mathcal{B} ''} \end{array} \right)\cr
& = M\sb{\mathcal{B}, \mathcal{ B} ''}(g\circ f).
\end{aligned}$$
{{% /proof %}}

## Imagen y núcleo

{{% definition %}}
Imagen y núcleo

Sea $f:V \to V'$ un homomorfismo de espacios vectoriales.

-   La **imagen del subespacio** $W\subset V$ es el conjunto
    $$f(W) = \{ f(\vec{w}) \in V' ~|~ \vec{w} \in W \} \subset V'.$$

-   La **imagen del homomorfismo** $f$ es el conjunto $f(V)$, y se
    notará por $\im(f)$.

-   El **núcleo** de $f$ es el conjunto
    $$\ker(f) = \{ \vec{v} \in V ~|~ f(\vec{v}) = \vec{0} \}.% = f^{-1}(\vec{0}\sb{V'}).$$
{{% /definition %}}

{{% example name="Ejemplo" %}}
Sean $V = \R^2, V' = \R^3$, donde tenemos ciertas bases
$\mathcal{B} = \{ \vec{u}\sb{1}, \vec{u}\sb{2} \}$ y
$\mathcal{B}' = \{ \vec{u}'_1, \vec{u}'_2, \vec{u}'_3 \}$, y consideremos
el homomorfismo $f:V \to V'$ dado por $$\begin{aligned}
    f(\vec{u}\sb{1}) & = 2 \vec{u}'_1 - \vec{u}'_2 + 2 \vec{u}'_3, \cr
    f(\vec{u}\sb{2}) & =  \vec{u}'_1 + \vec{u}'_3.
\end{aligned}$$ Entonces la matriz de $f$ respecto de $\mathcal{B}$ y
$\mathcal{B}'$ es
$$M\sb{\mathcal{B}, \mathcal{B}'} = A = \left( \begin{array}{rr} 2 & 1 \cr -1 & 0 \cr 2 & 1 \end{array} \right).$$
Tomemos $W = \langle \vec{u}\sb{1} \rangle$. Entonces
$$f(W) = \{ f(\vec{w}) ~|~ \vec{w} \in W \} = \{ f(\alpha \vec{u}\sb{1}) ~|~ \alpha \in \R \} = \{ \alpha (2 \vec{u}'_1 - \vec{u}'_2 + 2 \vec{u}'_3) ~|~ \alpha \in \R \} = \langle 2 \vec{u}'_1 - \vec{u}'_2 + 2 \vec{u}'_3 \rangle.$$
Si lo expresamos en coordenadas respecto a la base $\mathcal{B}'$,
podemos decir que
$$f(W) = \langle \left( \begin{array}{r} 2 \cr -1 \cr 2 \end{array} \right) \rangle.$$
Para determinar $\ker(f)$, tenemos que calcular los vectores
$$\vec{v} = \alpha\sb{1} \vec{u}\sb{1} + \alpha\sb{2} \vec{u}\sb{2}$$ tales que
$$\begin{aligned}
  \vec{0}\sb{V'} & = \alpha\sb{1} (2 \vec{u}'_1 - \vec{u}'_2 + 2 \vec{u}'_3) + \alpha\sb{2} (\vec{u}'_1 + \vec{u}'_3) \cr
  & = (2 \alpha\sb{1} + \alpha\sb{2}) \vec{u}'_1 - \alpha\sb{1} \vec{u}'_2 + (2 \alpha\sb{1} + \alpha\sb{2}) \vec{u}'_3.
\end{aligned}$$ Como $\mathcal{B}'$ es una base, tenemos que resolver el
sistema lineal homogéneo
$$\left\\{ \begin{array}{l} 2 \alpha\sb{1} + \alpha\sb{2} = 0, \cr -\alpha\sb{1} = 0, \cr 2 \alpha\sb{1} + \alpha\sb{2} = 0, \end{array} \right. \text{ que se expresa como } A \left( \begin{array}{c} \alpha\sb{1} \cr \alpha\sb{2} \end{array} \right) = \left( \begin{array}{r} 0 \cr 0 \cr 0 \end{array} \right).$$
Si calculamos la forma escalonada reducida por filas, nos queda
$$A \mapright{\mbox{G-J}} E\sb{A} =  \left( \begin{array}{cc} 1&0\cr{}0&1 \cr{}0&0\end{array} \right),$$
y la solución del sistema es $\alpha\sb{1} = 0, \alpha\sb{2} = 0$, es decir,
$\ker(f) = \{ \vec{0}\sb{V} \}$.
{{% /example %}}

{{% theorem name="Subespacios imagen y núcleo" %}}
Sea $f:V \to V'$ un homomorfismo de espacios vectoriales, y
$W \subset V$ un subespacio vectorial. Entonces

-   $f(W)$ es un subespacio vectorial de $V'$.

-   $\ker(f)$ es un subespacio vectorial de $V$.

En particular, $\im(f)$ es un subespacio vectorial de $V'$.
{{% /theorem %}}

{{% proof %}}
-   Dos vectores cualesquiera $\vec{w}'_1,\vec{w}'_2\in f(W)$ son siempre
    de la forma $\vec{w}'_1=f(\vec{w}\sb{1})$ y $\vec{w}'_2=f(\vec{w}\sb{2})$ para
    ciertos $\vec{w}\sb{1},\vec{w}\sb{2}\in V$. Como $W$ es un subespacio
    vectorial de $V$, se tiene que $\vec{w}\sb{1}+\vec{w}\sb{2}\in W$, y si
    $\alpha\in \K$, además $\alpha \vec{w}\sb{1}\in W$, luego
    $$\begin{aligned}
          \vec{w}'_1+\vec{w}'_2&=f(\vec{w}\sb{1})+f(\vec{w}\sb{2})=f(\vec{w}\sb{1}+\vec{w}\sb{2})\in f(W),\cr
          \alpha\vec{w}'_1&= \alpha f(\vec{w}\sb{1})=f(\alpha\vec{w}\sb{1})\in f(W).
\end{aligned}$$

-   Dados dos vectores $\vec{v}\sb{1},\vec{v}\sb{2}\in \ker(f)$ y un escalar
    $\alpha\in \K$, como $f(\vec{v}\sb{1}) = f(\vec{v}\sb{2}) = \vec{0}$, tenemos
    que
    $$f(\vec{v}\sb{1}+\vec{v}\sb{2})=f(\vec{v}\sb{1})+f(\vec{v}\sb{2}) = \vec{0}, \qquad f(\alpha\vec{v}\sb{1})=\alpha f(\vec{v}\sb{1}) = \vec{0},$$
    de donde $\vec{v}\sb{1}+\vec{v}\sb{2}\in \ker(f)$ y
    $\alpha \vec{v}\sb{1}\in  \ker(f)$.
{{% /proof %}}

Queremos ahora dar un método para el cálculo de los subespacios
anteriores.

{{% theorem name="Imagen de generadores" %}}
 Sea $f\colon V\rightarrow V'$ un homomorfismo de
espacios vectoriales, y $W \subset V$ un subespacio vectorial, con
$S =\{\vec{w}\sb{1},\ldots,\vec{w}\sb{r}\}\subset W$ un sistema de generadores de
$W$. Entonces $f(S)=\{ f(\vec{w}\sb{1}),\ldots,f(\vec{w}\sb{r})\}\subset f(W)$ es
un sistema de generadores de $f(W)$.
{{% /theorem %}}

{{% proof %}}
Si $\vec{w}'\in f(W)$ entonces existe $\vec{w} \in W$ tal que
$\vec{w}'=f(\vec{w})$. Como $S$ genera $W$, podemos expresar $\vec{w}$ como
combinación lineal de los elementos de $S$, es decir, existen escalares
$\alpha\sb{1}, \ldots, \alpha\sb{r} \in \K$ tales que
$\vec{w}=\alpha\sb{1}\vec{w}\sb{1}+\cdots+\alpha\sb{r}\vec{w}\sb{r}$. Por tanto
$$\vec{w}'=f(\vec{w})=\alpha\sb{1} f(\vec{w}\sb{1})+\cdots+\alpha\sb{r} f(\vec{w}\sb{r}).$$
Esto demuestra que todo vector de $f(W)$ es combinación lineal de
$f(S)$.
{{% /proof %}}

{{% example name="Cálculo de la imagen de un subespacio" %}}
 Sea $f:V \to V'$ un homomorfismo
definido por una matriz $A\sb{m \times n}$ respecto de bases respectivas
${\mathcal B}$ y ${\mathcal B}'$ de $V$ y $V'$, y
$W = \langle \vec{w}\sb{1}, \ldots, \vec{w}\sb{r} \rangle$ un subespacio vectorial
de $V$. Entonces $f(W)$ está generado por los vectores
$f(\vec{w}\sb{i}), i=1, \ldots, r$, cuyas coordenadas respecto de
$\mathcal{B}'$ son
$$[f(\vec{w}\sb{i})]\sb{\mathcal{B}'} = A \cdot [\vec{w}\sb{i}]\sb{\mathcal B}, i=1, \ldots, r.$$
{{% /example %}}

{{% example name="Ejemplo" %}}
Sea $f:\R^3 \to \R^2$ definido por $f(\vec{v}) = A \vec{v}$, donde
$$A =  \left( \begin{array}{ccc} 1&-1&1\cr{}2&3&0
\end{array} \right)\quad  \mbox{y} \quad  W = \langle \vec{w}\sb{1} =  \left( \begin{array}{c} 1\cr{}-1\cr{}0
\end{array} \right), \vec{w}\sb{2} =  \left( \begin{array}{c} -2\cr{}1\cr{}5
\end{array} \right) \rangle.$$ Entonces
$$f(W) = \langle A \vec{w}\sb{1}, A \vec{w}\sb{2} \rangle = \langle  \left( \begin{array}{c} 2\cr{}-1\end{array} \right) ,  \left( \begin{array}{c} 2\cr{}-1\end{array} \right) \rangle.$$
Observemos que $\{ \vec{w}\sb{1}, \vec{w}\sb{2} \}$ es un sistema linealmente
independiente, pero $\{ A \vec{w}\sb{1}, A \vec{w}\sb{2} \}$ no lo es. Lo único
que se garantiza es el carácter de sistema generador.
{{% /example %}}

Del mismo modo que podemos usar un sistema de generadores de
$W\subset V$ para obtener un sistema de generadores de $f(W)$, podemos
usar unas ecuaciones implícitas de un subespacio $W'\subset V'$ para
hallar unas ecuaciones implícitas de su preimagen $f^{-1}(W')$.

{{% example name="Cálculo de la imagen inversa de un subespacio" %}}
 Sea $f:V \to V'$ un
homomorfismo definido por una matriz $A\sb{m \times n}$ respecto de bases
respectivas ${\mathcal B}$ y ${\mathcal B}'$ de $V$ y $V'$, y sea
$W' \subset V'$ un subespacio vectorial, dado por unas ecuaciones
implícitas de respecto de $\mathcal B'$:
$$W'\colon \quad M \,\left(\begin{array}{c} x'_1\cr\vdots\cr x'_m \end{array} \right)=\vec{0}.$$
Entonces, unas ecuaciones implı́citas de $f^{-1}(W')\subset V$ respecto
de ${\mathcal B}$ vienen dadas por:
$$f^{-1}(W')\colon \quad M \cdot A \, \left(\begin{array}{c} x\sb{1}\cr\vdots\cr x\sb{n} \end{array} \right)=\vec{0}.$$
{{% /example %}}

La justificación es clara: un vector $\vec{x}' \in W'$ si y solamente si
$M \vec{x}' = \vec{0}$. Entonces $$\begin{aligned}
  \vec{x} \in f^{-1}(W') & \Leftrightarrow & f(\vec{x}) \in W' \cr
                        & \Leftrightarrow & A \vec{x} \in W' \cr
                        & \Leftrightarrow & M A \vec{x} = \vec{0}.
\end{aligned}$$

{{% example name="Ejemplo" %}}
Consideremos la aplicación lineal $f:\R^3 \to \R^4$ definida por la
matriz
$$A =  \left( \begin{array}{ccc} 2&6&-2\cr{}1&3&3 \cr{}-3&-9&1\cr{}1&3&2\end{array} \right),$$
y $W' = \langle \vec{w}'_1, \vec{w}'_2 \rangle$, donde
$$\vec{w}'_1 =  \left( \begin{array}{c} -2\cr{}3\cr{}1
\cr{}2\end{array} \right), \vec{w}'_2 =  \left( \begin{array}{c} 0\cr{}-3\cr{}1
\cr{}-2\end{array} \right).$$

-   Calculamos unas ecuaciones implı́citas de $W'$ (ver página ).
    $$\begin{aligned}
          \left( \begin{array}{cc|c} \vec{w}'_1 & \vec{w}'_2 & I\sb{4} \end{array} \right) & \mapright{\mbox{G-J}} &  \left( \begin{array}{cc|cccc} 1&0&0&0&1/2&1/4\cr{}0&1&0
    &0&1/2&-1/4\cr{}0&0&1&0&1&1/2\cr{}0&0&0&
    1&0&-3/2\end{array} \right) = \left( \begin{array}{c|c} E & P \end{array} \right).
\end{aligned}$$ Entonces unas ecuaciones implı́citas se obtienen con
    las dos últimas componentes del vector $P \vec{x}'$:
    $$P \vec{x}' =   \left( \begin{array}{c} 1/2 x'_3 +1/4 x'_4 \cr{}1/2 x'_3 -1/4 x'_4 \cr{} x'_1 + x'_3 +1/2 x'_4 \cr{} x'_2 - 3/2  x'_4 \end{array} \right) \Rightarrow W' \colon \left\\{ \begin{array}{rrrrcl} x'_1 & & + x'_3 & + \frac{1}{2} x'_4 & = & 0, \cr & x'_2 & & -\frac{3}{2} x'_4 & = & 0. \end{array} \right.$$

-   Ecuaciones implı́citas de $f^{-1}(W')$. Si llamamos $M$ a la matriz
    de coeficientes del sistema lineal homogéneo anterior, tenemos
    $$M = \left( \begin{array}{rrrr} 1 & 0 & 1 & \frac{1}{2} \cr 0 & 1 & 0 & - \frac{3}{2} \end{array} \right) \quad \mbox{ y } \quad M A =  \left( \begin{array}{ccc} -1/2&-3/2&0\cr{}-1/2&-3/2&0 \end{array} \right).$$
    Unas ecuaciones implı́citas de $f^{-1}(W')$ son
    $$\left\\{ \begin{array}{rrcl} -\frac{1}{2} x\sb{1} & - \frac{3}{2} x\sb{2} & = & 0 \cr -\frac{1}{2} x\sb{1} & - \frac{3}{2} x\sb{2} & = & 0 \end{array} \right.$$

Observemos que no se obtiene necesariamente un conjunto independiente de
ecuaciones.
{{% /example %}}

Dos casos particulares de imagen e imagen inversa de subespacios son,
respectivamente, $\operatorname{im}(f)=f(V)$ y
$\ker(f)=f^{-1}(\{\mathbf 0\})$. Si llamamos $A$ a la matriz
$M\sb{\mathcal B,\mathcal B'}(f)$ (respecto de bases dadas de $V$ y de
$V'$), podemos calcular ambos subespacios como sigue:

Para calcular $\operatorname{im}(f)$, observamos que las coordenadas de
los vectores de la base ${\mathcal B}$ respecto a ella misma son los
vectores $\vec{e}\sb{1}, \ldots, \vec{e}\sb{n}$, por lo que la imagen del
homomorfismo (respecto de $\mathcal B'$) está generada por los vectores
$A \vec{e}\sb{i}$, $i=1,\ldots,n$, es decir, por las columnas de la matriz
$A$. Luego $c\sb{\mathcal B'}(\operatorname{im}(f))=\col(A)$.

Por otro lado, unas ecuaciones implícitas de $\{\mathbf 0\}\subset V'$
vienen dadas por $I\mathbf x' =\mathbf 0$, por tanto unas ecuaciones
implícitas de $\ker(f)$ respecto de $\mathcal B$ vienen dadas por
$IA \mathbf x=\mathbf 0$, es decir, $A\mathbf x = \mathbf 0$. Por tanto,
$c\sb{\mathcal B}(\ker(f))=\nulo(A)$.

{{% example name="Cálculo de la imagen y núcleo de un homomorfismo" %}}
Sea $f:V \to V'$ un homomorfismo definido por una matriz
$A\sb{m \times n}$ respecto de bases respectivas ${\mathcal B}$ y
${\mathcal B}'$ de $V$ y $V'$. Entonces

-   una base de $\im(f)$ se obtiene a partir de una base de $\col(A)$
    (columnas básicas de $A$) ;

-   una base de $\ker(f)$ se obtiene a partir de una base de $\nulo(A)$
    (soluciones $\vec{h}\sb{i}$ obtenidas al hallar la solución general del
    sistema $A\mathbf x =\mathbf 0$).
{{% /example %}}

{{% example name="Ejemplo" %}}
Consideremos la aplicación lineal $f:\R^3 \to \R^4$ definida por
$f(\vec{v}) = A \vec{v}$, con
$$A =  \left( \begin{array}{ccc} 2&6&-2\cr{}1&3&3 \cr{}-3&-9&1\cr{}1&3&2\end{array} \right).$$
Entonces
$$\im(f) = \langle \left( \begin{array}{r} 2 \cr 1 \cr -3 \cr 1 \end{array} \right),  \left( \begin{array}{c} 6 \cr 3\cr -9
\cr 3 \end{array} \right),  \left( \begin{array}{c} -2\cr 3\cr 1 \cr 2 \end{array} \right) \rangle.$$
Como
$$A \mapright{\text{Gauss}} \left( \begin{array}{ccc} 2&6&-2\cr{}0&0&8
\cr{}0&0&0\cr{}0&0&0\end{array}
 \right),$$ el sistema anterior es generador, pero no linealmente
independiente. Una base de $\im(f)$ está formada por
$\{ A\sb{\ast 1}, A\sb{\ast 3} \}$. A partir de
$$\nulo(A) = \langle \vec{h}\sb{1} = \left( \begin{array}{r} -3 \cr 1 \cr 0 \end{array} \right) \rangle,$$
una base de $\ker(f)$ es $\{ \vec{h}\sb{1} \}$.
{{% /example %}}

{{% theorem name="Fórmula de la dimensión de homomorfismos" %}}
 Sea $f\colon V\rightarrow V'$
un homomorfismo entre $\K$-espacios vectoriales, con $\dim V$ finita.
Entonces $$\dim V=\dim[\ker(f)]  + \dim[\im(f)].$$
{{% /theorem %}}

[]{#thm:form-dim-hom label="thm:form-dim-hom"}

{{% proof %}}
Sea $n = \dim V$ y $s = \dim[\ker(f)]$. Como $\ker(f)$ es un subespacio
de $V$, es de dimensión finita y $s \le n$. Sea
${\mathcal B}\sb{0} = \{ \vec{v}\sb{1}, \ldots, \vec{v}\sb{s} \}$ base de $\ker(f)$ y
la extendemos a una base
$\{ \vec{v}\sb{1}, \ldots, \vec{v}\sb{s}, \vec{v}\sb{s+1}, \ldots, \vec{v}\sb{n} \}$ de
$V$. Vamos a probar que el sistema
$\{ f(\vec{v}\sb{s+1}), \ldots, f(\vec{v}\sb{n}) \}$ es base de $\im(f)$, con lo
que tendremos el resultado.

Veamos en primer lugar la independencia lineal. Sean
$\alpha\sb{1}, \ldots, \alpha\sb{n} \in \K$ tales que
$$\alpha\sb{s+1} f(\vec{v}\sb{s+1}) + \cdots + \alpha\sb{n} f(\vec{v}\sb{n}) = \vec{0}.$$
Esta igualdad se puede escribir como
$$f(\alpha\sb{s+1} \vec{v}\sb{s+1} + \cdots + \alpha\sb{n} \vec{v}\sb{n} ) = \vec{0}, \text{ que es equivalente a } \alpha\sb{s+1} \vec{v}\sb{s+1} + \cdots + \alpha\sb{n} \vec{v}\sb{n} \in \ker(f).$$
Por tanto,
$$\alpha\sb{s+1} \vec{v}\sb{s+1} + \cdots + \alpha\sb{n} \vec{v}\sb{n} = \beta\sb{1} \vec{v}\sb{1} + \cdots + \beta\sb{s} \vec{v}\sb{s} \text{ para ciertos escalares } \beta\sb{1}, \ldots, \beta\sb{s} \in \K.$$
Como $\{ \vec{v}\sb{1}, \ldots, \vec{v}\sb{n} \}$ es una base de $V$, entonces
$\alpha\sb{s+1} = \ldots = \alpha\sb{n} = 0$ (también los $\beta\sb{j}$) y hemos
probado la independencia lineal.

Ahora probamos que
$\im(f) = \langle f(\vec{v}\sb{s+1}), \ldots, f(\vec{v}\sb{n}) \rangle$. Sea
$\vec{w} \in \im(f)$. Por definición, existe $\vec{v} \in V$ tal que
$f(\vec{v}) = \vec{w}$ y podemos, entonces, encontrar unos escalares
$\lambda\sb{1}, \ldots, \lambda\sb{n}$ tales que
$$\vec{v} = \lambda\sb{1} \vec{v}\sb{1} + \cdots + \lambda\sb{n} \vec{v}\sb{n} = \sum\sb{i=1}^s \lambda\sb{i} \vec{v}\sb{i} + \sum\sb{i=s+1}^n \lambda\sb{i} \vec{v}\sb{i}.$$
Aplicamos $f$ a ambos lados de la igualdad, y tenemos en cuenta que
$f(\vec{v}\sb{i}) = 0$ para $i=1,\ldots,s$. Nos queda
$$\vec{w} = f(\vec{v}) = \underbrace{\sum\sb{i=1}^s \lambda\sb{i} f(\vec{v}\sb{i})}\sb{= \vec{0}} + \sum\sb{i=s+1}^n \lambda\sb{i} f(\vec{v}\sb{i}) = \sum\sb{i=s+1}^n \lambda\sb{i} f(\vec{v}\sb{i}) \in \langle f(\vec{v}\sb{s+1}), \ldots, f(\vec{v}\sb{n}) \rangle.$$
{{% /proof %}}

{{% remark %}}
Sea $f:V \to V'$ un homomorfismo definido por una matriz
$A\sb{m \times n}$ respecto de bases respectivas ${\mathcal B}$ y
${\mathcal B}'$ de $V$ y $V'$. El número de columnas linealmente
independiente en una matriz coincide con su rango. De aquı́ deducimos que
$$\dim[\im(f)]= \rg M\sb{{\mathcal B},{\mathcal B}'}(f) = \rg(A).$$

Por otro lado, unas ecuaciones implı́citas de $\ker(f)$ son
$A \vec{x} = \vec{0}$ y $$\dim[\ker(f)]=\dim V-\rg(A).$$ Los dos
enunciados se combinan en que
$\dim V = n = r + (n-r) = \dim[\im(f)] + \dim[\ker(f)]$, que es el
teorema anterior. Sin embargo, aunque el resultado es el mismo, hay una
pequeña diferencia en las hipótesis.
{{% /remark %}}

## Homomorfismos inyectivos y sobreyectivos

Los homomorfismos entre espacios vectoriales, en tanto que aplicaciones,
pueden ser inyectivos, sobreyectivos, ambas cosas (es decir, biyectivos)
o ninguno de ellos. En esta sección veremos la relación entre estos
conceptos y los homomorfismos.

{{% definition %}}
Isomorfismos Un homomorfismo $f\colon V\rightarrow V'$ se denomina
**isomorfismo** si la aplicación $f$ es biyectiva.
{{% /definition %}}

En algunos textos aparece la siguiente nomenclatura:

-   Si la aplicación $f$ es [in]{.underline}yectiva, decimos que $f$ es
    un **[mono]{.underline}morfismo**

-   Si la aplicación $f$ es [sobre]{.underline}yectiva, decimos que $f$
    es un **[epi]{.underline}morfismo**.

Si existe un isomorfismo entre dos espacios vectoriales $V$ y $V'$
decimos que son **isomorfos**.

{{% example name="Ejemplo" %}}
Algunos ejemplos conocidos de homomorfismos inyectivos, sobreyectivos y
biyectivos son los siguientes:

1.  Si $W\subset V$ es un subespacio vectorial, la **inclusión**
    $i\colon W\rightarrow V$, $i(\vec{v})=\vec{v}$, es inyectiva, y la
    **proyección natural** $p\colon V\rightarrow V/W$,
    $p(\vec{v})=\vec{v} + W$, es sobreyectiva.

2.  La identidad $\id\sb{V}\colon V\rightarrow V$, $\id\sb{V}(\vec{v})=\vec{v}$,
    es un isomorfismo.

3.  Sea $V$ un espacio vectorial de dimensión $n$, y ${\mathcal B}$ una
    base. La aplicación de coordenadas $$c\sb{\mathcal B}:V \to \K^n$$ que
    a cada vector de $V$ le hace corresponder sus coordenadas respecto
    de la base ${\mathcal B}$ es un isomorfismo (pág. )
{{% /example %}}

Por definición, un homomorfismo $f;V \to V'$ es sobreyectivo si y
solamente si la imagen es el espacio destino, esto es, $\im(f)=V'$. El
carácter inyectivo de un homomorfismos está determinado por su núcleo.

{{% theorem name="Caracterización de homomorfismos inyectivos y sobreyectivos" %}}
Sea $f:V \to V'$ un homomorfismo de espacios vectoriales sobre un mismo
cuerpo $\K$.

-   $f$ es sobreyectivo si y solamente si $\im(f) = V'$.

-   $f$ es inyectivo si y solamente si $\ker(f) = \{\vec{0}\}$.
{{% /theorem %}}

{{% proof %}}
La caracterización del carácter sobreyectivo ya la tenemos. Supongamos
que $f$ es inyectivo, y tomemos $\vec{v} \in \ker(f)$. Entonces
$f(\vec{v}) = \vec{0}\sb{V'} = f(\vec{0}\sb{V})$. Por el carácter inyectivo,
$\vec{v} = \vec{0}\sb{V}$, y tenemos la implicación. Recı́procamente,
supongamos que $\ker(f) = \vec{0}\sb{V}$. Sean $\vec{u}, \vec{v} \in V$ tales
que $f(\vec{u}) = f(\vec{v})$. Entonces
$\vec{0}\sb{V'} = f(\vec{u}) - f(\vec{v}) = f(\vec{u} - \vec{v})$, y
$\vec{u} - \vec{v} \in \ker(f) = \vec{0}\sb{V}$, de donde $\vec{u} = \vec{v}$.
{{% /proof %}}

En espacios vectoriales de dimensión finita podemos relacionar la
inyectividad y sobreyectividad con una matriz.

{{% example name="Caracterización de homomorfismos inyectivos y sobreyectivos en dimensión finita" %}}
Sea $f:V \to V'$ un homomorfismo definido por una matriz
$A\sb{m \times n}$ respecto de bases respectivas ${\mathcal B}$ y
${\mathcal B}'$ de $V$ y $V'$.

-   $f$ es inyectivo si y solamente si $\rg(A) = n$.

-   $f$ es sobreyectivo si y solamente si $\rg(A) = m$.

-   $f$ es biyectivo si y solamente si $m = n$ y $\det(A) \ne 0$.

-   Si $\dim V = \dim V'$, son equivalentes:

    1.  $f$ es inyectivo.

    2.  $f$ es sobreyectivo.

    3.  $f$ es biyectivo.
{{% /example %}}

{{% proof %}}
-   La fórmula de la dimensión nos dice que
    $$\dim V = n = \dim[ \im(f)] + \dim[\ker(f)].$$ Por el apartado
    anterior, $f$ es inyectivo si y solamente si $\dim[\ker(f)] = 0$,
    que es equivalente a $\dim[\im(f)] = n$, que es el rango de la
    matriz $A$.

-   $f$ es sobreyectivo si y solamente si el espacio imagen es todo
    $V'$, es decir, $\dim[\im(f)] = \dim V' = m$.

-   Si $f$ es biyectivo, entonces
    $n = \dim V = \dim[\im(f)] + \dim[\ker(f)] = \dim V' + 0 = m$. La
    matriz $A$ es cuadrada, y $\rg(A) = n$, por lo que es no singular.
    Recı́procamente, si $A$ es cuadrada y con determinante no nulo,
    entonces el sistema lineal homogéneo $A\vec{x} =  \vec{0}$ tiene
    solución única, que es la trivial. Por tanto, $f$ es inyectivo, y
    entonces $\dim[\im(f)] = n = m$, por lo que es sobreyectivo.

-   Se aplica la fórmula de la dimensión y los apartados anteriores.
{{% /proof %}}

{{% example name="Ejemplo" %}}
Vamos a considerar ejemplos de los diferentes tipos.

1.  Sea $f:\R^4 \to \R^3$ definida como $f(\vec{v}) = A \vec{v}$, donde
    $$A = \left( \begin{array}{rrrr} 1 & 0 & 0 & 0 \cr 0 & 1 & 0 & 0 \cr 0 & 0 & 0 & 0 \end{array} \right).$$
    Entonces $\rg(A) = 2$, por lo que $f$ no es sobreyectiva. Una
    aplicación lineal de $\R^4$ en $\R^3$ nunca puede ser inyectiva.

2.  Sea $f:\R^2 \to \R^4$ la aplicación lineal definida como
    $f(\vec{v}) = A \vec{v}$, con
    $$A = \left( \begin{array}{rr} 1 & 0 \cr 2 & 0 \cr -1 & 0 \cr 3 & 0 \end{array} \right).$$
    Ahora $\rg(A) = 1$, y tampoco es inyectiva. Una aplicación lineal de
    $\R^2$ en $\R^4$ no puede ser sobreyectiva.

3.  Sea $f:\R^3 \to \R^3$ la aplicación lineal definida como
    $f(\vec{v}) = A \vec{v}$, donde
    $$A = \left( \begin{array}{rrr} -1 & 2 & 0 \cr 0 & -1 & 1 \cr -1 & 3 & 4 \end{array} \right).$$
    Entonces $\det(A) \ne 0$, por lo que $f$ es biyectiva.
{{% /example %}}

{{% remark %}}
Dado un homomorfismo $f\colon V\rightarrow V'$ entre espacios
vectoriales de dimensión finita, $\dim V=n$ y $\dim V'=m$, los
resultados de esta sección demuestran que:

-   Si $n<m$, entonces $f$ nunca es sobreyectivo.

-   Si $n>m$, entonces $f$ nunca es inyectivo.

-   Si $n=m$, entonces $f$ es inyectivo si y solamente si es
    sobreyectivo.

En espacios vectoriales de dimensión infinita lo anterior no es válido.
Por ejemplo, sean $V = V'= \R[x]$. Sabemos que $\R[x]$ es un
$\R$-espacio vectorial de dimensión infinita. La aplicación que a un
polinomio $p(x) = a\sb{0} + a\sb{1} x + \cdots + a\sb{r} x^r$ le asocia
$xp(x)= a\sb{0} x +a\sb{1} x^2 +\cdots + a\sb{r} x^{r+1}$ es un homomorfismo
inyectivo, pero no es sobreyectivo.
{{% /remark %}}

{{% definition %}}
Endomorfismos y automorfismos

-   Si $f:V \to V$ es un homomorfismo con los mismos espacios
    vectoriales en el dominio y la imagen, decimos que $f$ es un
    **endomorfismo**.

-   Si $f$ es un endomorfismo biyectivo, decimos que es un
    **automorfismo**.
{{% /definition %}}

{{% example name="Ejemplo" %}}
La aplicación $f:\R^3 \to \R^3$ definida como $f(\vec{v}) = A \vec{v}$,
con
$$A = \left( \begin{array}{ccc} 1&-2&3\cr{}0&3&-3\cr{}0&1&-1\end{array} \right)$$
es un endomorfismo, pero no es un automorfismo, porque $\det(A) = 0$.
Sin embargo, la aplicación $g:\R^3 \to \R^3$ definida por
$g(\vec{v}) = B \vec{v}$, con
$$B = \left( \begin{array}{ccc} 1&-2&3\cr{}0&3&-3\cr{}0&1&0\end{array} \right)$$
es un automorfismo.
{{% /example %}}

{{% theorem name="Transformación de un sistema generador o independiente" %}}
Sea $f:V \to V'$ un homomorfismo entre espacios vectoriales,
$S = \{ \vec{u}\sb{1}, \ldots, \vec{u}\sb{r} \} \subset V$ un sistema linealmente
independiente, $G = \{ \vec{v}\sb{1}, \ldots, \vec{v}\sb{s} \} \subset V$ un
sistema generador y ${\mathcal B} = \{ \vec{w}\sb{1}, \ldots, \vec{w}\sb{n}  \}$
una base de $V$.

-   Si $f$ es inyectiva (monomorfismo), entonces
    $f(S) = \{ f(\vec{u}\sb{1}), \ldots, f(\vec{u}\sb{r}) \} \subset V'$ es un
    sistema linealmente independiente.

-   Si $f$ es sobreyectiva (epimorfismo), entonces
    $f(G) = \{ f(\vec{v}\sb{1}), \ldots, f(\vec{v}\sb{s}) \} \subset V'$ es un
    sistema generador.

-   Si $f$ es biyectiva (isomorfismo), entonces
    $f({\mathcal B}) = \{ f(\vec{w}\sb{1}), \ldots, f(\vec{w}\sb{n} ) \}$ es base
    de $V'$.
{{% /theorem %}}

{{% proof %}}
-   Sean $\alpha\sb{1},\ldots,\alpha\sb{r}\in \K$ escalares tales que
    $$\alpha _1 f(\vec{u}\sb{1})+\cdots +\alpha _r f(\vec{u}\sb{r})=\vec{0}\sb{V'}.$$
    Hay que demostrar que todos los coeficientes son nulos. Como $f$ es
    un homomorfismo, tenemos que
    $$f(\alpha _1 \vec{u}\sb{1}+\cdots+\alpha _r \vec{v}\sb{r}) = \alpha _1 f(\vec{u}\sb{1})+\cdots +\alpha\sb{r} f(\vec{u}\sb{r})=\vec{0}\sb{V'}=f(\vec{0}\sb{V}).$$
    Al ser $f$ inyectivo,
    $\alpha _1\vec{u}\sb{1}+\cdots+\alpha\sb{r}\vec{u}\sb{r}=\vec{0}\sb{V}$ y, como $S$ es
    linealmente independiente, todos los coeficientes son cero:
    $\alpha\sb{1}=\cdots=\alpha\sb{r}=0$.

-   Ya vimos en la sección anterior que $f(G)$ es un sistema de
    generadores de $\im(f)$. Por ser $f$ sobreyectivo, $\im(f)=V'$, y
    tenemos el resultado.

-   Es consecuencia inmediata de los apartados anteriores.
{{% /proof %}}

{{% theorem name="Composición de isomorfismos" %}}
-   Si $f:V \to V'$ es un isomorfismo, entonces la aplicación inversa
    $f^{-1}:V' \to V$ es un isomorfismo.

-   Si $f:V \to V', g:V' \to V''$ son isomorfismos, entonces la
    composición $g \circ f:V \to V''$ es un isomorfismo.
{{% /theorem %}}

{{% proof %}}
-   Como $f^{-1}$ es biyectiva, basta probar que $f^{-1}$ es un
    homomorfismo. Sean $\vec{v}'_{1},\vec{v}'_{2}\in V'$ y
    $\alpha \in \K$. Existen $\vec{v}\sb{1}, \vec{v}\sb{2} \in V$ tales que
    $f(\vec{v}\sb{1}) = \vec{v}'_1, f(\vec{v}\sb{2}) = \vec{v}'_2$, y
    $$\begin{aligned}
              f(\vec{v}\sb{1}) + f(\vec{v}\sb{2}) & = f(\vec{v}\sb{1} + \vec{v}\sb{2}) = \vec{v}'_1 + \vec{v}'_2,
\end{aligned}$$ de donde, por la inyectividad de $f$,
    $$\begin{aligned}
              f^{-1}(\vec{v}'_1 + \vec{v}'_2) & = \vec{v}\sb{1} + \vec{v}\sb{2} = f^{-1}(\vec{v}'_1) + f^{-1}(\vec{v}'_2).
\end{aligned}$$ Análogamente,
    $f(\alpha \vec{v}\sb{1}) = \alpha f(\vec{v}\sb{1}) = \alpha \vec{v}'_1$, y
    entonces
    $$f^{-1}(\alpha \vec{v}'_1) = \alpha \vec{v}\sb{1} = \alpha f^{-1}(\vec{v}'_1).$$

-   Es conocido que la composición de aplicaciones biyectivas es
    biyectiva. En la primera sección vimos que la composición de
    homomorfismos es un homomorfismo; por tanto la composición de
    isomorfismos es un isomorfismo.
{{% /proof %}}

{{% example name="Función inversa de un isomorfismo" %}}
 Sea $f:V \to V'$ un isomorfismo
definido por una matriz $A\sb{n \times n}$ respecto de bases respectivas
${\mathcal B}$ y ${\mathcal B}'$ de $V$ y $V'$. Entonces
$f^{-1}:V' \to V$ tiene como matriz a $A^{-1}$ respecto de las mismas
bases, esto es,
$$M\sb{\mathcal{B}',\mathcal{B}}(f^{-1})=M\sb{\mathcal{B},\mathcal{B}'}(f)^{-1}.$$
{{% /example %}}

{{% proof %}}
Usando la definición y las propiedades elementales de la matriz de un
homomorfismo obtenemos las ecuaciones siguientes, que demuestran la
tesis del enunciado: $$\begin{aligned}
M\sb{\mathcal{B}',\mathcal{B}}(f^{-1})M\sb{\mathcal{B},\mathcal{B}'}(f)&=M\sb{\mathcal{B},\mathcal{B}}( f^{-1}\circ f)\cr
&=M\sb{\mathcal{B},\mathcal{B}}( \mbox{id}\sb{V})\cr
&=I\sb{n},\cr&\cr
M\sb{\mathcal{B},\mathcal{B}'}(f)M\sb{\mathcal{B}',\mathcal{B}}(f^{-1})&=M\sb{\mathcal{B}',\mathcal{B}'}(f\circ f^{-1})\cr
&=M\sb{\mathcal{B}',\mathcal{B}'}(\mbox{id}\sb{V'})\cr
&=I\sb{n}.
\end{aligned}$$
{{% /proof %}}

{{% example name="Ejemplo" %}}
Consideremos el homomorfismo $f:\R^3 \to \R^3$ definido por
$f(\vec{v}) = A \vec{v}$, donde
$$A = \left( \begin{array}{rrr} -1 & 2 & 0 \cr 0 & -1 & 1 \cr -1 & 3 & 4 \end{array} \right).$$
Vimos antes que es un isomorfismo mediante el cálculo del determinante.
Con la forma escalonada reducida por filas podemos probarlo también, y a
la vez obtener la matriz de $f^{-1}$. En efecto $$\begin{aligned}
    \left( \begin{array}{c|c} A & I\sb{3} \end{array} \right) & \mapright{\mbox{G-J}} & \left( \begin{array}{rrr|rrr} 1&0&0&-\frac{7}{5}&-\frac{8}{5}& \frac{2}{5} \cr 0&1&0&-\frac{1}{5}&-\frac{4}{5} & \frac{1}{5} \cr 0&0&1&-\frac{1}{5} & \frac{1}{5} & \frac{1}{5} \end{array}
 \right) \cr & = & \left( \begin{array}{c|c} I\sb{3} & A^{-1} \end{array} \right).
\end{aligned}$$ De esta forma tenemos probado el carácter no singular de
$A$, y en la parte derecha aparece su matriz inversa.
{{% /example %}}

{{% theorem name="Clasificación de los espacios vectoriales de dimensión finita" %}}
 Dos
espacios vectoriales $V$ y $V'$ de dimensión finita son isomorfos si y
solamente si $\dim V = \dim V'$.
{{% /theorem %}}

{{% proof %}}
Si $V$ y $V'$ son isomorfos, sabemos que tienen la misma dimensión.
Veamos el recı́proco. Supongamos que $\dim V = \dim V' = n$ y
consideremos bases respectivas
$${\mathcal B} = \{ \vec{u}\sb{1}, \ldots, \vec{u}\sb{n} \}, {\mathcal B}' = \{ \vec{u}'_1, \ldots, \vec{u}'_n\},$$
que, por hipótesis, tienen el mismo número de elementos. Por el teorema
de existencia y unicidad de homomorfismos (pág. ), existe un único
homomorfismo $f:V \to V'$ definido por $f(\vec{u}\sb{i}) = \vec{u}'_i$, para
$i=1,\ldots,n$. Entonces $M\sb{\mathcal{B},\mathcal{B}'}(f) = I\sb{n}$, y $f$
es isomorfismo.
{{% /proof %}}

Otra forma de ver lo anterior es a través de la composición de
isomorfismos. Si fijamos bases respectivas $\mathcal{B}$ y
$\mathcal{B}'$ en $V$ y $V'$, entonces tenemos las aplicaciones de
coordenadas
$$c\sb{\mathcal B}\colon V\rightarrow \K^{n},\qquad c\sb{{\mathcal B}'} \colon V' \rightarrow \K^{n},$$
que son isomorfismos. 
$$\xymatrix{
  V \ar[d]^{\simeq}\sb{c\sb{\mathcal B}} \ar[dr]^{c\sb{\mathcal B} \circ c^{-1}\sb{{\mathcal B}'} }& \cr \K^n & V' \ar[l]^{\simeq}\sb{c\sb{{\mathcal B}'}}
    }$$ 
Entonces, tanto la inversa
$c\sb{{\mathcal B}'}^{-1}\colon \K^{n}\rightarrow V'$ como la composición
$c\sb{{\mathcal B}'}^{-1} \circ c\sb{\mathcal B}\colon V\rightarrow V'$ son
isomorfismos, luego $V$ y $V'$ son isomorfos.

## El espacio vectorial de los homomorfismos

Con los homomorfismos, aparte de componerlos, podemos realizar otras dos
operaciones, que dotará de estructura de espacio vectorial a un nuevo
conjunto.

{{% definition %}}
Operaciones con homomorfismos

Sean $f, g: V \to V'$ homomorfismos de espacios vectoriales, y
$\lambda \in \K$. Definimos

-   El **homomorfismo suma** $f+g\colon V\rightarrow V'$ como
    $$(f+g)(\vec{v})=f(\vec{v})+ g(\vec{v}).$$

-   El **producto por un escalar**
    $\lambda \cdot f\colon V\rightarrow V'$ como
    $$(\lambda \cdot f)(\vec{v})=\lambda f(\vec{v}).$$

Estas operaciones están definidas en el conjunto $\Hom(V,V')$ de los
homomorfismos de $V$ en $V'$.
{{% /definition %}}

Para que esta definición sea correcta, debemos probar que estas nuevas
aplicaciones son, efectivamente, homomorfismos. Tomemos vectores
$\vec{v}\sb{1}, \vec{v}\sb{2} \in V$ y un escalar $\alpha \in \K$. Entonces
$$\begin{aligned}
(f + g)(\vec{v}\sb{1} + \vec{v}\sb{2}) & = f(\vec{v}\sb{1} + \vec{v}\sb{2}) + g(\vec{v}\sb{1} + \vec{v}\sb{2}) \cr & = f(\vec{v}\sb{1}) + f(\vec{v}\sb{2}) + g(\vec{v}\sb{1}) + g(\vec{v}\sb{2}) \cr & = (f+g)(\vec{v}\sb{1}) + (f+g)(\vec{v}\sb{2}),
\end{aligned}$$ y $$\begin{aligned}
  (f+g)(\alpha \vec{v}\sb{1}) & = f(\alpha \vec{v}\sb{1}) + g(\alpha \vec{v}\sb{1}) \cr & = \alpha f(\vec{v}\sb{1}) + \alpha g(\vec{v}\sb{1}) \cr & = \alpha (f(\vec{v}\sb{1}) + g(\vec{v}\sb{1})) = \alpha (f+g)(\vec{v}\sb{1}).
\end{aligned}$$ Por otro lado, $$\begin{aligned}
  (\lambda \cdot f)(\vec{v}\sb{1} + \vec{v}\sb{2}) & = \lambda f(\vec{v}\sb{1} + \vec{v}\sb{2}) = \lambda (f(\vec{v}\sb{1}) + f(\vec{v}\sb{2})) \cr & = \lambda f(\vec{v}\sb{1}) + \lambda f(\vec{v}\sb{2}) = (\lambda \cdot f)(\vec{v}\sb{1}) + (\lambda \cdot f)(\vec{v}\sb{2}),
\end{aligned}$$ y $$\begin{aligned}
  (\lambda \cdot f)(\alpha \vec{v}\sb{1}) & = \lambda f(\alpha \vec{v}\sb{1}) = \lambda ( \alpha f(\vec{v}\sb{1})) = \alpha (\lambda f(\vec{v}\sb{1})) = \alpha (\lambda \cdot f)(\vec{v}\sb{1}).
\end{aligned}$$

{{% example name="Ejemplo" %}}
Consideremos los homomorfismos $f, g:\R^4 \to \R^2$ dados por
$f(\vec{v}) = A \vec{v}, g(\vec{v}) = B \vec{v}$, con
$$A = \left( \begin{array}{rrrr} 1 & -2 & 0 & -3 \cr 2 & -1 & 0 & -1 \end{array} \right), B = \left( \begin{array}{rrrr} 0 & 1 & 1 & 0 \cr 0 & 0 & 0 & 0 \end{array} \right).$$
Tenemos entonces que $$\begin{aligned}
(f+g)(\vec{e}\sb{1}) = f(\vec{e}\sb{1}) + g(\vec{e}\sb{1}) & = & \left( \begin{array}{r} 1 \cr 2 \end{array} \right) + \left( \begin{array}{r} 0 \cr 0 \end{array} \right) = \left( \begin{array}{r} 1 \cr 2 \end{array} \right), \cr
(f+g)(\vec{e}\sb{2}) = f(\vec{e}\sb{2}) + g(\vec{e}\sb{2}) & = & \left( \begin{array}{r} -2 \cr -1 \end{array} \right) + \left( \begin{array}{r} 1 \cr 0 \end{array} \right) = \left( \begin{array}{r} -1 \cr -1 \end{array} \right), \cr
(f+g)(\vec{e}\sb{3}) = f(\vec{e}\sb{3}) + g(\vec{e}\sb{3}) & = & \left( \begin{array}{r} 0 \cr 0 \end{array} \right) + \left( \begin{array}{r} 1 \cr 0 \end{array} \right) = \left( \begin{array}{r} 1 \cr 0 \end{array} \right), \cr
(f+g)(\vec{e}\sb{4}) = f(\vec{e}\sb{4}) + g(\vec{e}\sb{4}) & = & \left( \begin{array}{r} -3 \cr -1 \end{array} \right) + \left( \begin{array}{r} 0 \cr 0 \end{array} \right) = \left( \begin{array}{r} -3 \cr -1 \end{array} \right).
\end{aligned}$$ Por tanto, la matriz de $f+g$ respecto de las bases
estándar es $A+B$. Algo análogo ocurre con $\lambda \cdot f$, para
cualquier $\lambda \in \K$.
{{% /example %}}

{{% theorem %}}

$\Hom(V,V')$ como espacio vectorial El conjunto $\Hom(V,V')$, con las
operaciones de suma de homomorfismos y producto por un escalar, tiene
estructura de espacio vectorial.
{{% /theorem %}}

{{% proof %}}
Ya hemos visto que las operaciones son internas. Vamos a repasar las
propiedades que definen esta estructura.

-   $\Hom(V,V')$ es un grupo abeliano con respecto a la suma. La
    operación es conmutativa, asociativa, y el elemento neutro es la
    aplicación constante igual a $\vec{0}\sb{V'}$.

-   El producto por un escalar verifica:

    -   $(\lambda + \mu) \cdot f = (\lambda \cdot f) + (\mu \cdot f)$.
        Sea $\vec{v} \in V$. Se tiene que $$\begin{aligned}
              ((\lambda + \mu) \cdot f)(\vec{v}) & = & (\lambda + \mu) f(\vec{v}) = \lambda f(\vec{v}) + \mu f(\vec{v}) \cr & = & (\lambda \cdot f)(\vec{v}) + (\mu \cdot f)(\vec{v}) = (\lambda \cdot f + \mu \cdot f)(\vec{v}).
\end{aligned}$$

    -   $\lambda \cdot (f + g) = (\lambda \cdot f) + (\lambda \cdot f)$.
        Sea $\vec{v} \in V$. Entonces $$\begin{aligned}
              (\lambda \cdot (f+g))(\vec{v}) & = & \lambda (f+g)(\vec{v}) = \lambda (f(\vec{v}) + g(\vec{v})) = \lambda f(\vec{v}) + \lambda g(\vec{v}) \cr & = & (\lambda \cdot f)(\vec{v}) + (\lambda \cdot g)(\vec{v}) = (\lambda \cdot f + \lambda \cdot g)(\vec{v}).
\end{aligned}$$

    -   $\lambda \cdot (\mu \cdot f) = (\lambda \mu) \cdot f$. Si
        $\vec{v} \in V$, entonces $$\begin{aligned}
              (\lambda \cdot (\mu \cdot f))(\vec{v}) & = & \lambda (\mu \cdot f)(\vec{v}) = \lambda (\mu f(\vec{v})) = (\lambda \mu) f(\vec{v}) \cr & = & ((\lambda \mu) \cdot f)(\vec{v}).
\end{aligned}$$

    -   $1 \cdot f = f$. Inmediato.
{{% /proof %}}

{{% remark %}}
El conjunto de endomorfismos de $V$ es $\Hom(V,V)$, notado como
$\End(V)$ tiene estructura de espacio vectorial. El conjunto de
automorfismos, notado $\Aut(V)$, **no** lo es, pues no contiene al
homomorfismo nulo.
{{% /remark %}}

{{% theorem name="Interpretación matricial de las operaciones $+$ y $\cdot$" %}}
 Sean
$f, g:V \to V'$ homomorfismos definidos, respectivamente, por las
matrices $A\sb{m \times n}, B\sb{m \times n}$, con respecto a las bases
${\mathcal B}$ de $V$ y ${\mathcal B}'$ de $V'$. Tomemos un escalar
$\lambda \in \K$. Entonces $$\begin{aligned}
  M\sb{\mathcal{B},\mathcal{B}'}(f+g)&=M\sb{\mathcal{B},\mathcal{B}'}(f)+M\sb{\mathcal{B},\mathcal{B}'}(g) = A + B,\cr
  M\sb{\mathcal{B},\mathcal{B}'}(\lambda \cdot f)&=\lambda M\sb{\mathcal{B},\mathcal{B}'}(f) = \lambda A.
\end{aligned}$$
{{% /theorem %}}

{{% proof %}}
Estas ecuaciones son consecuencia inmediata de la definición de la
matriz de un homomorfismo y de la linealidad de la asignación de
coordenadas. En efecto, si
${\mathcal B}= \{ \vec{v}\sb{1},\ldots, \vec{v}\sb{n} \}$ entonces
$$\begin{aligned}
 M\sb{\mathcal{B},\mathcal{B}'}(f+g) & = \left( \begin{array}{c|c|c} (f+g)(\vec{v}\sb{1})\sb{\mathcal{B}'} & \cdots & (f+g)(\vec{v}\sb{n})\sb{\mathcal{B}'} \end{array} \right) \cr
 & = \left( \begin{array}{c|c|c} f(\vec{v}\sb{1})\sb{\mathcal{B}'}+g(\vec{v}\sb{1})\sb{\mathcal{B}'} & \cdots & f(\vec{v}\sb{n})\sb{\mathcal{B}'}+g(\vec{v}\sb{n})\sb{\mathcal{B}'} \end{array} \right)\cr
 & = \left( \begin{array}{c|c|c} f(\vec{v}\sb{1})\sb{\mathcal{B}'} & \cdots & f(\vec{v}\sb{n})\sb{\mathcal{B}'} \end{array} \right) + \left( \begin{array}{c|c|c} g(\vec{v}\sb{1})\sb{\mathcal{B}'} & \cdots & g(\vec{v}\sb{n})\sb{\mathcal{B}'} \end{array} \right) \cr
 & = M\sb{\mathcal{B},\mathcal{B}'}(f)+M\sb{\mathcal{B},\mathcal{B}'}(g),\cr{}\cr
 M\sb{\mathcal{B},\mathcal{B}'}(\alpha \cdot f) & = \left( \begin{array}{c|c|c} (\alpha \cdot f)(\vec{v}\sb{1})\sb{\mathcal{B}'} & \cdots & (\alpha \cdot f)(\vec{v}\sb{n})\sb{\mathcal{B}'} \end{array} \right) \cr
 & = \left( \begin{array}{c|c|c} \alpha f(\vec{v}\sb{1})\sb{\mathcal{B}'} & \cdots &  \alpha f(\vec{v}\sb{n})\sb{\mathcal{B}'} \end{array} \right) \cr
 & = \alpha M\sb{\mathcal{B},\mathcal{B}'}(f).
\end{aligned}$$
{{% /proof %}}

La siguiente cuestión es determinar la dimensión del espacio vectorial
$\Hom(V,V')$ en función de las dimensiones de $V$ y $V'$.

{{% theorem name="Dimensión de $\Hom(V,V')$" %}}
 Sean $V$ y $V'$ espacios vectoriales de
dimensión finita, con $\dim V = n, \dim V' = m$ y bases respectivas
${\mathcal B} \subset V, {\mathcal B}' \subset V'$. La aplicación
$M\sb{\mathcal{B},\mathcal{B}'}$ que asigna a cada homomorfismo de $V$ en
$V'$ su matriz respecto de estas bases es un isomorfismo,
$$\begin{aligned}
M\sb{\mathcal{B},\mathcal{B}'}\colon \Hom(V,V')&\longrightarrow \MatK{m}{n}{\K}\cr
f&\mapsto M\sb{\mathcal{B},\mathcal{B}'}(f).
\end{aligned}$$ En particular $\dim \Hom(V,V')=mn$.
{{% /theorem %}}

{{% proof %}}
La linealidad de $M\sb{\mathcal{B},\mathcal{B}'}$ se ha demostrado en la
proposición anterior, ası́ que solamente queda por ver que el
homomorfismo $M\sb{\mathcal{B},\mathcal{B}'}$ es biyectivo.

Sean ${\mathcal B} = \{ \vec{v}\sb{1},\ldots, \vec{v}\sb{n} \}$ y
${\mathcal B}' = \{ \vec{v}'_{1},\ldots, \vec{v}'_{m} \}$ las bases del
enunciado. Sabemos que un homomorfismo está unı́vocamente determinado por
su matriz, por tanto $M\sb{\mathcal{B},\mathcal{B}'}$ es inyectiva. La
aplicación $M\sb{\mathcal{B},\mathcal{B}'}$ es sobreyectiva, ya que dada
una matriz $A =(a\sb{ij}) \in \MatK{m}{n}{\K}$ el único homomorfismo
$f\colon V\rightarrow V'$ que satisface
$f(\vec{v}\sb{i})=a\sb{1i}\vec{v}'_{1}+\cdots+a\sb{mi}\vec{v}'_{m}$ para todo
$1\leq i\leq n$ tiene matriz $M\sb{\mathcal{B},\mathcal{B}'}(f)=A$.

La conclusión final se deduce de la igualdad
$\dim \MatK{m}{n}{\K} = m n$.
{{% /proof %}}

## Primer teorema de isomorfı́a

El cociente de espacios vectoriales posee la siguiente propiedad
universal, que implica que los espacios vectoriales $V/\ker(f)$ e
$\im(f)$ son isomorfos de forma natural.

{{% theorem name="Primer teorema de isomorfı́a" %}}
 Sea $f:V \to V'$ un homomorfismo de espacios
vectoriales. Entonces la aplicación $$\begin{aligned}
\bar{f}\colon V/\ker(f)&\longrightarrow \im(f),\cr
\vec{v}+\ker (f)&\mapsto f(\vec{v}),
\end{aligned}$$ está bien definida y es un isomorfismo. Además, el
siguiente diagrama es conmutativo: $$\xymatrix{
  V \ar[r]^f \ar[d]\sb{p} & V' \cr V / \ker(f) \ar[r]^{\bar{f}}\sb{\simeq} & \im(f) \ar[u]^i
  }$$ donde $p:V \to V/\ker(f)$ es la proyección canónica e
$i: \im(f) \to V'$ es la inclusión.
{{% /theorem %}}

{{% proof %}}
Veamos primero que la aplicación $\bar{f}$ está bien definida. Si
$\vec{v} + \ker(f) = \vec{v}' + \ker(f)$, entonces
$\vec{v}-\vec{v}'\in \ker (f)$, de donde $f(\vec{v}-\vec{v}')=\vec{0}$. Como
$f(\vec{v}-\vec{v}')=f(\vec{v})-f(\vec{v}')$ deducimos que
$f(\vec{v})=f(\vec{v}')$, y la definición de $\bar{f}(\vec{v} + \ker(f))$
dada en el enunciado no depende de la elección del representante de la
clase $\vec{v}+\ker (f)$.

La prueba de que $\bar{f}$ preserva la suma de vectores y el producto
por escalares es obvia, ya que $f$ es un homomorfismo y $\bar{f}$ se
define a partir de $f$.

Sea $\vec{v}+ \ker(f) \in \ker (\bar{f})$. Como, por definición,
$\bar{f}(\vec{v}+ \ker(f))=f(\vec{v})$, lo anterior equivale a decir que
$f(\vec{v})=\vec{0}$, es decir, $\vec{v}\in \ker (f)$. Por tanto
$\vec{v} + \ker(f) = \vec{0} + W$. Esto demuestra que
$\ker(\bar{f})=\{\vec{0} + \ker(f) \}$, esto es, $\bar{f}$ es inyectivo.

Veamos que $\bar{f}$ es sobreyectivo. Dado $\vec{v}' \in \im(f)$, por
definición de imagen existe $\vec{v}\in V$ tal que $f(\vec{v})=\vec{v}'$ y,
por tanto, $\bar{f}(\vec{v}+\ker (f))=\vec{v}'$.

Por último, verifiquemos que $f = i \circ \bar{f} \circ p$. Sea
$v \in V$. Entonces $$\begin{aligned}
(i \circ \bar{f} \circ p)(\vec{v}) & = (i \circ \bar{f})(\vec{v} + \ker(f)) = i(\bar{f}(\vec{v} + \ker(f)) \cr & = i(f(\vec{v})) = f(\vec{v}).
\end{aligned}$$
{{% /proof %}}

{{% remark %}}
[]{#rem:fact-canonica-01 label="rem:fact-canonica-01"} En el caso de
dimensión finita, el diagrama anterior nos permite calcular unas bases
especiales de $V$ y $V'$ con respecto a las cuales la matriz de $f$ es
muy sencilla. Sean $n = \dim V, m = \dim V'$ y $r = \dim[\im(f)]$.
Entonces $\dim[\ker(f)] = n-r$. Sea
${\mathcal B}\sb{0} = \{ \vec{u}\sb{r+1}, \ldots, \vec{u}\sb{n} \}$ una base de
$\ker(f)$. Existen (y podemos calcular) vectores
$\vec{u}\sb{1}, \ldots, \vec{u}\sb{r}$ tales que el sistema
${\mathcal B} = \{ \vec{u}\sb{1}, \ldots, \vec{u}\sb{r}, \vec{u}\sb{r+1}, \ldots, \vec{u}\sb{n} \}$
es base de $V$. Se trata de una ampliación, pero colocamos los vectores
de partida al final de la base. Se tiene entonces que

-   El sistema de clases
    ${\mathcal B}\sb{1} = \{ \vec{u}\sb{1} + \ker(f), \ldots, \vec{u}\sb{r} + \ker(f) \}$
    es una base del espacio cociente $V/\ker(f)$ (pág. ).

-   El sistema ${\mathcal B}\sb{2} = \{ f(\vec{u}\sb{1}), \ldots, f(\vec{u}\sb{r}) \}$
    es base de $\im(f)$ (fórmula de la dimensión de homomorfismos, pág.
    ).

Ahora construimos una ampliación del sistema ${\mathcal B}\sb{2}$ a una base
de $V'$, que denominamos
${\mathcal B}' = \{ f(\vec{u}\sb{1}), \ldots, f(\vec{u}\sb{r}), \vec{w}\sb{r+1}, \ldots, \vec{w}\sb{m-r} \}$.
Vamos a calcular las matrices de los homomorfismos del diagrama con
respecto a estas bases.

La matriz de $f$ respecto a ${\mathcal B}$ y ${\mathcal B}'$ es, por
definición, $$\begin{aligned}
  M\sb{\mathcal{B}, \mathcal{B}'}(f) & = \left( \begin{array}{cccccc} [f(\vec{u}\sb{1})]\sb{\mathcal{B}'} & \ldots & [f(\vec{u}\sb{r})]\sb{\mathcal{B}'} & [f(\vec{u}\sb{r+1})]\sb{\mathcal{B}'} & \ldots & [f(\vec{u}\sb{n})]\sb{\mathcal{B}'} \end{array} \right) \cr & = \left( \begin{array}{cccccc} 1 & \ldots & 0 & 0 & \ldots & 0 \cr \vdots & \ddots & \vdots & \vdots & \ddots & \vdots \cr 0 & \ldots & 1 & 0 & \ldots & 0 \cr \vdots & \ddots & \vdots & \vdots & \ddots & \vdots \cr 0 & \ldots 0 & 0 & 0 & \ldots & 0 \end{array} \right) = \left( \begin{array}{cc} I\sb{r} &  O \sb{r \times (n-r)} \cr  O \sb{(m-r) \times r} &  O \sb{(m-r) \times (n-r)} \end{array} \right)\sb{m \times n} = N\sb{r}.
\end{aligned}$$ Por tanto, existen bases de $V$ y $V'$ respecto de las
cuales el homomorfismo $f$ tiene una matriz de la forma $N\sb{r}$.
Construyamos, por completar, las matrices respecto de estas bases de los
restantes homomorfismos que intervienen en el diagrama.

La matriz de la proyección $p$ respecto a ${\mathcal B}$ y
${\mathcal B}\sb{1}$ es $$\begin{gathered}
    M\sb{\mathcal{B}, \mathcal{B}\sb{1}}(p) = \left( \begin{array}{cccccc} [p(\vec{u}\sb{1})]\sb{\mathcal{B}\sb{1}} & \ldots & [p(\vec{u}\sb{r})]\sb{\mathcal{B}\sb{1}} & [p(\vec{u}\sb{r+1})]\sb{\mathcal{B}\sb{1}} & \ldots & [p(\vec{u}\sb{n})]\sb{\mathcal{B}\sb{1}} \end{array} \right) \cr \shoveleft{= \left( \begin{array}{ccc} [\vec{u}\sb{1} + \ker(f)]\sb{\mathcal{B}\sb{1}} & \ldots & [\vec{u}\sb{r} + \ker(f)]\sb{\mathcal{B}\sb{1}} \end{array} \right. }\cr
    \shoveright{\left. \begin{array}{ccc} [\vec{u}\sb{r+1}+ \ker(f)]\sb{\mathcal{B}\sb{1}} & \ldots & [\vec{u}\sb{n} + \ker(f)]\sb{\mathcal{B}\sb{1}} \end{array} \right) } \cr = \left( \begin{array}{cccccc} 1 & \ldots & 0 & 0 & \ldots & 0 \cr \vdots & \ddots & \vdots & \vdots & \ddots & \vdots \cr 0 & \ldots & 1 & 0 & \ldots & 0 \end{array} \right) = \left( \begin{array}{cc} I\sb{r} &  O \sb{(n-r) \times (n-r)} \end{array} \right)\sb{(n-r) \times n}.
\end{gathered}$$ La matriz del isomorfismo $\bar{f}$ respecto a
$\mathcal{B}\sb{1}$ y $\mathcal{B}\sb{2}$ es $$\begin{aligned}
    M\sb{\mathcal{B}\sb{1},\mathcal{B}\sb{2}}(\bar{f}) & = \left( \begin{array}{ccc} [\bar{f}(\vec{u}\sb{1} + \ker(f))]\sb{\mathcal{B}\sb{2}} & \ldots & [\bar{f}(\vec{u}\sb{r} + \ker(f))]\sb{\mathcal{B}\sb{2}} \end{array} \right) \cr & = \left( \begin{array}{ccc} [f(\vec{u}\sb{1})]\sb{\mathcal{B}\sb{2}} & \ldots & [f(\vec{u}\sb{r})]\sb{\mathcal{B}\sb{2}} \end{array} \right) = I\sb{r}.
\end{aligned}$$ La matriz de la inclusión $i$ respecto a $\mathcal{B}\sb{2}$
y $\mathcal{B}'$ es $$\begin{aligned}
    M\sb{\mathcal{B}\sb{2}, \mathcal{B}'}(i) & = \left( \begin{array}{ccc} [i(f(\vec{u}\sb{1}))]\sb{\mathcal{B}'} & \ldots & [i(f(\vec{u}\sb{r}))]\sb{\mathcal{B}'} \end{array} \right) \cr & =  \left( \begin{array}{ccc} [f(\vec{u}\sb{1})]\sb{\mathcal{B}'} & \ldots & [f(\vec{u}\sb{r})]\sb{\mathcal{B}'} \end{array} \right)  \cr & = \left( \begin{array}{ccc} 1 & \ldots & 0 \cr \vdots & \ddots & \vdots \cr 0 & \ldots & 1 \cr 0 & \ldots & 0 \cr \vdots & \ddots & \vdots \cr 0 & \ldots & 0 \end{array} \right) = \left( \begin{array}{c} I\sb{r} \cr  O \sb{(m-r) \times r} \end{array} \right)\sb{m \times r}.
\end{aligned}$$
{{% /remark %}}

{{% example name="Ejemplo" %}}
[]{#ejemplo:fact-canonica-01 label="ejemplo:fact-canonica-01"} Sean
$V = \R^4, V' = \R^3$ y consideremos el homomorfismo $f:V \to V'$
definido como $f(\vec{v}) = A \vec{v}$, donde
$$A = \left( \begin{array}{rrrr} 1 & 2 & 3 & 4 \cr 2 & 4 & 6 & 7 \cr 1 & 2 & 3 & 6 \end{array} \right).$$
Mediante el procedimiento descrito, vamos a calcular bases de $V$ y $V'$
respecto de las cuales la matriz de $f$ es de la forma $N\sb{r}$, con
$r = \rg(A)$. Necesitamos, en primer lugar, una base de $\ker(f)$, que
implica el cálculo de $\nulo(A)$:
$$\left( \begin{array}{cccc} 1&2&3&4\cr{}2&4&6&7
\cr{}1&2&3&6\end{array} \right) \mapright{\GJ}  \left( \begin{array}{cccc} 1&2&3&0\cr{}0&0&0&1
\cr{}0&0&0&0\end{array} \right).$$ Entonces
$$\nulo(A) = \langle \vec{u}\sb{3} =  \left( \begin{array}{c} -2\cr{}1\cr{}0
\cr{}0\end{array} \right), \vec{u}\sb{4} =  \left( \begin{array}{c} -3\cr{}0\cr{}1
\cr{}0\end{array} \right) \rangle.$$ Ampliamos a una base
de $V$ con $\vec{u}\sb{1} = \vec{e}\sb{1}, \vec{u}\sb{2} = \vec{e}\sb{4}$ y llamemos
$\mathcal{B} = \{ \vec{u}\sb{1}, \vec{u}\sb{2}, \vec{u}\sb{3}, \vec{u}\sb{4} \}$. Entonces
$\{ f(\vec{u}\sb{1}), f(\vec{u}\sb{2}) \}$ es una base de $\im(f)$ (son las
columnas básicas de $A$) y la ampliamos a una base de $V'$ con
$\vec{w}\sb{3} = \vec{e}\sb{3}$. Si
$\mathcal{B}' = \{ f(\vec{u}\sb{1}), f(\vec{u}\sb{2}), \vec{w}\sb{3} \}$, entonces
$$M\sb{\mathcal{B}, \mathcal{B}'}(f) = \left( \begin{array}{rrrr} 1 & 0 & 0 & 0 \cr 0 & 1 & 0 & 0 \cr 0 & 0 & 0 & 0 \end{array} \right).$$
{{% /example %}}

{{% example name="Ejemplo" %}}
El primer teorema de isomorfı́a permite calcular una factorización de
rango pleno de una matriz $A$, esto es, si $\rg(A\sb{m \times n}) = r$
podemos expresar $A$ en la forma
$$A\sb{m \times n} = D\sb{m \times r} B\sb{r \times n}, \text{ donde } \rg(B) = r = \rg(D).$$
Sabemos que si fijamos las bases estándar $\mathcal{S}$ en $\K^n$ y
$\mathcal{S}'$ en $\K^m$, la matriz $A$ induce una aplicación lineal
$f:\K^n \to \K^m$ dada por $f(\vec{u}) = A \cdot \vec{u}$ y la matriz de
$f$ respecto de estas bases es $A$. El primer teorema de isomorfı́a da
lugar al diagrama $$\xymatrix{
  \K^n \ar[r]^f \ar[d]\sb{p} & \K^m \cr \K^n / \ker f \ar[r]^{\bar{f}} & \im(f) \ar[u]^i
  }$$ Sean $\vec{v}\sb{i\sb{1}}, \ldots, \vec{v}\sb{i\sb{r}}$ las columnas básicas de
$A$, que forman una base ${\mathcal B}\sb{2}$ de $\im(f)$. Tomemos los
vectores $\vec{e}\sb{i\sb{1}}, \ldots, \vec{e}\sb{i\sb{r}}$ de la base estándar de
$\K^n$ tales que $A \vec{e}\sb{i\sb{j}} = \vec{v}\sb{i\sb{j}}, j=1,\ldots, r$.
Probamos en primer lugar que
${\mathcal B}\sb{1} = \{ ( \vec{e}\sb{i\sb{1}} + \ker(f), \ldots, \vec{e}\sb{i\sb{r}} + \ker(f) ) \}$
forman una base de $\K^n/\ker(f)$. Como $\dim \K^n / \ker(f) = r$, basta
con probar que forman un sistema linealmente independiente. Si
$$\alpha\sb{1} (\vec{e}\sb{i\sb{1}} + \ker(f)) + \cdots + \alpha\sb{r} ( \vec{e}\sb{i\sb{r}} + \ker(f) ) = \vec{0} + \ker(f),$$
entonces $\sum\sb{j=1}^r \alpha\sb{j} \vec{e}\sb{i\sb{j}} \in \ker(f)$, de donde
$$\vec{0} = A \sum\sb{j=1}^r \alpha\sb{j} \vec{e}\sb{i\sb{j}} = \sum\sb{j=1}^r \alpha\sb{j} A \vec{e}\sb{i\sb{j}} = \sum\sb{j=1}^r \alpha\sb{j} \vec{v}\sb{i\sb{j}}, \text{ porque } A \vec{e}\sb{i\sb{j}} \text{ es la columna } i\sb{j}.$$
Como las columnas básicas de $A$ son independientes, todos los
coeficientes $\alpha\sb{j}$ son nulos.

Llamemos $B$ a la matriz del homomorfismo $p$ respecto de las bases
$\mathcal{S}$ de $\K^n$ y ${\mathcal B}\sb{1}$ de $\K^n/\ker(f)$. Es una
matriz de orden $r \times n$ y como la aplicación $p$ es sobreyectiva,
$B$ es de rango $r$.

Llamemos $D$ a la matriz del homomorfismo $i$ respecto de las bases
${\mathcal B}\sb{2}$ de $\im(f)$ y $\mathcal{S}'$ de $\K^m$. Es una matriz
de orden $m \times r$ y como la aplicación $i$ es inyectiva, $D$ es de
rango $r$.

Por otra parte, por la construcción de las bases ${\mathcal B}\sb{1}$ y
${\mathcal B}\sb{2}$, se tiene que
$$M\sb{{\mathcal B}\sb{1}, {\mathcal B}\sb{2}}(\bar{f}) = I\sb{r \times r}.$$ Como
$f = i \circ \bar{f} \circ p$, esta igualdad se traslada a estas
matrices como $A = D I B = D \cdot B$, que es la factorización buscada.
{{% /example %}}

## \* Otros teoremas de isomorfía

{{% theorem name="Segundo teorema de isomorfı́a" %}}
 Sean $W\sb{1}, W\sb{2}$ subespacios vectoriales de
$V$. Entonces $(W\sb{1} + W\sb{2})/W\sb{2} \simeq W\sb{1}/(W\sb{1} \cap W\sb{2})$.
{{% /theorem %}}

{{% proof %}}
Consideremos la aplicación $$\begin{aligned}
    f \colon &  W\sb{1} \to (W\sb{1}+W\sb{2})/W\sb{2} \cr \vec{w}\sb{1} & \mapsto \vec{w}\sb{1} + W\sb{2}.
\end{aligned}$$ Es fácil ver que $f$ es un homomorfismo de espacios
vectoriales. Si $\vec{w} + W\sb{2} \in (W\sb{1} + W\sb{2})/W\sb{2}$, entonces existen
$\vec{v}\sb{1} \in W\sb{1}, \vec{v}\sb{2} \in W\sb{2}$ tales que
$\vec{w} = \vec{v}\sb{1} + \vec{v}\sb{2}$, por lo que
$\vec{w} + W\sb{2} = \vec{v}\sb{1} + W\sb{2}$ (su diferencia es $\vec{v}\sb{2}$, que está
en $W\sb{2}$). Por tanto, $f$ es sobreyectiva. Su núcleo viene dado por
$$\ker f = \{ \vec{w}\sb{1} \in W\sb{1} ~|~ \vec{w}\sb{1} \in W\sb{2} \} = W\sb{1} \cap W\sb{2}.$$
Por el primer teorema de isomorfı́a, nos queda que
$W\sb{1}/(W\sb{1} \cap W\sb{2}) \simeq (W\sb{1} + W\sb{2})/W\sb{2}$.
{{% /proof %}}

{{% theorem name="Tercer teorema de isomorfı́a" %}}
 Sean $W\sb{1}, W\sb{2}$ subespacios vectoriales de
$V$, con $W\sb{1} \subset W\sb{2}$. Entonces $(V/W\sb{1})/(W\sb{2}/W\sb{1}) \simeq V/W\sb{2}$.
{{% /theorem %}}

{{% proof %}}
En primer lugar, observemos que $W\sb{2}/W\sb{1}$ es subespacio vectorial de
$V/W\sb{1}$, pues las operaciones son internas. Consideremos la aplicación
$$\begin{aligned}
    f \colon & V/W\sb{1} \to V/W\sb{2} \cr \vec{v} + W\sb{1} & \mapsto \vec{v} + W\sb{2}.
\end{aligned}$$ Esta aplicación está bien definida, pues si
$\vec{v} + W\sb{1} = \vec{v}' + W\sb{1}$, entonces
$\vec{v} - \vec{v}' \in W\sb{1} \subset W\sb{2}$, luego
$\vec{v} + W\sb{2} = \vec{v}' + W\sb{2}$. Es fácil ver que es un homomorfismo de
espacios vectoriales. Si $\vec{v} + W\sb{2} \in V/W\sb{2}$, podemos tomar la
clase $\vec{v} + W\sb{1} \in V/W\sb{1}$, que se aplica mediante $f$ en
$\vec{v} + W\sb{2}$. Por tanto, $f$ es sobreyectiva. Calculemos su núcleo:
$$\ker f = \{ \vec{v} + W\sb{1} ~|~ \vec{v} + W\sb{2} = \vec{0} + W\sb{2} \} = \{ \vec{v} + W\sb{1} ~|~ \vec{v} \in W\sb{2} \} = W\sb{2}/W\sb{1}.$$
Por el primer teorema de isomorfı́a, $$(V/W\sb{1})/(W\sb{2}/W\sb{1}) \simeq V/W\sb{2}.$$
{{% /proof %}}

{{% theorem name="Teorema de isomorfismo de retı́culos" %}}
 Sea $W$ un subespacio vectorial de
$V$. Existe una biyección entre los subespacios vectoriales de $V$ que
contienen a $W$ y los subespacios vectoriales de $V/W$. Además, si
$U\sb{1}, U\sb{2} \subset V$ son subespacios vectoriales que contienen a $W$,
entonces
$$U\sb{1}/W + U\sb{2}/W = (U\sb{1} + U\sb{2})/W, U\sb{1}/W \cap U\sb{2}/W = (U\sb{1} \cap U\sb{2})/W.$$
{{% /theorem %}}

{{% proof %}}
Llamemos
$$A = \{ U \subset V ~|~ U \mbox{ es un subespacio vectorial tal que } W \subset U \}$$
y sea $B$ el conjunto de subespacios vectoriales de $V/W$. Definimos la
aplicación $\Phi:A \to B$ que asocia a cada $U \in A$ el subespacio
$U/W$. Esto tiene sentido porque $W \subset U$. Vamos a probar que
$\Phi$ es una aplicación biyectiva.

-   $\Phi$ es inyectiva. Sean $U\sb{1}, U\sb{2}$ subespacios vectoriales de $V$
    que contienen a $W$, tales que $U\sb{1}/W = U\sb{2}/W$. Sea
    $\vec{u}\sb{1} \in U\sb{1}$. Entonces existe $\vec{u}\sb{2} \in U\sb{2}$ tal que
    $\vec{u}\sb{1} + W = \vec{u}\sb{2} + W$, de donde
    $\vec{u}\sb{1} - \vec{u}\sb{2} \in W \subset U\sb{2}$. Entonces existe
    $\vec{u}'_2 \in U\sb{2}$ con $\vec{u}\sb{1} = \vec{u}\sb{2} + \vec{u}'_2 \in U\sb{2}$, y
    hemos probado que $U\sb{1} \subset U\sb{2}$. Por simetrı́a, se tiene que
    $U\sb{2} \subset U\sb{1}$, y hemos probado que $U\sb{1} = U\sb{2}$.

-   $\Phi$ es sobreyectiva. Sea $\tilde{U}$ un subespacio vectorial de
    $V/W$. Definimos el conjunto
    $U = \{ \vec{v} \in V ~|~ \vec{v} + W \in \tilde{U} \}$. Vamos a
    probar que $U$ es un subespacio vectorial de $V$ que contiene a $W$.
    Sean $\vec{v}\sb{1}, \vec{v}\sb{2} \in U$, y $\alpha \in \K$. Entonces

    -   $\vec{v}\sb{1} + \vec{v}\sb{2} \in U$, porque
        $(\vec{v}\sb{1} + \vec{v}\sb{2}) + W = (\vec{v}\sb{1} + W) + (\vec{v}\sb{2} + W) \in \tilde{U}$,

    -   $\alpha \vec{v}\sb{1} \in U$, porque
        $(\alpha \vec{v}\sb{1}) + W = \alpha (\vec{v}\sb{1} + W) \in \tilde{U}$.

    Por otro lado, si $\vec{w} \in W$, entonces
    $\vec{w} + W = \vec{0} + W \in \tilde{U}$, por lo que $W \subset U$.
    Además, es claro que $\Phi(U) = \tilde{U}$.

Tenemos ası́ la correspondencia biyectiva, y podemos identificar todo
subespacio vectorial de $V/W$ como uno de la forma $U/W$, con $U$
subespacio vectorial de $V$ que contiene a $W$. Veamos las igualdades
para la suma y la intersección.

-   $U\sb{1}/W + U\sb{2}/W = (U\sb{1} + U\sb{2})/W$. Sea $\vec{u} + W \in U\sb{1}/W + U\sb{2}/W$.
    Entonces existen $\vec{u}\sb{1} \in U\sb{1}, \vec{u}\sb{2} \in U\sb{2}$ tales que
    $\vec{u} + W = (\vec{u}\sb{1} + W) + (\vec{u}\sb{2} + W) = (\vec{u}\sb{1} + \vec{u}\sb{2}) + W \in (U\sb{1} + U\sb{2})/W$.
    De igual forma, si $\vec{u} + W \in (U\sb{1} + U\sb{2})/W$, existen
    $\vec{u}\sb{1} \in U\sb{1}, \vec{u}\sb{2} \in U\sb{2}$ con
    $\vec{u} + W = (\vec{u}\sb{1} + \vec{u}\sb{2}) + W = (\vec{u}\sb{1} + W) + (\vec{u}\sb{2} + W) \in U\sb{1}/W + U\sb{2}/W$.

-   $U\sb{1}/W \cap U\sb{2}/W = (U\sb{1} \cap U\sb{2})/W$. Sea
    $\vec{u} + W \in U\sb{1}/W \cap U\sb{2}/W$. Entonces existen
    $\vec{u}\sb{1} \in U\sb{1}, \vec{u}\sb{2} \in U\sb{2}$ tales que
    $\vec{u}+W = \vec{u}\sb{1} + W = \vec{u}\sb{2} + W$, y podemos escribir
    $$\vec{u} = \vec{u}\sb{1} + \vec{w}\sb{1} = \vec{u}\sb{2} + \vec{w}\sb{2}, \mbox{ con } \vec{w}\sb{1}, \vec{w}\sb{2} \in W.$$
    Esto significa que
    $\vec{u}\sb{1} = \vec{u}\sb{2} + (\vec{w}\sb{2} - \vec{w}\sb{1}) \in U\sb{2}$, porque
    $W \subset U\sb{2}$, y llegamos a que $u\sb{1} \in U\sb{1} \cap U\sb{2}$. Por tanto,
    $\vec{u} + W \in (U\sb{1} \cap U\sb{2})/W$. Por otro lado, si
    $\vec{u} + W \in (U\sb{1} \cap U\sb{2})/W$, entonces existe
    $\vec{u}' \in U\sb{1} \cap U\sb{2}$ tal que $\vec{u} + W = \vec{u}' + W$. Por
    ello, $\vec{u} + W \in U\sb{1}/W \cap U\sb{2}/W$.
{{% /proof %}}

## Cambio de base y homomorfismos

Cuando asociamos una matriz a un homomorfismo $f:V \to V'$, la condición
inicial era el fijar unas bases en los espacios vectoriales. Es natural
preguntarse qué le ocurre a la matriz si se cambian las bases. Sean
$n = \dim V, m = \dim V'$ y consideremos entonces bases
${\mathcal B}\sb{1}, {\mathcal B}\sb{2}$ en $V$, y bases
${\mathcal B}'_1, {\mathcal B}'_2$ en $V'$. Por un lado, tenemos la
matriz del homomorfismo $f$ respecto de las bases ${\mathcal B}\sb{1}$ y
${\mathcal B}'_1$, que notamos por
$$M\sb{{\mathcal B}\sb{1},{\mathcal B}'_1} (f).$$ Por otro, el homomorfismo
$f$ tiene otra matriz asociada respecto de las bases ${\mathcal B}\sb{2}$ y
${\mathcal B}'_2$, que es $$M\sb{{\mathcal B}\sb{2},{\mathcal B}'_2} (f).$$
Partimos del diagrama $$\xymatrix{
 V \ar[rr]^{f}  && V' \ar[d]^{\id\sb{V'}} \cr V \ar[rr]^{f} \ar[u]^{\id\sb{V}} && V'
 }$$ que simplemente representa la igualdad
$f = \id\sb{V} \circ f \circ \id\sb{V'}$. Recordemos dos cuestiones que hemos
visto:

-   La composición de aplicaciones lineales se traduce, al tomar bases
    en cada espacio, en el producto de matrices.

-   La matriz de cambio de base en un espacio vectorial es la matriz de
    la aplicación identidad respecto de dichas bases:
    $$M\sb{\mathcal{B}\sb{2}, \mathcal{B}\sb{1}}(\id\sb{V}) = M(\mathcal{B}\sb{2}, \mathcal{B}\sb{1}),  M\sb{\mathcal{B}'_1, \mathcal{B}'_2}(\id\sb{V'}) = M(\mathcal{B}'_1, \mathcal{B}'_2).$$

Al tomar coordenadas, obtenemos el diagrama
$$\begin{array}{lcr} \mathcal{B}\sb{1} & & \mathcal{B}'_1 \cr
&
\xymatrix{
 \K^n \ar[rr]^{M\sb{\mathcal{B}\sb{1}, \mathcal{B}'_1}(f)}  && \K^m \ar[d]^{M(\mathcal{B}'_1, \mathcal{B}'_2)} \cr \K^n \ar[rr]^{M\sb{\mathcal{B}\sb{2}, \mathcal{B}'_2}(f)} \ar[u]^{M(\mathcal{B}\sb{2}, \mathcal{B}\sb{1})} && \K^m
 }
 &
 \cr \mathcal{B}\sb{2} & & \mathcal{B}'_2
 \end{array}$$ que se traduce en el resultado

{{% theorem %}}

Cambio de base y homomorfismos $$\begin{aligned}
     M\sb{\mathcal{B}\sb{2},\mathcal{B}'_2} (f) & = M(\mathcal{B}'_1,\mathcal{B}'_2) \cdot M\sb{\mathcal{B}\sb{1},\mathcal{B}'_1} (f) \cdot M(\mathcal{B}\sb{2},\mathcal{B}\sb{1}), \cr
     M\sb{\mathcal{B}\sb{1},\mathcal{B}'_1} (f) & = M(\mathcal{B}'_2,\mathcal{B}'_1) \cdot M\sb{\mathcal{B}\sb{2},\mathcal{B}'_2} (f) \cdot M(\mathcal{B}\sb{1},\mathcal{B}\sb{2}).
\end{aligned}$$
{{% /theorem %}}

[]{#thm:hom-cambio-base label="thm:hom-cambio-base"}

Recordemos que ya habı́amos hablado de la equivalencia de matrices (pág.
): dos matrices $A, B$ de orden $m \times n$ son **equivalentes** si
existen matrices no singulares $P\sb{m \times m}, Q\sb{n \times n}$ tales
que $B = P A Q$. ¿Qué relación podemos establecer entre este concepto
matricial y los homomorfismos? La respuesta es la siguiente proposición.
[]{#thm:cambio-base label="thm:cambio-base"}

{{% theorem name="Interpretación de la equivalencia de matrices" %}}
 Sean $A, B$ matrices de
orden $m \times n$, con coeficientes en un cuerpo $\K$. Entonces $A$ y
$B$ son matrices equivalentes si y solamente si existen bases
${\mathcal B}\sb{1}, {\mathcal B}\sb{2}$ de $V = \K^n$ y bases
${\mathcal B}'_1, {\mathcal B}'_2$ de $V' = \K^m$, y un homomorfismo
$f:V \to V'$, tales que
$$A = M\sb{{\mathcal B}\sb{1}, {\mathcal B}'_1}(f), \qquad B = M\sb{{\mathcal B}\sb{2}, {\mathcal B}'_2}(f).$$
{{% /theorem %}}

{{% proof %}}
-   Esta implicación ya la tenemos, sin más que tomar $P$ y $Q$ las
    matrices de cambio de base.

-   Fijamos $\mathcal{S}$ base estándar en $\K^n$, y $\mathcal{S}'$ base
    estándar en $\K^m$. Definimos $f:\K^n \to \K^m$ como
    $f(\vec{v}) = A \vec{v}$. Sabemos que
    $$A = M\sb{\mathcal{S},\mathcal{S}'}(f).$$ Por hipótesis, existen
    $P\sb{m \times m}, Q\sb{n \times n}$ no singulares tales que
    $B = P A Q$. Entonces se tiene que

    -   las columnas de la matriz $P^{-1}$ forman una base
        ${\mathcal B}'_2$ de $\K^m$, y

    -   las columnas de la matriz $Q$ forman una base ${\mathcal B}\sb{2}$
        de $\K^n$.

    La matriz del cambio de base $M({\mathcal B}\sb{2},\mathcal{S})$ es
    precisamente $Q$, y la matriz del cambio de base
    $M(\mathcal{S}', {\mathcal B}'_2)$ es igual a $P$. Entonces
    $$\begin{aligned}
               B & = & P A Q = M(\mathcal{S}', {\mathcal B}'_2) \cdot M\sb{\mathcal{S},\mathcal{S}'}(f) \cdot M({\mathcal B}\sb{2},\mathcal{S}) \cr & = & M\sb{{\mathcal B}\sb{2}, {\mathcal B}'_2}(f).
\end{aligned}$$
{{% /proof %}}

{{% example name="Ejemplo" %}}
Vamos a considerar de nuevo el ejemplo de la página , donde
$f:\R^4 \to \R^3$ está definida con respecto a la base estándar de cada
espacio por la matriz
$$M\sb{\mathcal{S}, \mathcal{S}'}(f) = A =  \left( \begin{array}{cccc} 1&2&3&4\cr{}2&4&6&7
\cr{}1&2&3&6\end{array} \right).$$ Allı́ vimos que con
respecto a las bases $$\begin{gathered}
  \mathcal{B} = \{ \vec{u}\sb{1} = \left( \begin{array}{r} 1 \cr 0 \cr 0 \cr 0 \end{array} \right), \vec{u}\sb{2} = \left( \begin{array}{r} 0 \cr 0 \cr 0 \cr 1 \end{array} \right), \vec{u}\sb{3} = \left( \begin{array}{r} -2 \cr 1 \cr 0 \cr 0 \end{array} \right), \vec{u}\sb{4} = \left( \begin{array}{r} -3 \cr 0 \cr 1 \cr 0 \end{array} \right) \}, \cr
  \mathcal{B}' = \{ f(\vec{u}\sb{1}) = \left( \begin{array}{r} 1 \cr 2 \cr 1 \end{array} \right), f(\vec{u}\sb{2}) = \left( \begin{array}{r} 4 \cr 7 \cr 6 \end{array} \right), \vec{w}\sb{3} = \left( \begin{array}{r} 0 \cr 0 \cr 1 \end{array} \right) \}
\end{gathered}$$ se tiene que
$$M\sb{\mathcal{B}, \mathcal{B}'}(f) = \left( \begin{array}{rrrr} 1 & 0 & 0 & 0 \cr 0 & 1 & 0 & 0 \cr 0 & 0 & 0 & 0 \end{array} \right) = N.$$
Esto significa que existe una relación de la forma
$$A = M(\mathcal{B}', \mathcal{S}') M\sb{\mathcal{B}, \mathcal{B}'}(f) M(\mathcal{S}, \mathcal{B}).$$
Tenemos que $$\begin{aligned}
  M(\mathcal{B}, \mathcal{S}) & = \left( \begin{array}{cccc} [\vec{u}\sb{1}]\sb{\mathcal{S}} & [\vec{u}\sb{2}]\sb{\mathcal{S}} & [\vec{u}\sb{3}]\sb{\mathcal{S}} & [\vec{u}\sb{4}]\sb{\mathcal{S}} \end{array} \right) = \left( \begin{array}{cccc} 1&0&-2&-3\cr{}0&0&1&0
\cr{}0&0&0&1\cr{}0&1&0&0\end{array}
 \right) = Q\sb{1}, \cr
  M(\mathcal{B}', \mathcal{S}') & = \left( \begin{array}{ccc} [f(\vec{u}\sb{1})]\sb{\mathcal{S}'} & [f(\vec{u}\sb{2})]\sb{\mathcal{S}'} & [w\sb{3})]\sb{\mathcal{S}'} \end{array} \right) =  \left( \begin{array}{ccc} 1&4&0\cr{}2&7&0
\cr{}1&6&1\end{array} \right) = P\sb{1}.
\end{aligned}$$ Por tanto, $A = P\sb{1} N Q\sb{1}^{-1}$ o bien
$N = P\sb{1}^{-1} A Q\sb{1}$. En general, vemos ası́ que el cambio de base y el
primer teorema de isomorfı́a permiten probar el teorema de la forma
normal del rango (pág. ).
{{% /example %}}

## \* Espacio dual

### \* Base dual

Dado $V$ un $\K$-espacio vectorial, podemos considerar el conjunto de
aplicaciones lineales $f:V \to \K$, que se denominan formas lineales. El
propio cuerpo $\K$ tiene estructura de $\K$-espacio vectorial, y el
conjunto $\Hom(V,\K)$ es un $\K$-espacio vectorial, de dimensión igual a
$\dim V$.

{{% definition %}}
Espacio dual El conjunto de las formas lineales $\Hom(V,\K)$ se denomina
**espacio dual** de $V$, y lo notaremos por $V^{\ast}$.
{{% /definition %}}

{{% example name="Ejemplo" %}}
Sea $V = \R^2$, y fijemos la base estándar $\mathcal{S}$. Cada forma
lineal $f:V \to \R$ está determinada por los valores $f(\vec{e}\sb{1})$ y
$f(\vec{e}\sb{2})$. En concreto, tomemos las formas $f\sb{i}:V \to \R, i=1,2$
dadas por
$$f\sb{1}(\vec{e}\sb{1}) = 1, f\sb{1}(\vec{e}\sb{2}) = 0, f\sb{2}(\vec{e}\sb{1}) = 0, f\sb{2}(\vec{e}\sb{2}) = 1.$$
Cualquier otra forma lineal $f$ se puede expresar como
$$f = f(\vec{e}\sb{1}) f\sb{1} + f(\vec{e}\sb{2}) f\sb{2}.$$
{{% /example %}}

Sabemos que si $V$ es de dimensión finita, entonces $V^{\ast}$ es isomorfo al
espacio de matrices de orden $1 \times n$ (matrices fila). Uno de los
elementos de $V^{\ast}$ es la aplicación lineal $\vec{0}^{\ast}:V \to \K$, definida
por $\vec{0}^{\ast}(\vec{v}) = 0$ para todo $\vec{v} \in V$.

{{% definition %}}
Base dual Sea $V$ un $\K$-espacio vectorial de dimensión finita, y
${\mathcal B} = \{ \vec{u}\sb{1}, \ldots, \vec{u}\sb{n} \}$ una base de $V$. La
**base dual** de ${\mathcal B}$ es el sistema
${\mathcal B}^{\ast} = \{ \vec{u}\sb{1}^{\ast}, \ldots, \vec{u}\sb{n}^{\ast} \}$ de formas
lineales definidas por
$$\vec{u}\sb{i}^{\ast}(\vec{u}\sb{j}) = \left\\{ \begin{array}{ll} 1 & \mbox{ si } i = j, \cr 0 & \mbox{ si } i \ne j. \end{array} \right.$$
{{% /definition %}}

En el ejemplo anterior hemos calculado la base dual de ${\mathcal S}$ en
$\R^2$. Hay que justificar el nombre de base que hemos dado a este
sistema. En efecto, consideremos una combinación lineal
$$\sum\sb{i=1}^n \alpha\sb{i} \vec{u}\sb{i}^{\ast} = \vec{0}^{\ast}.$$ Para cada
$j=1,\ldots, n$, se tiene que
$$0 = \vec{0}^{\ast}(\vec{u}\sb{j}) = \sum\sb{i=1}^n \alpha\sb{i} \vec{u}\sb{i}^{\ast}(\vec{u}\sb{j}) = \alpha\sb{j} \cdot 1$$
de donde los escalares $\alpha\sb{i} = 0$ para cada $i=1,\ldots, n$. Por
tanto, ${\mathcal B}^{\ast}$ es un sistema linealmente independiente. Por
otro lado, sea $f \in V^{\ast}$, que queda definido por los valores
$f(\vec{u}\sb{i}), i=1,\ldots, n$. Entonces es claro que
$$f = \sum\sb{i=1}^n f(\vec{u}\sb{i}) \vec{u}\sb{i}^{\ast}.$$

{{% example name="Ejemplo" %}}
Consideremos la base ${\mathcal B} = \{ \vec{u}\sb{1}, \vec{u}\sb{2}, \vec{u}\sb{3} \}$
de $\R^3$ dada por
$$\vec{u}\sb{1} = \left( \begin{array}{r} 1 \cr -1 \cr 0 \end{array} \right), \vec{u}\sb{2} = \left( \begin{array}{r} 2 \cr 1 \cr 1 \end{array} \right), \vec{u}\sb{3} = \left( \begin{array}{r} 0 \cr -1 \cr 1 \end{array} \right).$$
Queremos calcular cada una de las matrices fila asociadas a las
aplicaciones lineales $\vec{u}\sb{i}^{\ast}, i=1,2,3$. Sea
$$\vec{u}\sb{i}^{\ast} = \left( \begin{array}{ccc} a\sb{i1} & a\sb{i2} & a\sb{i3} \end{array} \right), i=1,2,3.$$
Entonces para $\vec{u}\sb{1}^{\ast}$ nos quedan las relaciones
$$\left( \begin{array}{ccc} a\sb{i1} & a\sb{i2} & a\sb{i3} \end{array} \right) \left( \begin{array}{r} 1 \cr -1 \cr 0 \end{array} \right) = 1, \left( \begin{array}{ccc} a\sb{i1} & a\sb{i2} & a\sb{i3} \end{array} \right) \left( \begin{array}{r} 2 \cr 1 \cr 1 \end{array} \right) = 0, \left( \begin{array}{ccc} a\sb{i1} & a\sb{i2} & a\sb{i3} \end{array} \right) \left( \begin{array}{r} 0 \cr -1 \cr 1 \end{array} \right) = 0.$$
En forma matricial es lo mismo que
$$\left( \begin{array}{ccc} a\sb{i1} & a\sb{i2} & a\sb{i3} \end{array} \right) \left( \begin{array}{rrr} 1 & 2 & 0 \cr -1 & 1 & -1 \cr 0 & 1 & 1 \end{array} \right) = \left( \begin{array}{r} 1 \cr 0 \cr 0 \end{array} \right).$$
Si procedemos de forma análoga con $\vec{u}\sb{2}^{\ast}$ y $\vec{u}\sb{3}^{\ast}$, y lo
unificamos con lo anterior, obtenemos la expresión
$$\left( \begin{array}{ccc} a\sb{11} & a\sb{12} & a\sb{13} \cr a\sb{21} & a\sb{22} & a\sb{23} \cr a\sb{31} & a\sb{32} & a\sb{33} \end{array} \right) \left( \begin{array}{rrr} 1 & 2 & 0 \cr -1 & 1 & -1 \cr 0 & 1 & 1 \end{array} \right) = \left( \begin{array}{rrr} 1 & 0 & 0 \cr 0 & 1 & 0 \cr 0 & 0 & 1 \end{array} \right).$$
El cálculo se reduce a obtener la inversa de la matriz cuyas columnas
son los vectores dados. En este caso,
$$\left( \begin{array}{ccc} a\sb{11} & a\sb{12} & a\sb{13} \cr a\sb{21} & a\sb{22} & a\sb{23} \cr a\sb{31} & a\sb{32} & a\sb{33} \end{array} \right) = \left( \begin{array}{rrr} 1 & 2 & 0 \cr -1 & 1 & -1 \cr 0 & 1 & 1 \end{array} \right)^{-1} =  \left( \begin{array}{ccc} 1/2&-1/2&-1/2\cr{}1/4&1/4&1
/4\cr{}-1/4&-1/4&3/4\end{array} \right).$$ Por tanto,
podemos escribir
$$u\sb{1}^{\ast}(\vec{x}) = \frac{1}{2} x\sb{1} - \frac{1}{2} x\sb{2} - \frac{1}{2} x\sb{3}, u\sb{2}^{\ast}(\vec{x}) = \frac{1}{4} x\sb{1} + \frac{1}{4} x\sb{2} + \frac{1}{4} x\sb{3}, \vec{u}\sb{3}^{\ast}(\vec{x}) = -\frac{1}{4} x\sb{1} - \frac{1}{4} x\sb{2} + \frac{3}{4} x\sb{3}.$$
{{% /example %}}

Este ejemplo nos proporciona un método de cálculo de la base dual,
fijada una base.

{{% example name="Cálculo de la base dual" %}}
Entrada: ${\mathcal B} = \{ \vec{u}\sb{1}, \ldots, \vec{u}\sb{n} \}$ base de $V$,
${\mathcal S}$ base de $V$ respecto a la que tomamos coordenadas.

Salida: ${\mathcal B}^{\ast}$ base dual de ${\mathcal B}$.

1.  Construimos la matriz $A = (a\sb{ij})$ de las coordenadas de los
    vectores $\vec{u}\sb{i}$ respecto de ${\mathcal S}$.

2.  Cálculo de $A^{-1}$.

3.  La fila $i$-ésima de $A^{-1}$ es la matriz fila asociada a
    $\vec{u}\sb{i}^{\ast}$ respecto de la base ${\mathcal S}$.
{{% /example %}}

{{% proof %}}
La matriz $A$ es no singular, porque ${\mathcal B}$ es una base. Si la
matriz asociada a $\vec{u}\sb{i}^{\ast}$ respecto de la base ${\mathcal S}$ es la
matriz fila
$\left( \begin{array}{ccc} b\sb{i1} & \ldots & b\sb{in} \end{array} \right)$,
entonces
$$\left( \begin{array}{ccc} b\sb{i1} & \ldots & b\sb{in} \end{array} \right) \left( \begin{array}{c} a\sb{1j} \cr \vdots \cr a\sb{nj} \end{array} \right) = \left\\{ \begin{array}{ll} 1 & \mbox{ si } i = j, \cr 0 & \mbox{ si } i \ne j. \end{array} \right.$$
Esto significa que la matriz fila asociada a cada $\vec{u}\sb{i}^{\ast}$ es la
fila $i$-ésima de $A^{-1}$.
{{% /proof %}}

{{% example name="Ejemplo" %}}
Consideremos las formas lineales $g\sb{i}:\R^3 \to \R. i=1,2,3$ dadas por
las matrices fila
$$[g\sb{1}]\sb{\mathcal S} = \left( \begin{array}{rrr} 1 & -1 & 1 \end{array} \right), [g\sb{2}]\sb{\mathcal S} = \left( \begin{array}{rrr} 1 & 0 & 1 \end{array} \right), [g\sb{2}]\sb{\mathcal S} = \left( \begin{array}{rrr} 2 & 1 & 3 \end{array} \right)$$
respecto de la base estándar $\mathcal{S}$. Vamos a probar que
${\mathcal C} = \{ g\sb{1}, g\sb{2}, g\sb{3} \}$ es una base del espacio dual
$(\R^3)^{\ast}$. Para ello, bastará comprobar que son linealmente
independientes, y lo haremos construyendo la matriz de coordenadas
respecto de la base ${\mathcal S}^{\ast}$ de $(\R^3)^{\ast}$. Es inmediato que
$$g\sb{1} = 1 \cdot \vec{e}\sb{1}^{\ast} + (-1) \cdot \vec{e}\sb{2}^{\ast} + 1 \cdot \vec{e}\sb{3}^{\ast}, g\sb{2} = 1 \cdot \vec{e}\sb{1}^{\ast} + 0 \cdot \vec{e}\sb{2}^{\ast} + 1 \cdot \vec{e}\sb{3}^{\ast}, g\sb{3} = 2 \cdot \vec{e}\sb{1}^{\ast} + 1 \cdot \vec{e}\sb{2}^{\ast} + 3 \cdot \vec{e}\sb{3}^{\ast},$$
y la matriz de coordenadas es no singular. Por tanto, son linealmente
independientes, y como la dimensión de $(\R^3)^{\ast}$ es 3, forman una base.

Una pregunta adicional es calcular la base ${\mathcal B}$ de $\R^3$ tal
que su dual ${\mathcal B}^{\ast}$ es igual a ${\mathcal C}$. Por el método
anterior, la matriz
$$C = \left( \begin{array}{rrr} 1 & -1 & 1 \cr 1 & 0 & 1 \cr 2 & 1 & 3 \end{array} \right)$$
es de la forma $A^{-1}$, donde $A$ es la matriz de coordenadas respecto
a la base estándar $\mathcal{S}$ de la base buscada. Entonces
$$A = C^{-1} =  \left( \begin{array}{ccc} -1&4&-1\cr{}-1&1&0
\cr{}1&-3&1\end{array} \right),$$ y esto significa que la
base ${\mathcal B} = \{ \vec{u}\sb{1}, \vec{u}\sb{2}, \vec{u}\sb{3} \}$ dada por
$$\vec{u}\sb{1} =  \left( \begin{array}{c} -1\cr{}-1\cr{}
1\end{array} \right), \vec{u}\sb{2} =  \left( \begin{array}{c} 4\cr{}1\cr{}-3
\end{array} \right), \vec{u}\sb{3} =  \left( \begin{array}{c} -1\cr{}0\cr{}1
\end{array} \right)$$ cumple que ${\mathcal B}^{\ast} = {\mathcal C}$.
{{% /example %}}

### \* Interpretación de la matriz traspuesta

Sea $f:V\sb{1} \to V\sb{2}$ un homomorfismo de $\K$-espacios vectoriales. Para
cada forma lineal $\varphi:V\sb{2} \to \K$ podemos construir la forma lineal
$\varphi \circ f:V\sb{1} \to \K$, y esto define una aplicación
$f^{\ast}:V\sb{2}^{\ast} \to V\sb{1}^{\ast}$.

{{% theorem name="Aplicación traspuesta" %}}
Dado un homomorfismo $f:V\sb{1} \to V\sb{2}$, la aplicación
$f^{\ast}:V\sb{2}^{\ast} \to V\sb{1}^{\ast}$ definida por $f^{\ast}(\varphi) = \varphi \circ f$ es
un homomorfismo entre los espacios vectoriales duales.

Además, si ${\mathcal B}$ y ${\mathcal C}$ son bases respectivas de
$V\sb{1}$ y $V\sb{2}$, se verifica que
$$M\sb{{\mathcal B}^{\ast}{\mathcal C}^{\ast}}(f^{\ast}) = M\sb{{\mathcal B}{\mathcal C}}(f)^t.$$
{{% /theorem %}}

{{% proof %}}
Sean $\varphi\sb{1}, \varphi\sb{2} \in V\sb{2}^{\ast}$, y $a\sb{1}, a\sb{2} \in \K$.
Debemos probar que
$$f^{\ast}(a\sb{1} \varphi\sb{1} + a\sb{2} \varphi\sb{2}) = a\sb{1} f^{\ast}(\varphi\sb{1}) + a\sb{2} f^{\ast}(\varphi\sb{2}).$$
Para ello, tomemos $\vec{v} \in V\sb{1}$. Entonces $$\begin{aligned}
    f^{\ast}(a\sb{1} \varphi\sb{1} + a\sb{2} \varphi\sb{2})(\vec{v}) & = & (a\sb{1} \varphi\sb{1} + a\sb{2} \varphi\sb{2}) \circ f (\vec{v}) \cr & = & a\sb{1} (\varphi\sb{1} \circ f)(\vec{v}) + a\sb{2} (\varphi\sb{2} \circ f)(\vec{v}) \cr & = & (a\sb{1} f^{\ast}(\varphi\sb{1}) + a\sb{2} f^{\ast}(\varphi\sb{2}))(\vec{v}).
\end{aligned}$$

Notemos
${\mathcal B} = \{ \vec{u}\sb{1}, \ldots, \vec{u}\sb{n} \}, {\mathcal C} = \{ \vec{w}\sb{1}, \ldots, \vec{w}\sb{m} \}$.
Sabemos que
$$M\sb{{\mathcal B}{\mathcal C}} = \left( \begin{array}{ccc} [f(\vec{u}\sb{1})]\sb{\mathcal C} & \ldots & [f(\vec{u}\sb{n})]\sb{\mathcal C} \end{array} \right).$$
Debemos calcular las coordenadas de $f^{\ast}(\vec{w}^{\ast}\sb{i})$ respecto de la
base ${\mathcal B}^{\ast}$. El homomorfismo
$f^{\ast}(\vec{w}^{\ast}\sb{i}) = \vec{w}^{\ast}\sb{i} \circ f$ tiene asociada en la base
${\mathcal B}$ la matriz
$$(\vec{w}\sb{i}^{\ast} \circ f)\sb{\mathcal B} = (\vec{w}\sb{i}^{\ast})\sb{\mathcal C} M\sb{{\mathcal B}{\mathcal C}}(f).$$
Sabemos que $(\vec{w}\sb{i}^{\ast})\sb{\mathcal C} = \vec{e}\sb{i}^t$, por lo que las
coordenadas buscadas la forman la fila $i$-ésima de la matriz $A$. Esta
fila es la columna $i$-ésima de la matriz buscada, de donde tenemos el
resultado. ◻
{{% /proof %}}
