# Álgebra Lineal y Geometría I

Esta web contiene la versión digital del profesor [Fernando Muro](https://personal.us.es/fmuro/) de las notas de clase elaboradas por los profesores del [Departamento de Álgebra](http://www.algebra.us.es/) de la asignatura obligatoria [Álgebra Lineal y Geometría I](https://www.us.es/estudiar/que-estudiar/oferta-de-grados/grado-en-matematicas/1710002) del [Grado en Matemáticas](http://www.us.es/estudios/grados/plan_171) de la [Universidad de Sevilla](http://www.us.es).

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/es/"><img alt="Licencia de Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/es/88x31.png" /></a><br />Este obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/es/">licencia de Creative Commons Reconocimiento-NoComercial-CompartirIgual 3.0 España</a>.

{{% warning %}}
Esta web está **en desarrollo**. El contenido actual puede contener erratas y será modificado para su mejora sin previo aviso. Se recomienda recargar con frecuencia para evitar que el navegador muestre copias antiguas almacenadas en la caché.
{{% /warning %}}